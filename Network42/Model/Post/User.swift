//
//  User.swift
//  Network42
//
//  Created by Apple3 on 24/01/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import Parse
import Foundation

struct User {
    var id: String?
    var name: String?
    var email: String?
    var userName: String?
    var mobile: String?
    var profilePic: PFFile?
    var profileType: String? //1- Public, 0- Private
    
    init() {
        
    }
    
    func getUser(_ dic: PFUser?) -> User {
        var this = User()
        guard let dic = dic else {
            return this
        }
        this.id = dic.objectId
        if dic.allKeys.contains("name") == true{
            if let name =  dic.value(forKey: "name") as? String{
                this.name = name
            }
        }
        if dic.allKeys.contains("email") == true{
            if let email = dic.value(forKey: "email") as? String{
                this.email = email
            }
        }
        if dic.allKeys.contains("user_name") == true{
            if let userName = dic.value(forKey: "user_name") as? String{
                this.userName = userName
            }
        }
        
        if dic.allKeys.contains("mobile_number") == true{
            if let mobile = dic.value(forKey: "mobile_number") as? String{
                this.mobile = mobile
            }
        }
        
        if dic.allKeys.contains("profile_image") == true{
            if let profilePic = dic.value(forKey: "profile_image") as? PFFile{
                this.profilePic = profilePic
            }
        }
        
        if dic.allKeys.contains("profile_type") == true{
            if let profileType = dic.value(forKey: "profile_type") as? String{
                this.profileType = profileType
            }
        }
        
        return this
    }
}
