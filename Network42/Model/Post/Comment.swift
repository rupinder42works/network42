//
//  Comment.swift
//  Network42
//
//  Created by Apple3 on 24/01/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import Parse
import Foundation

struct Comment {
    var id: String?
    var message: String?
    var time: String?
    var user: User?
    
    
    func getComment(_ obj: PFObject) -> Comment {
        var this = Comment()
        this.id = obj.objectId
        this.message = obj.value(forKey: "comment") as? String
        
        if let timeStampObj = obj.value(forKey: "comment_time") as? String
        {
            let newdateStr = timeStampObj.replacingOccurrences(of: "_", with: " ")
            let dateFormatter = DateFormatter()
            if timeStampObj.lowercased().contains("am") == true || timeStampObj.lowercased().contains("pm") == true{
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
            }else{
                dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                
            }
            let newdate = dateFormatter.date(from: newdateStr)
            let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
            this.time = timeStr
        }else{
              this.time = "Just now"
        }
        this.user = User().getUser((obj.value(forKey: "parent_id") as? PFUser))
        
        return this
    }
    
    func tempComment(_ comment: String, _ id: String) -> Comment {
        var this = Comment()
        this.id = id
        this.message = comment
        
        this.time = String().timeAgoSinceDate(date: Date(), numericDates: true)
        this.user = User().getUser(PFUser.current())
        return this
    }
    
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
}

