//
//  Post.swift
//  Network42
//
//  Created by Apple3 on 15/10/18.
//  Copyright © 2018 42works. All rights reserved.
//

import Parse
import Foundation


enum Type: String {
    case image = "IMAGE"
    case album = "ALBUM"
    case video = "VIDEO"
}

struct Post {
    var likeCount : Int!
    var commentCount: Int!
    var name: String!
    var description: String?
    var postMedia: [PFFile]?
    var thumbnail: [PFFile]?
    var postType: String!
    var likeArr: [String]?
    var postedBy: User?
    var object: PFObject!
    var iLiked: Bool = false
    var id: String!
    var type: Type!
    var createdOn: String?
    var albumType : [Type]?
    var link_title : String?
    var link_url  : String?
    var link_description : String?
    var link_image_url : String?
    
    var user : PFUser?
    
    func getPost(_ dic: PFObject) -> Post {
        var this = Post()
        this.name = dic.value(forKey: "name") as? String ?? ""
        this.description = dic.object(forKey: "description") as? String
        
        this.link_title = dic.object(forKey: "link_title") as? String
        this.link_description = dic.object(forKey: "link_description") as? String
        this.link_url = dic.object(forKey: "link_url") as? String
        this.link_image_url = dic.object(forKey: "link_image_url") as? String
        
        this.commentCount = Int(dic.value(forKey: "comment_count") as? String ?? "0")
        this.likeCount = Int(dic.value(forKey: "like_count") as? String ?? "0")
        this.postType = dic.value(forKey: "post_type") as? String ?? ""
        if let postMedia = dic.value(forKey: "postmedia") as? [PFFile] {
            this.postMedia = postMedia
        }
        if let postMedia = dic.value(forKey: "thumbnail") as? [PFFile] {
            this.thumbnail = postMedia
        }
        if let user = dic.value(forKey: "parent_id") as? PFUser{
            this.postedBy = User().getUser(user)
        }
        this.user = dic.value(forKey: "parent_id") as? PFUser
        this.id = dic.objectId
        this.object = dic
        this.type = Type(rawValue: (dic.value(forKey: "post_type") as? String ?? "IMAGE"))
        this.likeArr = dic.value(forKey: "user_like_post") as? [String]
        if let arr = this.likeArr {
            if let id =  PFUser.current()?.objectId {
                if arr.contains(id) == true{
                    this.iLiked = true
                }else{
                    this.iLiked = false
                }
            }
            
        }
        if let arr = dic.value(forKey: "media_types") as? [String] {
            this.albumType = [Type]()
            for item in arr{
                this.albumType?.append(Type(rawValue: item) ?? .image)
            }
        }
        if let str = dic.value(forKey: "time_stamp") as? String {
            
                let newdateStr = str.replacingOccurrences(of: "_", with: " ")
                let dateFormatter = DateFormatter()
                if newdateStr.lowercased().contains("pm") == true || newdateStr.lowercased().contains("am"){
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
                }else{
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                }
                let newDate = dateFormatter.date(from: newdateStr)
                if let newdate1 = newDate as? Date {
                    this.createdOn = str.timeAgoSinceDate(date: newdate1, numericDates: true)
                }
            
        }
        return this
    }
    
    func filterPost(_ post: [PFObject]) -> [Post] {
        var array = [Post]()
        for obj in post {
            array.append(self.getPost(obj))
        }
        return array
    }
}
