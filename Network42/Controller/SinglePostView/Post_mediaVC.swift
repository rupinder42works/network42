//
//  Post_mediaVC.swift
//  Network42
//
//  Created by Apple on 20/03/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import Parse

class Post_mediaVC: UIViewController {

    var objectForCollection : PFObject?
    var images = [UIImage]()
    @IBOutlet weak var collectionVw: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    @IBAction func click_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension Post_mediaVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let obj = objectForCollection{
            if let arr = obj.object(forKey: "postmedia") as? NSArray
            {
                return arr.count
            }
            else{
                return 0
            }
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostMedia_CollectionCell", for: indexPath) as? PostMedia_CollectionCell{
            if let obj = objectForCollection{
                if let arr = obj.object(forKey: "postmedia") as? NSArray
                {
                    if let fileObj = arr.object(at: indexPath.item) as? PFFile
                    {
                        cell.imgVw.file = fileObj
                        cell.imgVw.load { (image, error) in
                           // self.indicatorObj.stopAnimating()
                        }
                    }
                }else{
                    cell.imgVw.image = #imageLiteral(resourceName: "dummy-image-landscape")
                }
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    }
}
