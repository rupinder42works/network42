//
//  OtherUser_InfoCell.swift
//  Network42
//
//  Created by Apple on 12/03/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
@objc protocol profileImageDelegate {
    @objc optional func editImgClickedDelegate(_ index:Int,cell:OtherUser_InfoCell,type:String)
}

@objc protocol followDelegateProfile1 {
    @objc optional func followClickedDelegateProfile(cell:OtherUser_InfoCell)
    
}

@objc protocol followListDelegate1 {
    
    @objc optional func followClickedtolistCount(cell:OtherUser_InfoCell,type:String)
    @objc optional func followingClickedtolistCount(cell:OtherUser_InfoCell,type:String)
    @objc optional func sendMsgClickedtoChatlist(cell:OtherUser_InfoCell)
}

class OtherUser_InfoCell: UITableViewCell {
    
    @IBOutlet weak var vw_stackAdditional: UIStackView!
    @IBOutlet weak var ht_locationView: NSLayoutConstraint!
    @IBOutlet var locationInsideView: UIView!
    @IBOutlet weak var editUserImgBtn: UIButton!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var locIcon: UIImageView!
    
    @IBOutlet var profileBackView: UIView!
    @IBOutlet var singleViewIcon: UIImageView!
    @IBOutlet var singleViewLocation: UIView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var name: UILabel!
   
    @IBOutlet var followingLbl: UILabel!
    @IBOutlet var followLbl: UILabel!
    @IBOutlet var singleViewTitle: UILabel!
    @IBOutlet var websiteIcon: UIImageView!
    @IBOutlet var websiteLbl: UILabel!
    @IBOutlet var singleView_height: NSLayoutConstraint!
    
    weak var delegate: profileImageDelegate?
    weak var delegateProfile: followDelegateProfile1?
    weak var delegateFollowlist: followListDelegate1?
    @IBOutlet weak var followingCountLbl: UILabel!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var designation: UILabel!
    @IBOutlet var locationView: UIView!
    @IBOutlet var followerViewContainer: UIView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var editBgButtonObj: UIButton!
    @IBOutlet var bgImageHeight: NSLayoutConstraint!
    
    @IBOutlet var heightOfSingleViewLocation: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func editUserImg(_ sender: Any) {
        self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "user")
    }
    
    
    @IBAction func editBgImage(_ sender: Any) {
        self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "background")
    }
    
    @IBAction func followAction(_ sender: Any) {
        self.delegateProfile?.followClickedDelegateProfile!(cell: self)
    }
    
    @IBAction func finalFollowingList(_ sender: Any) {
        
        self.delegateFollowlist?.followingClickedtolistCount!(cell: self,type: "Follow")
        
    }
    @IBAction func finalFollowerList(_ sender: Any) {
        
        self.delegateFollowlist?.followClickedtolistCount!(cell: self,type: "Following")
    }
    
    @IBAction func messageTapped(_ sender: Any) {
        
        self.delegateFollowlist?.sendMsgClickedtoChatlist!(cell:self)
    }
}

