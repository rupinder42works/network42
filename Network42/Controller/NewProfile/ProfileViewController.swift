//
//  ProfileViewController.swift
//  Network42
//
//  Created by 42works on 27/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import AVFoundation
import ActiveLabel
import SwiftyGif
import JPVideoPlayer
import Firebase
import GoogleMobileAds

var nestedprofileArray:NSMutableArray =  [[],[],"","","",""]
@objc protocol updateImageonProfile {
    @objc optional func imageupdateDelegate(img:UIImage)
}

var refreshControl: UIRefreshControl!

class ProfileViewController: BaseViewController,profileImgDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,followDelegateProfile,followListDelegate,likeListingDelegate{
    
    @IBOutlet weak var imgVw_profileEdit: UIImageView!
    var isVideoSeen = false
    fileprivate var getAllData = false
    var objectId: String = ""
    var followingUserCountDict:NSMutableDictionary = [:]
    weak var delegate: updateImageonProfile?
    let followArray : NSMutableArray = []
    let followingArray : NSMutableArray = []
    var refreshControl: UIRefreshControl!
    var profileArray:NSMutableArray = []
    var fololowingcountArray:NSMutableArray = []
    @IBOutlet var profileAcIndicator: UIActivityIndicatorView!
    
    let imagePicker = UIImagePickerController()
    @IBOutlet var profileTable: UITableView!
    var  userimageToSend = UIImage.init(named: "dummyProfile")!
    var  bgimageToSend = UIImage()
    var uploadFlag:Bool = false
    var pickerTypeStr:String = ""
    var tempCartArray = NSMutableArray()
    var profileFlag :String = ""
    var profileImageArray:NSMutableArray = []
    var filterImgArray :NSMutableArray = []
    let companyArray:NSMutableArray = []
    
    let schoolArray:NSMutableArray = []
    var comapnyAPIresponse:Bool = false
    var getLocVal:String = ""
    var websiteVal:String = ""
    var bioVal:String = ""
    let companyobj:NSMutableArray = []
    let compobjIds:NSMutableArray = []
    let schoolobj:NSMutableArray = []
    let sobjId:NSMutableArray = []
    var dummyinfoTitle:NSMutableArray = []
    var dummyImgArray:NSMutableArray = []
    var wholeCompArray:NSMutableArray = []
    var objectidtoPass :String = ""
    var fetchUserProfile : Bool?{
        didSet{
            getProfileOfUser()
        }
    }
    
    @IBOutlet var backButtonObj: UIButton!
    @IBOutlet var editButtonObj: UIButton!
    
    var fromSearch : Bool = false
    var userProfile : Bool = true
    var friend : String = ""
    var userFromSearch : PFUser?
    var currentUser = PFUser.current()
    var refreshSearchBlocked : Bool = false
    var blocked : Bool = false
    var postExist : Bool = false
    var PFObjectArray : NSMutableArray = []

    @IBOutlet var popUpTableViewObj: UITableView!
    @IBOutlet var popUp: UIView!
    @IBOutlet var progressVIew: UIView!
    @IBOutlet var progressImage: UIImageView!
    @IBOutlet var heightOFProgressView: NSLayoutConstraint!
    @IBOutlet var heightOfPopUpTableView: NSLayoutConstraint!
    var popUpArray : NSArray = []
    
    var descriptionIndex : Int = 0
    
    @IBOutlet var headingLabelObj: UILabel!
    
    var notificationStatus:Bool = false
    
    var firstTime : Bool = true
    
    @IBOutlet var blockbtn: UIButton!
    var otherUserBL : [String] = []
    var myBL : [String] = []
    
    var videoIndexCell = 0
    
    var callback: (() -> Void)?
    
    //MARK:-Life cycles
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        nestedprofileArray =  [[],[],"","","",""]
        profileTable.isHidden = true
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(ProfileViewController.refreshTable), for: UIControl.Event.valueChanged)
        profileTable.addSubview(refreshControl) // not required when using UITableViewController
        profileTable.register(UINib(nibName: "Home_UrlCell", bundle: nil), forCellReuseIdentifier: "Home_UrlCell")
        profileTable.register(UINib(nibName: "Nopost_tableCell", bundle: Bundle.main), forCellReuseIdentifier: "Nopost_tableCell")
        profileTable.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        profileTable.register(UINib(nibName: "AdvertisementCell", bundle: nil), forCellReuseIdentifier: "AdvertisementCell")
        profileTable.register(UINib(nibName: "UserInfoCell", bundle: nil), forCellReuseIdentifier: "cell")
        profileTable.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "infoCell")
        profileTable.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "infoCellLast")
        
        profileTable.rowHeight = UITableView.automaticDimension
        profileTable.estimatedRowHeight = 702
        
        popUpTableViewObj.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "PostTableViewCell")
        popUpTableViewObj.rowHeight = UITableView.automaticDimension
        popUpTableViewObj.estimatedRowHeight = 702
        
        profileImageArray = [[],[],"","","",""]
        dummyinfoTitle = ["developer at Aditya Birla Group","Studied at VIT University","ankit@42works.net","3111554499","Single","April 8, 2005"]
        dummyImgArray = []
    }
    
    
    //MARK: - call service methods
    
    func firstFunction()
    {
//        if userFromSearch == nil{
//             userProfile = true
//             currentUser = PFUser.current()
//             self.getProfileOfUser()
//        }else{
//             userProfile = false
//            currentUser = userFromSearch
//             checkuserBlocked()
//            serviceCalling()
//            postListUI()
//        }
        if userProfile == true {
            currentUser = PFUser.current()
            checkuserBlocked()
            serviceCalling()
            postListUI()
        }else {
            if objectId == PFUser.current()?.objectId {
                userProfile = true
            } else {
                userProfile = false
            }
            if self.userFromSearch == nil {
                self.getProfileOfUser()
                checkuserBlocked()

            } else {
                currentUser = userFromSearch
                checkuserBlocked()
                serviceCalling()
                postListUI()
            }
        }
    }
    
    //MARK:- Private methods
    
    private func checkUserFollowed(){
        if let objId = PFUser.current()?.objectId{
            if objId != self.currentUser?.objectId{
                let blockedUserObj = PFQuery(className: "User_follow")
                blockedUserObj.whereKey("follower_id", equalTo: objId)
                if let objId = self.currentUser?.objectId{
                    blockedUserObj.whereKey("followed_id", equalTo:objId )
                }
                blockedUserObj.findObjectsInBackground { (objects, error) in
                    if error == nil{
                        if objects != nil{
                            if objects!.count > 0{
                                self.friend = "FOLLOWING"
                            }else{
                                self.friend = "FOLLOW"
                            }
                        }
                        self.profileTable.reloadData()
                    }
                }
            }
        }
    }
    
    private func checkuserBlocked(){
        if let objId = PFUser.current()?.objectId{
            if objId != self.currentUser?.objectId{
                let blockedUserObj = PFQuery(className: "BlockedUsers")
                blockedUserObj.whereKey("FromUser", equalTo: objId)
                if let objId = self.currentUser?.objectId{
                    blockedUserObj.whereKey("BlockedUser", equalTo:objId)
                }
                blockedUserObj.findObjectsInBackground { (objects, error) in
                    if error == nil{
                        if objects != nil{
                            if objects!.count > 0{
                                self.blockbtn.tag = 1
                            }else{
                                self.blockbtn.tag = 0
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func serviceCalling() {
        self.getImagesFromDb()
        //FOLLOWER COUNT AS
        self.getFollowList()
        self.getFollowingList()
    }
    
    
    private func postListUI() {
        print(currentUser?.username ?? "Network 42")
        self.headingLabelObj.text = currentUser?.object(forKey: "user_name") as? String ?? "Network 42"
        
        if notificationStatus == true{
            
            let followedId = userFromSearch?.value(forKey: "objectId") as! String
            getNotificationstatusApi(followedidStr: followedId)
        }
        
        
        let currentProbileId = self.currentUser?.objectId
        let loggedInId = PFUser.current()?.objectId
        var status : String = ""
        if let profileStatus = self.currentUser?.value(forKey: "profile_type") as? String
        {
            status = profileStatus
        }
        
        if let otherBlockedUsers = currentUser?.value(forKey: "blockedUsers") as? [String]
        {
            otherUserBL = otherBlockedUsers
        }
        
        if let myBlockedUsers = PFUser.current()?.value(forKey: "blockedUsers") as? [String]
        {
            myBL = myBlockedUsers
        }
        
        let loggedIn = PFUser.current()?.objectId
        let otherUserId = currentUser?.objectId
        if self.friend == "FOLLOWING" ||  currentProbileId == loggedInId || status == "1"
        {
            firstTime = false
            
            if  (!(otherUserBL.contains(loggedIn ?? "")) && !(myBL.contains(otherUserId ?? ""))) {
                self.postsListApi(isRefresh: true)
            }
        }
    }
    
    
    private func loadDataToView() {
        globalBool.showProgess = false
        
        if fromSearch {
            self.tabBarController?.tabBar.isHidden = true
            self.backButtonObj.isHidden = false
            
        } else  {
            self.tabBarController?.tabBar.isHidden = false
            self.backButtonObj.isHidden = true
        }
        
        if userProfile {
            editButtonObj.isHidden = false
            imgVw_profileEdit.isHidden = false
            blockbtn.isHidden = true
            
        } else {
            editButtonObj.isHidden = true
            imgVw_profileEdit.isHidden = true
            blockbtn.isHidden = false
        }
        
        // Profile is updated
        if userProfile == true
        {
            currentUser = PFUser.current()
        }else
        {
            currentUser = userFromSearch
        }
        
        if let pstatus = currentUser?.object(forKey: "profile_updated") as? String{
            //print("Updated user",currentUser!)
            profileImageArray = [[],[],"","","",""]
            //print("images array as",profileImageArray)
            //print("values are",pstatus)
            if pstatus == "1"{
                if let email = currentUser?.object(forKey: "email") as? String{
                    if email != ""{
                        nestedprofileArray.replaceObject(at: 2, with: email)
                        profileImageArray.replaceObject(at: 2, with: "email.png")
                    }
                }
                if let phone = currentUser?.object(forKey: "mobile_number") as? String{
                    if phone != ""{
                        nestedprofileArray.replaceObject(at: 3, with: phone)
                        profileImageArray.replaceObject(at: 3, with: "phone.png")
                    }
                }
                if let rstatus = currentUser?.object(forKey: "relationship_status") as? String{
                    if rstatus != ""{
                        nestedprofileArray.replaceObject(at: 4, with: rstatus)
                        profileImageArray.replaceObject(at: 4, with: "relationHeart.png")
                    }
                }
                if let rstatus = currentUser?.object(forKey: "dob") as? String{
                    if rstatus != ""{
                        nestedprofileArray.replaceObject(at: 5, with: rstatus)
                        profileImageArray.replaceObject(at: 5, with: "dob.png")
                    }
                }
                bioVal = ""
                
                if let bioStr = currentUser?.object(forKey: "bio") as? String{
                    if bioStr != ""{
                        bioVal = bioStr
                    }
                    
                    //  profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                //Pending Hide one feild show
                if let locStr = currentUser?.object(forKey: "location") as? String{
                    getLocVal = locStr
                }
                
                websiteVal = ""
                if let websiteStr = currentUser?.object(forKey: "website") as? String{
                    if websiteStr != ""{
                        //print(websiteStr)
                        websiteVal = websiteStr
                    }
                    // profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                //print(profileArray)
                //Get User companies list from User Work Experience Table
                self.getWorkExperience()
                
                let indexPath = NSIndexPath.init(row: 0, section: 0)
                profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                
                
                NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("ImageNotify"), object: nil)
                
            }
            else{
                self.getWorkExperience()
                nestedprofileArray =  [[],[],"","","",""]
                profileImageArray = [[],[],"","","",""]
                if let email = currentUser?.object(forKey: "email") as? String{
                    if email != ""{
                        nestedprofileArray.replaceObject(at: 2, with: email)
                        profileImageArray.replaceObject(at: 2, with: "email.png")
                    }
                }
            }
        }
    }
    
    
    @objc func showBgImage(cell: UserInfoCell) {
    }
    
    //Mark:---From CollectionView Notification call
    //MARK:-
    func getNotificationstatusApi(followedidStr:String){
        
        let query = PFQuery(className: "User_follow")
        query.whereKey("follower_id", equalTo:PFUser.current()?.objectId! ?? "")
        query.whereKey("followed_id", equalTo:followedidStr)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print(objects!)
                if objects?.count != 0{
                    for object in objects! {
                        print(object)
                        if let statusFlag = object.object(forKey: "status") as? String
                        {
                            print(statusFlag)
                            if statusFlag == "0"{
                                self.friend = "CANCEL"
                            }
                            else if statusFlag == "1"{
                                self.friend = "FOLLOWING"
                            }
                        }
                    }
                }
                else {
                    self.friend = "FOLLOW"
                }
                self.profileTable.reloadData()
                
                let currentProbileId = self.currentUser?.objectId
                let loggedInId = PFUser.current()?.objectId
                var status : String = ""
                if let profileStatus = self.currentUser?.value(forKey: "profile_type") as? String
                {
                    status = profileStatus
                }
                if self.friend == "FOLLOWING" ||  currentProbileId == loggedInId || status == "1"
                {
                    if self.firstTime
                    {
                        self.postsListApi(isRefresh: true)
                    }
                }
            }
            else {
            }
        })
    }
    
    
    
    @objc func refreshTable() {
        //Get User companies list from User Work Experience Table
        self.serviceCalling()
        if let status = currentUser?.value(forKey: "profile_type") as? String, status == "1" {
            self.getWorkExperience()
            self.postsListApi(isRefresh: true)
        } else {
            self.refreshControl.endRefreshing()
        }
    }
    
    
    @IBAction func edit(_ sender: Any) {
        let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = settingsSB.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        vc.bgimageToSend = self.bgimageToSend
        vc.profileCallback = {
            self.getImagesFromDb()
        }
        vc.userimageToSend = self.userimageToSend
        if companyArray.count > 0 || schoolArray.count > 0{
            vc.schoolArray = self.schoolArray
            
            vc.deleteObjId = self.objectidtoPass
            vc.getobjIdsArray = self.compobjIds
            vc.getSchoolobjIds = self.sobjId
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:-BlockAction
    @IBAction func blockAction(_ sender: UIButton) {
        
        if sender.tag == 0{
            var alert = UIAlertController()
            let user = currentUser
            let currentName = user?.value(forKey: "name") as! String
            alert = UIAlertController(title: "Block \(currentName)", message: "Are you sure you want to block \(currentName)?", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            
            alert.addAction(UIAlertAction(title:Constants.okTitle, style: .default, handler: { action in
                let objId = self.currentUser?.objectId
                print(objId!)
                
                let blockedUserObj = PFObject(className: "BlockedUsers")
                blockedUserObj["FromUser"] = PFUser.current()?.objectId ?? ""
                blockedUserObj["BlockedUser"] = self.currentUser?.objectId
                blockedUserObj["Parent_Blocked_User"] = PFObject(withoutDataWithClassName: "_User", objectId: self.currentUser?.objectId)
                blockedUserObj.saveInBackground(block: { (status, error) in
                    self.backMethod()
                    self.showMessage(messages.blockedUser)
                })
                self.blockbtn.tag = 1
                self.unfollowBlockedUser()
                self.unfollowMyself()
                var userId = ""
                if let id = self.currentUser?.objectId{
                    userId = id
                }
                self.deleteChatMessagesForBlckedUser(userId:userId)
                //            if let blockedUsers = PFUser.current()?.value(forKey: "blockedUsers") as? [String]
                //            {
                //                if blockedUsers.contains(objId!)
                //                {
                //                    self.showMessage(messages.alreadyBlocked)
                //                    return
                //                }else
                //                {
                //                    var blockedUserArray : [String] = blockedUsers
                //                    blockedUserArray.append(objId!)
                //                    print(blockedUserArray)
                //                    if let currentUser = PFUser.current(){
                //                        currentUser["blockedUsers"] = blockedUserArray
                //                        self.blocked = true
                //                        currentUser.saveInBackground(block: { (status, error) in
                //                            self.backMethod()
                //                            self.showMessage(messages.blockedUser)
                //                        })
                //                    }
                //
                //                    self.unfollowBlockedUser()
                //                    self.unfollowMyself()
                //                }
                //            }else
                //            {
                //                if let currentUser = PFUser.current(){
                //                    let arrayIds : [String] = [objId!]
                //                    currentUser["blockedUsers"] = arrayIds
                //                    self.blocked = true
                //                    currentUser.saveInBackground(block: { (status, error) in
                //                        self.backMethod()
                //                        self.showMessage(messages.blockedUser)
                //                    })
                //                }
                //
                //                self.unfollowBlockedUser()
                //                self.unfollowMyself()
                //            }
            }))
            self.present(alert, animated: true, completion: nil)
        }else if sender.tag == 1{
            var alert = UIAlertController()
            let user = currentUser
            let currentName = user?.value(forKey: "name") as! String
            alert = UIAlertController(title: "Unblock \(currentName)", message: "Are you sure you want to unblock \(currentName)?", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            
            alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
                let objId = self.currentUser?.objectId
                let blockedUserObj = PFQuery(className: "BlockedUsers")
                blockedUserObj.whereKey("FromUser", equalTo: PFUser.current()?.objectId ?? "")
                blockedUserObj.whereKey("BlockedUser", equalTo:objId )
                blockedUserObj.findObjectsInBackground { (objects, error) in
                    if error == nil{
                        if objects != nil{
                            if objects!.count > 0{
                                for value in objects!{
                                    value.deleteInBackground(block: { (success, error) in
                                        if success == true{
                                            self.blockbtn.tag = 0
                                        }
                                    })
                                }
                            }
                        }
                    }
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func unfollowBlockedUser()
    {
        if self.friend == "FOLLOWING"
        {
            let objectId = self.currentUser?.objectId
            let loggedIn = PFUser.current()?.objectId
            let query = PFQuery(className:"User_follow")
            query.whereKey("follower_id", equalTo: loggedIn!)
            query.whereKey("followed_id", equalTo: objectId!)
            query.findObjectsInBackground(block: { (objects, error) in
                if error == nil {
                    for objectt in objects! {
                        objectt.deleteInBackground(block: { (status, error) in
                            if status == true{
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                                self.friend = "FOLLOW"
                                self.profileTable.reloadData()
                            }
                        })
                    }
                }
                else {
                    //let message = (error?.localizedDescription)
                    //self.showMessage(message!)
                    print(error!)
                }
            })
        }
    }
    
    
    func unfollowMyself()
    {
        if self.friend == "FOLLOWING"
        {
            let objectId = self.currentUser?.objectId
            let loggedIn = PFUser.current()?.objectId
            print(objectId!)
            let query = PFQuery(className:"User_follow")
            query.whereKey("follower_id", equalTo:objectId!)
            query.whereKey("followed_id", equalTo:loggedIn!)
            query.findObjectsInBackground(block: { (objects, error) in
                if error == nil {
                    for objectt in objects! {
                        objectt.deleteInBackground(block: { (status, error) in
                            if status == true{
                                print(status)
                            }
                        })
                    }
                }
                else {
                    //let message = (error?.localizedDescription)
                    //self.showMessage(message!)
                    print(error!)
                }
            })
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.firstFunction()
        self.logController()
        if !isVideoSeen {
            
            popUp.isHidden = false
            popUp.fadeOut()
            
            let gif = UIImage(gifName: "loading.gif")
            let gifmanager = SwiftyGifManager(memoryLimit:20)
            self.progressImage.setGifImage(gif, manager: gifmanager)
            self.heightOFProgressView.constant = 0
            self.progressVIew.isHidden = true
            
            if userProfile == true || self.userFromSearch != nil {
                self.loadDataToView()
            }
        }
        
        isVideoSeen = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.updateMuted), name: NSNotification.Name(rawValue: "mute"), object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.postsListFirstRecord(indexObj:)), name: NSNotification.Name(rawValue: "postSuccessMyProfile"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        JPVideoPlayerManager.shared().stopPlay()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "postSuccessMyProfile"), object: nil);
    }
    
    @IBAction func backButton(_ sender: Any) {
        self.backMethod()
    }
    
    func backMethod()
    {
        nestedprofileArray =  [[],[],"","","",""]
        
        if refreshSearchBlocked && blocked
        {
            callback!()
        }
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @objc func click_noPost(sender:UIButton){
        let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
        let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePost_VC")  as! CreatePost_VC
        vc.fromMyProfile = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getProfileOfUser() {
        let nameQuery : PFQuery = PFUser.query()!
        nameQuery.whereKey("objectId", equalTo: objectId)
        nameQuery.findObjectsInBackground(block: { (objects, error) in
            print(objects!)
            for object in objects! {
                self.userFromSearch = object as? PFUser
            }
            self.currentUser = self.userFromSearch
            self.checkUserFollowed()
            self.checkuserBlocked()
            self.serviceCalling()
            self.postListUI()
            self.loadDataToView()
        })
    }
    
    //MARK:- remove value from firebase messages table in case of block
    func deleteChatMessagesForBlckedUser(userId:String){
        let DBref = FIRDatabase.database().reference().child("messages").child((PFUser.current()?.objectId!)!).child(userId)
        DBref.removeValue(completionBlock:
            { (error, DBref) in

        })
        
        let DBref1 = FIRDatabase.database().reference().child("messages").child(userId).child((PFUser.current()?.objectId!)!).childByAutoId()
        DBref1.removeValue(completionBlock:
            { (error, DBref) in
                
        })
        
    }
    
    //MARK:-For image updation at Edit profile
    
    @objc func methodOfReceivedNotification(notification: Notification){
        self.getImagesFromDb() //Take Action on Notification
    }
    
    //MARK:-Get Work Experience
    func getWorkExperience(){
        //Get work experience from User_work_history table
        self.profileAcIndicator.startAnimating()
        let img:NSMutableArray = []
        //print(self.profileImageArray)
        self.companyArray.removeAllObjects()
        self.compobjIds.removeAllObjects()
        self.companyobj.removeAllObjects()
        
        let objId = currentUser?.objectId
        let query = PFQuery(className:"User_work_history")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                cworkExpWholeobj.removeAllObjects()
                for object in objects! {
                    self.objectidtoPass = object.objectId! as String
                    cworkExpWholeobj.add(object)
                }
                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "from_year", ascending: false)
                let sortedResults = cworkExpWholeobj.sortedArray(using: [descriptor])
                //print(sortedResults)
                cworkExpWholeobj.removeAllObjects()
                self.companyArray.removeAllObjects()
                self.compobjIds.removeAllObjects()
                cworkExpWholeobj = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
                //print("After Sort",cworkExpWholeobj)
                for object in cworkExpWholeobj{
                    self.compobjIds.add((object as AnyObject).value(forKey: "objectId")!)
                    self.companyArray.add((object as AnyObject).value(forKey: "company_name")!)
                    img.add("work.png")
                    self.comapnyAPIresponse = true
                }
                //print(self.companyArray)
                //print(self.compobjIds)
                self.refreshControl.endRefreshing()
            }
            else {
                //print(error!)
                self.profileAcIndicator.stopAnimating()
                self.refreshControl.endRefreshing()
            }
            self.profileImageArray.replaceObject(at: 0, with: img)
            nestedprofileArray.replaceObject(at: 0, with: self.companyArray)
            
            self.getSchoolNameList()
        })
        
    }
    //MARK:-Fetch school data
    func getSchoolNameList(){
        
        //print(self.profileImageArray)
        //Get work education from User_edu table
        
        schoolArray.removeAllObjects()
        self.sobjId.removeAllObjects()
        self.schoolobj.removeAllObjects()
        let schoolImgArray:NSMutableArray=[]
        self.sobjId.removeAllObjects()
        
        let objId = currentUser?.objectId
        let query = PFQuery(className:"User_education")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                schollWholeobj.removeAllObjects()
                for object in objects! {
                    schollWholeobj.add(object)
                    //print(object.value(forKey: "school_name")!)
                    self.profileAcIndicator.stopAnimating()
                    self.refreshControl.endRefreshing()
                }
            } else {
                //print(error!)
                self.profileAcIndicator.stopAnimating()
                //                self.refreshControl.endRefreshing()
            }
            
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "batch_from", ascending: false)
            let sortedResults = schollWholeobj.sortedArray(using: [descriptor])
            ////print(sortedResults)
            schollWholeobj.removeAllObjects()
            self.schoolArray.removeAllObjects()
            self.sobjId.removeAllObjects()
            schollWholeobj = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
            ////print("After Sort SChool List",schollWholeobj)
            for object in schollWholeobj{
                self.schoolArray.add((object as AnyObject).value(forKey: "school_name")!)
                self.sobjId.add((object as AnyObject).value(forKey: "objectId")!);
                schoolImgArray.add("education.png")
            }
            
            
            nestedprofileArray.replaceObject(at: 1, with: self.schoolArray)
            self.profileImageArray.replaceObject(at: 1, with: schoolImgArray)
            //print("After school objects",self.schoolArray)
            //print(self.schoolArray)
            
            self.profileArray.removeAllObjects()
            self.filterImgArray.removeAllObjects()
            for index in 0 ..< nestedprofileArray.count {
                if nestedprofileArray.object(at: index) as? String != "" && (nestedprofileArray.object(at: index) as? NSArray)?.count != 0{
                    if let objArray = (nestedprofileArray.object(at: index) as? NSArray){
                        self.profileArray.addObjects(from: objArray as! [Any])
                        
                        
                    }
                    else{
                        self.profileArray.add(nestedprofileArray[index])
                    }
                }
                if self.profileImageArray.object(at: index) as? String != "" && (self.profileImageArray.object(at: index) as? NSArray)?.count != 0{
                    if let objImgArray = (self.profileImageArray.object(at: index) as? NSArray){
                        self.filterImgArray.addObjects(from: objImgArray as! [Any])
                        
                    }
                    else{
                        self.filterImgArray.add(self.profileImageArray[index])
                    }
                }
            }
            //print(self.profileArray)
            //print(self.filterImgArray)
            self.profileAcIndicator.stopAnimating()
            self.profileTable.isHidden = false
            self.profileTable.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:-Delegate followClickedDelegateProfile
    func followClickedDelegateProfile(cell:UserInfoCell) {
        
        if let followText = cell.btn_follow.titleLabel?.text{
            if followText.trimmingCharacters(in: .whitespacesAndNewlines) == "FOLLOW"
            {
                cell.indicator.startAnimating()
                cell.btn_follow.setTitle("", for: .normal)
                let objId = PFUser.current()?.objectId
                let User_followClass = PFObject(className: "User_follow")
                User_followClass["follower_id"] = objId
                User_followClass["followed_id"] = currentUser?.objectId
                User_followClass["parent_follower"] = PFUser.current()
                User_followClass["parent_following"] = currentUser
                
                var typeObj = "0"
                if let type = currentUser?.object(forKey: "profile_type") as? String
                {
                    if type == "1"
                    {
                        User_followClass["status"] = "1"
                        typeObj = "1"
                    }else
                    {
                        User_followClass["status"] = "0"
                    }
                }else
                {
                    User_followClass["status"] = "1"
                }
                
                User_followClass.saveInBackground { (success, error) in
                    cell.indicator.stopAnimating()
                    if(success)
                    {
                        //print("user follow")
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateSearchStatus"), object: nil)
                        
                        
                        if typeObj == "1" {
                            self.friend = "FOLLOWING"
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                            let time = formatter.string(from: Date())
                            let user = self.currentUser
                            
                            self.saveToNotifications(from_id: objId!, to_id: user!.objectId!, notification_type: "follow_public_user", time: time, post_id: "", parent_from: PFUser.current()!, parent_to: user!,commentStr: "")
                            
                            let currentName = PFUser.current()!.value(forKey: "name") as! String
                            self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.startedfollowing)",type:"follow_public_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                        } else {
                            self.friend = "CANCEL"
                            self.pushNotification()
                        }
                        
                        self.profileTable.reloadData()
                    }
                    else{
                        //print("error",error!)
                        self.friend = "FOLLOW"
                        cell.btn_follow.setTitle("FOLLOW", for: .normal)
                        let message = (error?.localizedDescription)
                        self.showMessage(message ?? "")
                        return
                    }
                }
            }else
            {
                var alert = UIAlertController()
                if followText.trimmingCharacters(in: .whitespacesAndNewlines) == "FOLLOWING"
                {
                    let user = currentUser
                    let currentName = user?.value(forKey: "name") as! String
                    alert = UIAlertController(title: "Are you sure you want to unfollow \(currentName)?", message: "", preferredStyle: UIAlertController.Style.alert)
                }else
                {
                    alert = UIAlertController(title: "Are you sure you want to cancel this request?", message: "", preferredStyle: UIAlertController.Style.alert)
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                    cell.indicator.stopAnimating()
                }))
                
                alert.addAction(UIAlertAction(title:Constants.okTitle, style: .default, handler: { action in
                    switch action.style{
                    case .default:
                        cell.indicator.startAnimating()
                        cell.isUserInteractionEnabled = false
                        
                        
                        let loggedIn = PFUser.current()?.objectId
                        let objectId = self.currentUser?.objectId
                        let query = PFQuery(className:"User_follow")
                        query.whereKey("follower_id", equalTo: loggedIn!)
                        query.whereKey("followed_id", equalTo: objectId!)
                        query.findObjectsInBackground(block: { (objects, error) in
                            cell.isUserInteractionEnabled = true
                            cell.indicator.stopAnimating()
                            if error == nil {
                                for objectt in objects! {
                                    objectt.deleteInBackground(block: { (status, error) in
                                        if status == true {
                                            cell.btn_follow.setTitle("FOLLOW", for: .normal)
                                            cell.btn_follow.backgroundColor = UIColor.clear
                                            cell.btn_follow.setTitleColor(ConstantColors.themeColor, for: .normal)
                                            cell.btn_follow.layer.borderColor = UIColor.lightGray.cgColor
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateSearchStatus"), object: nil)
                                            self.friend = "FOLLOW"
                                            self.profileTable.reloadData()
                                        }
                                    })
                                }
                            }
                            else {
                                cell.isUserInteractionEnabled = true
                                cell.btn_follow.setTitle("FOLLOWING", for: .normal)
                                self.friend = "FOLLOWING"
                                let message = (error?.localizedDescription)
                                self.showMessage(message!)
                                print(error!)
                            }
                        })
                        
                        
                        
                    case .cancel: break
                    print("cancel")
                        
                    case .destructive:
                        print("destructive")
                    }
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }
        
    }
    
    //Mark:message send
    func sendMsgClickedtoChatlist(cell:UserInfoCell){
        
        if let profileStatus = self.currentUser?.value(forKey: "profile_type") as? String, profileStatus == "1"  || cell.btn_follow.titleLabel?.text == "FOLLOWING"  {
            let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
            let vc = chatSB.instantiateViewController(withIdentifier: "SingleChatViewController")  as! SingleChatViewController
            vc.senderIdToPass = (currentUser?.objectId)!
            vc.titleStr = cell.name.text!
            vc.pushUsrId = (PFUser.current()?.objectId)!
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
            
        else{
            print("not a friend to chat")
            self.showMessage("You are not following this user yet.")
        }
    }
    
    func pushNotification()
    {
        let user = currentUser
        let currentName = PFUser.current()!.value(forKey: "name") as! String
        self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.requestSent)",type:"follow_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
    }
    
    //to list Follow count
    func followClickedtolistCount(cell:UserInfoCell,type:String){
        
        let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = tabbarSB.instantiateViewController(withIdentifier: "FollowListViewController")  as! FollowListViewController
        vc.type = "FOLLOWERS"
        vc.loggedInId = self.objectId
        vc.getfinalArray = self.followArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func followingClickedtolistCount(cell:UserInfoCell,type:String){
        let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = tabbarSB.instantiateViewController(withIdentifier: "FollowListViewController")  as! FollowListViewController
        vc.type = "FOLLOWING"
        if userProfile == false{
            if userFromSearch != nil{
                if let id = userFromSearch?.objectId{
                    vc.loggedInId = id
                }
            }else{
                if let id = currentUser?.objectId{
                    vc.loggedInId = id
                }
            }
        }else{
            if let id = currentUser?.objectId{
                vc.loggedInId = id
            }
        }
        vc.getfinalArray = self.followingArray
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == profileTable {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                if self.PFObjectArray.count>5
                {
                    self.postsListApi(isRefresh: false)
                }else
                {
                    if self.PFObjectArray.count>=1 && self.PFObjectArray.count<5
                    {
                      //  self.showMessage(messages.noPost)
                        return
                    }
                }
            }
        }
    } 
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    
    //    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
    //        return UITableViewAutomaticDimension
    //    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == profileTable {
            return UITableView.automaticDimension
        }
        //        else  if tableView == popUpTableViewObj
        //        {
        return 50
        //        }
        //        else
        //        {
        //            return UITableViewAutomaticDimension
        //        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == profileTable {
            if self.PFObjectArray.count > 0{
                return self.PFObjectArray.count + profileArray.count + 1
            }else{
                return  profileArray.count + 2
            }
            
        } else if tableView == popUpTableViewObj {
            return popUpArray.count
            
        } else {
            return profileArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == profileTable {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! UserInfoCell
                if let pstatus = currentUser?.object(forKey: "profile_updated") as? String{
                    self.profileFlag = pstatus
                }
                cell.delegate = self
                cell.delegateProfile = self
                cell.delegateFollowlist = self
                cell.editUserImgBtn.tag = indexPath.row
                
                if userProfile == true
                {
                    cell.editUserImgBtn.isHidden = false
                    cell.editBgButtonObj.isHidden = false
                }else
                {
                    cell.editUserImgBtn.isHidden = true
                    cell.editBgButtonObj.isHidden = true
                }
                
                if followArray.count == 1 {
                    cell.followLbl.text = messages.followerText
                    
                }
                else{
                    cell.followLbl.text = messages.followersText
                }
                cell.followersCount.text! = "\(self.followArray.count)"
                
                //Following count as
                
                cell.followingCountLbl.text = "\(self.followingArray.count)"
                cell.ht_locationView.constant = 0
                
                if profileFlag != "0"{
                    cell.designation.text = bioVal
                    if bioVal.isEmpty == true{
                        cell.designation.isHidden = true
                    }else{
                        cell.designation.isHidden = false
                    }
                    if getLocVal.trimmingCharacters(in: .whitespacesAndNewlines) != "" && websiteVal.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                        cell.singleViewLocation.isHidden = true
                        cell.locationInsideView.isHidden = false
                        cell.ht_locationView.constant = 18
                        cell.locationView.isHidden = false
                        cell.websiteIcon.isHidden = false
                        cell.locIcon.isHidden = false
                        cell.locationLbl.text = getLocVal
                        cell.locIcon.image = UIImage(named: "location.png")!
                        cell.websiteLbl.text = websiteVal
                        cell.websiteIcon.image = UIImage(named: "website.png")!
                        
                    } else if getLocVal.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                        cell.ht_locationView.constant = 0
                        cell.ht_singleLocation.constant = 18
                        cell.singleViewLocation.isHidden = false
                        cell.locationView.isHidden = true
                        cell.locationInsideView.isHidden = true
                        cell.singleViewTitle.text = getLocVal
                        cell.singleViewIcon.image = UIImage(named: "location.png")!
                    }
                    else if websiteVal.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                        cell.locationView.isHidden = true
                        cell.ht_locationView.constant = 0
                        cell.ht_singleLocation.constant = 18
                        cell.locationInsideView.isHidden = true
                        cell.singleViewLocation.isHidden = false
                        cell.singleViewTitle.text = websiteVal
                        cell.singleViewIcon.image = UIImage(named: "website.png")!
                    }
                }
                
                if let nameStr = currentUser?.object(forKey: "name") as? String{
                    cell.name.text = nameStr.capitalized
                }
                if userProfile == true{
                    cell.vw_otherUserFollow.isHidden = true
                    cell.ht_constrnt_followOther.constant = 0
                }else{
                    cell.vw_otherUserFollow.isHidden = false
                    cell.ht_constrnt_followOther.constant = 40
                    
                    // set title of follow btn whether user followed or not
                    if friend == "FOLLOWING"
                    {
                        cell.btn_follow.setTitle(friend, for: .normal)
                        cell.btn_follow.backgroundColor = ConstantColors.themeColor
                        cell.btn_follow.setTitleColor(UIColor.white, for: .normal)
                    }   else if friend == "CANCEL" || friend == "Cancel" {
                        cell.btn_follow.setTitle("CANCEL", for: .normal)
                        cell.btn_follow.backgroundColor = ConstantColors.themeColor
                        cell.btn_follow.setTitleColor(UIColor.white, for: .normal)
                    }
                        
                    else {
                        cell.btn_follow.setTitle(friend, for: .normal)
                        cell.btn_follow.backgroundColor = UIColor.clear
                        cell.btn_follow.setTitleColor(ConstantColors.themeColor, for: .normal)
                    }
                }
                
                ///image taping
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                cell.backgroundImage.isUserInteractionEnabled = true
                cell.backgroundImage.addGestureRecognizer(tapGestureRecognizer)
                
                let tapGestureRecognizerUser = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizerUser:)))
                cell.userImgView.isUserInteractionEnabled = true
                cell.userImgView.addGestureRecognizer(tapGestureRecognizerUser)
                
                self.profileAcIndicator.stopAnimating()
                cell.userImgView.image = self.userimageToSend
                
                if self.bgimageToSend.size == CGSize.init(width: 0.0, height: 0.0){
                    cell.backgroundImage.image = #imageLiteral(resourceName: "dummy-image-landscape")
                }else{
                    cell.backgroundImage.image = self.bgimageToSend
                }
                if getAllData {
                    //cell.bgImageHeight.constant = cell.TView.frame.maxY
                    self.perform(#selector(showBgImage), with: cell, afterDelay: 0.1)
                } else {
                    //cell.bgImageHeight.constant = 0
                }
                return cell
                
            } else if indexPath.row > 0 && indexPath.row <= profileArray.count {
                var designationStr:String = ""
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath as IndexPath) as! InfoTableViewCell
                
                let index = indexPath.row - 1
                
                cell.upShadowHeightConstraint.constant = 0
                cell.bottomShadowHeightConstraint.constant = 0
                cell.topMarginViewHeightConstraint.constant = 6
                if profileArray.count == 1{
                    cell.topMarginViewHeightConstraint.constant = 12
                    cell.upShadowHeightConstraint.constant = 30
                    
                }
                if index == profileArray.count - 1 {
                    cell.bottomMarginViewHeightConstraint.constant = 5
                    cell.bottomSpaceLabelConstriant.constant = 15
                    cell.bottomShadowHeightConstraint.constant = 30
                    
                } else {
                    cell.bottomMarginViewHeightConstraint.constant = 0
                    cell.bottomSpaceLabelConstriant.constant = 2
                    
                    if index == 0 {
                        cell.upShadowHeightConstraint.constant = 30
                        cell.topMarginViewHeightConstraint.constant = 12
                    }
                }
                
                if index >= profileArray.count && index < 0 {
                    print("index:..",index)
                    return cell
                }
                
                cell.selectionStyle = .none
                if let valueStr = profileArray.object(at: index) as? String {
                    var newVal:String = ""
                    if self.filterImgArray.object(at: index) as? String == "work.png" {
                        
                        if index < cworkExpWholeobj.count {
                            if let designation = (cworkExpWholeobj.object(at: index) as AnyObject).value(forKey: "designation") as? String {
                                if designation != "" {
                                    designationStr = "\(designation) at "
                                }
                            }
                            
                            if let toyearStr = (cworkExpWholeobj.object(at: index) as AnyObject).value(forKey: "to_year") as? String{
                                //print(toyearStr)
                                
                                let date = Date()
                                let calendar = Calendar.current
                                let components = calendar.dateComponents([.year, .month, .day], from: date)
                                let year =  components.year
                                //print(year as Any)
                                let currentYear = String(describing: year!)
                                
                                if toyearStr == currentYear || toyearStr == "Present" {
                                    newVal = "\(designationStr) \(valueStr)"
                                }
                                else  {
                                    newVal = "Former \(designationStr) \(valueStr)"
                                }
                            }
                        }
                    }
                        
                    else if self.filterImgArray.object(at: index) as? String == "education.png"{
                        newVal = "Studied at \(valueStr)"
                    } else {
                        newVal = valueStr
                    }
                    cell.infoLbl.text = newVal
                    
                }
                if let imageStr = self.filterImgArray.object(at: index) as? String{
                    cell.info_icon.image =  UIImage(named: imageStr)
                }
                
                return cell
            }
            else {
                //POSTS ui
                if postExist
                {
                    let index = indexPath.row - 1 - profileArray.count
                    if PFObjectArray.count > index{
                        if PFObjectArray.object(at: index) as? String == Constants.advertisement
                        {
                            let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementCell", for: indexPath as IndexPath) as! AdvertisementCell
                            cell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
                            cell.bannerView.rootViewController = self
                            cell.bannerView.load(GADRequest())
                            return cell
                            
                        }else
                        {
                            var obj = (PFObjectArray.object(at: index) as AnyObject)
                            var postType =  ""
                            if let post_type = obj.object(forKey: "post_type") as? String
                            {
                                postType = post_type
                            }
                            var key = "parent_id"
                            
                            var userName = ""
                            var profileImage : PFFile?
                            if let user = obj.object(forKey: key) as? PFUser
                            {
                                if let name = user.object(forKey: "name") as? String {
                                    userName = name
                                }
                                if let profileImageFile = user.object(forKey: "profile_image") as? PFFile
                                {
                                    profileImage = profileImageFile
                                }
                            }
                            
                            
                            var timeStamp = ""
                            
                            if let timeStampObj = obj.object(forKey: "time_stamp") as? String
                            {
                                let newdateStr = timeStampObj.replacingOccurrences(of: "_", with: " ")
                                let dateFormatter = DateFormatter()
                                if newdateStr.lowercased().contains("pm") == true || newdateStr.lowercased().contains("am"){
                                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
                                }else{
                                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                                }
                                
                                let newDate = dateFormatter.date(from: newdateStr)
                                if let newdate1 = newDate as? Date {
                                    let timeStr  = self.timeAgoSinceDate(date:newdate1,numericDates: true)
                                    timeStamp = timeStr
                                }
                            }
                            
                            var descriptionLbl : ActiveLabel?
                            var commentCount = "0"
                            if let comment = obj.value(forKey: "comment_count") as? String
                            {
                                if let commentNumber = Int(comment){
                                    commentCount = "\(commentNumber)"
                                }
                            }
                            
                            if postType == "URL"{
                                // for post type url load another cell
                                let cell = tableView.dequeueReusableCell(withIdentifier: "Home_UrlCell", for: indexPath as IndexPath) as! Home_UrlCell
                                cell.parent = tableView
                                cell.likeBtn.tag = index
                                cell.likeListingObj.tag = index
                                cell.commentListing.tag = index
                                cell.moreBtn.tag = index
                                cell.userProfileBtn.tag = index
                                cell.btn_comment.tag = index
                                cell.delegate = self
                                
                                cell.name.text = userName
                                if profileImage != nil
                                {
                                    cell.profileImage.file = profileImage!
                                    cell.profileImage.loadInBackground()
                                } else {
                                    cell.profileImage.image =  UIImage.init(named: "dummyProfile")
                                }
                                cell.time.text = timeStamp
                                descriptionLbl = cell.descriptionLbl
                                
                                if descriptionLbl != nil{
                                    setDescriptionLbl(descriptionLbl:descriptionLbl!,obj:obj,index:index)
                                }
                                
                                if let link_title = obj.object(forKey: "link_title") as? String
                                {
                                    if !(link_title == "") && postType == "URL"
                                    {
                                        cell.lineView.isHidden = true
                                        var urlStr = ""
                                        if let link_url = obj.object(forKey: "link_url") as? String
                                        {
                                            urlStr = link_url
                                        }
                                        if urlStr.contains("www.youtube.com/watch?v=") == true{
                                            cell.img_youtube.isHidden = false
                                            cell.imgVw_youtube.isHidden = false
                                            cell.img_youtube.isHidden = false
                                            cell.htConsrnt_youtube.constant = 110
                                            
                                        }else{
                                            cell.imgVw_youtube.image = nil
                                            cell.imgVw_youtube.isHidden = true
                                            cell.img_youtube.isHidden = true
                                            cell.htConsrnt_youtube.constant = 0
                                        }
                                        
                                        cell.urlView.isHidden = false
                                        cell.urlView.layer.shadowColor = UIColor.lightGray.cgColor
                                        cell.urlView.layer.shadowOffset = CGSize(width: 3, height: 3)
                                        cell.urlView.layer.shadowRadius = 3.0
                                        cell.urlView.layer.shadowOpacity = 1
                                        cell.urlView.clipsToBounds = false
                                        cell.urlView.layer.masksToBounds = false
                                        
                                        cell.urlTitle.text = link_title
                                        if let link_description = obj.object(forKey: "link_description") as? String
                                        {
                                            cell.urlDescription.text = link_description
                                        }
                                        if let link_url = obj.object(forKey: "link_url") as? String
                                        {
                                            cell.urlAgain.text = link_url
                                        }
                                        
                                        if let link_image_url = obj.object(forKey: "link_image_url") as? String
                                        {
                                            let url = URL(string: link_image_url)
                                            if urlStr.contains("www.youtube.com/watch?v=") == true{
                                                if let imageUrl = url {
                                                    cell.imgVw_youtube.af_setImage(withURL: imageUrl)
                                                }
                                            }else{
                                                cell.imgVw_youtube.image = nil
                                            }
                                            if !(url==nil)
                                            {
                                                cell.urlImage.af_setImage(withURL: url!)
                                                cell.widthOfUrlImage.constant = 50
                                            }else
                                            {
                                                cell.urlImage.image = nil
                                                cell.widthOfUrlImage.constant = 0
                                            }
                                        }
                                    }
                                }
                                if let likeArray = obj.value(forKey: "user_like_post") as? NSArray
                                {
                                    cell.likeListingObj.isEnabled = true
                                    cell.likeListingObj.setTitle("\(likeArray.count)", for: .normal)
                                    let objId = PFUser.current()?.objectId
                                    if likeArray.contains(objId!)
                                    {
                                        cell.likeSelected.isHighlighted = true
                                        cell.likeBtn.isSelected = true
                                    }else
                                    {
                                        cell.likeSelected.isHighlighted = false
                                        cell.likeBtn.isSelected = false
                                    }
                                }else
                                {
                                    cell.likeListingObj.isEnabled = false
                                    cell.likeListingObj.setTitle("0", for: .normal)
                                    cell.likeBtn.isSelected = false
                                    cell.likeSelected.isHighlighted = false
                                }
                                
                                cell.commentListing.setTitle("\(commentCount)", for: .normal)
                                cell.selectionStyle = .none
                                return cell
                            }else{
                                // for post type images/videos/album load another cell
                                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath as IndexPath) as! HomeCell
                                cell.parent = tableView
                                cell.likeBtn.tag = index
                                cell.likeListingObj.tag = index
                                cell.commentListing.tag = index
                                cell.moreBtn.tag = index
                                cell.userProfileBtn.tag = index
                                cell.btn_comment.tag = index
                                cell.delegate = self
                                
                                cell.name.text = userName
                                if profileImage != nil
                                {
                                    cell.profileImage.file = profileImage!
                                    cell.profileImage.loadInBackground()
                                } else {
                                    cell.profileImage.image =  UIImage.init(named: "dummyProfile")
                                }
                                
                                cell.time.text = timeStamp
                                
                                descriptionLbl = cell.descriptionLbl
                                
                                if descriptionLbl != nil{
                                    setDescriptionLbl(descriptionLbl:descriptionLbl!,obj:obj,index:indexPath.row)
                                }
                                cell.collectionContainer.isHidden = true
                                cell.collectionVIewHeight.constant = 0
                                cell.heightOFPageController.constant = 0
                                
                                //post type basis cell reuse
                                if postType == "ALBUM" || postType == "IMAGE" || postType == "VIDEO"
                                {
                                    cell.lineView.isHidden = true
                                    cell.collectionContainer.isHidden = false
                                    cell.collectionVIewHeight.constant = 450
                                    cell.tag = indexPath.row
                                    if let postmedia = obj.object(forKey: "postmedia") as? NSArray
                                    {
                                        if postmedia.count>1
                                        {
                                            cell.heightOFPageController.constant = 20
                                        }
                                        cell.objectForCollection = obj as? PFObject
                                        if postType == "VIDEO"
                                        {
                                            videoIndexCell = indexPath.row
                                        }
                                        cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: cell, forRow: index,changeVideo:true)
                                    }
                                }else
                                {
                                    cell.lineView.isHidden = false
                                    
                                    if !(videoIndexCell+1 == index || videoIndexCell-1 == index)
                                    {
                                        print("indexPath.row =",indexPath.row)
                                        //  JPVideoPlayerManager.shared().stopPlay()
                                    }
                                }
                                
                                if let likeArray = obj.value(forKey: "user_like_post") as? NSArray
                                {
                                    cell.likeListingObj.isEnabled = true
                                    cell.likeListingObj.setTitle("\(likeArray.count)", for: .normal)
                                    let objId = PFUser.current()?.objectId
                                    if likeArray.contains(objId!)
                                    {
                                        cell.likeSelected.isHighlighted = true
                                        cell.likeBtn.isSelected = true
                                    }else
                                    {
                                        cell.likeSelected.isHighlighted = false
                                        cell.likeBtn.isSelected = false
                                        //   cell.likeBtn.setImage(UIImage.init(named: "likeUnSelected"), for: .normal)
                                    }
                                }else
                                {
                                    cell.likeListingObj.isEnabled = false
                                    cell.likeListingObj.setTitle("0", for: .normal)
                                    cell.likeBtn.isSelected = false
                                    cell.likeSelected.isHighlighted = false
                                }
                                
                                cell.commentListing.setTitle("\(commentCount)", for: .normal)
                                
                                cell.selectionStyle = .none
                                return cell
                            }
                        }
                    }
                    return UITableViewCell()
                    
                }else
                {
                    // in case np
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "Nopost_tableCell", for: indexPath)
                        as! Nopost_tableCell
                    cell1.selectionStyle = .none
                    cell1.lbl_header.textAlignment = .center
                    cell1.btn_NoPost.isHidden = true
                    if self.currentUser?.objectId != PFUser.current()?.objectId{
                        cell1.lbl_header.text = "No Posts Found."
                       
                    }else{
                        cell1.lbl_header.text = "When you share photos and videos, they'll appear on your profile.\n Share your first photo or video."
//                        cell1.lbl_header.textAlignment = .center
//                        cell1.btn_NoPost.addTarget(self, action: #selector(click_noPost(sender:)), for: .touchUpInside)
//                        cell1.btn_NoPost.isHidden = false
                    }
                    return cell1
                }
            }
        } else if tableView == popUpTableViewObj
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath as IndexPath) as! PostTableViewCell
            cell.bottomLayoutObj.isActive = false
            
            if popUpArray.contains("CANCEL")
            {
                cell.topLayout.constant = 15
                cell.titleObj.text = popUpArray.object(at: indexPath.row) as? String
                cell.postCountObj.isHidden = true
            }else
            {
                cell.topLayout.constant = 10
                cell.titleObj.text = (popUpArray.value(forKey: "name") as AnyObject).object(at: indexPath.row) as? String
                cell.postCountObj.text = (popUpArray.value(forKey: "username") as AnyObject).object(at: indexPath.row) as? String
                cell.postCountObj.isHidden = false
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            
        }
        
        return UITableViewCell()
    }
    
    @objc func updateMuted(object:NSArray)
    {
        let array = object.value(forKey: "object") as? NSArray
        var i = 0
        for obj in PFObjectArray
        {
            let  objectdata = array?.object(at: 1) as? PFObject
            if objectdata?.value(forKey: "objectId") as? String == (obj as? PFObject)?.objectId
            {
                if let muteValue = objectdata?.value(forKey: "mute") as? Bool
                {
                    objectdata?.setObject(!(muteValue), forKey: "mute")
                    PFObjectArray.replaceObject(at: i, with: objectdata ?? obj)
                }
            }
            i=i+1
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popUpTableViewObj
        {
            
            if popUpArray.contains("CANCEL")
            {
                if popUpArray.object(at: indexPath.row) as! String == "CANCEL"
                {
                    self.popUp.fadeOut()
                }else if popUpArray.object(at: indexPath.row) as! String == "SHARE"
                {
                    if let descriptionObj = (PFObjectArray.object(at: descriptionIndex) as AnyObject).object(forKey: "description") as? String
                    {
                        
                        var btnTitle : String = ""
                        if let users_tagged_withArray = (PFObjectArray.object(at: descriptionIndex) as AnyObject).value(forKey: "users_tagged_with") as? NSArray
                        {
                            if users_tagged_withArray.count>0
                            {
                                if users_tagged_withArray.count == 1
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String)"
                                }else if users_tagged_withArray.count == 2
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \((users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String)"
                                }else
                                {
                                    
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \(users_tagged_withArray.count-1) others"
                                }
                            }
                        }
                        
                        
                        
                        var urlStr : String = ""
                        if let postmedia = (PFObjectArray.object(at: descriptionIndex) as AnyObject).object(forKey: "postmedia") as? NSArray
                        {
                            if let fileObj = postmedia.object(at: 0) as? PFFile
                            {
                                urlStr = fileObj.url ?? ""
                            }
                        }
                        
                        print(descriptionObj)
                        let vc = UIActivityViewController(activityItems: [descriptionObj+btnTitle,urlStr], applicationActivities: [])
                        present(vc, animated: true, completion: nil)
                    }
                    self.popUp.fadeOut()
                }
                else if popUpArray.object(at: indexPath.row) as! String == "DELETE"
                {
                    self.popUp.fadeOut()
                    
                    let alert = UIAlertController(title: "Are you sure you want to delete this post?", message: "", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                    alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
                        
                        let object = self.PFObjectArray.object(at: self.descriptionIndex) as! PFObject
                        object.deleteInBackground(block: { (status, error) in
                            print(status)
                            if status == true{
                                self.PFObjectArray.removeObject(at: self.descriptionIndex)
                                self.profileTable.reloadData()
                                let arr = ["delete",object] as NSArray
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
                            }else{
                                let message = (error?.localizedDescription)
                                print(message as Any)
                            }
                        })
                        
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                }else if popUpArray.object(at: indexPath.row) as! String == "EDIT"
                {
                    self.popUp.fadeOut()
                    let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
                    let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePost_VC")  as! CreatePost_VC
                    vc.objectForEdit  = PFObjectArray.object(at: descriptionIndex) as? PFObject
                    vc.fromEdit  = true
                    let id = (PFObjectArray.object(at: descriptionIndex) as AnyObject).value(forKey: "objectId") as! String
                    vc.postForEdit = id
                    vc.indexEdit = descriptionIndex
                    vc.fromMyProfile = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if popUpArray.object(at: indexPath.row) as! String == "REPORT ABUSE"
                {
                    self.popUp.fadeOut()
                    let objId = PFUser.current()?.objectId
                    let postId = (PFObjectArray.object(at: descriptionIndex) as AnyObject).value(forKey: "objectId") as! String
                    
                    let compClass = PFObject(className: "Report_abuse")
                    compClass["post_id"] =  postId
                    compClass["user_id"] =  objId
                    compClass.saveInBackground(block: { (status, error) in
                        print(status)
                        if status ==  true{
                            self.showMessage("This post is marked as spam.")
                        }
                        else{
                            let message = (error?.localizedDescription)
                            print(message as Any)
                        }
                    })
                }
            }else
            {
                self.popUp.fadeOut()
                self.otherUser(index: indexPath.row, name: ((popUpArray.value(forKey: "name") as AnyObject).object(at: indexPath.row) as? String)!)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    
    func setDescriptionLbl(descriptionLbl:ActiveLabel,obj:AnyObject,index:Int){
        if let descriptionObj = obj.object(forKey: "description") as? String
        {
            var btnTitle : String = ""
            if let users_tagged_withArray = obj.value(forKey: "users_tagged_with") as? NSArray
            {
                if users_tagged_withArray.count>0
                {
                    if users_tagged_withArray.count == 1
                    {
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String)"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {
                            self.withUser(index: index, name: $0)
                        }
                    }else if users_tagged_withArray.count == 2
                    {
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \((users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String)"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {
                            self.withUser(index: index, name: $0)
                        }
                        
                        let secondUser = (users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String
                        let customTypeSecond = ActiveType.custom(pattern: "\\s\(secondUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeSecond)
                        descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeSecond) {
                            self.withUser(index: index, name: $0)
                        }
                    }else
                    {
                        
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \(users_tagged_withArray.count-1) others"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {
                            self.withUser(index: index, name: $0)
                        }
                        
                        let otherString = "\(users_tagged_withArray.count-1) others"
                        let customTypeSecond = ActiveType.custom(pattern: "\\s\(otherString)\\b")
                        descriptionLbl.enabledTypes.append(customTypeSecond)
                        descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeSecond) {_ in
                            
                            
                            self.popUp.fadeIn()
                            self.popUpArray = users_tagged_withArray
                            
                            if self.popUpArray.count>6
                            {
                                self.popUpTableViewObj.isScrollEnabled = true
                                self.heightOfPopUpTableView.constant = 300
                            }else
                            {
                                let count = self.popUpArray.count
                                self.heightOfPopUpTableView.constant = CGFloat(count * 50)
                                self.popUpTableViewObj.isScrollEnabled = false
                            }
                            self.popUpTableViewObj.reloadData()
                        }
                    }
                }
            }
            
            descriptionLbl.customize { label in
                
                descriptionLbl.text = descriptionObj+btnTitle
                
                let arr = descriptionObj.components(separatedBy: "\n")
                
                if ( arr.count > 3) || (descriptionObj.characters.count > 100)
                {
                    var lengthObj = 0
                    if descriptionObj.characters.count > 100
                    {
                        lengthObj = 100
                    }else
                    {
                        lengthObj = descriptionObj.characters.count
                    }
                    
                    let index = descriptionObj.index(descriptionObj.startIndex, offsetBy: lengthObj)
                    let substringDescription = descriptionObj.substring(to: index)
                    
                    label.text = substringDescription+" Continue Reading"
                    let continueReading = ActiveType.custom(pattern: "\\sContinue Reading\\b")
                    descriptionLbl.enabledTypes.append(continueReading)
                    descriptionLbl.customColor[continueReading] = ConstantColors.themeColor
                    descriptionLbl.handleCustomTap(for: continueReading) {_ in
                        print("hello")
                        
                        
                        let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
                        let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        vc.secondTime = true
                        vc.isFromHashTag = false
                        vc.titleSecond = "network 42"
                        vc.showOnlySinglePost = true
                        let id = obj.value(forKey: "objectId") as! String
                        vc.postIdObject = id
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
                descriptionLbl.isHidden = false
                descriptionLbl.numberOfLines = 0
                descriptionLbl.lineSpacing = 4
                descriptionLbl.textColor = UIColor(red: 102.0/255, green: 117.0/255, blue: 127.0/255, alpha: 1)
                descriptionLbl.hashtagColor = ConstantColors.themeColor
                descriptionLbl.mentionColor = ConstantColors.themeColor
                descriptionLbl.URLColor = ConstantColors.themeColor
                descriptionLbl.URLSelectedColor = UIColor(red: 82.0/255, green: 190.0/255, blue: 41.0/255, alpha: 1)
                
                descriptionLbl.handleMentionTap {
                    self.descriptionUser(index: index, userName: $0)
                }
                
                descriptionLbl.handleHashtagTap {
                    // MARK: - Hashtag
                   
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HashTag_VC") as! HashTag_VC
                        vc.tagString = "#\($0)"
                        self.navigationController?.pushViewController(vc, animated: true)
                }
                
                descriptionLbl.handleURLTap {
                    let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
                    let vc = storyboardObj.instantiateViewController(withIdentifier: "MTSWebViewController") as! MTSWebViewController
                    
                    print($0.absoluteString)
                    if let link_url = obj.object(forKey: "link_url") as? String
                    {
                        vc.urlStr = link_url
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    //MARK:-Back Image
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        //print(tappedImage)
        if !(tappedImage.image?.size)!.equalTo(CGSize.zero)
        {
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "FullScreenImgViewController") as! FullScreenImgViewController
            vc.pickerTypeStr = "background"
            vc.userProfile = userProfile
            vc.bgimageToSend = tappedImage.image!
            vc.profileCallback = {
                self.getImagesFromDb()
                self.profileTable.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        // Your action
    }
    
    //MARK:-User Image Tapped
    @objc func imageTapped(tapGestureRecognizerUser: UITapGestureRecognizer)
    {
        
        let tappedImage = tapGestureRecognizerUser.view as! UIImageView
        
        if tappedImage.image != UIImage(named: "dummyProfile") {
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "FullScreenImgViewController") as! FullScreenImgViewController
            vc.pickerTypeStr = "user"
            vc.userProfile = userProfile
            vc.bgimageToSend = tappedImage.image!
            vc.profileCallback = {
                self.getImagesFromDb()
                self.profileTable.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    
    //MARK:-Delegate
    //Image Picker
    
    func editImgClickedDelegate(_ index:Int,cell:UserInfoCell,type: String){
        
        pickerTypeStr = type
        cameraAuthorization()
    }
    func cameraAuthorization(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            
            self.checkCameraAccess(vc:self)
            //            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func chooseSheet(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            
        }
        
    }
    func openGallary() {
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertClose(_ gesture: UITapGestureRecognizer) {
        //        if imageEditBtnFlag == true {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    func alertClose1(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        profileAcIndicator.startAnimating()
        if let pickedImage = info[.originalImage] as? UIImage {
            uploadFlag = true
            if pickerTypeStr == "user"{
                
                userimageToSend = pickedImage
                let avatar = PFFile(data: userimageToSend.jpegData(compressionQuality: 1.0)!)
                PFUser.current()!.setObject(avatar!, forKey: "profile_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    //print(status)
                    if status ==  true{
                        self.addImageAPI()
                        self.profileAcIndicator.stopAnimating()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                })
            }
            if pickerTypeStr == "background"{
                
                bgimageToSend = pickedImage
                
                let avatar = PFFile(data: bgimageToSend.jpegData(compressionQuality: 1.0)!)
                PFUser.current()!.setObject(avatar!, forKey: "cover_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    //print(status)
                    if status ==  true{
                        self.profileAcIndicator.stopAnimating()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                    
                })
            }
            
        }
        profileTable.reloadData()
        dismiss(animated: true, completion: nil)
    }    
    
    
    func addImageAPI(){
        //        let chatDictionary1:NSDictionary = [:]
        var urlString: String = ""
        
        let imageData:NSData = self.userimageToSend.mediumQualityJPEGNSData as NSData
        // Create a reference to the file you want to upload
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        let  timeStrToPass = formatter.string(from: Date())
        
        let riversRef = FIRStorage.storage().reference().child("images/\(timeStrToPass).jpg")
        let metadata = FIRStorageMetadata()
        metadata.contentType = "image/jpeg"
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = riversRef.put(imageData as Data, metadata: metadata) { (metadata, error) in
            
        }
        
        uploadTask.observe(.success) { snapshot in
            print(snapshot) // Upload completed successfully
            let downloadURL = snapshot.metadata?.downloadURL()
            print(downloadURL!)
            urlString = String(describing: downloadURL!)
            
            if urlString != ""{
                let DBref = FIRDatabase.database().reference()
                //Sender data for group
                
                let userId = PFUser.current()!.objectId!
                
                let userDictionary = ["user_img":urlString] as [String : Any]
                let myRef = DBref.child("users/\(userId)")
                myRef.updateChildValues(userDictionary) { (error, DBref) in
                    if error == nil{
                        print("error == ",error)
                    }
                }
            }
        }
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:-Get Images from server
    func getImagesFromDb(){
        
        if let userPicture = currentUser?["profile_image"] as? PFFile {
            userPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                let image = UIImage(data: imageData!)
                if image != nil {
                    //print("image value",image!)
                    self.userimageToSend = (image)!
                    self.uploadFlag = true
                    DispatchQueue.main.async {
                        
                        let indexPath = NSIndexPath.init(row: 0, section: 0)
                        if let indexPaths = self.profileTable.indexPathsForVisibleRows{
                            if let pathIndex = indexPath as? IndexPath{
                                if indexPaths.contains(pathIndex) == true{
                                    self.profileTable.reloadRows(at: [pathIndex], with: .none)
                                }
                            }
                        }
                    }
                }
            })
        }
        
        if let coverPicture = currentUser?["cover_image"] as? PFFile {
            coverPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                let image = UIImage(data: imageData!)
                if image != nil {
                    //print("image value",image!)
                    self.uploadFlag = true
                    self.bgimageToSend = (image)!
                    self.getAllData = true
                    DispatchQueue.main.async {
                        let indexPath = NSIndexPath.init(row: 0, section: 0)
                        self.profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                    }
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("ImageNotify")
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    func getFollowList(){
        
        if let status = currentUser?.value(forKey: "profile_type") as? String, status == "1" {
            self.getWorkExperience()
            self.postsListApi(isRefresh: true)
        }
        
        let query = PFQuery(className: "User_follow")
        query.whereKey("followed_id", equalTo:currentUser?.objectId! ?? "")
        query.includeKey("parent_follower")
        query.whereKey("status", equalTo:"1")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                //print(objects!)
                self.followArray.removeAllObjects()
                for object in objects! {
                    //print(object)
                    if let singleUser = object.object(forKey: "parent_follower") as? PFUser
                    {
                        //print(singleUser)
                        self.followArray.add(singleUser)
                    }
                }
                self.profileTable.isHidden = false
                self.profileAcIndicator.stopAnimating()
                self.profileTable.reloadData()
            }
            else {
                //print(error!)
            }
        })
    }
    
    func getFollowingList(){
        let query = PFQuery(className: "User_follow")
        query.whereKey("follower_id", equalTo:currentUser?.objectId! ?? "")
        query.includeKey("parent_following")
        query.whereKey("status", equalTo:"1")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                //print(objects!)
                self.followingArray.removeAllObjects()
                for object in objects! {
                    //print(object)
                    if let singleUser = object.object(forKey: "parent_following") as? PFUser
                    {
                        print(singleUser)
                        
                        self.followingArray.add(singleUser)
                    }
                }
                self.profileAcIndicator.stopAnimating()
                self.profileTable.isHidden = false
                self.profileTable.reloadData()
                
            }
            else {
                //print(error!)
            }
        })
    }
}


extension ProfileViewController
{
    func postsListApi(isRefresh: Bool)
    {
        let objId = currentUser?.objectId ?? ""
        self.profileAcIndicator.startAnimating()
        let query = PFQuery(className: "Post")
        query.order(byDescending: "createdAt")
        query.whereKey("user_id",equalTo:objId)
        query.includeKey("parent_id")
        query.limit = 20
        query.skip = isRefresh ? 0 : self.PFObjectArray.count
        query.findObjectsInBackground(block: { (objects, error) in
            self.refreshControl.endRefreshing()
            self.profileAcIndicator.stopAnimating()
            if error == nil {
                
                if isRefresh {
                    self.PFObjectArray.removeAllObjects()
                }
                
                if let Objects = objects{
                    if Objects.count > 0{
                        for object in Objects {
                            object.setObject(false, forKey: "mute")
                            self.PFObjectArray.add(object)
                            self.postExist = true
                            
                            if self.PFObjectArray.count%Constants.addAfter==0
                            {
                                self.PFObjectArray.add(Constants.advertisement)
                            }
                        }
                    }else{
                        if self.PFObjectArray.count > 0{
                             self.postExist = true
                        }
                        else{
                             self.postExist = false
                        }
                    }
                }
                            
                if objects!.count == 0 && self.PFObjectArray.count > 1 {
                    //                     self.postExist = false
                    //                     self.profileTable.reloadData()
                 //   self.showMessage(messages.noPost)
                    return
                }
            }
        })
    }
    
    //MARK:- postsListFirstRecord
    
    @objc func postsListFirstRecord(indexObj: NSNotification)
    {
        //print(indexObj)
        var arr : NSMutableArray = []
        arr = indexObj.object as! NSMutableArray
        //print(arr)
        let query = PFQuery(className: "Post")
        query.whereKey("objectId", equalTo:arr.object(at: 1))
        query.includeKey("parent_id")
        query.findObjectsInBackground(block: { (objects, error) in
            self.heightOFProgressView.constant = 0
            self.progressVIew.isHidden = true
            globalBool.showProgess = false
            if error == nil {
                
                for object in objects! {
                    print(object)
                    if arr.object(at: 2) as! Bool == false
                    {
                        self.PFObjectArray.insert(object, at: 0)
                    }else
                    {
                        self.PFObjectArray.replaceObject(at: arr.object(at: 0) as! Int, with: object)
                        
                        let arr = ["Edited",object] as NSArray
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
                    }
                    
                    let indexPath = NSIndexPath.init(row: arr.object(at: 0) as! Int, section: 0)
                    self.profileTable.scrollToRow(at: indexPath as IndexPath , at: .top, animated: true)
                    self.profileTable.reloadData()
                    
                }
            }
            else {
                //print(error!)
            }
        })
    }
    
    
    
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    func withUser(index:Int,name:String)
    {
        //print(name)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        if let users_tagged_withArray = (PFObjectArray.object(at: index) as AnyObject).value(forKey: "users_tagged_with") as? NSArray
        {
            if users_tagged_withArray.count>0
            {
                //print(users_tagged_withArray)
                let index = (users_tagged_withArray.value(forKey: "name")  as AnyObject).index(of: name)
                let id = (users_tagged_withArray.object(at: index)as AnyObject).value(forKey: "id") as! String
                //print(id)
                
                var idsArray : [String] = []
                idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
                if idsArray.contains(id)
                {
                    vc.friend = "FOLLOWING"
                }else
                {
                    vc.friend = "FOLLOW"
                }
                
                vc.objectId = id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func otherUser(index:Int,name:String)
    {
        //print(name)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        
        let id = (popUpArray.object(at: index)as AnyObject).value(forKey: "id") as! String
        //print(id)
        
        
        var idsArray : [String] = []
        idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
        if idsArray.contains(id)
        {
            vc.friend = "FOLLOWING"
        }else
        {
            vc.friend = "FOLLOW"
        }
        
        vc.objectId = id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.popUp.fadeOut()
    }
    
    
    
    
    func descriptionUser(index:Int,userName:String)
    {
        //print(userName)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        if let users_tagged_withArray = (self.PFObjectArray.object(at: index) as AnyObject).value(forKey: "tagged_users_in_desc") as? NSArray
        {
            //print(users_tagged_withArray)
            let index = (users_tagged_withArray.value(forKey: "username")  as AnyObject).index(of: userName)
            let id = (users_tagged_withArray.object(at: index)as AnyObject).value(forKey: "id") as! String
            //print(id)
            
            var idsArray : [String] = []
            idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
            if idsArray.contains(id)
            {
                vc.friend = "FOLLOWING"
            }else
            {
                vc.friend = "FOLLOW"
            }
            
            vc.objectId = id
            self.navigationController?.pushViewController(vc, animated: true)
        }else
        {
            self.showMessage(messages.notFound)
            return
        }
    }
    
    
    
    func alert(_ title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        vc.addAction(UIAlertAction(title: Constants.okTitle, style: .cancel, handler: nil))
        present(vc, animated: true, completion: nil)
    }
    
    
}


extension ProfileViewController : likeListDelegate
{
    
    func likeListingClicked(tag: Int) {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "LikesViewController") as! LikesViewController
        let id = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "objectId") as! String
        //print(id)
        vc.postId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func likeClicked(sender: UIButton) {
        
        let tag = sender.tag
        sender.isSelected = !sender.isSelected
        var likeArrayObj : NSMutableArray = []
        if let likeArray = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "user_like_post") as? NSMutableArray
        {
            likeArrayObj = likeArray
        }
        let objId = PFUser.current()?.objectId
        let id = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "objectId") as! String
        
        if sender.isSelected && !likeArrayObj.contains(objId ?? ""){
            likeArrayObj.add(objId ?? "")
        } else {
            likeArrayObj.remove(objId ?? "")
        }
        
        let postObj = PFObjectArray.object(at: tag) as! PFObject
        postObj.setValue(likeArrayObj, forKey: "user_like_post")
        PFObjectArray.replaceObject(at: tag, with: postObj)
        self.profileTable.reloadData()
        
        
        let arr = ["Liked",postObj] as NSArray
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
        
        
        let likesQuery = PFQuery(className:"Post_likes")
        likesQuery.whereKey("post_id", equalTo:id)
        likesQuery.whereKey("user_id", equalTo:objId!)
        likesQuery.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                if objects!.count==0
                {
                    let compClass = PFObject(className: "Post_likes")
                    compClass["post_id"] =  id
                    compClass["user_id"] =  objId
                    compClass["parent_id"] =  PFUser.current()
                    compClass.saveInBackground(block: { (status, error) in
                        //print(status)
                        if status ==  true{
                            // likeArrayObj.add(objId!)
                            self.likeArrayUpdate(id: id, likeArr: likeArrayObj, index: tag)
                            
                            if let userId = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "user_id") as? String
                            {
                                
                                let formatter = DateFormatter()
                                formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                                let time = formatter.string(from: Date())
                                let objId = PFUser.current()?.objectId
                                let nameQuery : PFQuery = PFUser.query()!
                                nameQuery.whereKey("objectId", equalTo: userId)
                                nameQuery.findObjectsInBackground(block: { (objects, error) in
                                    for object in objects! {
                                        
                                        self.saveToNotifications(from_id: objId!, to_id: userId, notification_type: "like", time: time, post_id: id, parent_from: PFUser.current()!, parent_to: (object as? PFUser)!,commentStr: "")
                                        self.likePushNotification(userId: userId,postId: id)
                                    }
                                })
                                
                            }
                        }
                        else{
                            let message = (error?.localizedDescription)
                            //print(message as Any)
                        }
                    })
                }else
                {
                    //print("liked already")
                    for object in objects! {
                        //print(object)
                        object.deleteInBackground(block: { (status, error) in
                            //print(status)
                            if status == true{
                                likeArrayObj.remove(objId!)
                                self.likeArrayUpdate(id: id, likeArr: likeArrayObj, index: tag)
                            }else{
                                let message = (error?.localizedDescription)
                                //print(message as Any)
                            }
                        })
                    }
                }
            }})
    }
    
    
    func likePushNotification(userId:String,postId:String)
    {
        if !(userId == PFUser.current()!.objectId!)
        {
            let currentName = PFUser.current()?.value(forKey: "name") as! String
            self.sendPushNotification(postId: postId, userId: userId,settingType: "push_post_like",message:"\(currentName) \(messages.likePost)",type:"post_like",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
        }
    }
    
    
    func likeArrayUpdate(id:String,likeArr:NSArray,index:Int)
    {
        ////////////////////////
        let query = PFQuery(className:"Post")
        query.whereKey("objectId", equalTo:id)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                for object in objects! {
                    
                    //print(object)
                    object["user_like_post"] =  likeArr
                    object.saveInBackground(block: { (status, error) in
                        //print(status)
                        if status ==  true{
                            self.updateAPIArray(index: index, likeArr: likeArr)
                        }
                        else{
                            let message = (error?.localizedDescription)
                            //print(message as Any)
                        }
                    })
                }
            }
            else {
                //print(error!)
            }
            
        })
    }
    
    func updateAPIArray(index:Int,likeArr:NSArray)
    {
        //print(likeArr)
        let obj = PFObjectArray.object(at: index) as! PFObject
        obj.setValue(likeArr, forKey: "user_like_post")
        //print(obj)
        PFObjectArray.replaceObject(at: index, with: obj)
        let indexPath = NSIndexPath.init(row: index+1, section: 0)
        self.profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
        
    }
    
    
    func commentClicked(tag: Int) {
        
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        let id = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "objectId") as! String
        if let obj  = PFObjectArray.object(at: tag) as? PFObject{
            vc.objectFromHome = obj
        }
        
        vc.postId = id
        vc.delegate = self
        
        if let userId = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "user_id") as? String
        {
            vc.postUserId = userId
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func URLClicked(tag: Int)
    {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "MTSWebViewController") as! MTSWebViewController
        
        if let link_url = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "link_url") as? String
        {
            vc.urlStr = link_url
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func moreClicked(sender:UIButton,tag: Int)
    {
        descriptionIndex = tag
        self.popUp.fadeIn()
        //        self.popUpArray = ["SHARE","DELETE","EDIT","CANCEL"]
        
        if let id = (PFObjectArray.object(at: tag) as AnyObject).object(forKey: "user_id") as? String
        {
            let objId = PFUser.current()?.objectId
            if id == objId!
            {
                self.popUpArray = ["SHARE","EDIT","DELETE","CANCEL"]
            }else
            {
                self.popUpArray = ["REPORT ABUSE","SHARE","CANCEL"]
            }
        }
        if self.popUpArray.count>6
        {
            self.popUpTableViewObj.isScrollEnabled = true
            self.heightOfPopUpTableView.constant = 300
        }else
        {
            let count = self.popUpArray.count
            self.heightOfPopUpTableView.constant = CGFloat(count * 50)
            self.popUpTableViewObj.isScrollEnabled = false
        }
        
        self.popUpTableViewObj.reloadData()
    }
    
    
    @IBAction func chatAction(_ sender: Any) {
        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
        let vc = chatSB.instantiateViewController(withIdentifier: "ChatListingViewController")  as! ChatListingViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func userProfileClicked(tag: Int)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        let id = (PFObjectArray.object(at: tag)as AnyObject).value(forKey: "user_id") as! String
        vc.objectId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
        
    func imageClicked(tag: Int,object:PFObject)
    {

    }
}


extension ProfileViewController : commentCountUpdateDelegate
{
    func updateCommentCount(index:Int,value:String)
    {
        let obj = (PFObjectArray.object(at: index) as AnyObject)
        if let object = obj as? PFObject
        {
            object.setValue(value, forKey: "comment_count")
            self.profileTable.reloadData()
            
            let arr = ["CommentCount",object] as NSArray
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
        }
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension ProfileViewController : checkCameraPermissionDelegate,checkGalleryPermissionDelegate{
    
    func handleGalleryAction() {
        
    }
    func handleCameraAction(){
        self.openCamera()
    }
    
}

