//
//  CreatePost_VC.swift
//  Network42
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Simran. All rights reserved.
//


import UIKit
import DKImagePickerController
import Parse
import AVKit
import AVFoundation
import Firebase
import SwiftLinkPreview

extension UIImage {
    var uncompressedPNGData: NSData      { return self.pngData() as! NSData }
    var highestQualityJPEGNSData: NSData { return self.jpegData(compressionQuality: 1.0)! as NSData  }
    var highQualityJPEGNSData: NSData    { return self.jpegData(compressionQuality: 0.75)! as NSData }
    var mediumQualityJPEGNSData: NSData  { return self.jpegData(compressionQuality: 0.5)! as NSData  }
    var lowQualityJPEGNSData: NSData     { return self.jpegData(compressionQuality: 0.25)! as NSData }
    var lowestQualityJPEGNSData:NSData   { return
        self.jpegData(compressionQuality: 0.0)! as NSData  }
}

class CreatePost_VC: BaseViewController {
    
    var NewAndOldHashTagIdsArray : NSMutableArray = []
    
    var existHashTag : Bool = false
    
    var hashTagsFromTextview = [String]()
    
    var rangeToAppend = NSRange()
    var removeCount : Int = 0
    
    var globalRange = NSRange()
    var globalInt : Int = 1
    
    @IBOutlet weak var lbl_postheading: HeadingLabel!
    var caption = ""
    var fromEdit = false
    var assetsObj: [DKAsset]?
    
    var showLoader = false
    
    var fromMyProfile = false
    var objectForEdit : PFObject?
    var postForEdit : String = ""
    var indexEdit : Int = 0
    
    var postId : String = ""
    var postIdPFObject : PFObject?
    
    var savedArrayUser : NSMutableArray = []
    var globalMediaType : NSMutableArray = []
    @IBOutlet weak var btn_tag: UIButton!
    var videoFile : NSMutableArray = []
    // tuple for images containing image/whether from edit/caption
    var imagesArray = [(UIImage,Bool,String,String)]()
    
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Get View Url
    var isImageExist = false
    var isUrlExist = false
    var titleToSend : String! = ""
    var descriptionToSend : String! = ""
    var urlToSend : String! = ""
    var imageToSend : String = ""
    var imageUrlAsString = ""
    
    
    //MARK:- view lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "PostDecriptionCell", bundle: nil), forCellReuseIdentifier: "PostDecriptionCell")
        tableView.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
        tableView.register(UINib(nibName: "URLTableViewCell", bundle: nil), forCellReuseIdentifier: "URLTableViewCell")
        tabBarController?.tabBar.isHidden = true
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableView.automaticDimension
        
        if fromEdit
        {
            lbl_postheading.text = "Edit Post"
            setTableIfEdit()
        }else{
            lbl_postheading.text = "Post to Network42"
        }
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        UserDefaults.standard.removeObject(forKey: "taggedUser")
        UserDefaults.standard.synchronize()
    }
    
    
    //for check url valid
    func verifyUrl(urlString: String?) -> Bool {
        guard let urlString = urlString,
            let url = URL(string: urlString) else {
                return false
        }
        
        return true
    }
    
    func getDataInEdit(){
        
        if let descriptionObj = objectForEdit?.object(forKey: "description") as? String
        {
            caption = descriptionObj
        }
        
        if let post_type = objectForEdit?.object(forKey: "post_type") as? String
        {
            if post_type == "ALBUM" || post_type == "IMAGE" || post_type == "VIDEO"
            {
                
                if let postmedia = objectForEdit?.object(forKey: "postmedia") as? NSArray
                {
                    for i in 0..<postmedia.count
                    {
                        if let fileObj = postmedia.object(at: i) as? PFFile
                        {
                            if let media_typesArr = objectForEdit?.object(forKey: "media_types") as? NSArray
                            {
                                
                                globalMediaType = media_typesArr as! NSMutableArray
                                if media_typesArr.count > i{
                                    if let media_types = media_typesArr.object(at: i) as? String
                                    {
                                        if media_types == "VIDEO"
                                        {
                                            videoFile.add(fileObj)
                                            
                                            if let thumbnail = objectForEdit?.object(forKey: "thumbnail") as? NSArray
                                            {
                                                if let fileObj = thumbnail.object(at: i) as? PFFile
                                                {
                                                    do {
                                                        let image = try UIImage(data:fileObj.getData())
                                                        //self.imagesArray.add(image!)
                                                    }
                                                    catch let error as NSError
                                                    {
                                                        print("Image generation failed with error \(error)")
                                                    }
                                                }
                                            }else
                                            {
                                                let image = UIImage.init(named: "black.png")
                                                //  self.imagesArray.add(image!)
                                            }
                                            if self.imagesArray.count == postmedia.count
                                            {
                                                self.tableView.reloadData()
                                            }
                                        }else
                                        {
                                            videoFile.add("")
                                            do {
                                                let image = try UIImage(data:fileObj.getData())
                                                // self.imagesArray.add(image!)
                                            }
                                            catch let error as NSError
                                            {
                                                print("Image generation failed with error \(error)")
                                            }
                                            
                                            if self.imagesArray.count == postmedia.count
                                            {
                                                self.tableView.reloadData()
                                            }
                                        }
                                        if let media_description = self.objectForEdit?.object(forKey: "media_description") as? [String]
                                        {
                                            var imageObj = self.imagesArray[i]
                                            imageObj.2 = media_description[i] ?? ""
                                            self.imagesArray[i] = imageObj
                                            self.tableView.reloadData()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }else if post_type == "URL"
            {
                self.isUrlExist = true
                if let link_titleObj = objectForEdit?.object(forKey: "link_title") as? String
                {
                    titleToSend = link_titleObj
                }
                if let link_descriptionObj = objectForEdit?.object(forKey: "link_description") as? String
                {
                    descriptionToSend = link_descriptionObj
                }
                if let link_urlObj = objectForEdit?.object(forKey: "link_url") as? String
                {
                    urlToSend = link_urlObj
                }
                if let link_image_urlObj = objectForEdit?.object(forKey: "link_image_url") as? String
                {
                    isImageExist = true
                    imageToSend = link_image_urlObj
                }
                self.tableView.reloadData()
            }
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK:- click actions
    
    @IBAction func click_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func click_post(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        if self.imagesArray.count == 0 && caption.isEmpty == true && self.urlToSend.isEmpty == true{
            print("no post")
        }
        else{
            var postClass = PFObject(className: "Post")
            //from edit
            if fromEdit
            {
                let queryComp = PFQuery(className: "Post")
                queryComp.whereKey("objectId", equalTo:postForEdit)
                queryComp.findObjectsInBackground(block: { (objects, error) in
                    if error == nil {
                        print(objects?.count as Any)
                        
                        for object in objects! {
                            postClass = object
                            // if self.caption.lowercased().contains("www") == true{
                            self.post(postClass: postClass)
                            self.saveHashTags()
                            // }
                        }
                    }
                    else{
                        let message = (error?.localizedDescription)
                        print(message as Any)
                    }
                })
            }else
            {
                postClass = PFObject(className: "Post")
                self.post(postClass: postClass)
                self.saveHashTags()
            }
        }
    }
    
    @objc func clickCross(sender:UIButton){
        self.imagesArray.remove(at: sender.tag)
        if  self.assetsObj != nil{
            if self.assetsObj!.count > sender.tag{
                self.assetsObj!.remove(at: sender.tag)
            }
        }
        self.tableView.reloadData()
    }
    
    @IBAction func click_selectCamera(_ sender: Any) {
        let alert = UIAlertController(title: "Select either", message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Photos", style: .default , handler:{ (UIAlertAction)in
            self.showGallery(type:"photos")
        }))
        
        alert.addAction(UIAlertAction(title: "Videos", style: .default , handler:{ (UIAlertAction)in
            self.showGallery(type:"videos")
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func click_openTaggedUsers(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagUserViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    //MARK:- helper methods
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSAttributedString.Key.font: font],
                                                 context: nil).size
    }
    
    func setTableIfEdit(){
        if fromEdit
        {
            if let descriptionObj = objectForEdit?.object(forKey: "description") as? String
            {
                caption = descriptionObj
            }
            if let post_type = objectForEdit?.object(forKey: "post_type") as? String
            {
                if post_type == "ALBUM" || post_type == "IMAGE" || post_type == "VIDEO"
                {
                    
                    if let postmedia = objectForEdit?.object(forKey: "postmedia") as? NSArray
                    {
                        for i in 0..<postmedia.count
                        {
                            if let fileObj = postmedia.object(at: i) as? PFFile
                            {
                                if let media_typesArr = objectForEdit?.object(forKey: "media_types") as? NSArray
                                {
                                    if let arr = media_typesArr as? NSMutableArray{
                                        globalMediaType = arr
                                    }
                                    if media_typesArr.count > i{
                                        if let media_types = media_typesArr.object(at: i) as? String
                                        {
                                            if media_types == "VIDEO"
                                            {
                                                videoFile.add(fileObj)
                                                
                                                if let thumbnail = objectForEdit?.object(forKey: "thumbnail") as? NSArray
                                                {
                                                    if let fileObj = thumbnail.object(at: i) as? PFFile
                                                    {
                                                        do {
                                                            let image = try UIImage(data:fileObj.getData())
                                                            let imageObj = (image ?? #imageLiteral(resourceName: "ImageFailed"),self.fromEdit,"","video")
                                                            self.imagesArray.append(imageObj)
                                                        }
                                                        catch let error as NSError
                                                        {
                                                            print("Image generation failed with error \(error)")
                                                        }
                                                    }
                                                }else
                                                {
                                                    let image = UIImage.init(named: "black.png")
                                                    let imageObj = (image ?? #imageLiteral(resourceName: "ImageFailed"),self.fromEdit,"","video")
                                                    self.imagesArray.append(imageObj)
                                                }
                                                self.tableView.reloadData()
                                                
                                            }else
                                            {
                                                do {
                                                    let image = try UIImage(data:fileObj.getData())
                                                    let imageObj = (image ?? #imageLiteral(resourceName: "ImageFailed"),self.fromEdit,"","image")
                                                    self.imagesArray.append(imageObj)
                                                }
                                                    
                                                catch let error as NSError
                                                {
                                                    print("Image generation failed with error \(error)")
                                                }
                                            }
                                            
                                            if let media_description = self.objectForEdit?.object(forKey: "media_description") as? [String]
                                            {
                                                var imageObj = self.imagesArray[i]
                                                imageObj.2 = media_description[i] ?? ""
                                                self.imagesArray[i] = imageObj
                                            }
                                            self.tableView.reloadData()
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else if post_type == "URL"
                {
                    
                    self.isUrlExist = true
                    if let link_titleObj = objectForEdit?.object(forKey: "link_title") as? String
                    {
                        titleToSend = link_titleObj
                    }
                    if let link_descriptionObj = objectForEdit?.object(forKey: "link_description") as? String
                    {
                        descriptionToSend = link_descriptionObj
                    }
                    if let link_urlObj = objectForEdit?.object(forKey: "link_url") as? String
                    {
                        urlToSend = link_urlObj
                    }
                    if let link_image_urlObj = objectForEdit?.object(forKey: "link_image_url") as? String
                    {
                        isImageExist = true
                        imageToSend = link_image_urlObj
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func showGallery(type:String)
    {
        let pickerController = DKImagePickerController()
        if fromEdit == true{
            var mediaType = ""
            if globalMediaType.count > 0{
                if let media_types = globalMediaType.object(at: 0) as? String
                {
                    mediaType = media_types
                }
            }
            if type == "photos"{
                pickerController.assetType = .allPhotos
                if mediaType == "VIDEO"
                {
                    self.showMessage("You can't select photos and videos together")
                    return
//                    pickerController.maxSelectableCount = 0
//                    pickerController.assetType = .allVideos
                }else if mediaType == "TEXT"{
                    pickerController.maxSelectableCount = 10
                }
                else{
                    pickerController.maxSelectableCount = 10 - globalMediaType.count
                }
                
                pickerController.allowMultipleTypes = false
            }
            else{
                pickerController
                if mediaType == "TEXT"
                {
                    pickerController.maxSelectableCount = 1
                }else if mediaType == "IMAGE"
                {
                    pickerController.maxSelectableCount = 0
                    self.showMessage("You can't select photos and videos together")
                    return
                }else{
                    pickerController.maxSelectableCount = 0
                    self.showMessage("You can't upload more than one video")
                    return
                   //
                }
                pickerController.assetType = .allVideos
                pickerController.allowMultipleTypes = false
            }
        }else{
            self.imagesArray.removeAll()
            self.videoFile.removeAllObjects()
            if type == "photos"{
                pickerController.maxSelectableCount = 10
                pickerController.assetType = .allPhotos
                pickerController.allowMultipleTypes = false
            }
            else{
                pickerController.maxSelectableCount = 1
                pickerController.assetType = .allVideos
                pickerController.allowMultipleTypes = false
            }
        }
        
        
        if let assetsObj = self.assetsObj {
            pickerController.select(assets: assetsObj)
        }
        
        
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            self.assetsObj = assets
            
            for i in 0..<assets.count
            {
                
                let asset = self.assetsObj![i]
                
                asset.fetchOriginalImage(options: nil, completeBlock: { (image, info) in
                    if asset.type == .video {
                        print("===",i)
                        if asset.duration > Double(60){
                            self.showMessage("Oops! Very large video. Video cannot exceed 1 minute duration.")
                            if self.imagesArray.count > i{
                                self.imagesArray.remove(at: i)
                            }
                            if let assetsValue = self.assetsObj{
                                if self.assetsObj!.count > i{
                                    self.assetsObj!.remove(at: i)
                                }
                            }
                            self.tableView.reloadData()
                            return
                        }else{
                            let imgObj : (UIImage,Bool,String,String) = (image!,false,"","video")
                            self.imagesArray.append(imgObj)
                            self.tableView.reloadData()
                            self.tableView.scrollToRow(at: IndexPath.init(row: self.imagesArray.count - 1, section: 0), at: .bottom, animated: false)
                        }
                    }else
                    {
                        let imgObj : (UIImage,Bool,String,String) = (image!,false,"","image")
                        self.imagesArray.append(imgObj)
                        self.tableView.reloadData()
                        self.tableView.scrollToRow(at: IndexPath.init(row: self.imagesArray.count - 1, section: 0), at: .bottom, animated: false)
                    }
                })
            }
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            pickerController.modalPresentationStyle = .formSheet
        }
        self.present(pickerController, animated: true) {}
    }
    
    //MARK: - hash tag methods
    
    func saveHashTags()
    {
        let str : NSMutableString =   NSMutableString(string: caption)
        var strArray = str.components(separatedBy: " ")
        
        for i in 0..<strArray.count
        {
            let word = strArray[i]
            if word.contains("#")
            {
                hashTagsFromTextview.append(word)
            }
        }
        hashTagsFromTextview = Array(Set(hashTagsFromTextview))
        
        NewAndOldHashTagIdsArray = NSMutableArray()
        for i in 0..<hashTagsFromTextview.count
        {
            let word = hashTagsFromTextview[i]
            self.hashTagExist(name: word)
        }
    }
    
    func hashTagExist(name:String) {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "Hashtags")
        query.whereKey("name", equalTo: name)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print(objects!)
                
                if objects!.count>0
                {
                    self.existHashTag = true
                    let objectId = objects?[0].value(forKey: "objectId")
                    self.NewAndOldHashTagIdsArray.add(objectId!)
                    self.compareHashTags()
                    
                }else
                {
                    self.existHashTag = false
                    let hashtagsClass = PFObject(className: "Hashtags")
                    hashtagsClass["name"] = name
                    hashtagsClass.saveInBackground { (success, error) in
                        if(success)
                        {
                            print("hashtag saved")
                            self.NewAndOldHashTagIdsArray.add(hashtagsClass.objectId!)
                            self.compareHashTags()
                        }
                        else{
                            print("error",error!)
                            return
                        }
                    }
                }
            }else {
                print(error!)
                self.existHashTag = false
            }
        })
    }
    
    func compareHashTags()
    {
        if hashTagsFromTextview.count == self.NewAndOldHashTagIdsArray.count
        {
            if fromEdit == false{
                if !(self.postId == "")
                {
                    if self.NewAndOldHashTagIdsArray.count > 0{
                        self.saveHashTagPost(postId: self.postId, postPFObject: self.postIdPFObject!, hashTagIds: self.NewAndOldHashTagIdsArray)
                    }
                    
                }
            }else{
                if !(self.postForEdit == "")
                {
                    if self.NewAndOldHashTagIdsArray.count > 0{
                        self.saveHashTagPost(postId: self.postForEdit, postPFObject: self.objectForEdit!, hashTagIds: self.NewAndOldHashTagIdsArray)
                    }
                    
                }
            }
           
        }
    }
    
    func saveHashTagPost(postId:String, postPFObject:PFObject ,hashTagIds:NSMutableArray)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        for i in 0..<hashTagIds.count
        {
            let hashTagPostsClass = PFObject(className: "Hashtag_posts")
            hashTagPostsClass["post_id"] = postId
            hashTagPostsClass["parent_post"] = postPFObject
            hashTagPostsClass["post_user"] = PFUser.current()
           // print(hashTagIds.object(at: i))
            hashTagPostsClass["hashtag_id"] = hashTagIds.object(at: i) as! String
            hashTagPostsClass.saveInBackground { (success, error) in
                if(success)
                {
                    print("hashtag post saved")
                }
                else{
                    print("error",error!)
                    return
                }
            }
        }
    }
    
    //MARK:- save post methods
    
    func post(postClass:PFObject)
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        let objId = PFUser.current()?.objectId
        
        let trimmeddescription = caption.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if fromEdit == false {
            postClass["time_stamp"] = formatter.string(from: Date())
        }
        postClass["like_count"] = "0"
        postClass["comment_count"] = "0"
        postClass["description"] = trimmeddescription
        postClass["user_id"] = objId
        //postClass["post_privacy"] = selectedType
        
        postClass["name"] = PFUser.current()?.value(forKey: "name") as? String
        postClass["user_name"] = PFUser.current()?.value(forKey: "user_name") as! String
        
        postClass["parent_id"] = PFUser.current()
        
        if let completeArray = UserDefaults.standard.object(forKey: "taggedUser") as? NSArray{
            if completeArray.count>0
            {
                postClass["users_tagged_with"] = completeArray
            }else
            {
                let tempArr : NSArray = []
                postClass["users_tagged_with"] = tempArr
            }
        }
        
        if savedArrayUser.count>0
        {
            postClass["tagged_users_in_desc"] = savedArrayUser
        }
        if caption.isEmpty == false{
            postClass["description"] = caption.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        if(imagesArray.count>0){
            if fromEdit == true{
                if self.imagesArray.count > 1{
                    //for album
                    postClass["post_type"] = "ALBUM"
                }else{
                    if let assets = self.assetsObj{
                        if assets != nil{
                            //for single video or image
                            let asset = assets[0]
                            if asset.type == .video{
                                postClass["post_type"] = "VIDEO"
                            }else{
                                postClass["post_type"] = "IMAGE"
                            }
                        }else{
                            //for single video or image
                            let images = self.imagesArray[0]
                            if images.3 == "video"{
                                postClass["post_type"] = "VIDEO"
                            }else{
                                postClass["post_type"] = "IMAGE"
                            }
                        }
                    }
                }
            }else{
                if let assets = self.assetsObj{
                    if assets.count > 0{
                        if assets.count > 1{
                            //for album
                            postClass["post_type"] = "ALBUM"
                        }
                        else{
                            //for single video or image
                            let asset = assets[0]
                            if asset.type == .video{
                                postClass["post_type"] = "VIDEO"
                            }else{
                                postClass["post_type"] = "IMAGE"
                            }
                        }
                    }
                }
            }
            
            var media_typeArray = [String]()
            var media_descriptionArray = [String]()
            var postmediaArray = [PFFile]()
            
            var thumbnailArray = [PFFile]()
            var j = 0
            if fromEdit == true{
                // for edit
                for i in 0..<imagesArray.count{
                    media_descriptionArray.append(self.imagesArray[i].2)
                    if globalMediaType.count > i{
                        if let media_types = globalMediaType.object(at: i) as? String
                        {
                            let image = self.imagesArray[i].0
                            let imageData = image.mediumQualityJPEGNSData
                            let thumbnail = PFFile(name: "image.png", data: imageData as Data)
                            thumbnailArray.append(thumbnail!)
                            
                            if media_types == "VIDEO"
                            {
                                media_typeArray.append("VIDEO")
                                postmediaArray.append(videoFile.object(at: i) as! PFFile)
                            }else
                            {
                                media_typeArray.append("IMAGE")
                                let image = self.imagesArray[i].0
                                let imageData = image.mediumQualityJPEGNSData
                                let imageFile = PFFile(name: "image.png", data: imageData as Data)
                                postmediaArray.append(imageFile!)
                            }
                        }
                        if (self.imagesArray.count)==postmediaArray.count
                        {
                            postClass["media_description"] = media_descriptionArray
                            postClass["media_types"] = media_typeArray
                            postClass["postmedia"] = postmediaArray
                            postClass["thumbnail"] = thumbnailArray
                            self.savePostInBG(postClassObj: postClass)
                        }
                    }else{
                        if self.assetsObj != nil
                        {
                            if self.assetsObj != nil{
                                
                                if self.assetsObj!.count > j{
                                    let asset = self.assetsObj![j]
                                    j = j+1
                                    if asset.type == .video
                                    {
                                        asset.fetchAVAsset { (avAsset, info) in
                                            
                                            print(avAsset!)
                                            let asset = avAsset! as? AVURLAsset
                                            let data = NSData(contentsOf: (asset?.url)!)
                                            if let duration : CMTime = asset?.duration{
                                                let videoSecs = Int(CMTimeGetSeconds(duration))
                                                if videoSecs <= 60{
                                                    postClass["duration"] = "\(videoSecs)"
                                                }
                                            }
                                            
                                            let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
                                            self.compressVideo(inputURL: (asset?.url)!, outputURL: compressedURL, handler: { (session) in
                                                print(session!)
                                                let data = NSData(contentsOf: compressedURL)
                                                print("compressed size = ",Double(data!.length/1048576))
                                                let imageFile = PFFile(name: "image.mp4", data: data! as Data)
                                                
                                                
                                                postmediaArray.append(imageFile!)
                                                media_typeArray.append("VIDEO")
                                                
                                                let image = self.imagesArray[i].0
                                                let imageData = image.mediumQualityJPEGNSData
                                                let thumbnail = PFFile(name: "image.png", data: imageData as Data)
                                                thumbnailArray.append(thumbnail!)
                                                if (self.imagesArray.count)==postmediaArray.count
                                                {
                                                    postClass["media_description"] = media_descriptionArray
                                                    postClass["media_types"] = media_typeArray
                                                    postClass["postmedia"] = postmediaArray
                                                    postClass["thumbnail"] = thumbnailArray
                                                    self.savePostInBG(postClassObj: postClass)
                                                }
                                            })
                                        }
                                    }else
                                    {
                                        media_typeArray.append("IMAGE")
                                        let image = self.imagesArray[i].0
                                        let imageData = image.mediumQualityJPEGNSData
                                        let imageFile = PFFile(name: "image.png", data: imageData as Data)
                                        postmediaArray.append(imageFile!)
                                        thumbnailArray.append(imageFile!)
                                        if (self.imagesArray.count)==postmediaArray.count
                                        {
                                            postClass["media_description"] = media_descriptionArray
                                            postClass["media_types"] = media_typeArray
                                            postClass["postmedia"] = postmediaArray
                                            postClass["thumbnail"] = thumbnailArray
                                            self.savePostInBG(postClassObj: postClass)
                                        }
                                    }
                                }
                            }
                        }
                        
                    }
                    
                    
                }
            }else{
                for i in 0..<(imagesArray.count)
                {
                    media_descriptionArray.append(self.imagesArray[i].2)
                    if self.assetsObj != nil
                    {
                        let asset = self.assetsObj![i]
                        if asset.type == .video
                        {
                            asset.fetchAVAsset { (avAsset, info) in
                                let asset = avAsset! as? AVURLAsset
                                let data = NSData(contentsOf: (asset?.url)!)
                                print("original size = ",Double(data!.length/1048576))
                                let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
                                self.compressVideo(inputURL: (asset?.url)!, outputURL: compressedURL, handler: { (session) in
                                    print(session!)
                                    let data = NSData(contentsOf: compressedURL)
                                    print("compressed size = ",Double(data!.length/1048576))
                                    let imageFile = PFFile(name: "image.mp4", data: data! as Data)
                                    postmediaArray.append(imageFile!)
                                    media_typeArray.append("VIDEO")
                                    // for getting duration
                                    if let duration : CMTime = asset?.duration{
                                        let videoSecs = Int(CMTimeGetSeconds(duration))
                                        if videoSecs <= 60{
                                            postClass["duration"] = "\(videoSecs)"
                                        }
                                    }
                                    
                                    let image = self.imagesArray[i].0
                                    let imageData = image.mediumQualityJPEGNSData
                                    let thumbnail = PFFile(name: "image.png", data: imageData as Data)
                                    thumbnailArray.append(thumbnail!)
                                    
                                    if (self.imagesArray.count)==postmediaArray.count
                                    {
                                        postClass["media_description"] = media_descriptionArray
                                        postClass["media_types"] = media_typeArray
                                        postClass["postmedia"] = postmediaArray
                                        postClass["thumbnail"] = thumbnailArray
                                        
                                        self.savePostInBG(postClassObj: postClass)
                                    }
                                })
                            }
                        }else
                        {
                            media_typeArray.append("IMAGE")
                            let image = self.imagesArray[i].0
                            let imageData = image.mediumQualityJPEGNSData
                            let imageFile = PFFile(name: "image.png", data: imageData as Data)
                            postmediaArray.append(imageFile!)
                            thumbnailArray.append(imageFile!)
                            
                            if (self.imagesArray.count)==postmediaArray.count
                            {
                                postClass["media_description"] = media_descriptionArray
                                postClass["media_types"] = media_typeArray
                                postClass["postmedia"] = postmediaArray
                                postClass["thumbnail"] = thumbnailArray
                                self.savePostInBG(postClassObj: postClass)
                            }
                        }
                    }
                }
            }
            
        }
        else
        {
            if self.urlToSend.isEmpty == false && titleToSend.isEmpty == false{
                postClass["link_title"] = titleToSend
                postClass["link_description"] = descriptionToSend
                postClass["link_url"] = urlToSend
                postClass["link_image_url"] = imageToSend
                postClass["post_type"] = "URL"
                self.savePostInBG(postClassObj: postClass)
            }else{
                postClass["post_type"] = "TEXT"
                if caption.isEmpty == false{
                    postClass["description"] = caption.trimmingCharacters(in: .whitespacesAndNewlines)
                    self.savePostInBG(postClassObj: postClass)
                }
            }
        }
        DispatchQueue.main.async {
            globalBool.showProgess = true
            
        }
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    func uploadPhotoOrvideo(asset:DKAsset,i:Int,media_descriptionArray:[String],postmediaArray:[PFFile],media_typeArray:[String],thumbnailArray:[PFFile],postClass:Post){
        
    }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    func savePostInBG(postClassObj:PFObject)
    {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        postClassObj.saveInBackground { (success, error) in
            if(success)
            {
                print("Post saved")
                
                if let completeArray = UserDefaults.standard.object(forKey: "taggedUser") as? NSArray{
                    if completeArray.count>0
                    {
                        let userIdArray = completeArray.value(forKey: "id") as! NSArray
                        
                        var count : Int = 0
                        for userId in userIdArray
                        {
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                            
                            let nameQuery : PFQuery = PFUser.query()!
                            nameQuery.whereKey("objectId", equalTo: userId)
                            nameQuery.findObjectsInBackground(block: { (objects, error) in
                                print(objects!)
                                count = count+1
                                let obj = objects![0] as? PFUser
                                let objId = PFUser.current()?.objectId
                                self.saveToNotifications(from_id: objId!, to_id: userId as! String, notification_type: "user_tag", time: formatter.string(from: Date()), post_id: self.postId, parent_from: PFUser.current()!, parent_to: obj!,commentStr: "")
                            })
                        }
                        
                        let currentName = PFUser.current()?.value(forKey: "name") as? String ?? ""
                        self.taggedUserPushNotification(userIdArray: userIdArray, postId: postClassObj.objectId!, pushType: "user_tag", message: "\(currentName) \(messages.userTagged)" )
                    }
                }
                
                
                if self.savedArrayUser.count>0
                {
                    let userIdArray = self.savedArrayUser.value(forKey: "id") as! NSArray
                    
                    var count : Int = 0
                    for userId in userIdArray
                    {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                        
                        let nameQuery : PFQuery = PFUser.query()!
                        nameQuery.whereKey("objectId", equalTo: userId)
                        nameQuery.findObjectsInBackground(block: { (objects, error) in
                            print(objects!)
                            count = count+1
                            let obj = objects![0] as? PFUser
                            let objId = PFUser.current()?.objectId
                            self.saveToNotifications(from_id: objId!, to_id: userId as! String, notification_type: "post_tag", time: formatter.string(from: Date()), post_id: self.postId, parent_from: PFUser.current()!, parent_to: obj!,commentStr: "")
                        })
                    }
                    
                    let currentName = PFUser.current()?.value(forKey: "name") as? String ?? ""
                    self.taggedUserPushNotification(userIdArray: userIdArray, postId: postClassObj.objectId!, pushType: "post_tag", message: "\(currentName) \(messages.userTagged)")
                    
                }
                
                let arr : NSMutableArray = [self.indexEdit,postClassObj.objectId!,self.fromEdit]
                
                if self.fromMyProfile
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postSuccessMyProfile"), object: arr)
                }else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postSuccess"), object: arr)
                }
                self.postId = postClassObj.objectId!
                self.postIdPFObject = postClassObj
                self.compareHashTags()
            }
            else{
                print("error",error!)
                self.showMessage((error?.localizedDescription)!)
                return
            }
        }
    }
    
    func taggedUserPushNotification(userIdArray:NSArray,postId:String,pushType:String,message:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        var count = 0
        let arrayOfUsers = NSMutableArray()
        
        for (_, element) in userIdArray.enumerated() {
            
            let childPath = "chatPush/\(element)/withUser"
            
            FIRDatabase.database().reference().child(childPath).observeSingleEvent(of: .value, with: { (snapshot) in
                
                print("...., \(element)")
                
                if let pushEnabled = snapshot.value as? Bool {
                    if pushEnabled {
                        arrayOfUsers.add(element)
                    }
                } else {
                    arrayOfUsers.add(element)
                }
                
                count += 1
                
                if count == userIdArray.count {
                    self.sendPushNotification(postId: postId, userId: "",settingType: "push_post_tag",message:message,type:pushType,userIdArray:arrayOfUsers,groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                }
            })
        }
    }
    
    
    //MARK:- reterive metadata from url
    
    func getViewUrl(varibale:URL) {
        showLoader = true
        let slp = SwiftLinkPreview(session:URLSession.shared,
                                   workQueue:  SwiftLinkPreview.defaultWorkQueue,
                                   responseQueue: DispatchQueue.main,
                                   cache: DisabledCache.instance)
        self.isImageExist = false
        self.titleToSend = ""
        self.descriptionToSend = ""
        self.urlToSend = ""
        if varibale.absoluteString.contains("www.youtube.com/watch?v=") == true{
            if let cell = self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? URLTableViewCell{
                cell.imgVw_youtube.isHidden = false
                cell.htconstrnt_youtubeImage.constant = 95
            }
        }else{
            if let cell = self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? URLTableViewCell{
                cell.imgVw_youtube.isHidden = true
                cell.htconstrnt_youtubeImage.constant = 0
            }
        }
        slp.previewLink(varibale.absoluteString, onSuccess: { (dic) in
            self.showLoader = false
            self.titleToSend = varibale.absoluteString
            self.descriptionToSend = varibale.absoluteString
            self.urlToSend = varibale.absoluteString
            
            if let title = dic["title"] as? String
            {
                self.titleToSend = title
            }
            
            if let description = dic["description"] as? String
            {
                self.descriptionToSend = description
            }
            if let url = dic["url"] as? String
            {
                self.urlToSend = url
            }
            if let imageUrl =  dic["image"] as? String
            {
                self.isImageExist = true
                self.imageToSend = imageUrl
            }else
            {
                self.isImageExist = false
            }
            let indexPath = NSIndexPath.init(row: 1, section: 0)
            self.tableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
            
        }) { (error) in
            self.showLoader = false
            self.isImageExist = false
            self.titleToSend = varibale.absoluteString
            self.descriptionToSend = varibale.absoluteString
            self.urlToSend = varibale.absoluteString
            let indexPath = NSIndexPath.init(row: 1, section: 0)
            self.tableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
        }
        
    }
    
}

//table view delegate and data source

extension CreatePost_VC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.imagesArray.count > 0{
            return self.imagesArray.count + 1
            
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: "PostDecriptionCell", for: indexPath) as? PostDecriptionCell
            {
                cell.postTextView.delegate = self
                
                cell.postTextView.tag = 1001
                cell.postTextView.isScrollEnabled = false
                if fromEdit == false{
                     cell.postTextView.text = caption
                }
                else{
                    if self.urlToSend.isEmpty == true{
                        cell.postTextView.text = caption
                    }else{
                         cell.postTextView.text = self.urlToSend
                    }
                }
                return cell
            }
            return UITableViewCell()
        }else if indexPath.row == 1{
            if self.urlToSend.isEmpty == false{
                if let cell = tableView.dequeueReusableCell(withIdentifier: "URLTableViewCell", for: indexPath) as? URLTableViewCell
                {
                    if showLoader
                    {
                        cell.indicatorObj.startAnimating()
                    }else{
                        cell.indicatorObj.stopAnimating()
                    }
                    cell.selectionStyle = .none
                    
                    cell.urlTitle.text = self.titleToSend
                    cell.urlDescription.text = self.descriptionToSend
                    cell.urlAgain.text = self.urlToSend
                    cell.img_youtube.isHidden = true
                    let url = URL(string: self.imageToSend)
                    if self.urlToSend.contains("www.youtube.com/watch?v=") == true{
                        if let urlstr = url{
                            cell.imgVw_youtube.af_setImage(withURL: urlstr)
                        }
                        cell.img_youtube.isHidden = false
                        cell.imgVw_youtube.isHidden = false
                        cell.htconstrnt_youtubeImage.constant = 120
                        
                    }else{
                        cell.imgVw_youtube.image = nil
                        cell.imgVw_youtube.isHidden = true
                        cell.htconstrnt_youtubeImage.constant = 0
                    }
                    
                    if !(url==nil)
                    {
                        cell.urlImage.af_setImage(withURL: url!)
                    }
//                    else
//                    {
//                        cell.urlImage.image = image
//                    }
                    
//                    if isImageExist == true
//                    {
                        cell.widthOfUrlImage.constant = 50
//                    }else
//                    {
//                        cell.widthOfUrlImage.constant = 0
//                    }
                    cell.delegate = self
                    if isUrlExist &&  imagesArray.count==0
                    {
                        cell.outterView.isHidden = false
                    }else
                    {
                        cell.outterView.isHidden = true
                    }
                    return cell
                }
            }else{
                // if images selected
                if indexPath.row <= self.imagesArray.count{
                    if let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as? ImageCell
                    {
                        let imageObj = imagesArray[indexPath.row - 1]
                        cell.crossButton.isHidden = imageObj.1
                        cell.setPostedImage(image: imageObj.0)
                        if imageObj.3 == "video" {
                            cell.playImage.isHidden = false
                        }else{
                            cell.playImage.isHidden = true
                        }
                        cell.imageObj.image = imageObj.0
                        cell.crossButton.tag = indexPath.row - 1
                        cell.captionTextView.text = imageObj.2
                        cell.captionTextView.tag = indexPath.row - 1
                        cell.captionTextView.delegate = self
                        cell.crossButton.addTarget(self, action: #selector(clickCross(sender:)), for: .touchUpInside)
                        return cell
                    }
                    return UITableViewCell()
                }
            }
        }
        else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath) as? ImageCell
            {
                let imageObj = imagesArray[indexPath.row - 1]
                cell.crossButton.isHidden = imageObj.1
                cell.setPostedImage(image: imageObj.0)
                cell.imageObj.image = imageObj.0
                if imageObj.3 == "video" {
                    cell.playImage.isHidden = false
                }else{
                    cell.playImage.isHidden = true
                }
                cell.crossButton.tag = indexPath.row - 1
                cell.captionTextView.text = imageObj.2
                cell.captionTextView.tag = indexPath.row - 1
                cell.captionTextView.delegate = self
                cell.crossButton.addTarget(self, action: #selector(clickCross(sender:)), for: .touchUpInside)
                return cell
            }
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension CreatePost_VC : UITextViewDelegate{
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        if textView.tag == 1001{
            if textView.text.contains("www"){
                caption = ""
            }
            else{
                caption = textView.text
                self.urlToSend = ""
                self.titleToSend = ""
            }
        }else{
            
            if self.imagesArray.count > textView.tag{
                var obj = self.imagesArray[textView.tag]
                obj.2 = textView.text
                self.imagesArray[textView.tag] = obj
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if textView.tag == 1001{
            //   globalRange = range
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            var textWidth = textView.frame.inset(by: textView.textContainerInset).width
            textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
            let boundingRect = sizeOfString(string: newText, constrainedToWidth: Double(textWidth), font: textView.font!)
            let numberOfLines = boundingRect.height / textView.font!.lineHeight;
            // print("text", text)
            ///////////////////////////////////////////////////
            
            if text == "" || text == " " || text == "\n" {
                let types: NSTextCheckingResult.CheckingType = .link
                let detector = try? NSDataDetector(types: types.rawValue)
                let matches = detector!.matches(in: textView.text, options: .reportCompletion, range: NSMakeRange(0,textView.text.characters.count))
                if matches.count > 0 {
                    let urlResult = matches.first
                    let url = urlResult?.url
                    if verifyUrl(urlString: textView.text) == true{
                        if self.imagesArray.count==0 && !isUrlExist
                        {
                            self.isUrlExist = true
                            if let cell = self.tableView.cellForRow(at: IndexPath.init(row: 1, section: 0)) as? URLTableViewCell{
                                cell.indicatorObj.startAnimating()
                            }
                            getViewUrl(varibale: url!)
                        }
                    }else{
                        self.showMessage("Oops! Invalid URL")
                    }
                    
                }else
                {
                    self.isUrlExist = false
                }
            }
            ////////////////////////////////////////////////////
            
            let isBackSpace = strcmp(text, "\\b")
            if (isBackSpace == -92) {
                globalInt = 0
                removeCount = removeCount-1
            }else
            {
                globalInt = 1
                removeCount = removeCount+1
            }
            let indexPath = IndexPath.init(row: 0, section: 0)
            if let cell = tableView.cellForRow(at: indexPath) as? PostDecriptionCell{
                cell.postTextView.frame.size.height = cell.postTextView.contentSize.height
                cell.postTextView.layoutIfNeeded()
                cell.layoutIfNeeded()
            }
        }
        else{
            if self.imagesArray.count > textView.tag{
                var obj = self.imagesArray[textView.tag]
                obj.2 = textView.text
                self.imagesArray[textView.tag] = obj
            }
        }
        return true
    }
}

//url delegate
extension CreatePost_VC : crossUrlDelegate{
    
    func crossUrlClicked() {
        self.urlToSend = ""
        self.titleToSend = ""
        self.isUrlExist = false
        self.descriptionToSend = ""
        self.tableView.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .automatic)
    }
}


