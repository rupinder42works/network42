//
//  BaseViewController.swift
//  Network42
//
//  Created by 42works on 09/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import TTGSnackbar
import BRYXBanner
import Parse
import AVKit
import AVFoundation
import Photos

protocol checkCameraPermissionDelegate{
   func handleCameraAction()
}

protocol checkGalleryPermissionDelegate{
    func handleGalleryAction()
}

class BaseViewController: UIViewController {
    
    @IBOutlet weak var activityIndicatorBK: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var imageGIF: UIImageView!
    var delegateCamera : checkCameraPermissionDelegate?
    var delegateGallery : checkGalleryPermissionDelegate?
    
    @IBOutlet weak var activityIndicatorForScreen: UIActivityIndicatorView!
    
    let durationTypes: [TTGSnackbarDuration] = [.short, .middle, .long]
    let animationTypes: [TTGSnackbarAnimationType] = [.slideFromBottomBackToBottom, .fadeInFadeOut, .slideFromLeftToRight, .slideFromTopBackToTop]
    
    var snackbar: TTGSnackbar = TTGSnackbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.logController()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RedirectToNavigation"), object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(BaseViewController.redirect(notification:)), name:NSNotification.Name(rawValue: "RedirectToNavigation"), object: nil)
    }
    
    @objc func redirect(notification: NSNotification)
    {
        print(notification.object!)
        if Constants.notificationFire == 1
        {
            Constants.notificationFire = 0
            var msgStr : String = ""
            var postTypeStr : String = ""
            var userName : String = ""
            var postId : String = ""
            var senderId : String = ""
            var groupId : String = ""
            var state : String = ""
            var groupNameStr : String = ""
            if let aps = (notification.object! as AnyObject).value(forKey: "aps") as? NSDictionary
            {
                //print(aps)
                if let alert = aps.value(forKey: "alert") as? NSDictionary {
                    if let message = alert["body"] as? NSString {
                        print(message)
                        msgStr = message as String
                        if let postType = alert["pushType"] as? String {
                            print(postType)
                            postTypeStr = postType as String
                        }
                        
                        if let name = alert["userName"] as? String {
                            print(name)
                            userName = name as String
                        }
                        
                        if let groupName = alert["groupName"] as? String {
                            print(groupName)
                            groupNameStr = groupName as String
                        }
                        
                        if let postType = alert["postId"] as? String {
                            postId = postType as String
                        }
                        
                        if let userIdObj = alert["userId"] as? String {
                            senderId = userIdObj as String
                        }
                        
                        
                        if let groupIdObj = alert["groupId"] as? String {
                            groupId = groupIdObj as String
                        }
                    }
                }
            }
            
            if let stateObj = (notification.object! as AnyObject).value(forKey: "state") as? String {
                state = stateObj
            }
            
            if state == "foreground"
            {
                if postTypeStr == "follow_user"
                {
                    self.tabBarController?.tabBar.isHidden = false
                    _ = self.navigationController?.popToRootViewController(animated: true)
                    self.tabBarController?.selectedIndex = 2
                }
                else if postTypeStr == "follow_request_accept"  || postTypeStr == "follow_public_user"
                {
                    let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                    let vc = tabbarSB.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                    vc.fromSearch = true
                    vc.userProfile = false
                    vc.friend = "FOLLOWING"
                    let nameQuery : PFQuery = PFUser.query()!
                    nameQuery.whereKey("objectId", equalTo: senderId)
                    
                    do {
                        let objects =  try  nameQuery.findObjects()
                        print(objects)
                        for object in objects {
                            vc.userFromSearch = object as? PFUser
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                        self.tabBarController?.selectedIndex = 0
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                    } catch {
                        print(error)
                    }
                }
                else if postTypeStr == "post_like" || postTypeStr == "post_comment" || postTypeStr == "user_tag" || postTypeStr == "comment_tag" || postTypeStr == "post_tag"
                {
                    let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
                    let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                    vc.secondTime = true
                    vc.titleSecond = "network 42"
                    vc.showOnlySinglePost = true
                    vc.postIdObject = postId
                    self.navigationController?.pushViewController(vc, animated: true)
                    self.tabBarController?.selectedIndex = 0
                }else if postTypeStr == "chat_message"
                {
                    let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                    let vc = chatSB.instantiateViewController(withIdentifier: "SingleChatViewController")  as! SingleChatViewController
                    vc.senderIdToPass = senderId
                    vc.titleStr = userName
                    vc.pushUsrId = (PFUser.current()?.objectId)!
                    self.navigationController?.pushViewController(vc, animated: true)
                    self.tabBarController?.selectedIndex = 0
                }
                else if postTypeStr == "group_add" || postTypeStr == "group_chat_message"
                {
                    let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                    let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatViewController")  as! GroupchatViewController
                    globalgroupName.gName = groupNameStr
                    vc.senderIdToPass = groupId
                    vc.fromView = "ChatList"
                    self.navigationController?.pushViewController(vc, animated: true)
                    self.tabBarController?.selectedIndex = 0
                }
            } else if postTypeStr == "follow_user"
            {
                self.tabBarController?.tabBar.isHidden = false
                _ = self.navigationController?.popToRootViewController(animated: true)
                self.tabBarController?.selectedIndex = 2
                
            }  else if postTypeStr == "follow_request_accept" || postTypeStr == "follow_public_user" {
                let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                let vc = tabbarSB.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
                vc.fromSearch = true
                vc.userProfile = false
                vc.friend = "FOLLOWING"
                let nameQuery : PFQuery = PFUser.query()!
                nameQuery.whereKey("objectId", equalTo: senderId)
                
                do {
                    let objects =  try  nameQuery.findObjects()
                    print(objects)
                    for object in objects {
                        vc.userFromSearch = object as? PFUser
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                    self.tabBarController?.selectedIndex = 0
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                } catch {
                    print(error)
                }
            }
            else if postTypeStr == "post_like" || postTypeStr == "post_comment" || postTypeStr == "user_tag" || postTypeStr == "comment_tag" || postTypeStr == "post_tag"
            {
                let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
                let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                vc.secondTime = true
                vc.titleSecond = "network 42"
                vc.showOnlySinglePost = true
                vc.postIdObject = postId
                self.navigationController?.pushViewController(vc, animated: true)
                self.tabBarController?.selectedIndex = 0
            }else if postTypeStr == "chat_message"
            {
                let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                let vc = chatSB.instantiateViewController(withIdentifier: "SingleChatViewController")  as! SingleChatViewController
                vc.senderIdToPass = senderId
                vc.titleStr = userName
                vc.pushUsrId = (PFUser.current()?.objectId)!
                self.navigationController?.pushViewController(vc, animated: true)
                self.tabBarController?.selectedIndex = 0
            }
            else if postTypeStr == "group_add" || postTypeStr == "group_chat_message"
            {
                let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatViewController")  as! GroupchatViewController
                globalgroupName.gName = groupNameStr
                vc.senderIdToPass = groupId
                vc.fromView = "ChatList"
                self.navigationController?.pushViewController(vc, animated: true)
                self.tabBarController?.selectedIndex = 0
            }
        }
    }
    
    
    func saveToNotifications(from_id:String,to_id:String,notification_type:String,time:String,post_id:String,parent_from:PFUser,parent_to:PFUser,commentStr:String)
    {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        
        if !(to_id == from_id)
        {
            let compClass = PFObject(className: "Notifications")
            compClass["from_id"] =  from_id
            compClass["to_id"] =  to_id
            compClass["notification_type"] =  notification_type
            compClass["time"] =  time
            compClass["post_id"] =  post_id
            compClass["parent_from"] =  parent_from
            compClass["parent_to"] =  parent_to
            compClass["PostComment"] =  commentStr
            compClass["PostId"] =  PFObject(withoutDataWithClassName: "Post", objectId: post_id)
            compClass.saveInBackground(block: { (status, error) in
                
                if status ==  true{
                    print("status=",status)
                    
                }
                else{
                    let message = (error?.localizedDescription)
                    print(message as Any)
                }
            })
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValidEmail(_ testStr:String) -> Bool
    {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func alertView(_ alertMeassage:String,controller:UIViewController)
    {
        let alert = UIAlertController(title: "Message", message:alertMeassage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Constants.okTitle, style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.keyBoardDown()
    }
    
    func keyBoardDown()
    {
        self.view.endEditing(true)
    }
    
    func resolutionSizeForLocalVideo(url:NSURL) -> CGSize? {
        guard let track = AVAsset(url: url as URL).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
    
    
    
    func sendPushNotification(postId:String,userId:String,settingType:String,message:String,type:String,userIdArray:NSArray,groupMembersId:String,MsgType:String,reciverId:String,username:String,groupName:String)
    {
        
        if userId == ""
        {
            self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:groupMembersId,MsgType:"",reciverId:"",username:username,groupName: groupName)
        }else
        {
            let query = PFQuery(className: "Settings")
            query.whereKey("user_id", equalTo:userId)
            query.getFirstObjectInBackground { (object, error) in
            if error == nil {
                    print(object!)
                    switch settingType {
                    case "profile_visibility":
                        if let profile_visibility = object?.object(forKey: "profile_visibility") as? String
                        {
                            print(profile_visibility)
                            if profile_visibility == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                            
                        }
                    case "push_accept_follow_request":
                        if let push_accept_follow_request = object?.object(forKey: "push_accept_follow_request") as? String
                        {
                            print(push_accept_follow_request)
                            if push_accept_follow_request == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                        }
                    case "push_comment_tag":
                        if let push_comment_tag = object?.object(forKey: "push_comment_tag") as? String
                        {
                            print(push_comment_tag)
                            if push_comment_tag == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                        }
                        
                        
                    case "push_message_send":
                        if let push_message_send = object?.object(forKey: "push_message_send") as? String
                        {
                            print(push_message_send)
                            if push_message_send == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                        }
                    case "push_post_comment":
                        if let push_post_comment = object?.object(forKey: "push_post_comment") as? String
                        {
                            print(push_post_comment)
                            if push_post_comment == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                        }
                    case "push_post_like":
                        if let push_post_like = object?.object(forKey: "push_post_like") as? String
                        {
                            print(push_post_like)
                            if push_post_like == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                        }
                        
                    case "push_post_share":
                        if let push_post_share = object?.object(forKey: "push_post_share") as? String
                        {
                            print(push_post_share)
                            if push_post_share == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                            
                        }
                    case "push_post_tag":
                        if let push_post_tag = object?.object(forKey: "push_post_tag") as? String
                        {
                            print(push_post_tag)
                            if push_post_tag == "1"
                            {
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                            
                        }
                    case "push_send_follow_request":
                        if let push_send_follow_request = object?.object(forKey: "push_send_follow_request") as? String
                        {
                            print(push_send_follow_request)
                            if push_send_follow_request == "1"
                            {
                                
                                self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                            }
                        }
                    default:
                        
                        self.sendPush(postId: postId, userId: userId,message:message,type:type,userIdArray:userIdArray,groupMembersId:"",MsgType:"",reciverId:"",username:username,groupName: groupName)
                        
                    }
                }
                else {
                    print(error!)
                }
            }
        }
    }
    
    
    func sendPush(postId:String,userId:String,message:String,type:String,userIdArray:NSArray,groupMembersId:String,MsgType:String,reciverId:String,username:String,groupName:String)  {
        
        let currentName = PFUser.current()?.value(forKey: "name") as! String
        let alertDictionary = ["body": message,
                               "groupId": groupMembersId,
                               "groupName": groupName,
                               "MsgType": MsgType,
                               "reciverId": reciverId,
                               "pushType": type,
                               "postId": postId,
                               "username":username,
                               "userId": "\(PFUser.current()!.objectId!)",
            "userName": currentName]
        let innerDictionary  = ["alert": alertDictionary,
                                "badge": "1",
                                "sound": "chime.aiff"
            ] as [String : Any]
        let mainDictionary = ["aps" : innerDictionary]
        print(mainDictionary)
        
        print(userIdArray)
        
        let pushQuery = PFInstallation.query()
        if userId == ""
        {
            pushQuery?.whereKey("userId", containedIn: userIdArray as! [Any])
        }else
        {
            pushQuery?.whereKey("userId", equalTo: userId)
        }
        
        pushQuery?.whereKey("isActive", equalTo: "1")
        
        let push = PFPush()
        push.setQuery(pushQuery as! PFQuery<PFInstallation>?)
        push.setData(mainDictionary)
        push.sendInBackground()
        
    }
 
    
    //MARK:- camera premissions
    
    func checkCameraAccess(vc:UIViewController) {
        if let vcDele = vc as? checkCameraPermissionDelegate{
            self.delegateCamera = vcDele
        }
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            print("Denied, request permission from settings")
            presentCameraSettings(str:"Camera access is denied")
        case .restricted:
            print("Restricted, device owner must approve")
        case .authorized:
            self.delegateCamera?.handleCameraAction()
            print("Authorized, proceed")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print("Permission granted, proceed")
                } else {
                    print("Permission denied")
                }
            }
        }
    }
    
    func checkGalleryAccess(vc:UIViewController) {
        if let vcDele = vc as? checkGalleryPermissionDelegate{
            self.delegateGallery = vcDele
        }
        PHPhotoLibrary.requestAuthorization { (status) in
            // No crash
            switch status{
            case .denied:
                print("Denied, request permission from settings")
                self.presentCameraSettings(str:"Camera access is denied")
            case .restricted:
                print("Restricted, device owner must approve")
            case .authorized:
                self.delegateGallery?.handleGalleryAction()
                print("Authorized, proceed")
            case .notDetermined:
                AVCaptureDevice.requestAccess(for: .video) { success in
                    if success {
                        print("Permission granted, proceed")
                    } else {
                        print("Permission denied")
                    }
                }
            }
        }
    }
    
    
    func presentCameraSettings(str:String) {
        let alertController = UIAlertController(title: "Error",
                                                message: str,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Cancel", style: .default))
        alertController.addAction(UIAlertAction(title: "Settings", style: .cancel) { _ in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        present(alertController, animated: true)
    }
    
    //MARK:- ActivityIndicator
    
    func showLoader(_ viewController: UIViewController?) {
        (appDelegate.window?.rootViewController as? UINavigationController)?.topViewController?.view.makeToastActivity(.center)
    }
    
    func hideLoader(_ viewController: UIViewController?) {
        (appDelegate.window?.rootViewController as? UINavigationController)?.topViewController?.view.hideToastActivity()
    }
    
    func startIndicator()  {
        if activityIndicator != nil {
            activityIndicator.backgroundColor = UIColor.red
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
            activityIndicatorBK.isHidden = false
            self.keyBoardDown()
        }
    }
    
    
    func startIndicatorForScreen()  {
        if activityIndicatorForScreen != nil {
            activityIndicatorForScreen.startAnimating()
            activityIndicatorForScreen.isHidden = false
            activityIndicatorBK.isHidden = false
            self.keyBoardDown()
        }
    }
    
    func stopIndicator()  {
        if activityIndicator != nil {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
        if activityIndicatorBK != nil {
            activityIndicatorBK.isHidden = true
        }
    }
    
    func stopIndicatorForScreen()  {
        
        
        if activityIndicatorForScreen != nil {
            activityIndicatorForScreen.stopAnimating()
            activityIndicatorForScreen.isHidden = true
        }
        
        if activityIndicatorBK != nil {
            activityIndicatorBK.isHidden = true
        }
    }
    
    
    func showMessage(_ message:String) {
        
        snackbar = TTGSnackbar.init(message: message, duration: durationTypes[1])
        // Change the content padding inset
        snackbar.contentInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
        
        // Change margin
        snackbar.leftMargin = 0
        snackbar.rightMargin = 0
        snackbar.bottomMargin = 0
        
        // Change message text font and color
        snackbar.messageTextColor = UIColor.white
        snackbar.messageTextFont = UIFont.init(name: "SanFranciscoText-Regular", size: 17)!
        
        // Change snackbar background color
        snackbar.backgroundColor = UIColor.black
        
        // Change animation duration
        snackbar.animationDuration = 0.5
        
        // Animation type
        snackbar.animationType = animationTypes[0]
        
        snackbar.layer.masksToBounds = false;
        snackbar.clipsToBounds = false;
        
        snackbar.show()
    }
    
    func dismissMessage()
    {
        snackbar.dismiss()
    }
}





extension UIApplication {
    class func topViewController(base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
