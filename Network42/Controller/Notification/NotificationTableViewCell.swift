//
//  NotificationTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 09/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI


@objc protocol notificationDelegate {
   
    func otherUserProfileClicked(tag:Int)
}

@objc protocol followNotificationDelegate {
    @objc optional func followClickedDelegate(index:Int,cell:NotificationTableViewCell)
}


class NotificationTableViewCell: UITableViewCell {
    @IBOutlet var userProfileBtn: UIButton!
    weak var delegate1: followNotificationDelegate?
    @IBOutlet var notificationTitleLbl: UILabel!
    @IBOutlet var timeLbl: UILabel!
    weak var delegate: notificationDelegate?
    
    @IBOutlet weak var btn_follow: UIButton!
    @IBOutlet weak var widthconstrnt_postImage: NSLayoutConstraint!
    @IBOutlet weak var vw_line: UIView!
    @IBOutlet weak var vw_background: UIView!
    @IBOutlet weak var btn_profile: UIButton!
    @IBOutlet var containerImage: UIImageView!
    @IBOutlet weak var constrnt_followWidth: NSLayoutConstraint!
    
    @IBOutlet weak var img_post: PFImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet var userImageView: PFImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func otherUserProfile(_ sender: Any) {
        self.delegate?.otherUserProfileClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func click_btnFollow(_ sender: UIButton) {
        self.delegate1?.followClickedDelegate!(index: (sender as AnyObject).tag,cell: self)
    }
    
    func config(_ user : User){
        notificationTitleLbl.text = user.name ?? ""
        timeLbl.text = user.userName ?? ""
        userImageView.setImage(fromUrl: user.profilePic, defaultImage: nil)
        userProfileBtn.layer.borderColor = UIColor.init(red: 205/255, green: 205/255, blue: 205/255, alpha: 0.8).cgColor
    }
    
}
