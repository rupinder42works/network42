//
//  FollowRequestsViewController.swift
//  Network42
//
//  Created by Apple1 on 31/01/2019.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import Parse

class FollowRequestsViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var followRequestCV: UICollectionView!

    //MARK:- Variables
    var followRequestArray : NSMutableArray = []
    
    //MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        followRequestCV.register(UINib(nibName: "RequestCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "RequestCollectionViewCell")
        // Do any additional setup after loading the view.
    }
    
    
    //MARK:- IBACtion
    @IBAction func backTap(){
        self.navigationController?.popViewController(animated: true)
    }
    

}
extension FollowRequestsViewController : UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return followRequestArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestCollectionViewCell",
                                                      for: indexPath) as! RequestCollectionViewCell
        
        
        cell.delegate = self
        cell.acceptBtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        //cell.seeAllBtn.tag = indexPath.row
        cell.nameLbl.textColor = ConstantColors.themeColor
        
        cell.acceptBtn.layer.borderWidth = 2.0
        cell.acceptBtn.layer.cornerRadius = 8.0
        cell.acceptBtn.setTitleColor(ConstantColors.themeColor, for: .normal)
        cell.acceptBtn.layer.borderColor = ConstantColors.lightGrayColor.cgColor
        
        cell.deleteBtn.layer.borderWidth = 2.0
        cell.deleteBtn.layer.cornerRadius = 8.0
        cell.deleteBtn.setTitleColor(ConstantColors.lightGrayColor, for: .normal)
        cell.deleteBtn.layer.borderColor = ConstantColors.lightGrayColor.cgColor
        
        //cell.seeAllBtn.isHidden = true
        
        let obect = (followRequestArray.object(at: indexPath.row) as AnyObject).object(forKey: "parent_follower") as? PFObject
        
        if let profileImageFile = obect?.value(forKey: "profile_image") as? PFFile
        {
            cell.userImage.file = profileImageFile
            cell.userImage.loadInBackground()
        }else
        {
            cell.userImage.image = UIImage.init(named: "dummyProfile")
        }
        
        if let nameObj = obect?.value(forKey: "name") as? String
        {
            cell.nameLbl.text = nameObj
        }
        
        if let nameObj = obect?.value(forKey: "user_name") as? String
        {
            cell.userName.text = nameObj
        }
        
        return cell
    }

}
extension FollowRequestsViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width, height: 77)
    }
}


extension FollowRequestsViewController : RequestDelegate{
    func acceptClicked(tag: Int, acceptIndicator: UIActivityIndicatorView, button: UIButton) {
        
    }
    
    func deleteClicked(tag: Int, deleteIndicator: UIActivityIndicatorView, button: UIButton) {
        
    }
    
    func seeAllClicked() {
        
    }
 
}
