//
//  NotificationsViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 09/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
class NotificationsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,notificationDelegate{
    //MARK:- Variables
    
    @IBOutlet weak var img_noNotification: UIImageView!
    @IBOutlet weak var lbl_noNotification: UILabel!
    var followedStr:String = ""
    var pageCount = 0
    var pagingSpinner = UIActivityIndicatorView()
    var attributednameArray:NSMutableArray = []
    var PFObjectArray : NSMutableArray = []
    var refreshControl: UIRefreshControl!
    var notificationListing = [PFObject]()
    
    //MARK:- IBOUtlets
    
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var collectionViewContainerHeight: NSLayoutConstraint!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var heightOfLabel: NSLayoutConstraint!
    @IBOutlet var collectionContainer: UIView!
    @IBOutlet var topLayoutWithCollection: NSLayoutConstraint!
    @IBOutlet var topLayoutTV: NSLayoutConstraint!
    @IBOutlet var seeAllBtnHeight : NSLayoutConstraint!
    @IBOutlet var seeAllBtnBottom : NSLayoutConstraint!
    @IBOutlet var seeAllBtn : UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        collectionViewContainerHeight.constant = 80 + 40 + 40
        
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "cell1")
        
        tableView.estimatedRowHeight = 702
        tableView.rowHeight = UITableView.automaticDimension
        
        tableView.delegate = self
        tableView.dataSource = self
        notifyactivityIndicator.startAnimating()
        
        collectionViewObj.register(UINib(nibName: "RequestCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "RequestCollectionViewCell")
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(NotificationsViewController.refresh), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshControl)        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.tabBarController?.tabBar.isHidden = false
        
        self.topLayoutWithCollection.isActive = false
        self.topLayoutTV.isActive = true
        self.collectionContainer.isHidden = true
        
        
        self.getAllNotificationAPI()
    }
    
    //MARK:- IBACtion
    
    @IBAction func seeAllClicked() {
        let tabBarStoryBoard = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = tabBarStoryBoard.instantiateViewController(withIdentifier: "FollowRequestsViewController") as! FollowRequestsViewController
        vc.followRequestArray = self.PFObjectArray
        self.navigationController?.pushViewController(vc, animated:true)
    }
    
    
    func checkWhetherUserFollowed(currentUserId:String,otherUserId:String,tag:Int){
        let query = PFQuery(className: "User_follow")
        query.whereKey("followed_id", equalTo:otherUserId)
        query.whereKey("follower_id", equalTo:currentUserId)
        query.whereKey("status", equalTo: "1")
        query.findObjectsInBackground(block: { (objects, error) in
            
            if error == nil {
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    print(object)
                    PFObjectArr.add(object)
                }
                
                if PFObjectArr.count>0
                {
                    if let cell = self.tableView.cellForRow(at: IndexPath.init(row:0 , section: tag)) as? NotificationTableViewCell{
                        cell.btn_follow.isHidden = false
                        cell.img_post.isHidden = true
                        cell.btn_follow.setTitle("Following", for: .normal)
                        cell.btn_follow.isHidden = false
                        cell.btn_follow.layer.borderColor = UIColor.white.cgColor
                        cell.btn_follow.layer.borderWidth = 0
                        cell.btn_follow.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1)
                        cell.btn_follow.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                    }
                }else
                {
                    if let cell = self.tableView.cellForRow(at: IndexPath.init(row:0 , section: tag)) as? NotificationTableViewCell{
                        cell.btn_follow.setTitle("Follow", for: .normal)
                        cell.btn_follow.isHidden = false
                        cell.btn_follow.layer.borderColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1).cgColor
                        cell.btn_follow.layer.borderWidth = 1.2
                        cell.btn_follow.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        cell.btn_follow.setTitleColor(#colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1), for: .normal)
                        cell.btn_follow.isHidden = false
                        cell.img_post.isHidden = true
                    }
                }
            }
            else {
                print(error!)
            }
        })
    }
    
    //MARK:-tableView
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.notificationListing.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.size.width
            , height: 1))
        let view1 = UIView.init(frame: CGRect.init(x: 15, y: 0, width: tableView.frame.size.width - 30
            , height: 1))
        view1.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        view.addSubview(view1)
        return view
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath as IndexPath) as! NotificationTableViewCell
        // create attributed string
        
        cell.delegate1 = self
        cell.userProfileBtn.tag = indexPath.section
        cell.userProfileBtn.layer.borderColor = UIColor.init(red: 205/255, green: 205/255, blue: 205/255, alpha: 0.8).cgColor
        cell.widthconstrnt_postImage.constant = 0
        cell.img_post.isHidden = true
        cell.btn_follow.isHidden = true
        if self.notificationListing.count > indexPath.section{
            if let parent_from = self.notificationListing[indexPath.section].value(forKey: "parent_from") as? PFUser{
                if let notifytype = self.notificationListing[indexPath.section].value(forKey: "notification_type") as? String{
                    print(notifytype)
                    if let usernameStr = parent_from.value(forKey: "name") as? String{
                        if notifytype == "like"{
                            followedStr = "@\(usernameStr) liked your post. "

                        }
                        else if notifytype == "comment_tag"{
                            followedStr = "@\(usernameStr) \(messages.userMentionedInComment)"
                        }
                        else if notifytype == "post_comment"{

                            var str = ""
                            if let notifytype = self.notificationListing[indexPath.section].value(forKey: "PostComment") as? String{
                                str = notifytype
                            }
                            if str.isEmpty == false{
                                followedStr = "@\(usernameStr) \(messages.commentPost) " + ": " + "\(str)"
                            }
                            else{
                                followedStr = "@\(usernameStr) \(messages.commentPost) "
                            }
                        }
                            
                        else if notifytype == "user_tag"{
                            
                            followedStr = "@\(usernameStr) \(messages.userTagged)"
                            
                        }
                        else if notifytype == "post_tag"{
                            
                            followedStr = "@\(usernameStr) \(messages.userTagged)"
                            
                        }
                        else if notifytype == "follow_request_accept"{
                            followedStr = "@\(usernameStr) \(messages.requestAccept)"
                            cell.widthconstrnt_postImage.constant = 60
                            
                            var currentUserId = ""
                            var otherUserId = ""
                            if let id = self.notificationListing[indexPath.section].value(forKey: "to_id") as? String{
                                currentUserId = id
                            }
                            if let id = self.notificationListing[indexPath.section].value(forKey: "from_id") as? String{
                                otherUserId = id
                            }
                            checkWhetherUserFollowed(currentUserId:currentUserId,otherUserId:otherUserId,tag:indexPath.section)
                            
                        }
                        else if notifytype == "follow_public_user"{
                            followedStr = "@\(usernameStr) \(messages.startedfollowing)"
                            cell.widthconstrnt_postImage.constant = 60
                            
                            var currentUserId = ""
                            var otherUserId = ""
                            if let id = self.notificationListing[indexPath.section].value(forKey: "to_id") as? String{
                                currentUserId = id
                            }
                            if let id = self.notificationListing[indexPath.section].value(forKey: "from_id") as? String{
                                otherUserId = id
                            }
                            checkWhetherUserFollowed(currentUserId:currentUserId,otherUserId:otherUserId,tag:indexPath.section)
                        }
                        
                        if notifytype == "like" || notifytype == "post_comment" || notifytype == "post_tag"{
                            if let postObject = self.notificationListing[indexPath.section].value(forKey: "PostId") as? PFObject{
                                postObject.fetchInBackground { (obj, error) in
                                    if error == nil{
                                        if let obj1 = obj as? PFObject{
                                            cell.widthconstrnt_postImage.constant = 0
                                            
                                            if let arr = obj1.value(forKey: "postmedia") as? NSArray{
                                                if let fileObj = arr.object(at: 0) as? PFFile
                                                {
                                                    if let post_type = obj1.object(forKey: "post_type") as? String
                                                    {
                                                        if post_type == "IMAGE" || post_type == "ALBUM"
                                                        {
                                                            cell.img_post.file = fileObj
                                                            cell.img_post.loadInBackground()
                                                            cell.img_post.isHidden = false
                                                            cell.widthconstrnt_postImage.constant = 60
                                                        }else{
                                                            cell.widthconstrnt_postImage.constant = 0
                                                        }
                                                    }
                                                }else{
                                                    cell.widthconstrnt_postImage.constant = 0
                                                }
                                            }
                                        }
                                    }
                                }
                            }else{
                                if notifytype == "follow_public_user"{
                                    cell.btn_follow.isHidden = false
                                    cell.widthconstrnt_postImage.constant = 60
                                }else{
                                    cell.widthconstrnt_postImage.constant = 0
                                }
                                
                            }
                        }
                        //complete name
                        let attrString: NSMutableAttributedString = NSMutableAttributedString(string: followedStr)
                        attrString.addAttribute(NSAttributedString.Key.font, value: UIFont.init(name: "SanFranciscoText-Regular", size: 14)!, range: NSMakeRange(0, attrString.length))
                        
                        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.black, range: NSMakeRange(0, attrString.length))
                        //set name colour
                        let range2 = (followedStr as NSString).range(of: usernameStr)
                        attrString.addAttribute(NSAttributedString.Key.foregroundColor, value:UIColor.black, range: range2)
                        attrString.addAttribute(NSAttributedString.Key.font, value:UIFont.init(name: "SanFranciscoText-Semibold", size: 14)!, range: range2)
                        
                        cell.notificationTitleLbl.attributedText = attrString
                    }
                }
                
                if parent_from.object(forKey: "profile_image") != nil {
                    let ImageFile = parent_from.object(forKey: "profile_image") as! PFFile
                    cell.userImageView.file = ImageFile
                    cell.userImageView.loadInBackground()
                } else {
                    cell.userImageView.image = UIImage.init(named: "dummyProfile")
                }
            }
            
            
            if let  timeStampStr = self.notificationListing[indexPath.section].value(forKey: "time") as? String{
                
                print(timeStampStr)
                let newdateStr = timeStampStr.replacingOccurrences(of: "_", with: " ")
                let dateFormatter = DateFormatter()
                if timeStampStr.lowercased().contains("am") == true || timeStampStr.lowercased().contains("pm") == true{
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
                }else{
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                }
                let newdate = dateFormatter.date(from: newdateStr)
                let timeStr  = self.timeAgoSinceDate(date:newdate ?? Date(),numericDates: true)
                print(timeStr)
                cell.timeLbl.text! = timeStr
            }
        }

        cell.vw_background.layer.cornerRadius = 8
        cell.vw_background.layer.shadowColor = ConstantColors.lightGrayColor.cgColor
        cell.vw_background.layer.shadowOffset = CGSize.zero
        cell.vw_background.layer.shadowOpacity = 1
        cell.vw_background.layer.shadowRadius = 1.0
        cell.vw_background.layer.shadowRadius = 1.0
        cell.vw_background.layer.masksToBounds = false
       
        
        //cell.contentView.layer.shadowPath = UIBezierPath(rect: cell.contentView.bounds).cgPath
        
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let id = self.notificationListing[indexPath.section].value(forKey: "post_id") as? String
        
        if id == ""
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.fromSearch = true
            vc.userProfile = false
            vc.friend = "FOLLOWING"
             if let parent_from = self.notificationListing[indexPath.section].value(forKey: "parent_from") as? PFUser{
                vc.userFromSearch = parent_from
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else {
            let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
            let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            vc.secondTime = true
            vc.titleSecond = "Post Detail"
            vc.showOnlySinglePost = true
            vc.postIdObject = id!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func otherUserProfileClicked(tag: Int)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        vc.friend = "FOLLOWING"
        if let parent_from = self.notificationListing[tag].value(forKey: "parent_from") as? PFUser{
            vc.userFromSearch = parent_from
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet var notifyactivityIndicator: UIActivityIndicatorView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:-get Notification API
    
    func getAllNotificationAPI(){
        
        self.notificationListing.removeAll()
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        
        let objId = PFUser.current()?.objectId
        print(objId!)
        let query = PFQuery(className:"Notifications")
        query.whereKey("to_id", equalTo:objId!)
        query.limit =  100
        query.skip = self.notificationListing.count
        query.includeKey("parent_from")
        query.order(byDescending: "createdAt")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                for object in objects! {
                    if object.value(forKey: "notification_type") as? String ?? "" == "follow_user" || object.value(forKey: "notification_type") as? String ?? "" == "follow_request" {
                        continue
                    }
                    
                    self.notificationListing.append(object)
                }
                self.getRequests()

            }
            else {
                print(error!)
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                self.showMessage(NSLocalizedDescriptionKey)
                self.pagingSpinner.stopAnimating()
                self.notifyactivityIndicator.stopAnimating()
            }
        })
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        if scrollView == tableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                pagingSpinner = UIActivityIndicatorView(style: .gray)
                pagingSpinner.startAnimating()
                pagingSpinner.color = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
                pagingSpinner.hidesWhenStopped = true
                
                if self.notificationListing.count>5 {
                    tableView.tableFooterView = pagingSpinner
                    self.getAllNotificationAPI()
                    
                }
            }
        }
    }
    
    //MARK:-Conver Timestamp
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }

}


extension NotificationsViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    
    @objc func refresh()
    {
        self.getRequests()
        notificationListing.removeAll()
        self.tableView.reloadData()
        self.getAllNotificationAPI()
    }
    
    //MARK:- Get Followed Requests
    
    func getRequests()
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "User_follow")
        let objId = PFUser.current()?.objectId
        query.whereKey("followed_id", equalTo:objId!)
        query.whereKey("status", equalTo: "0")
        query.order(byDescending: "createdAt")
        query.includeKey("parent_follower")
        query.findObjectsInBackground(block: { (objects, error) in
            self.refreshControl.endRefreshing()
            if error == nil {
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    print(object)
                    PFObjectArr.add(object)
                }
                
                if PFObjectArr.count>0
                {
                    self.topLayoutWithCollection.isActive = true
                    self.collectionContainer.isHidden = false
                    self.topLayoutTV.isActive = false

                    
                    print(PFObjectArr.count,"self.PFObjectArray.count")
                    
                    if PFObjectArr.count == 1{
                        self.seeAllBtnHeight.constant = 0
                        self.seeAllBtnBottom.constant = 0
                        self.seeAllBtn.isHidden = true
                        self.collectionViewContainerHeight.constant = 100
                    }else{
                        self.seeAllBtnHeight.constant = 30
                        self.seeAllBtnBottom.constant = 10
                        self.seeAllBtn.isHidden = false
                        self.collectionViewContainerHeight.constant = 130
                    }
                    
                }else
                {
                    self.topLayoutWithCollection.isActive = false
                    self.collectionContainer.isHidden = true
                    self.topLayoutTV.isActive = true
                }
                

                self.PFObjectArray = PFObjectArr
               
                self.collectionViewObj.reloadData()
                
                self.notifyactivityIndicator.stopAnimating()
                //                self.notificationListing.append(contentsOf: objects!)
                print(self.notificationListing)
                if self.notificationListing.count > 0 || PFObjectArr.count > 0{
                    self.tableView.isHidden = false
                    self.img_noNotification.isHidden = true
                    self.lbl_noNotification.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.img_noNotification.isHidden = false
                    self.lbl_noNotification.isHidden = false
                }
                self.tableView.reloadData()
                self.pagingSpinner.stopAnimating()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
            }
            else {
                print(error!)
            }
        })
    }
    
    //MARK:- Collection View Delegates and Data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //pageControlObj.numberOfPages = PFObjectArray.count
        print("collection count=",PFObjectArray.count)
        return PFObjectArray.count > 0 ? 1 : 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestCollectionViewCell",
                                                      for: indexPath) as! RequestCollectionViewCell
        
        cell.delegate = self
        cell.acceptBtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
       // cell.seeAllBtn.tag = indexPath.row
        cell.nameLbl.textColor = ConstantColors.themeColor
        
        cell.acceptBtn.layer.borderWidth = 2.0
        cell.acceptBtn.layer.cornerRadius = 5.0
        cell.acceptBtn.setTitleColor(ConstantColors.themeColor, for: .normal)
        cell.acceptBtn.layer.borderColor = ConstantColors.lightGrayColor.cgColor
        
        cell.deleteBtn.layer.borderWidth = 2.0
        cell.deleteBtn.layer.cornerRadius = 5.0
        cell.deleteBtn.setTitleColor(ConstantColors.lightGrayColor, for: .normal)
        cell.deleteBtn.layer.borderColor = ConstantColors.lightGrayColor.cgColor
        
       // cell.seeAllBtn.isHidden = PFObjectArray.count > 1 ? true : false

        let obect = (PFObjectArray.object(at: indexPath.row) as AnyObject).object(forKey: "parent_follower") as? PFObject
        
        if let profileImageFile = obect?.value(forKey: "profile_image") as? PFFile
        {
            cell.userImage.file = profileImageFile
            cell.userImage.loadInBackground()
        }else
        {
            cell.userImage.image = UIImage.init(named: "dummyProfile")
        }
        
        if let nameObj = obect?.value(forKey: "name") as? String
        {
            cell.nameLbl.text = nameObj
        }
        
        if let nameObj = obect?.value(forKey: "user_name") as? String
        {
            cell.userName.text = nameObj
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        vc.friend = "FOLLOW"
        vc.notificationStatus = true
        let user = (self.PFObjectArray.object(at: indexPath.row) as AnyObject).object(forKey: "parent_follower") as? PFUser
        vc.userFromSearch = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width, height: 70.0)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func acceptedPushNotification(userId:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        if !(userId == PFUser.current()!.objectId!)
        {
            let currentName = PFUser.current()?.value(forKey: "name") as! String
            self.sendPushNotification(postId: "", userId: userId,settingType: "push_accept_follow_request",message:"\(currentName) \(messages.requestAccept)",type:"follow_request_accept",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
        }
    }
}

extension NotificationsViewController : RequestDelegate{
    
    //MARK:- RequestDelegate
  
    func acceptClicked(tag: Int,acceptIndicator:UIActivityIndicatorView,button: UIButton) {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        print("accept")
        acceptIndicator.startAnimating()
        button.setTitle("", for: .normal)
        self.collectionViewObj.isUserInteractionEnabled = false
        let obect = PFObjectArray.object(at: tag) as! PFObject
        obect["status"] = "1"
        obect.saveInBackground { (success, error) in
            
            acceptIndicator.stopAnimating()
            button.setTitle("ACCEPT", for: .normal)
            self.collectionViewObj.isUserInteractionEnabled = true
            if(success)
            {
                print("user updated")
                
                
                let user = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_follower") as? PFUser
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                self.saveToNotifications(from_id: PFUser.current()!.objectId!, to_id: user!.objectId!, notification_type: "follow_request_accept", time: formatter.string(from: Date()), post_id: "", parent_from: PFUser.current()!, parent_to: user!,commentStr: "")
                
                
                self.acceptedPushNotification(userId: user!.objectId!)
                
                
                self.PFObjectArray.removeObject(at: tag)
                self.collectionViewObj.reloadData()
                
                
                if self.PFObjectArray.count>0
                {
                    self.topLayoutWithCollection.isActive = true
                    self.collectionContainer.isHidden = false
                    self.topLayoutTV.isActive = false
                }else
                {
                    self.topLayoutWithCollection.isActive = false
                    self.collectionContainer.isHidden = true
                    self.topLayoutTV.isActive = true
                }
                if self.PFObjectArray.count == 1{
                    self.seeAllBtnHeight.constant = 0
                    self.seeAllBtnBottom.constant = 0
                    self.seeAllBtn.isHidden = true
                    self.collectionViewContainerHeight.constant = 100
                }else{
                    self.seeAllBtnHeight.constant = 30
                    self.seeAllBtnBottom.constant = 10
                    self.seeAllBtn.isHidden = false
                    self.collectionViewContainerHeight.constant = 130
                }
                
                if self.notificationListing.count > 0 || self.PFObjectArray.count > 0{
                    self.tableView.isHidden = false
                    self.img_noNotification.isHidden = true
                    self.lbl_noNotification.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.img_noNotification.isHidden = false
                    self.lbl_noNotification.isHidden = false
                }
            }
            else{
                print("error",error!)
                let message = (error?.localizedDescription)
                self.showMessage(message!)
                return
            }
        }
    }
    
    
    func deleteClicked(tag: Int,deleteIndicator:UIActivityIndicatorView,button: UIButton) {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        deleteIndicator.startAnimating()
        button.setTitle("", for: .normal)
        self.collectionViewObj.isUserInteractionEnabled = false
        let obect = PFObjectArray.object(at: tag) as! PFObject
        obect.deleteInBackground(block: { (status, error) in
            print(status)
            deleteIndicator.stopAnimating()
            button.setTitle("DELETE", for: .normal)
            self.collectionViewObj.isUserInteractionEnabled = true
            if status == true{
                self.PFObjectArray.removeObject(at: tag)
                self.collectionViewObj.reloadData()
                
                if self.PFObjectArray.count>0
                {
                    self.topLayoutWithCollection.isActive = true
                    self.collectionContainer.isHidden = false
                    self.topLayoutTV.isActive = false
                }else
                {
                    self.topLayoutWithCollection.isActive = false
                    self.collectionContainer.isHidden = true
                    self.topLayoutTV.isActive = true
                }
                
                if self.PFObjectArray.count == 1{
                    self.seeAllBtnHeight.constant = 0
                    self.seeAllBtnBottom.constant = 0
                    self.seeAllBtn.isHidden = true
                    self.collectionViewContainerHeight.constant = 100
                }else{
                    self.seeAllBtnHeight.constant = 30
                    self.seeAllBtnBottom.constant = 10
                    self.seeAllBtn.isHidden = false
                    self.collectionViewContainerHeight.constant = 130
                }
                
                if self.notificationListing.count > 0 || self.PFObjectArray.count > 0{
                    self.tableView.isHidden = false
                    self.img_noNotification.isHidden = true
                    self.lbl_noNotification.isHidden = true
                }else{
                    self.tableView.isHidden = true
                    self.img_noNotification.isHidden = false
                    self.lbl_noNotification.isHidden = false
                }
            }else{
                print("error",error!)
                let message = (error?.localizedDescription)
                self.showMessage(message!)
                return
            }
            
        })
    }
}


extension NotificationsViewController : followNotificationDelegate{
    
    func followClickedDelegate(index:Int,cell:NotificationTableViewCell)
    {
        print("==",index)
        var currentUserId = ""
        var otherUserId = ""
        if let id = self.notificationListing[index].value(forKey: "to_id") as? String{
            currentUserId = id
        }
        if let id = self.notificationListing[index].value(forKey: "from_id") as? String{
            otherUserId = id
        }
        if cell.btn_follow.titleLabel?.text == "Follow"
        {
          //  cell.indicator.startAnimating()
            cell.btn_follow.setTitle("", for: .normal)
            cell.isUserInteractionEnabled = false
            let objId = currentUserId
            let User_followClass = PFObject(className: "User_follow")
            User_followClass["follower_id"] = objId
            User_followClass["followed_id"] = otherUserId
            if let  obj = self.notificationListing[index].value(forKey: "parent_from") as? PFObject{
                User_followClass["parent_following"] = obj
            }
            User_followClass["parent_follower"] = PFUser.current()
            
            User_followClass["status"] = "1"
            
            
            User_followClass.saveInBackground { (success, error) in
           //     cell.indicator.stopAnimating()
                cell.isUserInteractionEnabled = true
                if(success)
                {
                    print("user follow")
                    // self.getIds()
                    cell.btn_follow.setTitle("Following", for: .normal)
                    cell.btn_follow.isHidden = false
                    cell.btn_follow.layer.borderColor = UIColor.white.cgColor
                    cell.btn_follow.layer.borderWidth = 0
                    cell.btn_follow.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1)
                    cell.btn_follow.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                    // let user = self.PFObjectArray.object(at: index) as? PFUser
                    var user : PFUser?
                     if let  obj = self.notificationListing[index].value(forKey: "parent_from") as? PFUser{
                        user = obj
                    }
                    let currentName = PFUser.current()?.value(forKey: "name") as! String
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                            let time = formatter.string(from: Date())
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                            
                            self.saveToNotifications(from_id: objId, to_id: user!.objectId!, notification_type: "follow_public_user", time: time, post_id: "", parent_from: PFUser.current()!, parent_to: user!,commentStr: "")
                            
                            self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.startedfollowing)",type:"follow_public_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                    
                }
                else{
                    print("error",error!)
                    //cell.indicator.stopAnimating()
                    cell.btn_follow.setTitle("Follow", for: .normal)
                    let message = (error?.localizedDescription)
                    self.showMessage(message!)
                    return
                }
            }
        }else
        {
            var alert = UIAlertController()
            if cell.btn_follow.titleLabel?.text == "Following"
            {
                var name = ""
//                if let t =  (self.getfinalArray.object(at: index) as AnyObject).value(forKey: "user_name") as? String{
//                    name = t
//                }
                alert = UIAlertController(title: "Are you sure you want to unfollow \(name)?", message: "", preferredStyle: UIAlertController.Style.alert)
            }else
            {
                alert = UIAlertController(title: "Are you sure you want to cancel this request?", message: "", preferredStyle: UIAlertController.Style.alert)
            }
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
                switch action.style{
                case .default:
                   
                    
                    //cell.indicator.startAnimating()
                    // cell.isUserInteractionEnabled = false
                    //   cell.followBtn.setTitle("", for: .normal)
                    
                    var currentUserId = ""
                    var otherUserId = ""
                    if let id = self.notificationListing[index].value(forKey: "to_id") as? String{
                        currentUserId = id
                    }
                    if let id = self.notificationListing[index].value(forKey: "from_id") as? String{
                        otherUserId = id
                    }
                    
                    let loggedIn = currentUserId
                    let query = PFQuery(className:"User_follow")
                    query.whereKey("follower_id", equalTo: loggedIn)
                    query.whereKey("followed_id", equalTo: otherUserId)
                    
                    query.findObjectsInBackground(block: { (objects, error) in
                      //  cell.indicator.stopAnimating()
                        if error == nil {
                            for objectt in objects! {
                                
                                objectt.deleteInBackground(block: { (status, error) in
                                    print(status)
                                    cell.isUserInteractionEnabled = true
                                    if status == true{
                                        cell.btn_follow.setTitle("Follow", for: .normal)
                                        cell.btn_follow.isHidden = false
                                        cell.btn_follow.layer.borderColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1).cgColor
                                        cell.btn_follow.layer.borderWidth = 1.2
                                        cell.btn_follow.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                                        cell.btn_follow.setTitleColor(#colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1), for: .normal)
                                        if cell.btn_follow.titleLabel?.text == "Following"
                                        {
                                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                                        }
                                    }else
                                    {
                                    }
                                })
                            }
                        }
                        else {
                            
                            let message = (error?.localizedDescription)
                            self.showMessage(message!)
                            print(error!)
                        }
                    })
                    
                case .cancel:
                  //  cell.indicator.stopAnimating()
                    print("cancel")
                    
                case .destructive:
                   // cell.indicator.stopAnimating()
                    print("destructive")
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}
