//
//  Bloack_tableCell.swift
//  Network42
//
//  Created by Apple on 28/02/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class Bloack_tableCell: UITableViewCell {

    @IBOutlet weak var img_profile: PFImageView!
    
    @IBOutlet weak var lbl_username: TimeLabel!
    @IBOutlet weak var lvbl_name: NameLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
