//
//  ViewingProfileViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 09/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
class ViewingProfileViewController: BaseViewController {
    
    @IBOutlet weak var img_publicProfile: UIImageView!
    @IBOutlet weak var img_privateProfile: UIImageView!
    @IBOutlet var profileModesActivityIndicator: UIActivityIndicatorView!
    let checkedImage = UIImage(named: "check")! as UIImage
    let uncheckedImage = UIImage(named: "uncheck")! as UIImage
    var profileModeFlag:String = ""
    var getProfileType:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.privateBtn.tintColor = .clear
        self.checkBtn.tintColor = .clear
        self.getProfileAPI()
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBOutlet var activityIndicatorBKView: UIView!
    
    
    @IBOutlet var checkBtn: UIButton!
    
    @IBAction func publicTapped(_ sender: Any) {
        privateBtn.isSelected = false
      //   privateBtn.setImage(uncheckedImage, for: .normal)
        checkBtn.isSelected  = !checkBtn.isSelected
        if (!checkBtn.isSelected)
        {
            NSLog(" Not Selected");
           // checkBtn.setImage(uncheckedImage, for: .normal)
             img_publicProfile.image = #imageLiteral(resourceName: "uncheck")
             img_privateProfile.image = #imageLiteral(resourceName: "radioSelected")
        }
        else
        {
            NSLog(" Selected");
            //checkBtn.setImage(checkedImage, for: .normal)
             img_publicProfile.image = #imageLiteral(resourceName: "radioSelected")
             img_privateProfile.image = #imageLiteral(resourceName: "uncheck")
            
        }
        profileModeFlag = "1"
        getProfileType = "Public"
        self.profileModesActivityIndicator.startAnimating()
        updateViewProfileAPI(getProfileFlag: profileModeFlag)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var privateBtn: UIButton!
    @IBAction func privateTapped(_ sender: Any) {
        checkBtn.isSelected = false
       // checkBtn.setImage(uncheckedImage, for: .normal)
        privateBtn.isSelected  = !privateBtn.isSelected
        if (!privateBtn.isSelected)
        {
            NSLog(" Not Selected");
           // privateBtn.setImage(uncheckedImage, for: .normal)
            img_privateProfile.image = #imageLiteral(resourceName: "uncheck")
             img_publicProfile.image = #imageLiteral(resourceName: "radioSelected")
        }
        else
        {
            NSLog(" Selected");
           // privateBtn.setImage(checkedImage, for: .normal)
             img_privateProfile.image = #imageLiteral(resourceName: "radioSelected")
              img_publicProfile.image = #imageLiteral(resourceName: "uncheck")
            
        }
        profileModeFlag = "0"
        getProfileType = "Private"
        self.profileModesActivityIndicator.startAnimating()
        updateViewProfileAPI(getProfileFlag: profileModeFlag)
    }
    
    func updateViewProfileAPI(getProfileFlag:String){
        let objId = PFUser.current()?.objectId
        print(objId!)
        let query = PFQuery(className:"Settings")
        query.whereKey("user_id", equalTo:objId!)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                if (objects?.count != 0) {
                    print(objects as Any)
                    for compClass in objects! {
                        compClass["profile_visibility"] = getProfileFlag
                       compClass.saveInBackground(block: { (status, error) in
                            print(status)
                            if status ==  true{
                                print(status)
                                self.profileModesActivityIndicator.stopAnimating()
                            }
                            
                        })
                    }
                }
            }
            else{
                let message = (error?.localizedDescription)
                print(message as Any)
                self.profileModesActivityIndicator.stopAnimating()
                self.showMessage(message!)
            }
        })
        
        
        
        if let currentUser = PFUser.current(){
           
                        currentUser["profile_type"] = getProfileFlag
                        currentUser.saveInBackground()
        }

    }
    //MARK:-Get Push FLags
    func getProfileAPI(){
        
        let objId = PFUser.current()?.objectId
        print(objId!)
        let query = PFQuery(className:"Settings")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                self.profileModesActivityIndicator.stopAnimating()
                print(objects as Any)
                for object in objects! {
                   print(object)
                    if let pushLikeFlag = object.value(forKey: "profile_visibility") as? String{
                        print(pushLikeFlag)
                        if pushLikeFlag == "1"{
                          //  self.checkBtn.setImage(self.checkedImage, for: .normal)
                            self.img_publicProfile.image = #imageLiteral(resourceName: "radioSelected")
                            self.checkBtn.isSelected = true
                        }
                        else{
                            //self.privateBtn.setImage(self.checkedImage, for: .normal)
                            self.privateBtn.isSelected = true
                            self.img_privateProfile.image = #imageLiteral(resourceName: "radioSelected")
                        }
                    
                    }
                 }
            }
            else {
                print(error!)
                self.showMessage(NSLocalizedDescriptionKey)
                }
            
        })
        
    }

    
}
