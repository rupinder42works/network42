//
//  PrivacyPolicyViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 09/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class PrivacyPolicyViewController: UIViewController,UIWebViewDelegate{

    @IBAction func backTapped(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet var privacyPolicyactivityIndicator: UIActivityIndicatorView!
    @IBOutlet var webViewPolicy: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Your webView code goes here
        let url = NSURL (string: "http://42works.net/network42-privacy-policy/")
        let requestObj = URLRequest(url: url! as URL)
        
        webViewPolicy.loadRequest(requestObj)
        webViewPolicy.delegate = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        // Removes it:
        privacyPolicyactivityIndicator.stopAnimating()
    }
   
}
