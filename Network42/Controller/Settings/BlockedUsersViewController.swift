//
//  BlockedUsersViewController.swift
//  Network42
//
//  Created by Apple1 on 04/02/2019.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit

class BlockedUsersViewController: BaseViewController {

    @IBOutlet weak var lbl_noBlock: UILabel!
    @IBOutlet weak var img_noBlock: UIImageView!
    //MARK:- IBOUtlets
    @IBOutlet weak var blockedUserTableView : UITableView!{
        didSet{
            blockedUserTableView.estimatedRowHeight = 72
            blockedUserTableView.rowHeight = UITableView.automaticDimension
            blockedUserTableView.register(UINib(nibName: "Bloack_tableCell", bundle: Bundle.main), forCellReuseIdentifier: "Bloack_tableCell")
        }
    }

    //MARK:- Variables
    var blockedUsersVM = BlockedUserVM()
    var blockedUserArr = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
         super.viewDidAppear(true)
         self.postInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    //MARK:- Private
    private func postInit() {
      
        blockedUsersVM.updateLoadingStatus = {[weak self]() in
            if self?.blockedUsersVM.isLoading ?? false {
                self?.showLoader(self)
            } else {
                self?.hideLoader(self)
            }
        }
        
        
        blockedUsersVM.showEmptyMsgClausre = {[weak self]() in
            DispatchQueue.main.async {
                if let message = self?.blockedUsersVM.alertMessage {
                    self?.showMessage(message)
                }
            }
        }
        
        blockedUsersVM.responseRecieved = {[weak self]() in
            DispatchQueue.main.async {
                self?.blockedUserArr = self?.blockedUsersVM.blockedUserArr ?? [User]()
                if let blocked = self?.blockedUserArr{
                    if blocked.count > 0 {
                        self?.lbl_noBlock.isHidden = true
                        self?.img_noBlock.isHidden = true
                        self?.blockedUserTableView.isHidden = false
                      
                    }else{
                        self?.lbl_noBlock.isHidden = false
                        self?.img_noBlock.isHidden = false
                        self?.blockedUserTableView.isHidden = true
                    }
                      self?.blockedUserTableView.reloadData()
                }
               
                
            }
        }
        
        blockedUsersVM.getBlockedUsers()
       
    }
    
    
    //MARK:- IBActions
    
    @IBAction func backTap(){
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension BlockedUsersViewController : UITableViewDelegate , UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return blockedUserArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "Bloack_tableCell", for: indexPath) as? Bloack_tableCell
        let model = self.blockedUserArr[indexPath.section]
        cell?.lvbl_name.text = model.name ?? ""
        cell?.lbl_username.text = model.userName ?? ""
        cell?.img_profile.setImage(fromUrl: model.profilePic, defaultImage: nil)
        return cell ?? Bloack_tableCell()
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = UIView()
        footerView.backgroundColor = UIColor.clear
        return footerView
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "TabBarSB", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        vc.friend = "FOLLOW"
        vc.objectId = blockedUserArr[indexPath.section].id ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
