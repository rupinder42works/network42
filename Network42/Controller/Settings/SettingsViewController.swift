//
//  SettingsViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 21/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import Firebase

class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var hedingArray:NSMutableArray = []
    var FbhedingArray:NSMutableArray = []
    var subHeadingArray:NSMutableArray = []
    
    @IBOutlet var indicatorObj: UIActivityIndicatorView!
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "SettingTopTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "SettingsotherTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell1")
        
        tableView.estimatedRowHeight = 72
        tableView.rowHeight = UITableView.automaticDimension
        
        hedingArray = ["Push Notifications","Change Password","Profile Viewing Options","Blocked Users","Delete Account","Privacy Policy"]
        subHeadingArray = ["Choose which mobile push notifications you receive.","Choose a unique password to protect your account.","Choose whether you are visible or viewing in private mode.","List of users you have blocked.","Deleting your account will delete your account info, profile photo and message history on this phone.","Choose to read our privacy policy."]
        let currentUser = PFUser.current()
        if let loginTypeStr = currentUser?.object(forKey: "login_type") as? String{
            print("values",loginTypeStr)
            if loginTypeStr == "facebook"{
                hedingArray.removeObject(at: 1)
                subHeadingArray.removeObject(at: 1)
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        
    }
    
    @IBAction func action_logout(_ sender: UIButton) {
        let alert = UIAlertController(title: "Are you sure you want to logout?", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
            nestedprofileArray =  [[],[],"","","",""]
            self.installer()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func getIconImage(index:Int) -> UIImage{
        switch index {
        case 0:
            return UIImage.init(named: "notifications.pdf") ?? UIImage()
        case 1:
            return UIImage.init(named: "change password.pdf") ?? UIImage()
        case 2:
            return UIImage.init(named: "profile view.pdf") ?? UIImage()
        case 3:
            return UIImage.init(named: "blocked users.pdf") ?? UIImage()
        case 4:
            return UIImage.init(named: "delete account.pdf") ?? UIImage()
        case 5:
            return UIImage.init(named: "notifications.pdf") ?? UIImage()
            
        default:
            return UIImage()
        }
    }
    
    //MARK:- delete account helper methods
    
    func deleteAccountAlert(){
        let alert = UIAlertController(title: "Are you sure you want to delete your account?", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            let delVM = DeleteUserVM.init()
            delVM.delegate = self
            delVM.indicatorDelegate = self
            delVM.deleteUserAccount()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func method_deleteAccount(){
        let DBref = FIRDatabase.database().reference()
        //Sender data for group
        
    }
    
    // MARK:- Table view delgate and data source

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hedingArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // if indexPath.row < 3 || indexPath.row == 4{
            return UITableView.automaticDimension
       // }
       // return 65
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                as! SettingTopTableViewCell
            cell.selectionStyle = .none
            cell.headingTitle.text = hedingArray[indexPath.row] as? String
            cell.imgVw_downArrow.isHidden = false
            cell.subheadingTitle.text = subHeadingArray[indexPath.row] as? String
            cell.imgVw_front.image = getIconImage(index: indexPath.row)
            return cell
    }
    
    //    func didselectrow
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if hedingArray.object(at: indexPath.row) as? String == "Push Notifications"{
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "PushNotificationViewController") as! PushNotificationViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Change Password" {
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "ChangepasswordViewController") as! ChangepasswordViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Profile Viewing Options" {
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "ViewingProfileViewController") as! ViewingProfileViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Blocked Users"{
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "BlockedUsersViewController") as! BlockedUsersViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }else if hedingArray.object(at: indexPath.row) as? String == "Privacy Policy" {
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Delete Account"{
            deleteAccountAlert()
        }
    }
    
    
    func installer()
    {
      
        let installationClass = PFInstallation.current()
        installationClass?["userId"] = PFUser.current()?.objectId!
        installationClass?["isActive"] = "0"
        installationClass?.saveInBackground { (success, error) in
            self.indicatorObj.stopAnimating()
            if(success)
            {
                print("installation saved")
            }
            else{
                print("error",error!)
                //return
            }
            PFUser.logOut()
            UserDefaults.standard.set(false, forKey: "LoggedIn")
            let tabBarSB = UIStoryboard.init(name: "LoginSB", bundle: nil)
            let nav = tabBarSB.instantiateViewController(withIdentifier: "NavViewController") as! NavViewController
            let vc = tabBarSB.instantiateViewController(withIdentifier: "LoginViewController")
            nav.viewControllers = [vc]
            let window = UIApplication.shared.windows[0] as UIWindow
            window.rootViewController=nav
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
}

extension SettingsViewController : notifyCompletionDelegate,startIndicatorDelegate{
    func notify(){
        self.installer()
        
    }
    
    func startIndicator(){
        self.indicatorObj.startAnimating()
    }
}
