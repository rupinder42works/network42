//
//  AddWorkExperienceViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 23/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
class AddWorkExperienceViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource{
   
    
    @IBOutlet weak var img_chk: UIImageView!
    var currentYeartopass:String = ""
  
    var index:Int = 0
   @IBOutlet var addBtn: TKTransitionSubmitButton!
    @IBOutlet var scrollView: UIScrollView!
    var selectedValuefromPicker:String = ""
    var selectionRow:Int = 0
    var pickerType:String = ""
    var wholeCompObj:NSMutableArray = []
    var companyObj: PFObject?
   // var companyObj = PFObject()
    var educationObj:NSMutableArray = []
    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var fromTf: MyTextField!
    @IBOutlet var descriptionTF: MyTextField!
    @IBOutlet var cityTf: MyTextField!
    @IBOutlet var designationTf: MyTextField!
    @IBOutlet var companyTf: MyTextField!
    let checkedImage = #imageLiteral(resourceName: "radioSelected")
    let uncheckedImage = UIImage(named: "uncheck")! as UIImage
    var yearArray :NSMutableArray = []
    
    override func viewDidLoad() {
        self.tabBarController?.tabBar.isHidden = true
        super.viewDidLoad()
        img_chk.image = uncheckedImage
//        print(companyObj as Any)
//        print(wholeCompObj)
//        print("whole Arra",cworkExpWholeobj)
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let year =  components.year
        print(year as Any)
         let currentVal:Int = year!
        
        if companyObj != nil{
            
            headingLabel.text = "Edit Work Experience"
            titleLabel.text = "    Edit Work Experience"
            if let compName = (companyObj as AnyObject).value(forKey: "company_name") as? String{
                print(compName)
                workSelectedStr = compName
             }
            if let designationStr = (companyObj as AnyObject).value(forKey: "designation") as? String{
                designationTf.text = designationStr
                
            }
            if let city = (companyObj as AnyObject).value(forKey: "location") as? String{
                print(city)
                 locSelectedStr = city
                
            }
            print(companyObj as Any)
            if let dStr = (companyObj as AnyObject).object(forKey: "description") as? String{
                print(dStr)
               // print(dStr.value(forKey: "description") as Any)
              descriptionTF.text = dStr
                
            }
           
            if let fromYear = (companyObj as AnyObject).value(forKey: "from_year") as? String{
                 fromTf.text = fromYear
            }
            if let toYear = (companyObj as AnyObject).value(forKey: "to_year") as? String{
                let getyearVal = Int(toYear)
                if getyearVal == currentVal{
                    currentYeartopass = "Present"
                    currentBtn.isSelected  = true
                    img_chk.image = checkedImage
                   // currentBtn.setImage(checkedImage, for: .normal)
                }
                else{
                    currentYeartopass = toYear
                    currentBtn.isSelected  =  false
                    img_chk.image = uncheckedImage
                   // currentBtn.setImage(uncheckedImage, for: .normal)
                }
             
                toYearTF.text! = currentYeartopass
                
            }
            addBtn.setTitle("UPDATE",for: .normal)
           // currentBtn.setImage("check", for: .normal)
        }
        else{
             addBtn.setTitle("ADD",for: .normal)
            workSelectedStr = ""
            locSelectedStr = ""
            currentYeartopass = ""
            
        }
        
        yearArray.removeAllObjects()
        var startYear = 1967
       
        for i in startYear..<currentVal + 1 {
            print(i)
             yearArray.add(startYear)
            startYear = startYear + 1
            
        }
        print(yearArray)
        
 }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
//MARK:-Picker Actions
    
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var pickerBgView: UIView!
    @IBAction func cancelTapped(_ sender: Any) {
        pickerBgView.isHidden = true
        self.fromTf .resignFirstResponder()
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
        pickerBgView.isHidden=true
        picker.isHidden=true
    }
    @IBOutlet var doneBtn: UIButton!
    
    @IBOutlet var picker: UIPickerView!
    @IBAction func doneTapped(_ sender: Any) {
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
        self.fromTf .resignFirstResponder()
         self.hidekeyboard()
        pickerBgView.isHidden = true
        picker.isHidden = true
        print(selectedValuefromPicker)
        
        currentBtn.isSelected  =  false
        //currentBtn.setImage(uncheckedImage, for: .normal)
        img_chk.image = uncheckedImage
        if pickerType == "FromYear" {
            
            if toYearTF.text != "" {
                let aFrom:Int = Int(selectedValuefromPicker) ?? 0 // firstText is UITextField
                let bto:Int = Int(toYearTF.text ?? "0") ?? 0
                
                if bto < aFrom && toYearTF.text != "Present"
                {
                    self.showMessage("To date should be greater than from date.")
                    return
                }
            }
            
            fromTf.text = selectedValuefromPicker
        }
        else{
            if fromTf.text == "" {
                return
            }
            
            let aFrom:Int = Int(fromTf.text ?? "0") ?? 0 // firstText is UITextField
            let bto:Int = Int(selectedValuefromPicker) ?? 0
            
            if bto < aFrom
            {
                self.showMessage("To date should be greater than from date.")
                return
            }
            
            toYearTF.text = selectedValuefromPicker
            self.currentYeartopass = toYearTF.text!
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        print(toYearTF.text! )
        companyTf.text = workSelectedStr
        cityTf.text = locSelectedStr
    
    }
    @IBOutlet var fromYearBtn: UIButton!
    @IBOutlet var toYearBtn: UIButton!
    @IBAction func toYearTapped(_ sender: Any) {
         hidekeyboard()
        pickerType = "toYear"
        pickerBgView.isHidden = false
        picker.isHidden = false
        picker.selectRow(yearArray.count - 1, inComponent: 0, animated: false)
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y+70)
       
        picker.reloadAllComponents()
    }
    @IBAction func fromYearTapped(_ sender: Any) {
        hidekeyboard()
        pickerType = "FromYear"
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y+70)
        pickerBgView.isHidden = false
        picker.isHidden = false
        picker.selectRow(yearArray.count - 1, inComponent: 0, animated: false)
        picker.reloadAllComponents()
        
        
    }
    //MARK:Add Tapped
   
   
    @IBAction func addTapped(_ sender: Any) {
        
        print(fromTf.text!)
        print(self.currentYeartopass)
      
        if companyTf.text == "" {
            self.showMessage("Please enter company.")
        }
            
        else if designationTf.text == "" {
            self.showMessage("Please enter designation.")
        }
            
        else if fromTf.text == "" || toYearTF.text == "" {
           self.showMessage("Please select work experience duration.")
        }
            
        else if fromTf.text! != "" && self.currentYeartopass != ""{
            if self.currentYeartopass == "Present" {
                let date = Date()
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year, .month, .day], from: date)
                let year =  components.year
                print(year as Any)
                self.currentYeartopass = String(describing: year!)
            }
            
            let aFrom:Int = Int(fromTf.text!)! // firstText is UITextField
            let bto:Int = Int(self.currentYeartopass)!
            print(aFrom)
            print(bto)
            if bto < aFrom
            {
                self.showMessage("To date should be greater than from date.")
            }
            else{
                addBtn.startLoadingAnimation()
                self.activityIndicatorBK.isHidden = false
                self.companyExistanceCheck()
            }
        }
        else{
            addBtn.startLoadingAnimation()
            self.activityIndicatorBK.isHidden = false
            self.companyExistanceCheck()
        }
      
    }
    
    func companyExistanceCheck(){
        
        let queryComp = PFQuery(className: "Companies")
            queryComp.whereKey("company_name", equalTo:companyTf.text!)
            queryComp.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print(objects?.count as Any)
                if (objects?.count != 0) {
                    //company Exists already
                    self.saveNewcompanyObject()
                }
                else{
                    self.saveNewcompanyObject()
                    let compClass = PFObject(className: "Companies")
                     compClass.saveInBackground(block: { (status, error) in
                        print(status)
                        if status ==  true{
                            compClass["company_name"] =  self.companyTf.text!
                            print(objects as Any)
                            for object in objects! {
                                cworkExpWholeobj.add(object)
                            }
                            print(cworkExpWholeobj)
                            self.activityIndicatorBK.isHidden = true
                            self.addBtn.returnToOriginalState()
                            self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                            
                        }
                        else{
                            let message = (error?.localizedDescription)
                            print(message as Any)
                        }
                    })
                }
                  print(objects?.count as Any) // Output console displays "true"
                }
            else{
                self.activityIndicatorBK.isHidden = true
                self.addBtn.returnToOriginalState()
                self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                self.showMessage("Error while adding data.")
                }
                
            })
        

    }
    func saveNewcompanyObject(){
        
        print(currentYeartopass)
        //Update code
        if companyObj != nil{
            let query = PFQuery(className: "User_work_history")
           //  let user = PFUser.current()!
            let userId = companyObj?.value(forKey: "objectId")
            print(companyObj?.value(forKey: "objectId") as Any)
            //  print(PFUser.current()?.objectId!)
            query.whereKey("objectId", equalTo:userId ?? "")
            //here you would just find the results
            query.findObjectsInBackground(block: { (objects, error) in
                if error == nil {
                    print(objects?.count as Any)
                    if (objects?.count != 0) {
                        //company Exists already
                        print(objects as Any)
                        //do this for any columns you want updated
                        for user in objects! {
                            user["company_name"] = self.companyTf.text!
                            user["designation"] = self.designationTf.text!
                            user["location"] = self.cityTf.text!
                            user["description"] = self.descriptionTF.text!
                            user["from_year"] = self.fromTf.text!
                            user["to_year"] = self.currentYeartopass//to save the newly updated user
                            user.saveInBackground(block: { (status, error) in
                                print(status)
                                if status ==  true{
                                    print(cworkExpWholeobj)
                                    print(objects as Any)
                                    for object in objects! {
                                        cworkExpWholeobj.replaceObject(at: self.index, with: object)
                                    }
                                    print(cworkExpWholeobj)
                                   // nestedArray.replaceObject(at: 1, with: [self.companyTf.text!,"A"])
                                     self.activityIndicatorBK.isHidden = true
                                    self.addBtn.returnToOriginalState()
                                    self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                                    self.poptoBackView()
                                }
                                else{
                                    let message = (error?.localizedDescription)
                                    print(message as Any)
                                }
                            })
                        }
                    }
                    print(objects?.count as Any) // Output console displays "true"
        
                }
            })
        }
        else{
            //ADD code
        if let objectID = PFUser.current()?.objectId{
            print(objectID)
            let workClass = PFObject(className: "User_work_history")
            workClass["company_name"] =  companyTf.text!
            workClass["designation"] = designationTf.text!
            workClass["location"] = cityTf.text!
            workClass["description"] = descriptionTF.text!
            workClass["from_year"] = fromTf.text!
            workClass["to_year"] = toYearTF.text!
            workClass["user_id"] = objectID
            workClass.saveInBackground(block: { (status, error) in
                print(status)
                if status ==  true{
                    print(status)
                    print(cworkExpWholeobj)
                    cworkExpWholeobj.add(workClass)
                    //Sort with From Year in decending order
                    let descriptor: NSSortDescriptor = NSSortDescriptor(key: "from_year", ascending: false)
                    let sortedResults = cworkExpWholeobj.sortedArray(using: [descriptor])
                    print(sortedResults)
                    cworkExpWholeobj.removeAllObjects()
                    cworkExpWholeobj = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
                    print("After Sort",cworkExpWholeobj)
                    self.activityIndicatorBK.isHidden = true
                    self.addBtn.returnToOriginalState()
                    self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                    self.poptoBackView()
                }
                else{
                    let message = (error?.localizedDescription)
                    print(message as Any)
                    self.activityIndicatorBK.isHidden = true
                    self.addBtn.returnToOriginalState()
                    self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                }
            })
            
        }
        }
    }
    func poptoBackView(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as! [EditViewController];
        
        for aViewController in viewControllers {
            if(aViewController is EditViewController){
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
    }
    @IBOutlet var currentBtn: UIButton!
    @IBOutlet var toYearTF: MyTextField!
    @IBAction func currentTapped(_ sender: Any) {
        let checkedImage = #imageLiteral(resourceName: "radioSelected")
        let uncheckedImage = UIImage(named: "uncheck")! as UIImage
        currentBtn.isSelected  = !currentBtn.isSelected
    
     
        if (!currentBtn.isSelected)
        {
            NSLog(" Not Selected");
          //  currentBtn.setImage(uncheckedImage, for: .normal)
            img_chk.image = uncheckedImage
            toYearTF.text = currentYeartopass
            
        }
        else
        {
            NSLog(" Selected");
            toYearTF.text = "Present"
            img_chk.image = checkedImage

            //currentBtn.setImage(checkedImage, for: .normal)
               let date = Date()
                let calendar = Calendar.current
                let components = calendar.dateComponents([.year, .month, .day], from: date)
                let year =  components.year
                print(year as Any)
                currentYeartopass = String(describing: year!)
         }
    }
    
    //MARK:-Picker Delegates
    // returns the # of rows in each component..
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return yearArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        selectionRow = row
        print(String(describing: yearArray[row]))
       selectedValuefromPicker = String(describing: yearArray[row])
        return String(describing: yearArray[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectionRow = row
        selectedValuefromPicker = String(describing: yearArray[picker.selectedRow(inComponent: 0)])

    }
    //MARK:-HIde keyboard
    func hidekeyboard(){
         self.view.endEditing(true)
    }
    //MARK:-ADD Companies listing
    @IBOutlet var companyBtn: UIButton!
    @IBAction func companyTapped(_ sender: Any) {
        let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "WorkViewController") as! WorkViewController
        self.present(vc, animated: true, completion: nil)
        
        
    }
    @IBOutlet var cityBtn: UIButton!
    @IBAction func cityTapped(_ sender: Any) {
            let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
            let vc = storyboardObj.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
            self.present(vc, animated: true, completion: nil)
        
    }

}
