//
//  AddEducationViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 23/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse

class AddEducationViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    @IBOutlet var scrollView: UIScrollView!
    var yearArray :NSMutableArray = []
    var selectedValuefromPicker:String = ""
    var selectionRow:Int = 0
    var index:Int = 0
    var pickerType:String = ""
    var schoolObj :PFObject?
    @IBOutlet var cancelBtn: UIButton!
    @IBOutlet var headingLabel: UILabel!
    @IBOutlet var titleLabel : UILabel!
    @IBAction func cancelTapped(_ sender: Any) {
        pickerBgView.isHidden = true
        self.fromTf .resignFirstResponder()
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
        pickerBgView.isHidden=true
        picker.isHidden=true
        
    }
    @IBOutlet var picker: UIPickerView!
    @IBOutlet var doneBtn: UIButton!
    
    @IBAction func doneTapped(_ sender: Any) {
        hidekeyboard()
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: 0)
        self.fromTf .resignFirstResponder()
        //        removeKeyboard()
        pickerBgView.isHidden = true
        picker.isHidden = true
        print(selectedValuefromPicker)
        
        
        if pickerType == "FromYear" {
            
            if toTf.text != "" {
                let aFrom:Int = Int(selectedValuefromPicker) ?? 0 // firstText is UITextField
                let bto:Int = Int(toTf.text ?? "0") ?? 0
                
                if bto < aFrom
                {
                    self.showMessage("To date should be greater than from date.")
                    return
                }
            }
            
            fromTf.text = selectedValuefromPicker
        }
        else{
            if fromTf.text == "" {
                return
            }
            
            let aFrom:Int = Int(fromTf.text ?? "0") ?? 0 // firstText is UITextField
            let bto:Int = Int(selectedValuefromPicker) ?? 0
            
            if bto < aFrom
            {
                self.showMessage("To date should be greater than from date.")
                return
            }
            
            toTf.text = selectedValuefromPicker
        }
    }
    
    
    @IBOutlet var pickerBgView: UIView!
    //MARK:-Add Values
    @IBOutlet var addBtn: TKTransitionSubmitButton!
    
    @IBAction func addTapped(_ sender: Any) {
        
        if schoolTf.text == "" {
            self.showMessage("Please enter school.")
            return
            
        }
            
        else if fromTf.text == "" || toTf.text == "" {
            self.showMessage("Please select education duration.")
            return
        }
            
        else if fromTf.text! != "" && toTf.text! != ""{
            let aFrom:Int? = Int(fromTf.text!) // firstText is UITextField
            let bto:Int? = Int(toTf.text!)
            print(aFrom!)
            print(bto!)
            if bto! < aFrom!
            {
                self.showMessage("To year can't be smaller.")
                return
            }
            else{
                addBtn.startLoadingAnimation()
                self.activityIndicatorBK.isHidden = false
                self.schoolExistanceCheck()
            }
        }
        else{
            addBtn.startLoadingAnimation()
            self.activityIndicatorBK.isHidden = false
            self.schoolExistanceCheck()
        }
    }
    func schoolExistanceCheck(){
        print(schoolTf.text!)
        let queryComp = PFQuery(className: "schools")
        queryComp.whereKey("school_name", equalTo:schoolTf.text!)
        queryComp.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print(objects?.count as Any)
                if (objects?.count != 0) {
                    self.saveNewschoolObject() //already in DB school
                }
                else{
                    self.saveNewschoolObject()
                    let compClass = PFObject(className: "schools")
                    compClass["school_name"] =  self.schoolTf.text!
                    compClass.saveInBackground(block: { (status, error) in
                        print(status)
                        print(self.schoolTf.text!)
                        if status ==  true{
                            print(schollWholeobj)
                            print(self.index)
                            print(objects as Any)
                            for object in objects! {
                                schollWholeobj.add(object)
                            }
                            print(schollWholeobj)
                            self.activityIndicatorBK.isHidden = true
                            self.addBtn.returnToOriginalState()
                            self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                            
                        }
                        else{
                            let message = (error?.localizedDescription)
                            print(message as Any)
                        }
                    })
                }
                print(objects?.count as Any) // Output console displays "true"
            }
            else{
                self.activityIndicatorBK.isHidden = true
                self.addBtn.returnToOriginalState()
                self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                self.showMessage("Error while adding data.")
            }
            
        })
        
    }
    func saveNewschoolObject() {
        print(schoolObj)
        
        if schoolObj != nil{
            let query = PFQuery(className: "User_education")
            //  let user = PFUser.current()!
            let userId = schoolObj?.value(forKey: "objectId")
            print(userId!)
            query.whereKey("objectId", equalTo:userId!)
            //here you would just find the results
            query.findObjectsInBackground(block: { (objects, error) in
                if error == nil {
                    print(objects?.count as Any)
                    if (objects?.count != 0) {
                        //company Exists already
                        print(objects as Any)
                        //do this for any columns you want updated
                        for user in objects! {
                            user["school_name"] = self.schoolTf.text!
                            user["degree"] = self.degreeTf.text!
                            user["field_of_study"] = self.fosTf.text!
                            user["batch_from"] = self.fromTf.text!
                            user["batch_to"] = self.toTf.text!
                            user["description"] = self.descrTf.text!
                            user.saveInBackground(block: { (status, error) in
                                print(status)
                                if status ==  true{
                                    print(schollWholeobj)
                                    print(self.index)
                                    print(objects as Any)
                                    for object in objects! {
                                        schollWholeobj.replaceObject(at: self.index, with: object)
                                    }
                                    print(schollWholeobj)
                                    self.activityIndicatorBK.isHidden = true
                                    self.addBtn.returnToOriginalState()
                                    self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                                    self.poptoBackView()
                                }
                                else{
                                    let message = (error?.localizedDescription)
                                    print(message as Any)
                                }
                            })
                        }
                    }
                }
            })
        }
        else{
            if let objectID = PFUser.current()?.objectId{
                print(objectID)
                let workClass = PFObject(className: "User_education")
                workClass["school_name"] =  schoolTf.text!
                workClass["degree"] = degreeTf.text!
                workClass["field_of_study"] = fosTf.text!
                workClass["batch_from"] = fromTf.text!
                workClass["batch_to"] = toTf.text!
                workClass["description"] = descrTf.text!
                workClass["user_id"] = objectID
                workClass.saveInBackground(block: { (status, error) in
                    print(status)
                    print(schollWholeobj)
                    if status ==  true{
                        print(status)
                        print(workClass)
                        schollWholeobj.add(workClass)
                        
                        //Sort with From Year in decending order
                        let descriptor: NSSortDescriptor = NSSortDescriptor(key: "batch_from", ascending: false)
                        let sortedResults = schollWholeobj.sortedArray(using: [descriptor])
                        print(sortedResults)
                        schollWholeobj.removeAllObjects()
                        schollWholeobj = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
                        print("After Sort education List",schollWholeobj)
                        
                        self.activityIndicatorBK.isHidden = true
                        self.addBtn.returnToOriginalState()
                        self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                        self.poptoBackView()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        print(message as Any)
                        self.activityIndicatorBK.isHidden = true
                        self.addBtn.returnToOriginalState()
                        self.addBtn.layer.cornerRadius = self.addBtn.normalCornerRadius
                    }
                })
                print(schollWholeobj)
                
            }
        }
        
    }
    @IBOutlet var descrTf: MyTextField!
    
    @IBOutlet var toTf: MyTextField!
    @IBOutlet var fromTf: MyTextField!
    @IBOutlet var fosTf: MyTextField!
    @IBOutlet var degreeTf: MyTextField!
    @IBOutlet var schoolTf: MyTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
      
        yearArray = ["2016","2017"]
        // Do any additional setup after loading the view.
        //        self.hideKeyboard()
        print(schoolObj as Any)
        if schoolObj != nil{
            headingLabel.text = "Edit Education"
            titleLabel.text = "    Update Education"
            addBtn.titleLabel?.text = "Update"
            if let compName = (schoolObj as AnyObject).value(forKey: "school_name") as? String{
                print(compName)
                workSelectedStr = compName
            }
            if let degree = (schoolObj as AnyObject).value(forKey: "degree") as? String{
                degreeTf.text = degree
                
            }
            if let fosStr = (schoolObj as AnyObject).value(forKey: "field_of_study") as? String{
                print(fosStr)
                fosTf.text = fosStr
                
            }
            print(schoolObj as Any)
            if let dStr = (schoolObj as AnyObject).object(forKey: "description") as? String{
                print(dStr)
                // print(dStr.value(forKey: "description") as Any)
                descrTf.text = dStr
                
            }
            
            if let fromYear = (schoolObj as AnyObject).value(forKey: "batch_from") as? String{
                fromTf.text = fromYear
            }
            if let toYear = (schoolObj as AnyObject).value(forKey: "batch_to") as? String{
                toTf.text = toYear
            }
            addBtn.setTitle("UPDATE",for: .normal)
        }
        else{
            addBtn.setTitle("ADD",for: .normal)
            workSelectedStr = ""
            locSelectedStr = ""
        }
        
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year, .month, .day], from: date)
        let year =  components.year
        print(year as Any)
        
        
        yearArray.removeAllObjects()
        var startYear = 1967
        let currentVal:Int = year!
        for i in startYear..<currentVal + 1{
            print(i)
            yearArray.add(startYear)
            startYear = startYear + 1
            
        }
        print(yearArray)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        print(workSelectedStr)
        schoolTf.text = workSelectedStr
    }
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-Actions
    @IBOutlet var toYearBtn: UIButton!
    @IBAction func toYearTapped(_ sender: Any) {
        self.hidekeyboard()
        self.toTf .resignFirstResponder()
        pickerType = "toYear"
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y+70)
        pickerBgView.isHidden = false
        picker.selectRow(yearArray.count - 1, inComponent: 0, animated: false)
        picker.isHidden = false
        picker.reloadAllComponents()
        
    }
    @IBOutlet var fromYearBtn: UIButton!
    @IBAction func fromYearTapped(_ sender: Any) {
        self.hidekeyboard()
        pickerType = "FromYear"
        scrollView.contentOffset = CGPoint(x: scrollView.frame.origin.x, y: scrollView.frame.origin.y+70)
        pickerBgView.isHidden = false
        picker.isHidden = false
        picker.selectRow(yearArray.count - 1, inComponent: 0, animated: false)
        picker.reloadAllComponents()
    }
    
    //MARK:-Picker Delegates
    // returns the # of rows in each component..
    //MARK:-Picker Delegates
    // returns the # of rows in each component..
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return yearArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        selectionRow = row
        print(String(describing: yearArray[row]))
        selectedValuefromPicker = String(describing: yearArray[row])
        return String(describing: yearArray[row])
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        selectionRow = row
        selectedValuefromPicker = String(describing: yearArray[picker.selectedRow(inComponent: 0)])
        
    }
    
    //MARK:-HIde keyboard
    func hidekeyboard(){
        self.view.endEditing(true)
    }
    //MARK:-School Tapped
    @IBOutlet var schoolBtn: UIButton!
    @IBAction func schoolTapped(_ sender: Any) {
        let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "WorkViewController") as! WorkViewController
        vc.fromView = "school"
        self.present(vc, animated: true, completion: nil)
        
    }
    func poptoBackView(){
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as! [EditViewController];
        
        for aViewController in viewControllers {
            if(aViewController is EditViewController){
                self.navigationController!.popToViewController(aViewController, animated: true);
            }
        }
    }
}
