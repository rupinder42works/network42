//
//  LocationViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 27/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Alamofire
var locSelectedStr:String = ""
class LocationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var fromView:String = ""
    @IBOutlet var locTableView: UITableView!
    @IBOutlet var emptyLocationView: UIView!
    var  locationArray:NSArray = []
    var locationFlag = false
    @IBOutlet var locationTF: MyTextField!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        locationTF.borderStyle = UITextField.BorderStyle.none
        locationTF.layer.cornerRadius = 5
        locationTF.layer.borderWidth = 1
        locationTF.layer.borderColor = UIColor.clear.cgColor
        locationTF.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        locTableView.register(UINib(nibName: "searchCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
        locTableView.estimatedRowHeight = 50
        locTableView.rowHeight = UITableView.automaticDimension
    }
    
    @IBOutlet var cancelBtn: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        if self.locationFlag == false{
            self.emptyLocationView.isHidden = false
            
        }
        else{
            self.emptyLocationView.isHidden = true

        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-
    @IBAction func cancelTapped(_ sender: Any) {
        locSelectedStr = ""
        self.dismiss(animated: true, completion: nil)
        
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        cancelBtn.isHidden = true
        activityIndicator.isHidden = false
        locSelectedStr = textField.text!
        hitLocationApi(getTextfeild: textField.text! as NSString)
        
    }
    func hitLocationApi(getTextfeild:NSString) {
        self.activityIndicator.startAnimating()
        print(getTextfeild)
        let baseUrl: String = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(getTextfeild)&types=(cities)&key=AIzaSyBa-LT2cQTQwg-xo5OFl7RQ5z0OxOo38as"
        let escapedAddress = baseUrl.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed);
        Alamofire.request(escapedAddress!).responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                print(responseData.result.value!)
                if responseData.result.isSuccess {
                    
                    if let jsonResponseGroups = responseData.result.value as? NSDictionary {
                        ////print(jsonResponseGroups)
                        self.locationArray = []
                        
                        if let res = jsonResponseGroups.object(forKey: "predictions") as? NSArray {
                            if res.count > 0{
                                self.self.locationFlag = true
                                self.locationArray = res
                                self.emptyLocationView.isHidden = true
                                self.activityIndicator.stopAnimating()
                                self.cancelBtn.isHidden = false
                            }
                            else{
                                self.locationFlag = false
                                self.emptyLocationView.isHidden = false
                                self.cancelBtn.isHidden = false
                                self.activityIndicator.isHidden = true
                            }
                        }
                        self.locTableView .reloadData()
                    }
                }
            }
        }
        
    }
    //MARK:Table View Delegates
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return locationArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            as! searchCell
        if let locationName = (self.locationArray[indexPath.row] as AnyObject).value(forKey: "description") as? String {
            cell.label.text  = locationName
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        if let locationName = (self.locationArray[indexPath.row] as AnyObject).value(forKey: "description") as? String {
            locSelectedStr = locationName
            locationTF.resignFirstResponder()
             dismiss(animated: true, completion: nil)
        }
      
    }
}
