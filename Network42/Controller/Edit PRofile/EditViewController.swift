//
//  EditViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 22/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import Firebase


var cworkExpWholeobj:NSMutableArray = []
var schollWholeobj:NSMutableArray = []

class EditViewController: BaseViewController,UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,EditImgDelegate,sectionHeaderActionDelegate,UITextFieldDelegate,updateDelegate,UIViewControllerTransitioningDelegate,updateImageonProfile,rowHeaderActionDelegate{
    
    var statusFlag:Bool = false
    var deleteObjId:String = ""
    var nestedArray:NSMutableArray = [[],[],[],[],[],[],[],[],[]]
    var getobjIdsArray :NSMutableArray = []
    var getSchoolobjIds :NSMutableArray = []
    var workExperienceCompany:NSMutableArray = []
    var schoolArray :NSMutableArray = []
    let imagePicker = UIImagePickerController()
    var uploadFlag:Bool = false
    var pickerTypeStr:String = ""
    var  userimageToSend = UIImage()
    var  bgimageToSend = UIImage()
    @IBOutlet var eprofileAcIndicator: UIActivityIndicatorView!
    var profileFlag:String = ""
    
    var headerTitleArray:NSMutableArray = []
    var placeHolderArray:NSMutableArray = []
    var expandButtonType :NSMutableArray = []
    var valuesArray:NSMutableArray = []
    
    var profileCallback : (() -> Void)?
    var companyObjtoPass:NSMutableArray = []
    var schoolObjtoPass:NSMutableArray = []
    //MARK:LifeCycle
    @IBOutlet var editProfileTable: UITableView!
    
    @IBOutlet var datePicker: UIDatePicker!
    
    @IBOutlet var datePickeBgView: UIView!
    var  selectedDateFromPicker:String = ""
    var rStatusSelected:Bool = false
    var locationSelected:Bool = false
    var currentindexPath: NSIndexPath!
    var currentSection :Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        companyObjtoPass.removeAllObjects()
        for i in 0 ..< cworkExpWholeobj.count {
            companyObjtoPass.add(cworkExpWholeobj[i])
        }
        schoolObjtoPass.removeAllObjects()
        for i in 0 ..< schollWholeobj.count {
            schoolObjtoPass.add(schollWholeobj[i])
        }
//        print(schoolObjtoPass)
//        print(nestedArray)
        
        self.tabBarController?.tabBar.isHidden = true
         editProfileTable.register(UINib(nibName: "EProfileWithImageHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "EProfileWithImageHeaderFooterView")
        //2nd cell
        editProfileTable.register(UINib(nibName: "EProfileTextfeildsTableViewCell", bundle: nil), forCellReuseIdentifier: "cell1")
        editProfileTable.register(UINib(nibName: "EditProfileTFHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "EditProfileTFHeaderFooterView")
        //Expanded Cell
        editProfileTable.register(UINib(nibName: "SectionTableViewCell", bundle: nil), forCellReuseIdentifier: "cell2")
        editProfileTable.register(UINib(nibName: "FooterHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "FooterHeaderFooterView")
        
        editProfileTable.rowHeight = UITableView.automaticDimension
        editProfileTable.estimatedRowHeight = 50
        
//        nestedArray = [[],[],[],[],[],[],[],[],[]]
        headerTitleArray = ["Work Experience","Education","Phone","Bio","Date of Birth","Relationship Status","Location","Website"]
        placeHolderArray = ["Add work experience","Add education","Enter your phone number","Add an intro for yourself","Enter your date of birth","Enter a relationship status","Enter your city","Enter your website"]
        
        expandButtonType = ["+","+","","+","","","",""]
        valuesArray = ["","","","","","","","",""]
        
        self.setDatafromProfile()
       
       
        
    }
    @objc func imageChanged(notification: NSNotification){
        //do stuff
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        print(cworkExpWholeobj)
        print(schollWholeobj)
        self.workExperienceCompany.removeAllObjects()
        self.schoolArray.removeAllObjects()
        self.getobjIdsArray.removeAllObjects()
        self.getSchoolobjIds.removeAllObjects()
        for object in cworkExpWholeobj {
            print(object)
            self.workExperienceCompany.add((object as AnyObject).value(forKey: "company_name")!)
            self.getobjIdsArray.add((object as AnyObject).value(forKey: "objectId")!)
        }
        
        for object in  schollWholeobj{
            print(object)
            self.schoolArray.add((object as AnyObject).value(forKey: "school_name")!)
            self.getSchoolobjIds.add((object as AnyObject).value(forKey: "objectId")!)
        }
        print(self.workExperienceCompany)
        print(self.schoolArray)
        if workExperienceCompany.count > 0{
            nestedArray.replaceObject(at: 1, with: workExperienceCompany)
           editProfileTable.reloadSections(NSIndexSet(index: 1) as IndexSet, with: .none)
        }
       
        if schoolArray.count > 0{
            nestedArray.replaceObject(at: 2, with: schoolArray)
            editProfileTable.reloadSections(NSIndexSet(index: 2) as IndexSet, with: .none)
            
        }
        
        print(nestedArray)
        if rStatusValueStr != ""{
            rStatusSelected = false
            locationSelected = false
            editProfileTable.reloadSections(NSIndexSet(index: 6) as IndexSet, with: .none)
        }
        if locSelectedStr  != ""{
            locationSelected = false
             rStatusSelected = false
            editProfileTable.reloadSections(NSIndexSet(index: 7) as IndexSet, with: .none)
        }
       // editProfileTable.reloadData()
        //self.setDatafromProfile()
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:-TableView Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (nestedArray.object(at: indexPath.section) as AnyObject).count > 0{
        return UITableView.automaticDimension
        }
        else{
            return 0
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return nestedArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(nestedArray)
        currentSection = section
        return (nestedArray.object(at: section) as AnyObject).count - 1
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath as IndexPath) as! SectionTableViewCell
        cell.tag = indexPath.section
        currentindexPath = indexPath as NSIndexPath
        editProfileTable.tag = indexPath.row
        cell.deleteBtn.tag =  indexPath.row
        cell.textLabel?.tag = indexPath.row
        cell.selectionStyle = .none
        cell.delegate = self
        editProfileTable.separatorStyle = .none
        cell.actIndicator.stopAnimating()
        cell.deleteBtn.isHidden =  false
        cell.img_minus.isHidden = false
        if nestedArray.count > 0{
       // print(nestedArray)
        if let objArray = (nestedArray.object(at: indexPath.section) as? NSArray){
           cell.titleLbl.text = objArray.object(at: indexPath.row + 1) as? String
        }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 381
        }else{
            return 65
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        currentSection = section
        if section == 0{
           let cell = self.editProfileTable.dequeueReusableHeaderFooterView(withIdentifier: "EProfileWithImageHeaderFooterView") as! EProfileWithImageHeaderFooterView
            
            cell.nameTf.tag = section
            cell.delegate = self
            cell.editUserBtn.tag = section
            cell.nameTf.delegate = self
            cell.userImgView.layoutIfNeeded()
            cell.userImgView.layer.cornerRadius = cell.userImgView.frame.size.height/2
            //set userimage view border
            cell.userImgView.layer.borderWidth = 4
            cell.userImgView.layer.borderColor = UIColor(red: 255.0/255, green: 255.0/255, blue: 255.0/255, alpha: 1.0).cgColor
            
            ///image taping
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
            cell.backgroundImg.isUserInteractionEnabled = true
            cell.backgroundImg.addGestureRecognizer(tapGestureRecognizer)
            
            let tapGestureRecognizerUser = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizerUser:)))
            cell.userImgView.isUserInteractionEnabled = true
            cell.userImgView.addGestureRecognizer(tapGestureRecognizerUser)
            
            let nameVal =  valuesArray.object(at: section) as? String
            cell.nameTf.text = nameVal?.capitalized

            cell.userImgView.image = userimageToSend
           
            if self.bgimageToSend.size == CGSize.init(width: 0.0, height: 0.0)
            {
                 cell.backgroundImg.image = #imageLiteral(resourceName: "dummy-image-landscape")
            }else{
                 cell.backgroundImg.image = bgimageToSend
            }
            return cell
        }else{
            let cell = self.editProfileTable.dequeueReusableHeaderFooterView(withIdentifier: "EditProfileTFHeaderFooterView") as! EditProfileTFHeaderFooterView
            cell.actIndicator.stopAnimating()
            cell.deleteBtn.isHidden =  false
            cell.img_minus.isHidden = false
            cell.newTf.tag = section
            cell.delegate = self
            cell.addBtn.tag = section
            cell.bgBtn.isHidden = false
            cell.bgBtn.tag = section
            cell.newTf.delegate = self
            cell.imgeVw_downArrowOrcalendar.isHidden = true
            cell.txtVw_bio.delegate = self
            cell.txtVw_bio.tag = section
            if section == 4{
                cell.txtVw_bio.isHidden = false
            }else{
                cell.txtVw_bio.isHidden = true
            }
            cell.titleLbl.text = headerTitleArray.object(at: section-1) as? String
            cell.newTf.placeholder = placeHolderArray.object(at: section-1) as? String
            if !(section == 1||section == 2){
                cell.addBtn.isHidden = true
                if section == 4{
                    cell.txtVw_bio.text = valuesArray.object(at: section) as? String

                }else{
                    if let str = valuesArray.object(at: section) as? String{
                        cell.newTf.text = str
                    }else{
                        cell.newTf.text = ""
                    }
                }
            }
            else{
                //set button title
             //   cell.Add_width.constant = 165
                cell.addBtn.isHidden = false
                print(nestedArray.object(at: section))
                if let objArray = (nestedArray.object(at: section) as? NSArray){
                        if objArray.count > 0{
                            if let val = objArray.object(at: 0) as? String{
                                if val == ""{
                                    cell.deleteicon_height.constant = 0
                                }
                                else{
                                    cell.deleteicon_height.constant = 23
                                }
                                
                            }
                            cell.newTf.text = objArray.object(at: 0) as? String
                        }
                    else
                        {
                            cell.deleteicon_height.constant = 0
                        }
                  cell.addBtn.setTitle(expandButtonType.object(at: section-1) as? String, for: .normal)
            }
            }
            
            //Bg Buttton DOb,RelatioShip,Location
            
            if !(section == 5||section == 6||section == 7){
//                cell.bgButton_height.constant = 0
                cell.bgBtn.isHidden = true
            }
            else{
                print(valuesArray)
                if section ==  5{
                    print(selectedDateFromPicker)
                    if selectedDateFromPicker != ""{
                        cell.newTf.text = selectedDateFromPicker
                        valuesArray.replaceObject(at: section, with: cell.newTf.text!)
                        datePickeBgView.isHidden = true
                        datePicker.isHidden = true
                        
                    }
                    cell.imgeVw_downArrowOrcalendar.isHidden = false
                    cell.imgeVw_downArrowOrcalendar.image = #imageLiteral(resourceName: "calender")
                }
                else if section == 6{
                    cell.imgeVw_downArrowOrcalendar.image = #imageLiteral(resourceName: "down arrow")
                    cell.imgeVw_downArrowOrcalendar.isHidden = false
                    if rStatusValueStr != ""{
                        cell.newTf.text = rStatusValueStr
                        valuesArray.replaceObject(at: section, with: cell.newTf.text!)
                        rStatusSelected = false
                    }else{
                        cell.newTf.text = ""
                    }
                }
                else if section == 7 {
                    cell.imgeVw_downArrowOrcalendar.image = #imageLiteral(resourceName: "down arrow")
                    cell.imgeVw_downArrowOrcalendar.isHidden = false
                    if locSelectedStr != ""{
                        cell.newTf.text = locSelectedStr
                        valuesArray.replaceObject(at: section, with: cell.newTf.text!)
                        locationSelected = false
                    }else{
                         cell.newTf.text = ""
                    }
                 
                }
                else{
                    //textfeild values from Data base
                    let dbValues =  valuesArray.object(at: section) as? String
                    cell.newTf.text = dbValues
                }
    
            }
            //work experience update work
             if section == 1 || section == 2{
//                cell.bgButton_height.constant = 70
                cell.bgBtn.isHidden = false
              }
            if section != 4{
                if cell.newTf.text?.isEmpty == false{
                    cell.deleteBtn.isHidden = false
                    cell.img_minus.isHidden = false
                }else{
                    cell.deleteBtn.isHidden = true
                    cell.img_minus.isHidden = true
                }
            }else{
                if cell.txtVw_bio.text?.isEmpty == false{
                    cell.deleteBtn.isHidden = false
                    cell.img_minus.isHidden = false
                }else{
                    cell.deleteBtn.isHidden = true
                    cell.img_minus.isHidden = true
                }
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
        print(cworkExpWholeobj)
            if cworkExpWholeobj.count > 0{
                let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                let vc = settingsSB.instantiateViewController(withIdentifier: "AddWorkExperienceViewController") as! AddWorkExperienceViewController
                //            vc.wholeCompObj = cworkExpWholeobj
                vc.index = indexPath.row + 1
                vc.companyObj = cworkExpWholeobj[indexPath.row + 1] as? PFObject
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if indexPath.section == 2{
            print(indexPath.row + 1)
            if schollWholeobj.count > 0{
                let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                let vc = settingsSB.instantiateViewController(withIdentifier: "AddEducationViewController") as! AddEducationViewController
                vc.index = indexPath.row + 1
                vc.schoolObj = schollWholeobj[indexPath.row + 1] as? PFObject
                self.navigationController?.pushViewController(vc, animated: true)
            }

        }
    }
    func  tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 8 {
            return 100
        }
        else{
            return CGFloat.leastNormalMagnitude
        }
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
         let cell = self.editProfileTable.dequeueReusableHeaderFooterView(withIdentifier: "FooterHeaderFooterView") as! FooterHeaderFooterView
        cell.delegate = self
        return cell
    }
    //MARK:-Delegate
    //Image Picker
    
    func editImgClickedDelegate(_ index:Int,cell:EProfileWithImageHeaderFooterView,type: String){
        
        pickerTypeStr = type
        cameraAuthorization()
    }
    func cameraAuthorization(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func chooseSheet(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            
        }
        
    }
    func openGallary() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertClose(_ gesture: UITapGestureRecognizer) {
        //        if imageEditBtnFlag == true {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    func alertClose1(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            eprofileAcIndicator.startAnimating()
            uploadFlag = true
            if pickerTypeStr == "user"{
                
                let avatar = PFFile(data: pickedImage.jpegData(compressionQuality: 0.5)!)
                
                self.userimageToSend = pickedImage
                
                PFUser.current()!.setObject(avatar!, forKey: "profile_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    print(status)
                    if status ==  true {                        
                        self.userimageToSend = pickedImage
                        self.uploadFlag = true
                        self.eprofileAcIndicator.stopAnimating()
                        self.addImageAPI()
                        self.editProfileTable.reloadData()
                        self.profileCallback?()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                })
                
            }else
            {
                let avatar = PFFile(data: pickedImage.jpegData(compressionQuality: 0.5)!)
                
                self.bgimageToSend = pickedImage
                
                PFUser.current()!.setObject(avatar!, forKey: "cover_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    print(status)
                    if status ==  true {
                        self.eprofileAcIndicator.stopAnimating()
                        self.uploadFlag = true
                        self.bgimageToSend = pickedImage
                        self.editProfileTable.reloadData()
                        self.profileCallback?()
                    } else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                    
                })
            }
        }
         editProfileTable.reloadData()
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func addImageAPI(){
        //        let chatDictionary1:NSDictionary = [:]
        var urlString: String = ""
        
        let imageData:NSData = self.userimageToSend.mediumQualityJPEGNSData as NSData
        // Create a reference to the file you want to upload
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        let  timeStrToPass = formatter.string(from: Date())
        
        let riversRef = FIRStorage.storage().reference().child("images/\(timeStrToPass).jpg")
        let metadata = FIRStorageMetadata()
        metadata.contentType = "image/jpeg"
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = riversRef.put(imageData as Data, metadata: metadata) { (metadata, error) in
            
        }
        
        uploadTask.observe(.success) { snapshot in
            print(snapshot) // Upload completed successfully
            let downloadURL = snapshot.metadata?.downloadURL()
            print(downloadURL!)
            urlString = String(describing: downloadURL!)
            
            if urlString != ""{
                let DBref = FIRDatabase.database().reference()
                //Sender data for group
                
                let userId = PFUser.current()!.objectId!
                
                let userDictionary = ["user_img":urlString] as [String : Any]
                let myRef = DBref.child("users/\(userId)")
                myRef.updateChildValues(userDictionary) { (error, DBref) in
                    if error == nil{
                        print("error == ",error)
                    }
                }
            }
            
            
        }
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func sectionActionClicked(cell:EditProfileTFHeaderFooterView){
        
               print(cell.addBtn.tag)
                print((nestedArray.object(at: cell.addBtn.tag) as AnyObject))
        let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        if cell.addBtn.tag == 1{
            //Add Work Experience
            
            let vc = settingsSB.instantiateViewController(withIdentifier: "AddWorkExperienceViewController") as! AddWorkExperienceViewController
            vc.index = cell.addBtn.tag
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else if cell.addBtn.tag == 2{
            let vc = settingsSB.instantiateViewController(withIdentifier: "AddEducationViewController") as! AddEducationViewController
            vc.index = cell.addBtn.tag
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        
        
    }
    //MARK:-Settings Tapped
    @IBAction func settingsTapped(_ sender: Any) {
        let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = settingsSB.instantiateViewController(withIdentifier: "SettingsViewController")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:-DOB CLICKED
    func  otherScreenClicked(cell:EditProfileTFHeaderFooterView,type:String){
        // for DOb
        cell.newTf.resignFirstResponder()
        if cell.bgBtn.tag == 5{
            datePickeBgView.isHidden = false
            datePicker.isHidden = false
            
            let currentDate: NSDate = NSDate()
            
            let calendar: NSCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
            // let calendar: NSCalendar = NSCalendar.currentCalendar()
            calendar.timeZone = NSTimeZone(name: "UTC")! as TimeZone
            
            let components: NSDateComponents = NSDateComponents()
            components.calendar = calendar as Calendar
            
            components.year = -12
            let maxDate: NSDate = calendar.date(byAdding: components as DateComponents, to: currentDate as Date, options: NSCalendar.Options(rawValue: 0))! as NSDate
            
            datePicker.maximumDate = Date()
            
            components.day = 01
            components.month = 01
            components.year = 1967
            
            datePicker.minimumDate = calendar.date(from: components as DateComponents)
            
            print("maxDate: \(maxDate)")
            locationSelected = false
            
        }
            // for Relation Status
        else if cell.bgBtn.tag == 6{
            rStatusSelected = true
            locationSelected = false
            let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
            let vc = storyboardObj.instantiateViewController(withIdentifier: "RelationShipStatusViewController") as! RelationShipStatusViewController
            self.present(vc, animated: true, completion: nil)
            editProfileTable.reloadSections(NSIndexSet(index: 6) as IndexSet, with: .none)
        }
            // for Location Status
        else if cell.bgBtn.tag == 7{
            locationSelected = true
            rStatusSelected = false
            let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
            let vc = storyboardObj.instantiateViewController(withIdentifier: "LocationViewController") as! LocationViewController
            self.present(vc, animated: true, completion: nil)
            
            editProfileTable.reloadSections(NSIndexSet(index: 7) as IndexSet, with: .none)
        }
        else if cell.bgBtn.tag == 1{
            print(cworkExpWholeobj)
            if cworkExpWholeobj.count > 0{
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "AddWorkExperienceViewController") as! AddWorkExperienceViewController
//            vc.wholeCompObj = cworkExpWholeobj
            vc.index = 0
            vc.companyObj = cworkExpWholeobj[0] as? PFObject
            self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        else if cell.bgBtn.tag == 2{
            
            print(schollWholeobj)
            if schollWholeobj.count > 0{
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "AddEducationViewController") as! AddEducationViewController
            vc.index = 0
            vc.schoolObj = schollWholeobj[0] as? PFObject
            self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    //MARK:-Picker Delegates
    @IBOutlet var doneBtn: UIButton!
    @IBAction func doneTapped(_ sender: Any) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        let strDate = dateFormatter.string(from: datePicker.date)
        selectedDateFromPicker = strDate
        print(selectedDateFromPicker)
        editProfileTable.reloadSections(NSIndexSet(index: 5) as IndexSet, with: .none)
        
    }
    @IBOutlet var cancelBtn: UIButton!
    @IBAction func cancelTapped(_ sender: Any) {
        locationSelected = false
        datePickeBgView.isHidden = true
        datePicker.isHidden = true
    }
    //MARK:-back Tapped
    @IBAction func backTapped(_ sender: Any) {
        let currentUser = PFUser.current()
        if let pstatus = currentUser?.object(forKey: "profile_updated") as? String{
            print("Updated user",currentUser!)
            print("values are",pstatus)
            if pstatus == "1"{
                
            NotificationCenter.default.post(name: Notification.Name("ImageNotify"), object: nil)
            }
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    //Txtfeild delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {    //delegate method
        print(textField.text!)
        if textField.tag == 3{
            textField.keyboardType = UIKeyboardType.phonePad
        }
        
    }
    var replacedIndex :Int = 0
    func textFieldDidEndEditing(_ textField: UITextField) {
        valuesArray.replaceObject(at: textField.tag, with: textField.text!)
        editProfileTable.reloadSections(NSIndexSet(index: textField.tag) as IndexSet, with: .none)

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 4 {
            if (textField.text?.length)! > 140 && string != "" {
                return false
            }
            return true
        }
        
        return true
    }
    
    
    //MARK:-Update Clicked
    func updateDelegate(cell:FooterHeaderFooterView) {
       if valuesArray.object(at: 8) as? String != "" && !(validateUrl(urlString: (valuesArray.object(at: 8) as? NSString)!)){
                 self.showMessage("Enter valid website url.")
        } else{
            cell.updateBtn.startLoadingAnimation()
        print("values Array as:",valuesArray)
        if let currentUser = PFUser.current(){
            currentUser["name"] = valuesArray.object(at: 0)
            currentUser["mobile_number"] = valuesArray.object(at: 3)
            currentUser["bio"] = valuesArray.object(at: 4)
            currentUser["dob"] = valuesArray.object(at: 5)
            currentUser["relationship_status"] = valuesArray.object(at: 6)
            currentUser["location"] = valuesArray.object(at: 7)
            currentUser["website"] = valuesArray.object(at: 8)
            currentUser["profile_updated"] = "1"

            nestedprofileArray.replaceObject(at: 0, with: [valuesArray.object(at: 1),"A"])
            nestedprofileArray.replaceObject(at: 1, with: [valuesArray.object(at: 2),"B"])
            nestedprofileArray.replaceObject(at: 2, with: valuesArray.object(at: 8))
            nestedprofileArray.replaceObject(at: 3, with: valuesArray.object(at: 3))
            nestedprofileArray.replaceObject(at: 4, with: valuesArray.object(at: 6))
            nestedprofileArray.replaceObject(at: 5, with: valuesArray.object(at: 5))
            //set other fields the same way....
            //currentUser.saveInBackground()
            
            currentUser.saveInBackground(block: { (status, error) in
                cell.updateBtn.returnToOriginalState()
                cell.updateBtn.layer.cornerRadius = cell.updateBtn.normalCornerRadius
                self.showMessage(messages.profileAlertMsg)
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as! [ProfileViewController];
                
                for aViewController in viewControllers {
                    if(aViewController is ProfileViewController){
                        self.navigationController!.popToViewController(aViewController, animated: true);
                    }
                }
            })
       }
        }
        
    }
    //MARK:-Validation fro website
    func validateUrl (urlString: NSString) -> Bool {
        let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
    //MARK:-Delete row
    func deleteSectionActionClicked(cell:EditProfileTFHeaderFooterView){
        
        let alert = UIAlertController(title: "Are you sure you want to delete?", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                print(cell.newTf.text!)
                cell.newTf.text = ""
                cell.deleteicon_height.constant = 0
                cell.actIndicator.startAnimating()
                cell.img_minus.isHidden = true
                cell.deleteBtn.isHidden =  true
                var objectidtodelete :String = ""
                if cell.newTf.tag == 1{
                   objectidtodelete = self.getobjIdsArray.object(at: self.editProfileTable.tag) as! String
                    self.deleteEntry(row:self.editProfileTable.tag,sec:cell.newTf.tag ,type: "sectionType",classType: "User_work_history",idtodel: objectidtodelete)
                }
                else if cell.newTf.tag == 2{
                   objectidtodelete = self.getSchoolobjIds.object(at: self.editProfileTable.tag) as! String
                    self.deleteEntry(row:self.editProfileTable.tag,sec:cell.newTf.tag,type: "sectionType",classType: "User_education",idtodel: objectidtodelete)
                   // self.deleteEducation(cellTFtag: cell.newTf.tag-1,type: "sectionType",classType:"Edu")
                }else{
                    self.valuesArray.replaceObject(at: cell.newTf.tag, with: "")
                    if cell.newTf.tag == 6{
                        rStatusValueStr = ""
                        if let str = UserDefaults.standard.value(forKey: "indexKey") as? Int{
                            UserDefaults.standard.removeObject(forKey: "indexKey")
                            UserDefaults.standard.synchronize()
                        }
                    }else if cell.newTf.tag == 7{
                        locSelectedStr = ""
                    }
                    
                    self.editProfileTable.reloadData()
                   // self.editProfileTable.reloadSections(NSIndexSet(index:cell.newTf.tag) as IndexSet, with: .none)
                    //if let cell = self.editProfileTable
                }
              // self.editProfileTable.reloadSections(NSIndexSet(index:cell.newTf.tag) as IndexSet, with: .none)
                print(self.nestedArray)
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
            }
        }))
     
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK:-Delete Row
     func deleterowActionClicked(cell:SectionTableViewCell,tag:Int){
        print(tag)
        print(cell.textLabel?.tag as Any)
        print(getSchoolobjIds)
        let uiAlert = UIAlertController(title: "Are you sure you want to delete?", message: "", preferredStyle: UIAlertController.Style.alert)
        self.present(uiAlert, animated: true, completion: nil)
        uiAlert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
            cell.actIndicator.startAnimating()
            cell.deleteBtn.isHidden = true
            cell.img_minus.isHidden = true
            var objectidtodelete :String = ""
            if tag == 1{
                objectidtodelete = self.getobjIdsArray.object(at: (((cell.textLabel?.tag)! + 1))) as! String
                self.deleteEntry(row:(cell.textLabel?.tag)!,sec:tag,type: "rowType",classType: "User_work_history",idtodel: objectidtodelete)
            }
            else if tag == 2{
              objectidtodelete = self.getSchoolobjIds.object(at: (cell.textLabel?.tag)! + 1) as! String
                self.deleteEntry(row:(cell.textLabel?.tag)!,sec:tag,type: "rowType",classType: "User_education",idtodel: objectidtodelete)
            }
        }))
        uiAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            print("Click of cancel button")
        }))
      }
    
    func deleteEntry(row:Int,sec:Int,type:String,classType:String,idtodel:String){

        let query = PFQuery(className:classType)
        query.whereKey("objectId", equalTo:idtodel)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print("deletion",objects!)
                for objectt in objects! {
                    objectt.deleteInBackground(block: { (status, error) in
                        print(status)
                        if status == true{
                            if type == "sectionType"{
                                (self.nestedArray.object(at: sec) as AnyObject).replaceObject(at: 0, with: "")
                                
                            }else{
                                (self.nestedArray.object(at: sec) as AnyObject).removeObject(at:(row)+1)
                            }
                            if sec == 1{
                                self.getWorkExperience()
                            }
                            else if sec == 2{
                                self.getSchoolNameList()
                            }
                            self.editProfileTable.reloadData()
                        }
                    })
                    }
                print(self.statusFlag)
            }
            else {
                print(error!)
            }
        })
  }
    func setDatafromProfile(){
        let currentUser = PFUser.current()
        if let pstatus = currentUser?.object(forKey: "profile_updated") as? String{
            print("Updated user",currentUser!)
            print("values are",pstatus)
            self.profileFlag = pstatus
        }
        if let pImg = currentUser?.object(forKey: "profile_image"){
            if let userPicture = pImg as? PFFile {
                userPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                    let image = UIImage(data: imageData!)
                    if image != nil {
                        print("image value",image!)
                        self.uploadFlag = true
                    }
                    else{
                        print("emptyImage")
                        self.uploadFlag = false
                    }
                })
            }
        }
        else{
            uploadFlag = false
        }
        if let pImg = currentUser?.object(forKey: "cover_image"){
            if let coverPicture = pImg as? PFFile {
                coverPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                    let image = UIImage(data: imageData!)
                    if image != nil {
                        print("image value",image!)
                        self.uploadFlag = true
                    }
                    else{
                        print("emptyImage")
                        self.uploadFlag = false
                    }
                })
            }
        }
        else{
            uploadFlag = false
        }
        if let nameStr = currentUser?.object(forKey: "name") as? String{
            print(nameStr)
            valuesArray.replaceObject(at: 0, with: nameStr)
        }
        if let phoneStr = currentUser?.object(forKey: "mobile_number") as? String{
            print(phoneStr)
            valuesArray.replaceObject(at: 3, with: phoneStr)
        }
        if let bioStr = currentUser?.object(forKey: "bio") as? String{
            print(bioStr)
            valuesArray.replaceObject(at: 4, with: bioStr)
        }
        if let dobStr = currentUser?.object(forKey: "dob") as? String{
            print(dobStr)
            valuesArray.replaceObject(at: 5, with: dobStr)
        }
        if let rStr = currentUser?.object(forKey: "relationship_status") as? String{
            print(rStr)
            valuesArray.replaceObject(at: 6, with: rStr)
        }
        if let locStr = currentUser?.object(forKey: "location") as? String{
            print(locStr)
            valuesArray.replaceObject(at: 7, with: locStr)
        }
        if let webStr = currentUser?.object(forKey: "website") as? String{
            print(webStr)
            valuesArray.replaceObject(at: 8, with: webStr)
        }
        
        // Define identifier
        print(valuesArray)
    }
    //MARK:-Get Work Experience
    func getWorkExperience(){
        //Get work experience from User_work_history table
    
        cworkExpWholeobj.removeAllObjects()
        let objId = PFUser.current()?.objectId
        let query = PFQuery(className:"User_work_history")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                for object in objects! {
                    
                    print(object)
                    print(object.value(forKey: "description")!)
                    print(object.objectId! as String)
                    cworkExpWholeobj.add(object)
                 }
                self.editProfileTable.reloadData()
            }
            else {
                print(error!)
            }
           
        })
        
    }
    //MARK:-Fetch school data
    func getSchoolNameList(){
       
        schollWholeobj.removeAllObjects()
        let objId = PFUser.current()?.objectId
        let query = PFQuery(className:"User_education")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                for object in objects! {
                    schollWholeobj.add(object)
                }
                self.editProfileTable.reloadData()
            } else {
                print(error!)
                
            }
        })
    }
    //MARK:-Back Image
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        print(tappedImage)
        if !(tappedImage.image?.size)!.equalTo(CGSize.zero)
        {
       
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "FullScreenImgViewController") as! FullScreenImgViewController
            vc.bgimageToSend = tappedImage.image!
            vc.pickerTypeStr = "background"
            vc.userProfile = true
            vc.profileCallback = {
                self.getImagesFromDb()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        // Your action
    }
    //MARK:-User Image Tapped
    @objc func imageTapped(tapGestureRecognizerUser: UITapGestureRecognizer)
    {
        
        let tappedImage = tapGestureRecognizerUser.view as! UIImageView
        if !(tappedImage.image?.size)!.equalTo(CGSize.zero)
        {
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "FullScreenImgViewController") as! FullScreenImgViewController
            vc.bgimageToSend = tappedImage.image!
            vc.pickerTypeStr = "user"
            vc.userProfile = true
            vc.profileCallback = {
                self.getImagesFromDb()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        // Your action
    }
    
    
    func getImagesFromDb(){
         let currentUser = PFUser.current()
        
        if let userPicture = currentUser?["profile_image"] as? PFFile {
            userPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                let image = UIImage(data: imageData!)
                if image != nil {
                    //print("image value",image!)
                    self.userimageToSend = (image)!
                    self.uploadFlag = true
                    DispatchQueue.main.async {
                        self.editProfileTable.reloadData()
                    }
                }
            })
        }
        
        if let coverPicture = currentUser?["cover_image"] as? PFFile {
            coverPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                let image = UIImage(data: imageData!)
                if image != nil {
                    //print("image value",image!)
                    self.uploadFlag = true
                    self.bgimageToSend = (image)!
                    DispatchQueue.main.async {
                        self.editProfileTable.reloadData()
                    }
                }
            })
            
        }
    }
}

extension EditViewController : UITextViewDelegate{
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.resignFirstResponder()
        valuesArray.replaceObject(at: textView.tag, with: textView.text!)
        editProfileTable.reloadSections(NSIndexSet(index: textView.tag) as IndexSet, with: .none)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        textView.becomeFirstResponder()
    }
}
