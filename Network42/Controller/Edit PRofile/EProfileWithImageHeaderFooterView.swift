//
//  EProfileWithImageHeaderFooterView.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 27/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol EditImgDelegate {
    @objc optional func editImgClickedDelegate(_ index:Int,cell:EProfileWithImageHeaderFooterView,type:String)
}
class EProfileWithImageHeaderFooterView: UITableViewHeaderFooterView,UITextFieldDelegate{

    weak var delegate: EditImgDelegate?
    
    @IBOutlet weak var lbl_uploadPhoto: UILabel!
    @IBOutlet var userImgView: UIImageView!
    
    @IBOutlet var editUserBtn: UIButton!
    @IBOutlet var backgroundImg: UIImageView!
    @IBOutlet weak var nameTf: UITextField!
    @IBOutlet var editBgImageBtn: UIButton!
    
    @IBAction func editBgImageTapped(_ sender: Any) {
        self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "background")
    }
    
    @IBAction func editUserImageTapped(_ sender: Any) {
         self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "user")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
