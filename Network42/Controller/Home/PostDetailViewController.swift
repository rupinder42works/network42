//
//  PostDetailViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 12/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import MMPlayerView
import AVFoundation

class PostDetailViewController: BaseViewController {
    
    var following = false
    @IBOutlet weak var vw_commentContainer: UIView!
    //MARK:- Variables
    var postId = ""
    var detailViewModel: DetailViewModel!
    lazy var mmPlayerLayer: MMPlayerLayer = {
        let l = MMPlayerLayer()
        l.cacheType = .memory(count: 5)
        l.coverFitType = .fitToPlayerView
        l.videoGravity = AVLayerVideoGravity.resizeAspectFill
        l.replace(cover: CoverA.instantiateFromNib())
        return l
    }()
    var postInfoCell = PostInfoCell()
    
    
    //MARK:- IBOutlet
    @IBOutlet var commentBtn: TKTransitionSubmitButton!
    @IBOutlet weak var textViewComment: RSKPlaceholderTextView!
    @IBOutlet weak var tablePostDetail: UITableView?{
        didSet {
            tablePostDetail?.register(UINib(nibName: "PostInfoCell", bundle: nil), forCellReuseIdentifier: "PostInfoCell")
            tablePostDetail?.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        }
    }
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        vw_commentContainer.layer.borderColor = UIColor.lightGray.cgColor
        self.mmPlayerLayer.autoHideCoverType = .disable
        self.mmPlayerLayer.mmDelegate = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(PostDetailViewController.pushNotifymethod(notification:)), name:NSNotification.Name(rawValue: "postDetailController"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        detailViewModel = DetailViewModel(postId: postId)
        
        detailViewModel.showAlertClosure = {
            if let alert = self.detailViewModel.alertMessage {
                self.showMessage(alert)
            }
        }
        
        detailViewModel.updateLoadingStatus = {
            if self.detailViewModel.isLoading {
                self.showLoader(self)
            } else {
                self.hideLoader(self)
            }
        }
        detailViewModel.reloadTableViewClosure = {
            self.tablePostDetail?.reloadData()
        }
        
        detailViewModel.getPost()
        self.logController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        postInfoCell.mmPlayerLayer?.playView = nil
        postInfoCell.mmPlayerLayer?.player?.isMuted = true
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "postDetailController"), object: nil)
    }
    
    
    
    //MARK:- PUSH Functions
    @objc func pushNotifymethod(notification: NSNotification) {
        print(notification)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func commentTap(){
        
    }
    
    //MARK:- IBAction
    @IBAction func postComment() {
        if textViewComment.text.trim() == "" {
            detailViewModel.alertMessage = messages.noComment
            return
        }
        commentBtn.startLoadingAnimation()
        commentBtn.isSelected = true
        detailViewModel.sendComment(textViewComment.text) { status in
            DispatchQueue.main.async {
                self.commentBtn.returnToOriginalState()
                self.commentBtn.isSelected = false
                if status {
                    self.textViewComment.text = ""
                    self.textViewComment.endEditing(true)
                }
            }
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteComment(_ indexObj:Int) {
        let alert = UIAlertController(title: "Are you sure you want to delete this comment?", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
            self.detailViewModel.deleteComment(indexObj, completed: { status in
                
            })
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func likeListing(sender:UIButton){
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "LikesViewController") as! LikesViewController
        if let obj = detailViewModel.array[sender.tag] as? Post {
            vc.postId = obj.id
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //
    @objc func followClicked(sender:UIButton){
        if let cell = self.tablePostDetail?.cellForRow(at: IndexPath.init(row: sender.tag, section: 0)) as? PostInfoCell{
            cell.delegate?.followClickedDelegate!(index: sender.tag, cell: cell)
        }
    }
    
    @objc func openProfile(sender:UIButton){
        let vc = UIStoryboard(name: "TabBarSB", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        if  following == true{
            vc.friend = "FOLLOWING"
        }
        else{
            vc.friend = "FOLLOW"
        }
        
        if let obj = detailViewModel.array[sender.tag] as? Post {
            if let user = obj.postedBy as? User{
                vc.objectId = user.id ?? ""
            }
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func checkWhetherUserFollowed(currentUserId:String,otherUserId:String,tag:Int){
        let query = PFQuery(className: "User_follow")
        query.whereKey("followed_id", equalTo:otherUserId)
        query.whereKey("follower_id", equalTo:currentUserId)
        query.whereKey("status", equalTo: "1")
        query.findObjectsInBackground(block: { (objects, error) in
            
            if error == nil {
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    print(object)
                    PFObjectArr.add(object)
                }
                
                if PFObjectArr.count>0
                {
                    if let cell = self.tablePostDetail?.cellForRow(at: IndexPath.init(row: tag, section: 0)) as? PostInfoCell{
                        cell.btn_follow.isHidden = false
                        self.following = true
                        cell.btn_follow.setTitle("Following", for: .normal)
                        cell.btn_follow.isHidden = false
                        cell.btn_follow.layer.borderColor = UIColor.white.cgColor
                        cell.btn_follow.layer.borderWidth = 0
                        cell.btn_follow.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1)
                        cell.btn_follow.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                    }
                }else
                {
                    if let cell = self.tablePostDetail?.cellForRow(at: IndexPath.init(row: tag, section: 0)) as? PostInfoCell{
                        self.following = false
                        cell.btn_follow.setTitle("Follow", for: .normal)
                        cell.btn_follow.isHidden = false
                        cell.btn_follow.layer.borderColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1).cgColor
                        cell.btn_follow.layer.borderWidth = 1.2
                        cell.btn_follow.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        cell.btn_follow.setTitleColor(#colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1), for: .normal)
                        cell.btn_follow.isHidden = false
                        
                    }
                }
            }
            else {
                print(error!)
            }
        })
    }
}




extension PostDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detailViewModel.array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            if let obj = detailViewModel.array[indexPath.row] as? Post{
                postInfoCell = tableView.dequeueReusableCell(withIdentifier: "PostInfoCell", for: indexPath as IndexPath) as! PostInfoCell
                postInfoCell.btn_profile.tag = indexPath.row
                postInfoCell.btn_name.tag = indexPath.row
                postInfoCell.mmPlayerLayer?.playView = nil
                postInfoCell.mmPlayerLayer = self.mmPlayerLayer
                postInfoCell.config(obj)
                var otherUserid = ""
                if let user = obj.postedBy as? User{
                    otherUserid = user.id ?? ""
                }
                let currentUserId = PFUser.current()?.objectId ?? ""
            checkWhetherUserFollowed(currentUserId:currentUserId,otherUserId:otherUserid,tag:indexPath.row)
                postInfoCell.imageProfile.layoutIfNeeded()
                postInfoCell.imageProfile.layer.cornerRadius = postInfoCell.imageProfile.frame.size.height/2
                postInfoCell.vw_profileBackground.layer.borderColor = UIColor.init(red: 205/255, green: 205/255, blue: 205/255, alpha: 0.8).cgColor
                postInfoCell.likeClosure = { (isLike, count) in
                    self.detailViewModel.likeUpdate(isLike, count: count)
                }
                postInfoCell.delegate = self
                postInfoCell.btn_follow.tag = indexPath.row
                postInfoCell.btn_follow.addTarget(self, action: #selector(followClicked(sender:)), for: .touchUpInside)
                postInfoCell.btn_likeCount.tag = indexPath.row
                postInfoCell.btn_likeCount.addTarget(self, action: #selector(likeListing(sender:)), for: .touchUpInside)
                postInfoCell.btn_profile.addTarget(self, action: #selector(openProfile(sender:)), for: .touchUpInside)
                postInfoCell.btn_name.addTarget(self, action: #selector(openProfile(sender:)), for: .touchUpInside)
                return postInfoCell
            }
            else{
                return UITableViewCell()
            }
            
        } else {
            if let obj = detailViewModel.array[indexPath.row] as? Comment {
                let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath as IndexPath) as! CommentCell
                cell.config(obj)
                return cell
            }
        }
        
        return UITableViewCell()
    }
}

extension PostDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let obj = detailViewModel.array?[indexPath.row] as? Comment, obj.user?.id == PFUser.current()?.objectId {
            self.deleteComment(indexPath.row)
        }
    }
}

extension PostDetailViewController : MMPlayerLayerProtocol{
    func touchInVideoRect(contain: Bool){
        
        if mmPlayerLayer.player?.isMuted == true{
            mmPlayerLayer.player?.isMuted = false
        }
        else{
            mmPlayerLayer.player?.isMuted = true
        }
    }
}


extension PostDetailViewController : userFollowDelegate{
    
    func followClickedDelegate(index:Int,cell:PostInfoCell)
    {
        print("==",index)
        
        if cell.btn_follow.titleLabel?.text == "Follow"
        {
            cell.indicatorView.startAnimating()
            cell.indicatorView.isHidden = false
            cell.btn_follow.setTitle("", for: .normal)
            cell.isUserInteractionEnabled = false
            let objId = PFUser.current()?.objectId ?? ""
            let User_followClass = PFObject(className: "User_follow")
            User_followClass["follower_id"] = objId
            if let  obj =  detailViewModel.array[index] as? Post{
                var otherId = ""
                if let user = obj.postedBy as? User{
                    otherId = user.id ?? ""
                }
                User_followClass["followed_id"] = otherId
                User_followClass["parent_following"] = obj.user
            }
            User_followClass["parent_follower"] = PFUser.current()
            
            User_followClass["status"] = "1"
            
            User_followClass.saveInBackground { (success, error) in
                cell.indicatorView.stopAnimating()
                cell.indicatorView.isHidden = true
                cell.isUserInteractionEnabled = true
                if(success)
                {
                    print("user follow")
                    // self.getIds()
                    self.following = true
                    cell.btn_follow.setTitle("Following", for: .normal)
                    cell.btn_follow.isHidden = false
                    cell.btn_follow.layer.borderColor = UIColor.white.cgColor
                    cell.btn_follow.layer.borderWidth = 0
                    cell.btn_follow.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1)
                    cell.btn_follow.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                    // let user = self.PFObjectArray.object(at: index) as? PFUser
                    var user : PFUser?
                    
                    if let  obj =   self.detailViewModel.array[index] as? Post{
                        if let user1 = obj.user as? PFUser{
                                user  = user1
                        }
                    }
                    let currentName = PFUser.current()?.value(forKey: "name") as! String
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                    let time = formatter.string(from: Date())
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                    
                    self.saveToNotifications(from_id: objId, to_id: user!.objectId!, notification_type: "follow_public_user", time: time, post_id: "", parent_from: PFUser.current()!, parent_to: user!,commentStr: "")
                    
                    self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.startedfollowing)",type:"follow_public_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                    
                }
                else{
                    print("error",error!)
                    cell.indicatorView.stopAnimating()
                    cell.indicatorView.isHidden = true
                    cell.btn_follow.setTitle("Follow", for: .normal)
                    let message = (error?.localizedDescription)
                    self.showMessage(message!)
                    return
                }
            }
        }else
        {
            var alert = UIAlertController()
            if cell.btn_follow.titleLabel?.text == "Following"
            {
                var name = ""
                if let t =  self.detailViewModel.array[index] as? Post {
                    if let user = t.postedBy as? User{
                        name = user.name ?? ""
                    }
                }
                alert = UIAlertController(title: "Are you sure you want to unfollow \(name)?", message: "", preferredStyle: UIAlertController.Style.alert)
            }else
            {
                alert = UIAlertController(title: "Are you sure you want to cancel this request?", message: "", preferredStyle: UIAlertController.Style.alert)
            }
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
                switch action.style{
                case .default:
                    self.following = false
                    
                    
                     cell.indicatorView.startAnimating()
                    cell.indicatorView.isHidden = false
                    // cell.isUserInteractionEnabled = false
                    cell.btn_follow.setTitle("", for: .normal)
                    
                    let loggedIn = PFUser.current()?.objectId ?? ""
                    let query = PFQuery(className:"User_follow")
                    query.whereKey("follower_id", equalTo: loggedIn)
                    if let  obj =   self.detailViewModel.array[index] as? Post{do {
                        var otherId = ""
                            if let user = obj.postedBy as? User{
                                otherId = user.id ?? ""
                            }

                        query.whereKey("followed_id", equalTo: otherId)
                        }
                        
                        query.findObjectsInBackground(block: { (objects, error) in
                             cell.indicatorView.stopAnimating()
                            cell.indicatorView.isHidden = true
                            if error == nil {
                                for objectt in objects! {
                                    
                                    objectt.deleteInBackground(block: { (status, error) in
                                        print(status)
                                        cell.isUserInteractionEnabled = true
                                        if status == true{
                                            cell.btn_follow.setTitle("Follow", for: .normal)
                                            cell.btn_follow.isHidden = false
                                            cell.btn_follow.layer.borderColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1).cgColor
                                            cell.btn_follow.layer.borderWidth = 1.2
                                            cell.btn_follow.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                                            cell.btn_follow.setTitleColor(#colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1), for: .normal)
                                            if cell.btn_follow.titleLabel?.text == "Following"
                                            {
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                                            }
                                            
                                        }else
                                        {
                                        }
                                    })
                                }
                            }
                            else {
                                
                                let message = (error?.localizedDescription)
                                self.showMessage(message!)
                                print(error!)
                            }
                        })
                    }
                case .cancel:
                     cell.indicatorView.stopAnimating()
                     cell.indicatorView.isHidden = true
                    print("cancel")
                    
                case .destructive:
                      cell.indicatorView.stopAnimating()
                      cell.indicatorView.isHidden = true
                    print("destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
}

