//
//  PostInfoCell.swift
//  Network42
//
//  Created by Apple3 on 23/01/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import ParseUI
import MMPlayerView

@objc protocol userFollowDelegate {
    @objc optional func followClickedDelegate(index:Int,cell:PostInfoCell)
}


class PostInfoCell: UITableViewCell {
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    
    @IBOutlet weak var img_likeSelected: UIImageView!
    @IBOutlet weak var btn_name: UIButton!
    @IBOutlet weak var btn_profile: UIButton!
    //MARK:- Variables
    //var videoImg = UIImageView()
    @IBOutlet weak var btn_follow: UIButton!
    var delegate : userFollowDelegate?
    lazy var post = Post()
    var mmPlayerLayer: MMPlayerLayer?
    var type: Type! {
        didSet {
            if let type = type {
                stackUrlView.isHidden = true
                stackAlbumView.isHidden = true
                imageSingleView.isHidden = true
                
                switch type {
                case .image:
                    imageSingleView.isHidden = false
                    imageSingleView.setImage(fromUrl: post.postMedia?.first, defaultImage: nil)
                    break
                    
                case .video:
                    imageSingleView.isHidden = false
                    if let url = URL(string: post.thumbnail?.first?.url ?? ""){
                       mmPlayerLayer?.thumbImageView.af_setImage(withURL: url)
                    }

                    mmPlayerLayer?.playView = imageSingleView
                    if let url = URL(string: post.postMedia?.first?.url ?? "")
                    {
                        self.mmPlayerLayer?.set(url: url)
                    }
                    self.mmPlayerLayer?.resume()
                    
                    break
                    
                case .album:
                    stackAlbumView.isHidden = false
                    break
                }
            }
        }
    }
    
    //MARK:- Closures
    var likeClosure:((_ isLike: Bool, _ count: Int)->())?
    
    //MARK:- IBOutlets
    @IBOutlet var pageControlObj: UIPageControl!
    
    @IBOutlet weak var vw_profileBackground: UIView!
    @IBOutlet weak var stackUrlView: UIStackView!
    @IBOutlet weak var stackNameView: UIStackView!
    @IBOutlet weak var stackAlbumView: UIStackView!
    @IBOutlet weak var viewComment: UIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var btn_likeCount: UIButton!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var btn_commentCount: UIButton!
    
    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var imageSingleView: PFImageView!
    @IBOutlet weak var imageProfile: PFImageView!
    
    @IBOutlet weak var collectionViewAlbum: UICollectionView! {
        didSet {
            collectionViewAlbum.register(UINib(nibName: "HomeCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    
    func config(_ post: Post) {
        self.post = post
        self.type = post.type
        self.imageProfile?.setImage(fromUrl: post.postedBy?.profilePic, defaultImage: nil)
        labelName?.text = post.postedBy?.name
        labelTime?.text = post.createdOn
        labelDescription?.text = post.description
        labelDescription?.isHidden = post.description?.trim() == "" ? true : false
        let commentCount = "\(post.commentCount ?? 0)"
        btn_commentCount.setTitle(commentCount, for: .normal)
        let likeCount = "\(post.likeCount ?? 0)"
        btn_likeCount.setTitle(likeCount, for: .normal)
        buttonLike.isSelected = post.iLiked
        img_likeSelected.isHighlighted = post.iLiked
        if self.type == .album{
            self.collectionViewAlbum?.reloadData()
        }
    }
    
    //MARK:- IBAction
    @IBAction func likeButtonTap(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        let count = sender.isSelected ? (post.likeCount ?? 0) + 1 : (post.likeCount ?? 0) - 1
        post.likeCount = count
        let likeCount = "\(count)"
        btn_likeCount.setTitle(likeCount, for: .normal)
        img_likeSelected.isHighlighted = sender.isSelected
        likeClosure?(sender.isSelected, count)
    }
    

    @IBAction func commentBtnTap(_ sender: UIButton) {
        
    }
    
}

extension PostInfoCell: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControlObj.currentPage = indexPath.row
    }
}

extension PostInfoCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.pageControlObj.numberOfPages = post.postMedia?.count ?? 0
        return post.postMedia?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell else {
            return UICollectionViewCell()
        }
        cell.muteButton.isHidden = true
        if self.type == .album
        {
             if let obj = self.post.postMedia?[indexPath.row] {
                cell.config(obj,self.post.albumType?[indexPath.row] ?? .image)
             }
        }else{
            return cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 180)
    }
}


