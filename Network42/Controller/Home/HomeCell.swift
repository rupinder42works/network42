//
//  HomeCell.swift
//  Network42
//
//  Created by 42works on 11/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import ActiveLabel
import AlamofireImage
import AVKit
import AVFoundation
import JPVideoPlayer

@objc protocol likeListingDelegate {
    func likeListingClicked(tag:Int)
    func likeClicked(sender: UIButton)
    func commentClicked(tag:Int)
    func URLClicked(tag:Int)
    func moreClicked(sender:UIButton,tag:Int)
    func userProfileClicked(tag:Int)
    
    func imageClicked(tag:Int,object:PFObject)
}

class HomeCell: UITableViewCell {
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var btn_comment: UIButton!

    @IBOutlet var likeListingObj: UIButton!
    @IBOutlet var likeBtn: UIButton!
    @IBOutlet var commentListing: UIButton!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet var userProfileBtn: UIButton!
    
    @IBOutlet weak var likeSelected: UIImageView!
    @IBOutlet weak var vw_profileImg: UIView!

    @IBOutlet var indicatorObject: UIActivityIndicatorView!
    @IBOutlet var profileImage: PFImageView!
    @IBOutlet var time: UILabel!
    @IBOutlet var name: UILabel!
    
    @IBOutlet var descriptionLbl: ActiveLabel!
    @IBOutlet var heightOfDescription: NSLayoutConstraint!
    
    @IBOutlet var collectionContainer: UIView!
    @IBOutlet var collectionVIewHeight: NSLayoutConstraint!
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var pageControlObj: UIPageControl!
    @IBOutlet var heightOFPageController: NSLayoutConstraint!
    var objectForCollection : PFObject?
    @IBOutlet var topOfDescription: NSLayoutConstraint!
    
    weak var delegate: likeListingDelegate?
    
    var avPlayer: AVPlayer!
    
    //var currentIndex : Int = 0
    
    var changeVideo : Bool = false
    
    var playerDictionary = [[:]]
    
    var muted : Bool = false
    var parent : UITableView?
    
    var isVisible = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func like(_ sender: Any) {
        self.delegate?.likeClicked(sender: sender as? UIButton ?? UIButton())
    }
    
    @IBAction func comment(_ sender: Any) {
        self.delegate?.commentClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func URLAction(_ sender: Any) {
        self.delegate?.URLClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func more(_ sender: Any) {
        if let button = sender as? UIButton{
            self.delegate?.moreClicked(sender:button,tag:button.tag)
        }
    }

    @IBAction func likeListingAction(_ sender: Any) {
        self.delegate?.likeListingClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func userProfile(_ sender: Any) {
        self.delegate?.userProfileClicked(tag: (sender as AnyObject).tag)
    }
}


extension HomeCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func reloadSingleRow()
    {
        collectionViewObj.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let arr = objectForCollection?.object(forKey: "postmedia") as? NSArray ?? NSArray()
        pageControlObj.numberOfPages = arr.count
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let arr = objectForCollection?.object(forKey: "postmedia") as? NSArray ?? NSArray()
        if let fileObj = arr.object(at: indexPath.item) as? PFFile
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeVideo_CollectionViewCell",
                                                          for: indexPath) as! HomeVideo_CollectionViewCell
            let urlStr = fileObj.url! as NSString
            let extensionStr = urlStr.pathExtension
            let extensionArray : NSMutableArray = ["mp4"]
            
            if extensionArray.contains(extensionStr)
            {
                cell.videoLayer.isHidden = true
                if let str = fileObj.url{
                    var imageStr = ""
                    var fileImage : PFFile?
                    
                    if let thumbnail = objectForCollection?.object(forKey: "thumbnail") as? NSArray
                    {
                        if let fileObj = thumbnail.object(at: indexPath.row) as? PFFile
                        {
                            imageStr = ""
                            fileImage = fileObj
                        }
                    }
                    else
                    {
                        imageStr =  "default.png"
                        fileImage = nil
                    }
                   
                   cell.btn_mute.isSelected = false
                    if let p = self.parent{
                        if let parentController = p.viewContainingController() as? HomeViewController{
                            parentController.videoLayerDelegate = cell
                        }
                    }
                    cell.delegate = self
                    var duration = 0
                    if let timedur = objectForCollection?.object(forKey: "duration") as? String{
                        duration = Int(timedur) ?? 0
                    }
                    cell.configureCell(imageUrl: imageStr, description: "Video", videoUrl: str,file:fileImage,duration:duration)
                }
                return cell
            }
            else
            {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell",
                                                              for: indexPath) as! HomeCollectionViewCell
                cell.muteButton.isHidden = true
                cell.imageObj.isHidden = false
                cell.videoImage.isHidden = true
                cell.imageObj.file = fileObj
                cell.indicatorObj.startAnimating()
                cell.vw_description.isHidden = true
                if let descArray = objectForCollection?.object(forKey: "media_description") as? NSArray
                {
                    if let desc = descArray.object(at: indexPath.row) as? String
                    {
                        if desc.isEmpty == false{
                            cell.vw_description.isHidden = false
                            cell.lbl_imgdescription.text = " " + "\(desc)"

                        }else{
                            cell.vw_description.isHidden = true
                        }
                    }
                }
                cell.imageObj.load { (image, error) in
                    cell.indicatorObj.stopAnimating()
                }
                return cell
            }
        }
        return UICollectionViewCell()
    }
    
    func timer(muteObj:Bool)
    {
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.delegate?.imageClicked(tag: indexPath.row, object: objectForCollection!)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControlObj.currentPage = indexPath.row
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func setCollectionViewDataSourceDelegate
        <D: UICollectionViewDataSource & UICollectionViewDelegate>
        (dataSourceDelegate: D, forRow row: Int,changeVideo:Bool) {
        
        collectionViewObj.register(UINib(nibName: "HomeCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        collectionViewObj.register(UINib(nibName: "HomeVideo_CollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "HomeVideo_CollectionViewCell")
        
        if #available(iOS 10.0, *)
        {
            collectionViewObj.isPrefetchingEnabled = false
        }
                
        collectionViewObj.delegate = dataSourceDelegate
        collectionViewObj.dataSource = dataSourceDelegate
        collectionViewObj.tag = row
        

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: UIScreen.main.bounds.size.width, height:450)
        collectionViewObj.isPagingEnabled = true
        collectionViewObj.setCollectionViewLayout(layout, animated: false)
        collectionViewObj.reloadData()
    }
}

extension HomeCell : MuteActionDelegate
{
    func mute() {
        //avPlayer.isMuted = !(avPlayer.isMuted)
        
     //   JPVideoPlayerManager.shared().muted = !JPVideoPlayerManager.shared().muted
        //        JPVideoPlayerManager.shared().setPlayerMute(!(JPVideoPlayerManager.shared().playerIsMute()))
    }
    
}

extension HomeCell : muteDelegate{
    func clickMute(cell:HomeVideo_CollectionViewCell){
        
    }
}

