//
//  HomeViewController.swift
//  Network42
//
//  Created by 42works on 10/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Parse
import ActiveLabel
import AlamofireImage
import AVFoundation
import AVKit
import SwiftyGif
import JPVideoPlayer
import GoogleMobileAds
import Firebase

protocol removeVideoLayerDelegate{
    func removeLayerFromCell()
}

class HomeViewController: BaseViewController,likeListingDelegate,likeListDelegate,commentCountUpdateDelegate {
    
    var homeApiCalled = false
    var isFromHashTag = false
    var isFromLike = false
    @IBOutlet var createBtn: UIButton!
    @IBOutlet var tableViewObj: UITableView!
    @IBOutlet var indicatorObj: UIActivityIndicatorView!
    @IBOutlet var popUpTableViewObj: UITableView!
    @IBOutlet var popUp: UIView!
    @IBOutlet var heightOfPopUpTableView: NSLayoutConstraint!
    var PFObjectArray : NSMutableArray = []
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var img_chatSend: UIImageView!
    var videoLayerDelegate : removeVideoLayerDelegate?
    
    var indexPathObj = NSIndexPath.init(row: 0, section: 0)
    
    var begin : CGFloat = 0
    var end : CGFloat = 0
    var current : String = ""
    
    var popUpArray : NSArray = []
    @IBOutlet var progressVIew: UIView!
    @IBOutlet var progressImage: UIImageView!
    @IBOutlet var heightOFProgressView: NSLayoutConstraint!
    
    var descriptionIndex : Int = 0
    
    @IBOutlet var noPostLabel: UILabel!
    @IBOutlet var backBtn: UIButton!
    @IBOutlet var topHeadingTitle: UILabel!
    @IBOutlet var topHeadingImage: UIImageView!
    var secondTime : Bool = false
    var titleSecond : String = ""
    @IBOutlet var chatBtn: UIButton!
    @IBOutlet var plusIcon: UIImageView!
    
    var pageCount : Int = 0
    
    var showOnlySinglePost : Bool = false
    var postIdObject : String = ""
    
    // var countObj : Int = 0
    
    var trying : Bool = true
    
    var scrollRow : Int = 0
    
    var tempInt : Int = 0
    
    var videoIndexCell = 0
    
    @IBOutlet var redIconImage: UIImageView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
       
        createBtn.layer.shadowColor = UIColor.lightGray.cgColor
        createBtn.layer.shadowOffset = CGSize(width: 4, height: 4)
        createBtn.layer.shadowRadius = 4.0
        createBtn.layer.shadowOpacity = 1
        createBtn.clipsToBounds = false
        createBtn.layer.masksToBounds = false
        
        redIconImage.layer.cornerRadius = redIconImage.frame.size.width/2
        
        tableViewObj.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
         tableViewObj.register(UINib(nibName: "Home_UrlCell", bundle: nil), forCellReuseIdentifier: "Home_UrlCell")
        tableViewObj.register(UINib(nibName: "AdvertisementCell", bundle: nil), forCellReuseIdentifier: "AdvertisementCell")
        
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 702
        
        popUpTableViewObj.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "PostTableViewCell")
        popUpTableViewObj.rowHeight = UITableView.automaticDimension
        popUpTableViewObj.estimatedRowHeight = 702
        
        
        createBtn.layer.masksToBounds = true
        createBtn.layer.cornerRadius = createBtn.frame.width/2
        createBtn.layer.masksToBounds = false
        createBtn.layer.shadowColor = UIColor.darkGray.cgColor
        createBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        createBtn.layer.shadowOpacity = 1
        createBtn.layer.shadowRadius = 1.0
        
        UserDefaults.standard.set(true, forKey: "LoggedIn")
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.fetchUser), name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.navigateToController(notification:)), name:NSNotification.Name(rawValue: "homeController"), object: nil)
        
        //change tabbar color
        self.tabBarController?.tabBar.tintColor = ConstantColors.themeColor
        
        popUp.isHidden = false
        popUp.fadeOut()
        
        let gif = UIImage(gifName: "loading.gif")
        let gifmanager = SwiftyGifManager(memoryLimit:20)
        self.progressImage.setGifImage(gif, manager: gifmanager)
        self.heightOFProgressView.constant = 0
        self.progressVIew.isHidden = true
        

        if self.secondTime
        {
            createBtn.isHidden = true
            self.topHeadingTitle.text = titleSecond.contains("#") ? titleSecond : "#\(titleSecond)"
            self.topHeadingImage.isHidden = true
            if isFromHashTag == true{
                self.chatBtn.isHidden = true
                self.img_chatSend.isHidden = true
            }else{
                self.chatBtn.isHidden = false
                self.img_chatSend.isHidden = false
            }
            self.backBtn.isHidden = false
            self.plusIcon.isHidden = true
            if showOnlySinglePost
            {
                self.chatBtn.isHidden = true
                self.img_chatSend.isHidden = true
                self.likeAndCommentPost(postId: postIdObject)
            }else
            {
                self.hashTags(hashTag: titleSecond.contains("#") ? titleSecond : "#\(titleSecond)")
            }
        } else
        {
            self.fetchUser()
            
            refreshControl = UIRefreshControl()
            refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            refreshControl.addTarget(self, action:#selector(HomeViewController.refreshTable), for: UIControl.Event.valueChanged)
            tableViewObj.addSubview(refreshControl)
        }
    }
    
    func bookMark()
    {
        if  Reachability.isConnectedToNetwork() == false {
            
            self.showMessage(messages.internet)
            return
        }
        let DBref = FIRDatabase.database().reference()
        DBref.child("bookmarkMessage").observe(.value, with: { snapshot in
            for rest in snapshot.children.allObjects as! [FIRDataSnapshot] {
                var restDict = [String:Any]()
                restDict = rest.value as! [String: Any]
                if let objId = PFUser.current()?.objectId{
                    if rest.key == objId
                    {
                        print(restDict)
                        if (restDict as AnyObject).value(forKey: "bookmarkStatus") as? String == "0"
                        {
                            self.redIconImage.backgroundColor = UIColor.red
                        }else
                        {
                            self.redIconImage.backgroundColor = UIColor.clear
                        }
                    }
                }
            }
        })
    }
    
    override func viewDidAppear(_ animated: Bool) {
        pausePlayeVideos()
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.FromProfile(object:)), name: NSNotification.Name(rawValue: "UpdateList"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.updateMuted), name: NSNotification.Name(rawValue: "mute"), object: nil)
        
        if globalBool.showProgess
        {
            self.heightOFProgressView.constant = 60
            self.progressVIew.isHidden = false
        }else
        {
            self.heightOFProgressView.constant = 0
            self.progressVIew.isHidden = true
        }
        
        self.bookMark()
        
    }
    
    @objc func FromProfile(object:NSArray)
    {
        //        print(object)
        let array = object.value(forKey: "object") as? NSArray
        if array?.object(at: 0) as? String == "delete"
        {
            for obj in PFObjectArray
            {
                //print((obj as? PFObject)?.objectId ?? "no")
                
                let  objectdata = array?.object(at: 1) as? PFObject
                if objectdata?.value(forKey: "objectId") as? String == (obj as? PFObject)?.objectId
                {
                    PFObjectArray.remove(obj)
                    self.tableViewObj.reloadData()
                }
            }
        }else if array?.object(at: 0) as? String == "CommentCount" || array?.object(at: 0) as? String == "Edited" || array?.object(at: 0) as? String == "Liked"
        {
            var i = 0
            for obj in PFObjectArray
            {
                let  objectdata = array?.object(at: 1) as? PFObject
                if objectdata?.value(forKey: "objectId") as? String == (obj as? PFObject)?.objectId
                {
                    PFObjectArray.replaceObject(at: i, with: objectdata ?? obj)
                    self.tableViewObj.reloadData()
                }
                i=i+1
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true) 
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        //  JPVideoPlayerManager.shared().stopPlay()
        Constants.visitedSecondHome = true
        super.viewDidDisappear(true)
        pausePlayeVideos()
        //globalBool.showProgess = false
          NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "postSuccess"), object: nil);
         NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "Deleted"), object: nil);
    }
    
    
    @objc  func refreshTable() {
        if secondTime == false
        {
            self.callHomeListApi(isRefresh : true)
        }
    }
    
    func updateCommentCount(index:Int,value:String)
    {
        var obj = (PFObjectArray.object(at: index) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: index) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        if let object = obj as? PFObject
        {
            object.setValue(value, forKey: "comment_count")
            print(object)
            self.isPostExist()
            self.tableViewObj.reloadData()
            
            if secondTime
            {
                Constants.visitedSecondHome = true
            }
        }
    }
    
    //MARK:- PUSH Functions
    @objc func navigateToController(notification: NSNotification) {
        print(notification)
        print(notification)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postDetailController"), object: notification)
        let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = tabbarSB.instantiateViewController(withIdentifier: "PostDetailViewController")  as! PostDetailViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(HomeViewController.callHomeListApiForFirstRecord(indexObj:)), name: NSNotification.Name(rawValue: "postSuccess"), object: nil)
        if isFromHashTag == true{
            self.chatBtn.isHidden = true
        }
        tableViewObj.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        self.logController()
        if self.secondTime
        {
            self.tabBarController?.tabBar.isHidden = true
        }else
        {
            self.tabBarController?.tabBar.isHidden = false
            if Constants.visitedSecondHome
            {
                self.PFObjectArray.removeAllObjects()
                self.callHomeListApi()
            }
        }
        if self.PFObjectArray.count > 0{
            self.tableViewObj.reloadData()
        }        
    }
    
    @IBAction func back(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func createPost(_ sender: Any) {
        
        if globalDic.tagUserDictionary.count>0
        {
            let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
           // let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePostViewController")  as! CreatePostViewController
             let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePost_VC")  as! CreatePost_VC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @objc func fetchUser()
    {
        if  Reachability.isConnectedToNetwork() == false {
            if !(self.refreshControl == nil)
            {
                self.refreshControl.endRefreshing()
            }
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "User_follow")
        query.whereKey("follower_id", equalTo:PFUser.current()?.objectId! ?? "")
        query.includeKey("parent_following")
        query.whereKey("status", equalTo:"1")
        
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
              //  print(objects?.count as Any)
                
                let name : NSMutableArray = []
                let ids : NSMutableArray = []
                let userName : NSMutableArray = []
                let imageArray : NSMutableArray = []
                let tagUserDictionary : NSMutableDictionary = [:]
                
                for object in objects! {
                  //  print(object)
                    if let singleUser = object.object(forKey: "parent_following") as? PFUser
                    {
                        print(singleUser)
                        ids.add(singleUser.objectId!)
                        name.add(singleUser.object(forKey: "name") as! String)
                        userName.add(singleUser.object(forKey: "user_name") as! String)
                        
                        if let userPicture = singleUser["profile_image"] as? PFFile {
                            imageArray.add(userPicture)
                        }else
                        {
                            imageArray.add("")
                        }
                    }
                }
                
                tagUserDictionary.setValue(name, forKey: "name")
                tagUserDictionary.setValue(ids, forKey: "id")
                tagUserDictionary.setValue(userName, forKey: "userName")
                tagUserDictionary.setValue(imageArray, forKey: "image")
                globalDic.tagUserDictionary = tagUserDictionary
                
                self.PFObjectArray.removeAllObjects()
                self.callHomeListApi(isRefresh: false)
            }
            else {
                print(error!)
            }
        })
    }
    
    
    func likeListingClicked(tag: Int) {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "LikesViewController") as! LikesViewController
        
        var obj = (PFObjectArray.object(at: tag) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        let id = obj.value(forKey: "objectId") as! String
        print(id)
        vc.postId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func likeClicked(sender: UIButton) {
        
        let tag = sender.tag
        
        sender.isSelected = !sender.isSelected
        
        
        var obj = (PFObjectArray.object(at: tag) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        var likeArrayObj : NSMutableArray = []
        if let likeArray = obj.value(forKey: "user_like_post") as? NSMutableArray {
            likeArrayObj = likeArray
        }
        
        
        let objId = PFUser.current()?.objectId
        let id = obj.value(forKey: "objectId") as! String
        
        if sender.isSelected && !likeArrayObj.contains(objId ?? ""){
            likeArrayObj.add(objId ?? "")
        } else {
            likeArrayObj.remove(objId ?? "")
        }
        
        let likesQuery = PFQuery(className:"Post_likes")
        likesQuery.whereKey("post_id", equalTo:id)
        likesQuery.whereKey("user_id", equalTo:objId!)
        likesQuery.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print("object===",objects!)
                
                if objects!.count==0
                {
                    
                    let compClass = PFObject(className: "Post_likes")
                    compClass["post_id"] =  id
                    compClass["user_id"] =  objId
                    compClass["parent_id"] =  PFUser.current()
                    compClass.saveInBackground(block: { (status, error) in
                        print(status)
                        self.view.isUserInteractionEnabled = true
                        if status ==  true{
                            
                            if !(likeArrayObj.contains(objId!))
                            {
                                likeArrayObj.add(objId!)
                            }
                            let objectUpdate = obj as! PFObject
                            self.likeArrayUpdate(object: objectUpdate, likeArr: likeArrayObj, index: tag)
                            
                            if let userId = obj.object(forKey: "user_id") as? String
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                                let time = formatter.string(from: Date())
                                let objId = PFUser.current()?.objectId
                                
                                let nameQuery : PFQuery = PFUser.query()!
                                nameQuery.whereKey("objectId", equalTo: userId)
                                nameQuery.findObjectsInBackground(block: { (objects, error) in
                                    //print(objects!)
                                    for object in objects! {
                                        
                                        self.saveToNotifications(from_id: objId!, to_id: userId, notification_type: "like", time: time, post_id: id, parent_from: PFUser.current()!, parent_to: (object as? PFUser)!,commentStr: "")
                                        self.likePushNotification(userId: userId,postId: id)
                                    }
                                })
                            }
                        }
                        else{
                            
                            let message = (error?.localizedDescription)
                            print(message as Any)
                        }
                    })
                }else
                {
                    print("liked already")
                    for object in objects! {
                        //print(object)
                        object.deleteInBackground(block: { (status, error) in
                            self.view.isUserInteractionEnabled = true
                            print(status)
                            if status == true{
                                likeArrayObj.remove(objId!)
                                //self.likeArrayUpdate(id: id, likeArr: likeArrayObj, index: tag)
                                let objectUpdate = obj as! PFObject
                                self.likeArrayUpdate(object: objectUpdate, likeArr: likeArrayObj, index: tag)
                            }else{
                                let message = (error?.localizedDescription)
                                print(message as Any)
                            }
                        })
                    }
                }
            }else
            {
                self.view.isUserInteractionEnabled = true
            }})
    }
    
    func likePushNotification(userId:String,postId:String)
    {
        if !(userId == PFUser.current()!.objectId!)
        {
            
            let currentName = PFUser.current()?.value(forKey: "name") as! String
            self.sendPushNotification(postId: postId, userId: userId,settingType: "push_post_like",message:"\(currentName) \(messages.likePost)",type:"post_like",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
        }
    }
    
    
    /*   func likeArrayUpdate(id:String,likeArr:NSArray,index:Int)
     {
     ////////////////////////
     let query = PFQuery(className:"Post")
     query.whereKey("objectId", equalTo:id)
     query.findObjectsInBackground(block: { (objects, error) in
     if error == nil {
     for object in objects! {
     
     print(object)
     object["user_like_post"] =  likeArr
     object.saveInBackground(block: { (status, error) in
     print(status)
     if status ==  true{
     self.updateAPIArray(index: index, likeArr: likeArr)
     }
     else{
     let message = (error?.localizedDescription)
     print(message as Any)
     }
     })
     }
     }
     else {
     print(error!)
     }
     
     })
     }*/
    
    
    
    
    func likeArrayUpdate(object:PFObject,likeArr:NSArray,index:Int)
    {
        ////////////////////////
        
        
        //print(object)
        object["user_like_post"] =  likeArr
        object.saveInBackground(block: { (status, error) in
            print(status)
            if status ==  true{
                self.updateAPIArray(index: index, likeArr: likeArr)
            }
            else{
                let message = (error?.localizedDescription)
                print(message as Any)
            }
        })
        
        
        
    }
    
    
    
    func updateAPIArray(index:Int,likeArr:NSArray)
    {
        let obj = PFObjectArray.object(at: index) as! PFObject
        obj.setValue(likeArr, forKey: "user_like_post")
        PFObjectArray.replaceObject(at: index, with: obj)
        let indexPath = NSIndexPath.init(row: index, section: 0)
        self.tableViewObj.reloadRows(at: [indexPath as IndexPath], with: .none)
        
        if secondTime
        {
            Constants.visitedSecondHome = true
            //            print(obj)
            //            let arr = ["Liked",obj] as NSArray
            //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
        }
    }
    
    
    func commentClicked(tag: Int) {
        
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        
        var obj = (PFObjectArray.object(at: tag) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        let id = obj.value(forKey: "objectId") as! String
        vc.objectFromHome = obj as? PFObject
        vc.postId = id
        vc.delegate = self
        vc.indexOfpost = tag
        vc.fromProfile = false
        if let userId = obj.object(forKey: "user_id") as? String
        {
            vc.postUserId = userId
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func URLClicked(tag: Int)
    {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "MTSWebViewController") as! MTSWebViewController
        
        var obj = (PFObjectArray.object(at: tag) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        if let link_url = obj.object(forKey: "link_url") as? String
        {
            vc.urlStr = link_url
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func moreClicked(sender:UIButton,tag: Int)
    {
        descriptionIndex = tag
        //sender.setImage(#imageLiteral(resourceName: "log out"), for: .normal)
        
        var obj = (PFObjectArray.object(at: tag) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        
        self.popUp.fadeIn()
        if let id = obj.object(forKey: "user_id") as? String
        {
            let objId = PFUser.current()?.objectId
            if id == objId!
            {
                self.popUpArray = ["SHARE","EDIT","DELETE","CANCEL"]
            }else
            {
                self.popUpArray = ["REPORT ABUSE","SHARE","CANCEL"]
            }
        }
        if let cell = tableViewObj.cellForRow(at: IndexPath.init(row: tag, section: 0)) as? HomeCell{
            let frame = cell.contentView.convert(cell.moreBtn.frame, to: self.view)
            //            cell.moreBtn.setImage(UIImage.init(named: "WHITEDOTS"), for: .normal)
            //            self.popUp.bringSubviewToFront(cell.moreBtn)
            print(frame)
            popUpTableViewObj.frame.origin.y = frame.origin.y - 66
            popUpTableViewObj.layoutIfNeeded()
            popUpTableViewObj.layoutSubviews()
        }
        
        
        if self.popUpArray.count>6
        {
            self.popUpTableViewObj.isScrollEnabled = true
            self.heightOfPopUpTableView.constant = 300
        }else
        {
            let count = self.popUpArray.count
            self.heightOfPopUpTableView.constant = CGFloat(count * 50)
            self.popUpTableViewObj.isScrollEnabled = false
        }
        self.popUpTableViewObj.reloadData()
    }
    
    
    @IBAction func chatAction(_ sender: Any) {
        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
        let vc = chatSB.instantiateViewController(withIdentifier: "ChatListingViewController")  as! ChatListingViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func userProfileClicked(tag: Int)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        
        var obj = (PFObjectArray.object(at: tag) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        if let id = obj.value(forKey: "user_id") as? String
        {
            let objId = PFUser.current()?.objectId
            if !(id == objId){
                vc.userProfile = false
            }
             vc.objectId = id
        }
        
        vc.friend = "FOLLOWING"
       
        self.navigationController?.pushViewController(vc, animated: true)
        
        /*
         let nameQuery : PFQuery = PFUser.query()!
         nameQuery.whereKey("objectId", equalTo: id)
         nameQuery.findObjectsInBackground(block: { (objects, error) in
         print(objects!)
         for object in objects! {
         vc.userFromSearch = object as? PFUser
         self.navigationController?.pushViewController(vc, animated: true)
         }
         })*/
    }
    
    
    func imageClicked(tag: Int,object:PFObject)
    {
        if let postType = object.object(forKey: "post_type") as? String{
            if postType == "IMAGE" || postType == "ALBUM"{
                if let postmedia = object.object(forKey: "postmedia") as? NSArray
                {
                    if postmedia.count>0
                    {
                        let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
                        let vc = CreatePostSB.instantiateViewController(withIdentifier: "Post_mediaVC")  as! Post_mediaVC
                        vc.objectForCollection  = object
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
            }
        }       
    }
    
    @IBAction func backAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    //    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    //         if scrollView == tableViewObj {
    //            for values in tableViewObj.visibleCells{
    //                print(values.tag)
    //            }
    //         }
    //    }
    
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        //        if scrollView == tableViewObj {
        //           pausePlayeVideos()
        //        }
    }
    
    func pausePlayeVideos(){
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: tableViewObj)
    }
    
    @objc func appEnteredFromBackground() {
        ASVideoPlayerController.sharedVideoPlayer.pausePlayeVideosFor(tableView: tableViewObj, appEnteredFromBackground: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        if scrollView == tableViewObj {
            //            if !decelerate {
            //                pausePlayeVideos()
            //            }
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                if secondTime == false
                {
                    if self.PFObjectArray.count>5
                    {
                        self.callHomeListApi(isRefresh: false)
                    }else
                    {
                        if self.PFObjectArray.count>=1 && self.PFObjectArray.count<5
                        {
                            //self.showMessage(messages.noPost)
                            return
                        }
                    }
                }
                
            }
        }
    }
}


extension HomeViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewObj
        {
            return UITableView.automaticDimension
        }
        return 50
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewObj
        {
            if PFObjectArray.count > 0 {
                return PFObjectArray.count
            }
            return 0
        }
        return popUpArray.count
    }
    
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tableViewObj
        {
            pausePlayeVideos()
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewObj
        {
            if PFObjectArray.count > indexPath.row{
                if PFObjectArray.object(at: indexPath.row) as? String == Constants.advertisement
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementCell", for: indexPath as IndexPath) as! AdvertisementCell
                        cell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
                        cell.bannerView.rootViewController = self
                        cell.bannerView.load(GADRequest())
                        return cell
                }else
                {
                    var obj = (PFObjectArray.object(at: indexPath.row) as AnyObject)
                    var postType =  ""
                    if let post_type = obj.object(forKey: "post_type") as? String
                    {
                        postType = post_type
                    }
                    var key = ""
                    if isFromHashTag || secondTime {
                        key = "parent_post"
                    } else {
                        key = "parent_id"
                    }
                    if isFromLike {
                        key = "parent_id"
                    }
                    var userName = ""
                    var profileImage : PFFile?
                    if let user = obj.object(forKey: key) as? PFUser
                    {
                        if let name = user.object(forKey: "name") as? String {
                            userName = name
                        }
                        if let profileImageFile = user.object(forKey: "profile_image") as? PFFile
                        {
                            profileImage = profileImageFile
                        }
                    }
                    
                    if isFromHashTag {
                        obj = ((PFObjectArray.object(at: indexPath.row) as AnyObject).object(forKey: key) as AnyObject)
                    }
                    var timeStamp = ""
                    
                    if let timeStampObj = obj.object(forKey: "time_stamp") as? String
                    {
                        let newdateStr = timeStampObj.replacingOccurrences(of: "_", with: " ")
                        let dateFormatter = DateFormatter()
                        if newdateStr.lowercased().contains("pm") == true || newdateStr.lowercased().contains("am"){
                            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
                        }else{
                            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                        }
                        
                        let newDate = dateFormatter.date(from: newdateStr)
                        if let newdate1 = newDate as? Date {
                            let timeStr  = self.timeAgoSinceDate(date:newdate1,numericDates: true)
                            timeStamp = timeStr
                        }
                    }
                    
                    var descriptionLbl : ActiveLabel?
                    var commentCount = "0"
                    if let comment = obj.value(forKey: "comment_count") as? String
                    {
                        if let commentNumber = Int(comment){
                            commentCount = "\(commentNumber)"
                        }
                    }
                    
                    if postType == "URL"{
                        // for post type url load another cell
                        let cell = tableView.dequeueReusableCell(withIdentifier: "Home_UrlCell", for: indexPath as IndexPath) as! Home_UrlCell
                        cell.parent = tableView
                        cell.likeBtn.tag = indexPath.row
                        cell.likeListingObj.tag = indexPath.row
                        cell.commentListing.tag = indexPath.row
                        cell.moreBtn.tag = indexPath.row
                        cell.userProfileBtn.tag = indexPath.row
                        cell.btn_comment.tag = indexPath.row
                        cell.delegate = self
                        
                        cell.name.text = userName
                        if profileImage != nil
                        {
                            cell.profileImage.file = profileImage!
                            cell.profileImage.loadInBackground()
                        } else {
                            cell.profileImage.image =  UIImage.init(named: "dummyProfile")
                        }
                        cell.time.text = timeStamp
                        descriptionLbl = cell.descriptionLbl
                        
                        if descriptionLbl != nil{
                            setDescriptionLbl(descriptionLbl:descriptionLbl!,obj:obj,index:indexPath.row)
                        }
                        
                        if let link_title = obj.object(forKey: "link_title") as? String
                        {
                            if !(link_title == "") && postType == "URL"
                            {
                                cell.lineView.isHidden = true
                                var urlStr = ""
                                if let link_url = obj.object(forKey: "link_url") as? String
                                {
                                    urlStr = link_url
                                }
                                if urlStr.contains("www.youtube.com/watch?v=") == true{
                                    cell.img_youtube.isHidden = false
                                    cell.imgVw_youtube.isHidden = false
                                    cell.img_youtube.isHidden = false
                                    cell.htConsrnt_youtube.constant = 110
                                    
                                }else{
                                    cell.imgVw_youtube.image = nil
                                    cell.imgVw_youtube.isHidden = true
                                    cell.img_youtube.isHidden = true
                                    cell.htConsrnt_youtube.constant = 0
                                }
                                
                                cell.urlView.isHidden = false
                                cell.urlView.layer.shadowColor = UIColor.lightGray.cgColor
                                cell.urlView.layer.shadowOffset = CGSize(width: 3, height: 3)
                                cell.urlView.layer.shadowRadius = 3.0
                                cell.urlView.layer.shadowOpacity = 1
                                cell.urlView.clipsToBounds = false
                                cell.urlView.layer.masksToBounds = false
                                
                                cell.urlTitle.text = link_title
                                if let link_description = obj.object(forKey: "link_description") as? String
                                {
                                    cell.urlDescription.text = link_description
                                }
                                if let link_url = obj.object(forKey: "link_url") as? String
                                {
                                    cell.urlAgain.text = link_url
                                }
                                
                                if let link_image_url = obj.object(forKey: "link_image_url") as? String
                                {
                                    let url = URL(string: link_image_url)
                                    if urlStr.contains("www.youtube.com/watch?v=") == true{
                                        if let imageUrl = url {
                                            cell.imgVw_youtube.af_setImage(withURL: imageUrl)
                                        }
                                    }else{
                                        cell.imgVw_youtube.image = nil
                                    }
                                    if !(url==nil)
                                    {
                                        
                                        cell.urlImage.af_setImage(withURL: url!)
                                        cell.widthOfUrlImage.constant = 50
                                    }else
                                    {
                                        cell.urlImage.image = nil
                                        cell.widthOfUrlImage.constant = 0
                                    }
                                }
                            }
                        }
                        if let likeArray = obj.value(forKey: "user_like_post") as? NSArray
                        {
                            cell.likeListingObj.isEnabled = true
                            cell.likeListingObj.setTitle("\(likeArray.count)", for: .normal)
                            let objId = PFUser.current()?.objectId
                            if likeArray.contains(objId!)
                            {
                                cell.likeSelected.isHighlighted = true
                                cell.likeBtn.isSelected = true
                            }else
                            {
                                cell.likeSelected.isHighlighted = false
                                cell.likeBtn.isSelected = false
                            }
                        }else
                        {
                            cell.likeListingObj.isEnabled = false
                            cell.likeListingObj.setTitle("0", for: .normal)
                            cell.likeBtn.isSelected = false
                            cell.likeSelected.isHighlighted = false
                        }
                        
                        cell.commentListing.setTitle("\(commentCount)", for: .normal)
                        cell.selectionStyle = .none
                        return cell
                    }else{
                        // for post type images/videos/album load another cell
                        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath as IndexPath) as! HomeCell
                        cell.parent = tableView
                        cell.likeBtn.tag = indexPath.row
                        cell.likeListingObj.tag = indexPath.row
                        cell.commentListing.tag = indexPath.row
                        cell.moreBtn.tag = indexPath.row
                        cell.userProfileBtn.tag = indexPath.row
                        cell.btn_comment.tag = indexPath.row
                        cell.delegate = self
                        
                        cell.name.text = userName
                        if profileImage != nil
                        {
                            cell.profileImage.file = profileImage!
                            cell.profileImage.loadInBackground()
                        } else {
                            cell.profileImage.image =  UIImage.init(named: "dummyProfile")
                        }
                        
                        cell.time.text = timeStamp
                        
                        descriptionLbl = cell.descriptionLbl
                        
                        if descriptionLbl != nil{
                            setDescriptionLbl(descriptionLbl:descriptionLbl!,obj:obj,index:indexPath.row)
                        }
                        cell.collectionContainer.isHidden = true
                        cell.collectionVIewHeight.constant = 0
                        cell.heightOFPageController.constant = 0
                        
                        //post type basis cell reuse
                        if postType == "ALBUM" || postType == "IMAGE" || postType == "VIDEO"
                        {
                            cell.lineView.isHidden = true
                            current = "Image"
                            cell.collectionContainer.isHidden = false
                            cell.collectionVIewHeight.constant = 450
                            cell.tag = indexPath.row
                            if let postmedia = obj.object(forKey: "postmedia") as? NSArray
                            {
                                if postmedia.count>1
                                {
                                    cell.heightOFPageController.constant = 20
                                }
                                cell.objectForCollection = obj as? PFObject
                                if postType == "VIDEO"
                                {
                                    videoIndexCell = indexPath.row
                                }
                                cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: cell, forRow: indexPath.row,changeVideo:true)
                            }
                        }else
                        {
                            cell.lineView.isHidden = false
                            
                            if !(videoIndexCell+1 == indexPath.row || videoIndexCell-1 == indexPath.row)
                            {
                                print("indexPath.row =",indexPath.row)
                                //  JPVideoPlayerManager.shared().stopPlay()
                            }
                        }
                        
                        if let likeArray = obj.value(forKey: "user_like_post") as? NSArray
                        {
                            cell.likeListingObj.isEnabled = true
                            cell.likeListingObj.setTitle("\(likeArray.count)", for: .normal)
                            let objId = PFUser.current()?.objectId
                            if likeArray.contains(objId!)
                            {
                                cell.likeSelected.isHighlighted = true
                                cell.likeBtn.isSelected = true
                            }else
                            {
                                cell.likeSelected.isHighlighted = false
                                cell.likeBtn.isSelected = false
                                //   cell.likeBtn.setImage(UIImage.init(named: "likeUnSelected"), for: .normal)
                            }
                        }else
                        {
                            cell.likeListingObj.isEnabled = false
                            cell.likeListingObj.setTitle("0", for: .normal)
                            cell.likeBtn.isSelected = false
                            cell.likeSelected.isHighlighted = false
                        }
                        
                        cell.commentListing.setTitle("\(commentCount)", for: .normal)
                        
                        cell.selectionStyle = .none
                        return cell
                    }
                }
            }
            return UITableViewCell()
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath as IndexPath) as! PostTableViewCell
            cell.bottomLayoutObj.isActive = false
            if popUpArray.contains("CANCEL")
            {
                cell.topLayout.constant = 15
                if let s = popUpArray.object(at: indexPath.row) as? String{
                    if s == "REPORT ABUSE"{
                        cell.titleObj.text = "Report Abuse"
                    }
                    else{
                        cell.titleObj.text = s.lowercased().capitalizingFirstLetter()
                    }
                }
                cell.postCountObj.isHidden = true
            }else
            {
                cell.topLayout.constant = 10
                if let s = (popUpArray.value(forKey: "name") as AnyObject).object(at: indexPath.row) as? String{
                    cell.titleObj.text = s.lowercased().capitalizingFirstLetter()
                }
                cell.postCountObj.text = (popUpArray.value(forKey: "username") as AnyObject).object(at: indexPath.row) as? String
                cell.postCountObj.isHidden = false
            }
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var obj = (PFObjectArray.object(at: descriptionIndex) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: descriptionIndex) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        if tableView == popUpTableViewObj
        {
            var str = ""
            if let cell = tableView.cellForRow(at: indexPath) as? PostTableViewCell{
                str = cell.titleObj.text ?? ""
            }
            if popUpArray.contains("CANCEL")
            {
                if str.uppercased() == "CANCEL"
                {
                    self.popUp.fadeOut()
                }else if str.uppercased() == "SHARE"
                {
                    if let descriptionObj = obj.object(forKey: "description") as? String
                    {
                        var btnTitle : String = ""
                        if let users_tagged_withArray = obj.value(forKey: "users_tagged_with") as? NSArray
                        {
                            if users_tagged_withArray.count>0
                            {
                                if users_tagged_withArray.count == 1
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String)"
                                }else if users_tagged_withArray.count == 2
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \((users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String)"
                                }else
                                {
                                    
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \(users_tagged_withArray.count-1) others"
                                }
                            }
                        }
                        
                        
                        
                        var urlStr : String = ""
                        if let postmedia = obj.object(forKey: "postmedia") as? NSArray
                        {
                            if let fileObj = postmedia.object(at: 0) as? PFFile
                            {
                                urlStr = fileObj.url ?? ""
                            }
                        }
                        
                        var type = ""
                        if let media_types = obj.object(forKey: "media_types") as? NSArray
                        {
                            print(media_types)
                            type = (media_types.object(at: 0) as? String)!
                        }
                        
                        print(descriptionObj+btnTitle,urlStr)
                        
                        
                        if  type == "IMAGE"
                        {
                            let url = NSURL(string:urlStr)
                            let data = NSData(contentsOf:url! as URL)
                            if (data?.length)! > 0     {
                                let vc = UIActivityViewController(activityItems: [descriptionObj+btnTitle,UIImage(data:data! as Data)! ], applicationActivities: [])
                                present(vc, animated: true, completion: nil)
                            }}else
                        {
                            let vc = UIActivityViewController(activityItems: [descriptionObj+btnTitle,urlStr], applicationActivities: [])
                            present(vc, animated: true, completion: nil)
                        }
                    }
                    self.popUp.fadeOut()
                }
                else if str.uppercased() == "DELETE"
                {
                    self.popUp.fadeOut()
                    
                    let alert = UIAlertController(title: "Are you sure you want to delete this post?", message: "", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
                    alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
                        
                        let object = obj as! PFObject
                        var postId = ""
                        if let id = object.objectId{
                            postId = id
                        }
                        
                        object.deleteInBackground(block: { (status, error) in
                            print(status)
                            if status == true{
                                self.PFObjectArray.removeObject(at: self.descriptionIndex)
                                self.isPostExist()
                                self.tableViewObj.reloadData()
                            }else{
                                let message = (error?.localizedDescription)
                                print(message as Any)
                            }
                        })
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                }else if str.uppercased() == "EDIT"
                {
                    self.popUp.fadeOut()
                    let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
                    let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePost_VC")  as! CreatePost_VC
                    vc.objectForEdit  = obj as? PFObject
                    vc.fromEdit  = true
                    let id = obj.value(forKey: "objectId") as! String
                    vc.postForEdit = id
                    vc.indexEdit = descriptionIndex
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if str.uppercased() == "REPORT ABUSE"
                {
                    self.popUp.fadeOut()
                    let objId = PFUser.current()?.objectId
                    let postId = obj.value(forKey: "objectId") as! String
                    
                    let compClass = PFObject(className: "Report_abuse")
                    compClass["post_id"] =  postId
                    compClass["user_id"] =  objId
                    compClass.saveInBackground(block: { (status, error) in
                        print(status)
                        if status ==  true{
                            self.showMessage("This post is marked as spam.")
                        }
                        else{
                            let message = (error?.localizedDescription)
                            print(message as Any)
                        }
                    })
                }
            }else
            {
                self.popUp.fadeOut()
                self.otherUser(index: indexPath.row, name: ((popUpArray.value(forKey: "name") as AnyObject).object(at: indexPath.row) as? String)!)
            }
            
        }else
        {
        }
    }
    
    @objc func updateMuted(object:NSArray)
    {
        let array = object.value(forKey: "object") as? NSArray
        var i = 0
        for obj in PFObjectArray
        {
            let  objectdata = array?.object(at: 1) as? PFObject
            if objectdata?.value(forKey: "objectId") as? String == (obj as? PFObject)?.objectId
            {
                if let muteValue = objectdata?.value(forKey: "mute") as? Bool
                {
                    objectdata?.setObject(!(muteValue), forKey: "mute")
                    PFObjectArray.replaceObject(at: i, with: objectdata ?? obj)
                }
            }
            i=i+1
        }
    }
    
    func isRowZeroVisible(row:Int)->Bool
    {
        let array = tableViewObj.indexPathsForVisibleRows
        print(array)
        for index in array!
        {
            if (index.row == row) {
                return true
            }
        }
        return false
    }
    
    func generateThumbImage(url : NSURL) -> UIImage{
        let asset : AVAsset = AVAsset.init(url: url as URL)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time        : CMTime = CMTimeMake(value: 1, timescale: 30)
        do {
            let imageRef = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return UIImage()
        }
    }
    
    func withUser(index:Int,name:String)
    {
        print(name)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        print(index)
        
        var obj = (PFObjectArray.object(at: index) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: index) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        if let users_tagged_withArray = obj.value(forKey: "users_tagged_with") as? NSArray
        {
            if users_tagged_withArray.count>0
            {
                print(users_tagged_withArray)
                let index = (users_tagged_withArray.value(forKey: "name")  as AnyObject).index(of: name)
                let id = (users_tagged_withArray.object(at: index)as AnyObject).value(forKey: "id") as! String
                print(id)
                
                var idsArray : [String] = []
                idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
                if idsArray.contains(id)
                {
                    vc.friend = "FOLLOWING"
                }else
                {
                    vc.friend = "FOLLOW"
                }
                
                vc.objectId = id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func otherUser(index:Int,name:String)
    {
        print(name)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        print(index)
        
        let id = (popUpArray.object(at: index)as AnyObject).value(forKey: "id") as! String
        print(id)
        
        var idsArray : [String] = []
        idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
        if idsArray.contains(id)
        {
            vc.friend = "FOLLOWING"
        }else
        {
            vc.friend = "FOLLOW"
        }
        
        vc.objectId = id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.popUp.fadeOut()
    }
    
    
    
    
    func descriptionUser(index:Int,userName:String)
    {
        print(userName)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        print(index)
        
        var obj = (PFObjectArray.object(at: index) as AnyObject)
        
        if isFromHashTag {
            obj = ((PFObjectArray.object(at: index) as AnyObject).object(forKey: "parent_post") as AnyObject)
        }
        
        if let users_tagged_withArray = obj.value(forKey: "tagged_users_in_desc") as? NSArray
        {
            print(users_tagged_withArray)
            let index = (users_tagged_withArray.value(forKey: "username")  as AnyObject).index(of: userName)
            if index<99999
            {
                let id = (users_tagged_withArray.object(at: index)as AnyObject).value(forKey: "id") as! String
                print(id)
                
                var idsArray : [String] = []
                idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
                if idsArray.contains(id)
                {
                    vc.friend = "FOLLOWING"
                }else
                {
                    vc.friend = "FOLLOW"
                }
                
                vc.objectId = id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else
        {
            self.showMessage(messages.notFound)
            return
        }
    }
    //set description lbl data
    func setDescriptionLbl(descriptionLbl:ActiveLabel,obj:AnyObject,index:Int){
        if let descriptionObj = obj.object(forKey: "description") as? String
        {
            var btnTitle : String = ""
            if let users_tagged_withArray = obj.value(forKey: "users_tagged_with") as? NSArray
            {
                if users_tagged_withArray.count>0
                {
                    if users_tagged_withArray.count == 1
                    {
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String)"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {
                            self.withUser(index: index, name: $0)
                        }
                    }else if users_tagged_withArray.count == 2
                    {
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \((users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String)"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {
                            self.withUser(index: index, name: $0)
                        }
                        
                        let secondUser = (users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String
                        let customTypeSecond = ActiveType.custom(pattern: "\\s\(secondUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeSecond)
                        descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeSecond) {
                            self.withUser(index: index, name: $0)
                        }
                    }else
                    {
                        
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \(users_tagged_withArray.count-1) others"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {
                            self.withUser(index: index, name: $0)
                        }
                        
                        let otherString = "\(users_tagged_withArray.count-1) others"
                        let customTypeSecond = ActiveType.custom(pattern: "\\s\(otherString)\\b")
                        descriptionLbl.enabledTypes.append(customTypeSecond)
                        descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeSecond) {_ in
                            
                            
                            self.popUp.fadeIn()
                            self.popUpArray = users_tagged_withArray
                            
                            if self.popUpArray.count>6
                            {
                                self.popUpTableViewObj.isScrollEnabled = true
                                self.heightOfPopUpTableView.constant = 300
                            }else
                            {
                                let count = self.popUpArray.count
                                self.heightOfPopUpTableView.constant = CGFloat(count * 50)
                                self.popUpTableViewObj.isScrollEnabled = false
                            }
                            self.popUpTableViewObj.reloadData()
                        }
                    }
                }
            }
            
            descriptionLbl.customize { label in
                
                descriptionLbl.text = descriptionObj+btnTitle
                
                let arr = descriptionObj.components(separatedBy: "\n")
                
                if (secondTime == false && arr.count > 3) || (secondTime == false && descriptionObj.characters.count > 100)
                {
                    var lengthObj = 0
                    if descriptionObj.characters.count > 100
                    {
                        lengthObj = 100
                    }else
                    {
                        lengthObj = descriptionObj.characters.count
                    }
                    
                    let index = descriptionObj.index(descriptionObj.startIndex, offsetBy: lengthObj)
                    let substringDescription = descriptionObj.substring(to: index)
                    
                    label.text = substringDescription+" Continue Reading"
                    let continueReading = ActiveType.custom(pattern: "\\sContinue Reading\\b")
                    descriptionLbl.enabledTypes.append(continueReading)
                    descriptionLbl.customColor[continueReading] = ConstantColors.themeColor
                    descriptionLbl.handleCustomTap(for: continueReading) {_ in
                        print("hello")
                       
                        
                        let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
                        let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        vc.secondTime = true
                        vc.isFromHashTag = false
                        vc.titleSecond = "network 42"
                        vc.showOnlySinglePost = true
                        let id = obj.value(forKey: "objectId") as! String
                        vc.postIdObject = id
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
                descriptionLbl.isHidden = false
                descriptionLbl.numberOfLines = 0
                descriptionLbl.lineSpacing = 4
                descriptionLbl.textColor = UIColor(red: 102.0/255, green: 117.0/255, blue: 127.0/255, alpha: 1)
                descriptionLbl.hashtagColor = ConstantColors.themeColor
                descriptionLbl.mentionColor = ConstantColors.themeColor
                descriptionLbl.URLColor = ConstantColors.themeColor
                descriptionLbl.URLSelectedColor = UIColor(red: 82.0/255, green: 190.0/255, blue: 41.0/255, alpha: 1)
                
                descriptionLbl.handleMentionTap {
                    self.descriptionUser(index: index, userName: $0)
                }

                descriptionLbl.handleHashtagTap {
                    // MARK: - Hashtag
                    if self.secondTime == false
                    {
//                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
//                        vc.secondTime = true
//                        vc.titleSecond = $0
//                        print($0)
//                        self.navigationController?.pushViewController(vc, animated: true)
                        
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "HashTag_VC") as! HashTag_VC
                        vc.tagString = "#\($0)" 
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
                descriptionLbl.handleURLTap {
                    let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
                    let vc = storyboardObj.instantiateViewController(withIdentifier: "MTSWebViewController") as! MTSWebViewController
                    
                    print($0.absoluteString)
                    if let link_url = obj.object(forKey: "link_url") as? String
                    {
                        vc.urlStr = link_url
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    
    func alert(_ title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        vc.addAction(UIAlertAction(title: Constants.okTitle, style: .cancel, handler: nil))
        present(vc, animated: true, completion: nil)
    }
    
    func isPostExist() {
        if PFObjectArray.count == 0 {
            noPostLabel.isHidden = false
        } else {
            noPostLabel.isHidden = true
        }
    }
}


extension HomeViewController
{
    func callHomeListApi(isRefresh: Bool = false)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        self.homeApiCalled = true
        let displayLimit = 20
        let objId = PFUser.current()?.objectId
        var idsArray : [String] = []
        if let ids = globalDic.tagUserDictionary.value(forKey: "id") as? [String]
        {
            idsArray = ids
            idsArray.append(objId!)
        }
     
        print(idsArray)
        self.indicatorObj.startAnimating()
        let query = PFQuery(className: "Post")
        query.order(byDescending: "createdAt")
        query.whereKey("user_id", containedIn: idsArray)
        query.includeKey("parent_id")
        if Constants.visitedSecondHome
        {
            Constants.visitedSecondHome = false
        }else
        {
            query.limit =  displayLimit
            query.skip = isRefresh ? 0 : self.PFObjectArray.count
        }
        query.findObjectsInBackground(block: { (objects, error) in
            if self.refreshControl != nil{
                self.refreshControl.endRefreshing()
            }
            if self.indicatorObj != nil{
                self.indicatorObj.stopAnimating()
            }
            if error == nil {
                 self.homeApiCalled = false
                if isRefresh {
                    self.PFObjectArray.removeAllObjects()
                }
                
                for object in objects! {
                    print(object)
                    
                    object.setObject(true, forKey: "mute")
                    self.PFObjectArray.add(object)
                    
                    if self.PFObjectArray.count%Constants.addAfter==0
                    {
                        self.PFObjectArray.add(Constants.advertisement)
                    }
                }
                self.isPostExist()
                self.tableViewObj.reloadData()
                self.tableViewObj.layoutIfNeeded()
                self.pageCount = self.pageCount + 1
                
                if objects!.count == 0 && self.PFObjectArray.count > 1 {
                    //self.showMessage(messages.noPost)
                    return
                }
            }
            else {
                print(error!)
            }
        })
    }
    
    
    func hashTags(hashTag:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "Hashtags")
        query.whereKey("name", equalTo: hashTag)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                var hashTagId : String = ""
                for object in objects! {
                    //print(object)
                    hashTagId = object.objectId!
                }
                
                let query = PFQuery(className: "Hashtag_posts")
                query.includeKey("parent_post")
                query.includeKey("post_user")
                query.order(byDescending: "createdAt")
                query.whereKey("hashtag_id", equalTo: hashTagId)
                query.findObjectsInBackground(block: { (objects, error) in
                    if error == nil {
                        
                        //let PFObjectArr : NSMutableArray = []
                        //  print(objects!)
                        for object in objects! {
                            //  print(object)
                            object.setObject(true, forKey: "mute")
                            self.PFObjectArray.add(object)
                        }
                        let postStr = self.PFObjectArray.count > 1 ? "Posts" : "Post"
                        self.topHeadingTitle.text = "\(hashTag) (\(self.PFObjectArray.count) \(postStr))"
                        self.isFromHashTag = true
                        self.isPostExist()
                        self.tableViewObj.reloadData()
                    }
                    else {
                        print(error!)
                    }
                })
            }
            else {
                print(error!)
            }
        })
    }
    
    
    
    @objc func callHomeListApiForFirstRecord(indexObj: NSNotification)
    {

        globalBool.showProgess = true
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        var arr : NSMutableArray = []
        arr = indexObj.object as! NSMutableArray
        let query = PFQuery(className: "Post")
        query.whereKey("objectId", equalTo:arr.object(at: 1))
        query.includeKey("parent_id")
        query.findObjectsInBackground(block: { (objects, error) in
            self.heightOFProgressView.constant = 0
            self.progressVIew.isHidden = true
            globalBool.showProgess = false
            if error == nil {
                
                for object in objects! {
               //     print(object)
                    if let typeStr = object.value(forKey: "post_type") as? String{
                        
                        if  self.homeApiCalled == false{
                            if arr.object(at: 2) as! Bool == false
                            {
                                self.PFObjectArray.insert(object, at: 0)
                            }else
                            {
                                self.PFObjectArray.replaceObject(at: arr.object(at: 0) as! Int, with: object)
                            }
                        }
                    }
                    
                    if self.PFObjectArray.count>1
                    {
                        let indexPath = NSIndexPath.init(row: arr.object(at: 0) as! Int, section: 0)
                        self.tableViewObj.scrollToRow(at: indexPath as IndexPath , at: .top, animated: true)
                    }
                    self.isPostExist()
                    self.tableViewObj.reloadData()
                }
            }
            else {
                print(error!)
            }
        })
    }
    
    
    func likeAndCommentPost(postId: String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "Post")
        query.whereKey("objectId", equalTo:postId)
        query.includeKey("parent_id")
        query.findObjectsInBackground(block: { (objects, error) in
            self.heightOFProgressView.constant = 0
            self.progressVIew.isHidden = true
            globalBool.showProgess = false
            if error == nil {
                
                for object in objects! {
                    print(object)
                    object.setObject(true, forKey: "mute")
                    self.PFObjectArray.add(object)
                }
                self.isPostExist()
                self.isFromLike = true
                self.tableViewObj.reloadData()
            }
            else {
                print(error!)
            }
        })
    }
    
}


extension HomeViewController
{
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "Just now"
        } else {
            return "Just now"
        }
    }
}





