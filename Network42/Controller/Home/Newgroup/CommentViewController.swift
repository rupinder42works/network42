//
//  CommentViewController.swift
//  Network42
//
//  Created by 42works on 19/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import Firebase
import IQKeyboardManagerSwift

@objc protocol commentCountUpdateDelegate {
    func updateCommentCount(index:Int,value:String)
}


class CommentViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var tableViewObj: UITableView!
    
    var postId : String = ""
    var postUserId : String = ""
    var indexOfpost : Int = 0
    var fromProfile : Bool = false
    var PFObjectArray : NSMutableArray = []
    @IBOutlet var likeLbl: UILabel!
    @IBOutlet var commentTextView: RSKPlaceholderTextView!
    
    var objectFromHome : PFObject?
    
    var commentObj : String = ""
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet var commentBtn: TKTransitionSubmitButton!
    
    @IBOutlet weak var btn_backComment: UIButton!
    
    @IBOutlet weak var vw_container: UIView!
    var globalRange = NSRange()
    var globalInt : Int = 1
    var currentWord : String = ""
    var updatedWord : String = ""
    var savedArrayUser : NSMutableArray = []
    var taggedUser : Bool = false
    var tagUserNameArray : [String] = []
    var searchResultArray :[String] = []
    var indexArray : [Int] = []
    @IBOutlet var taggedContainer: UIView!
    @IBOutlet var taggedUserTable: UITableView!
    var searchActive : Bool = false
    var rangeToAppend = NSRange()
    var tagNameArray : NSMutableArray = []
    var tagProfileImageArray : NSMutableArray = []
    var idsArray : NSMutableArray = []
    
    weak var delegate: commentCountUpdateDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  print(objectFromHome)
        vw_container.layer.borderColor = UIColor.lightGray.cgColor
        self.tabBarController?.tabBar.isHidden = true
        IQKeyboardManager.shared.enable = false
        
        idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! NSMutableArray
        tagUserNameArray =  globalDic.tagUserDictionary.value(forKey: "userName")  as! [String]
        tagNameArray =  globalDic.tagUserDictionary.value(forKey: "name") as! NSMutableArray
        tagProfileImageArray =  globalDic.tagUserDictionary.value(forKey: "image") as! NSMutableArray
        
        
        tableViewObj.register(UINib(nibName: "CommentCell", bundle: nil), forCellReuseIdentifier: "CommentCell")
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 702
        
        taggedUserTable.register(UINib(nibName: "TagTableViewCell", bundle: nil), forCellReuseIdentifier: "TagTableViewCell")
        taggedUserTable.rowHeight = UITableView.automaticDimension
        taggedUserTable.estimatedRowHeight = 702
        
        
        if let likeArray = objectFromHome?.value(forKey: "user_like_post") as? NSArray
        {
            likeLbl.text = "\(likeArray.count) likes"
        }else
        {
            likeLbl.text = "0 likes"
        }
        
        
        self.Post_comments()
        
        IQKeyboardManager.shared.enable = true
        // NotificationCenter.default(self, selector: #selector(CommentViewController.keyboardWasShown(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
     //   NotificationCenter.default.addObserver(self, selector: #selector(CommentViewController.keyboardWasShown(notification:)), name:UIResponder.keyboardWillShowNotification, object: nil);
        
       // NotificationCenter.default.addObserver(self, selector: #selector(CommentViewController.keyboardWillHide(notification:)), name:UIResponder.keyboardWillHideNotification, object: nil);
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.tabBarController?.tabBar.isHidden = true
        taggedContainer.isHidden = true
    }
    
    @objc func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConstraint.constant = keyboardFrame.size.height
        })
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.bottomConstraint.constant = 0
        })
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func toLikes(_ sender: Any) {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "LikesViewController") as! LikesViewController
        vc.postId = postId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    
    
    
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView==taggedUserTable
        {
            if searchActive
            {
                return searchResultArray.count
            }
            return tagNameArray.count
        }
        return PFObjectArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView==tableViewObj
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath as IndexPath) as! CommentCell
            
            if let user = (PFObjectArray.object(at: indexPath.row) as AnyObject).value(forKey: "parent_id") as? PFUser{
                 cell.name.text = user.value(forKey: "name") as? String
                if let userPicture = user["profile_image"] as? PFFile {
                    cell.profileImage.file = userPicture
                    cell.profileImage.loadInBackground()
                }
                else
                {
                    cell.profileImage.image = UIImage.init(named: "dummyProfile")
                }
            }else{
                 cell.name.text = "Anonymous"
                 cell.profileImage.image = UIImage.init(named: "dummyProfile")
            }
           
            cell.comment.text = (PFObjectArray.object(at: indexPath.row) as AnyObject).value(forKey: "comment") as? String
            if let timeStampObj = (PFObjectArray.object(at: indexPath.row) as AnyObject).value(forKey: "comment_time") as? String
            {
                let newdateStr = timeStampObj.replacingOccurrences(of: "_", with: " ")
                let dateFormatter = DateFormatter()
                if timeStampObj.lowercased().contains("am") == true || timeStampObj.lowercased().contains("pm") == true{
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
                }else{
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"

                }
                let newdate = dateFormatter.date(from: newdateStr)
                let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                cell.timeObj.text = timeStr
            }
            
            
            cell.comment.customize { label in
                cell.comment.mentionColor = ConstantColors.themeColor
                cell.comment.hashtagColor = cell.comment.textColor
                cell.comment.handleMentionTap {
                    self.descriptionUser(index: indexPath.row, userName: $0)
                }
            }
            cell.selectionStyle = .none
            return cell
            
        }else
        {
            if searchActive
            {
                if taggedUser
                {
                    let   cell = tableView.dequeueReusableCell(withIdentifier: "TagTableViewCell", for: indexPath as IndexPath) as! TagTableViewCell
                    let indexObj = indexArray[indexPath.row]
                    cell.name.text = tagNameArray.object(at: indexObj) as? String
                    cell.userName.text = tagUserNameArray[indexObj]
                    if let file = tagProfileImageArray.object(at: indexObj) as? PFFile
                    {
                        cell.profileImage.file = file
                        cell.profileImage.loadInBackground()
                    }else
                    {
                        cell.profileImage.image = UIImage.init(named: "dummyProfile")
                    }
                    cell.selectionStyle = .none
                    return cell
                    
                }else
                {
                    let  cell = tableView.dequeueReusableCell(withIdentifier: "HashTagCell", for: indexPath as IndexPath) as! HashTagCell
                    cell.name.text = searchResultArray[indexPath.row]
                    cell.selectionStyle = .none
                    return cell
                    
                }
            }
            else
            {
                let   cell = tableView.dequeueReusableCell(withIdentifier: "TagTableViewCell", for: indexPath as IndexPath) as! TagTableViewCell
                cell.name.text = tagNameArray.object(at: indexPath.row) as? String
                cell.userName.text = tagUserNameArray[indexPath.row]
                if let file = tagProfileImageArray.object(at: indexPath.row) as? PFFile
                {
                    cell.profileImage.file = file
                    cell.profileImage.loadInBackground()
                }else
                {
                    cell.profileImage.image = UIImage.init(named: "dummyProfile")
                }
                cell.selectionStyle = .none
                return cell
            }
            
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == taggedUserTable
        {
            let str : NSMutableString =   NSMutableString(string: commentTextView.text)
            if searchActive == true
            {
                
                let indexObj = indexArray[indexPath.row]
                let dic : NSMutableDictionary = [:]
                dic.setValue(idsArray.object(at: indexObj) as! String, forKey: "id")
                dic.setValue(tagNameArray.object(at: indexObj) as! String, forKey: "name")
                dic.setValue(tagUserNameArray[indexObj], forKey: "username")
                savedArrayUser.add(dic)
                str.replaceCharacters(in: rangeToAppend, with: "@"+tagUserNameArray[indexObj])
                
            }else
            {
                let dic : NSMutableDictionary = [:]
                dic.setValue(idsArray.object(at: indexPath.row) as! String, forKey: "id")
                dic.setValue(tagNameArray.object(at: indexPath.row) as! String, forKey: "name")
                dic.setValue(tagUserNameArray[indexPath.row], forKey: "username")
                savedArrayUser.add(dic)
                str.replaceCharacters(in: rangeToAppend, with: "@"+tagUserNameArray[indexPath.row])
            }
            
            let   mutableStr = NSMutableString(string: str)
            print(mutableStr)
            commentTextView.text = mutableStr as String
            taggedContainer.isHidden = true
        }else
        {
            self.view.endEditing(true)
            if let user = (PFObjectArray.object(at: indexPath.row) as AnyObject).value(forKey: "parent_id") as? PFUser{
                if user.objectId == PFUser.current()?.objectId{
                     self.alertMethod(indexObj: indexPath.row)
                }
            }
        }
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func alertMethod(indexObj:Int)
    {
        let alert = UIAlertController(title: "Are you sure you want to delete this commment?", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
            
            let object = self.PFObjectArray.object(at: indexObj) as? PFObject
            object?.deleteInBackground(block: { (status, error) in
                print(status)
                if status == true{
                    self.PFObjectArray.removeObject(at: indexObj)
                    self.tableViewObj.reloadData()
                    
                    let queryComp = PFQuery(className: "Post")
                    queryComp.whereKey("objectId", equalTo:self.postId)
                    queryComp.findObjectsInBackground(block: { (objects, error) in
                        if error == nil {
                            print(objects?.count as Any)
                            
                            for object in objects! {
                                object["comment_count"] = "\(self.PFObjectArray.count)"
                                object.saveInBackground { (success, error) in
                                    if(success)
                                    {
                                        self.delegate?.updateCommentCount(index: self.indexOfpost, value: "\(self.PFObjectArray.count)")
                                    }
                                    else{
                                        self.showMessage((error?.localizedDescription)!)
                                        return
                                    }
                                }
                            }
                        }
                        else{
                            let message = (error?.localizedDescription)
                            print(message as Any)
                            self.showMessage((error?.localizedDescription)!)
                        }
                    })
                    
                    
                }else{
                    let message = (error?.localizedDescription)
                    print(message as Any)
                }
            })
            
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
    func descriptionUser(index:Int,userName:String)
    {
        print(userName)
        let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = tabbarSB.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        
        print(index)
        if let users_tagged_in_comments = (self.PFObjectArray.object(at: index) as AnyObject).value(forKey: "users_tagged_in_comments") as? NSArray
        {
            print(users_tagged_in_comments)
            let index = (users_tagged_in_comments.value(forKey: "username")  as AnyObject).index(of: userName)
            if index<2000
            {
                let id = (users_tagged_in_comments.object(at: index)as AnyObject).value(forKey: "id") as! String
                print(id)
                let objId = PFUser.current()?.objectId
                
                vc.userProfile = false
                if objId == id
                {
                    vc.userProfile = true
                }
                vc.friend = "FOLLOWING"
                
                let nameQuery : PFQuery = PFUser.query()!
                nameQuery.whereKey("objectId", equalTo: id)
                nameQuery.findObjectsInBackground(block: { (objects, error) in
                    print(objects!)
                    for object in objects! {
                        vc.userFromSearch = object as? PFUser
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                })
            }else
            {
                self.showMessage(messages.notFound)
                return
            }
        }else
        {
            self.showMessage(messages.notFound)
            return
        }
    }
    
    
    
    //MARK:-Get Post_comments
    func Post_comments(){
        let query = PFQuery(className:"Post_comments")
        query.includeKey("parent_id")
        query.whereKey("post_id", equalTo: postId)
        //query.order(byDescending: "createdAt")
        query.order(byAscending: "createdAt")
        query.findObjectsInBackground(block: { (objects, error) in
            self.indicator.stopAnimating()
            if error == nil {
                print(objects!)
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    PFObjectArr.add(object)
                }
                self.PFObjectArray = PFObjectArr
                self.tableViewObj.reloadData()
            }
            else {
                print(error!)
                self.showMessage(NSLocalizedDescriptionKey)
            }
            
        })
        
    }
    
    
    func Post_comments_One_Obj(){
        let query = PFQuery(className:"Post_comments")
        query.includeKey("parent_id")
        query.whereKey("post_id", equalTo: postId)
        query.whereKey("objectId", equalTo: self.commentObj)
        query.findObjectsInBackground(block: { (objects, error) in
            self.btn_backComment.isHidden = false
          //  self.commentBtn.setImage(UIImage.init(named: "sendComment.png"), for: .normal)
//            self.commentBtn.returnToOriginalState()
//            self.commentBtn.layer.cornerRadius = self.commentBtn.normalCornerRadius
            if error == nil {
                print(objects!)
                for object in objects! {
                    self.PFObjectArray.add(object)
                }
                self.btn_backComment.isHidden = false
                self.commentTextView.resignFirstResponder()
                self.commentTextView.text = ""
                self.tableViewObj.reloadData()
                
                let indexPath = NSIndexPath.init(row: self.PFObjectArray.count-1, section: 0)
                self.tableViewObj.scrollToRow(at: indexPath as IndexPath, at: .bottom, animated: true)
                self.tableViewObj.reloadData()
                
            }
            else {
                
                self.btn_backComment.isHidden = false
                print(error!)
                self.showMessage(NSLocalizedDescriptionKey)
            }
            
        })
        
    }
    
    
    
    
    @IBAction func postComment(_ sender: Any) {
        
        guard commentTextView.text.characters.count>0 else {
            return
        }
        
        if let firstObj = commentTextView.text.characters.first {
            if firstObj == " "
            {
                return
            }
        }
        
        commentTextView.resignFirstResponder()
        
        
        let query = PFQuery(className:"BlockedUsers")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        query.whereKey("BlockedUser", equalTo: id)
        query.whereKey("FromUser", equalTo: self.postUserId)
        query.findObjectsInBackground(block: { (objects2, error) in
            //self.isLoading = false
            if error == nil {
                if objects2?.count ?? 0 > 0{
                    self.showMessage("You can't comment on this post because user has blocked you")
                }else{
                    self.commentBtn.startLoadingAnimation()
                    self.commentBtn.setImage(nil, for: .normal)
                    self.btn_backComment.isHidden = true
                    
                    let commentClass = PFObject(className: "Post_comments")
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                    
                    commentClass["post_id"] = self.postId
                    commentClass["comment"] = self.commentTextView.text
                    commentClass["comment_time"] = formatter.string(from: Date())
                    commentClass["parent_id"] = PFUser.current()
                    commentClass["users_tagged_in_comments"] = self.savedArrayUser
                    
                    commentClass.saveInBackground { (success, error) in
                        if(success)
                        {
                            self.commentObj = commentClass.objectId!
                            
                            print("Post saved")
                            
                            let queryComp = PFQuery(className: "Post")
                            queryComp.whereKey("objectId", equalTo:self.postId)
                            queryComp.findObjectsInBackground(block: { (objects, error) in
                                if error == nil {
                                    print(objects?.count as Any)
                                    
                                    for object in objects! {
                                        object["comment_count"] = "\(self.PFObjectArray.count+1)"
                                        object.saveInBackground { (success, error) in
                                            
                                            if(success)
                                            {
                                                self.delegate?.updateCommentCount(index: self.indexOfpost, value: "\(self.PFObjectArray.count+1)")
                                                print("Post saved")
                                                let nameQuery : PFQuery = PFUser.query()!
                                                nameQuery.whereKey("objectId", equalTo: self.postUserId)
                                                nameQuery.findObjectsInBackground(block: { (objects, error) in
                                                    // print(objects!)
                                                    var comment = ""
                                                    if let str =  commentClass["comment"] as? String{
                                                        comment = str
                                                    }
                                                    let obj = objects![0] as? PFUser
                                                    let objId = PFUser.current()?.objectId
                                                    self.saveToNotifications(from_id: objId!, to_id: self.postUserId, notification_type: "post_comment", time: formatter.string(from: Date()), post_id: self.postId, parent_from: PFUser.current()!, parent_to: obj!,commentStr:comment)
                                                    self.commentPushNotification(userId: self.postUserId,postId: self.postId)
                                                })
                                                
                                                
                                                if self.savedArrayUser.count>0
                                                {
                                                    let userIdArray = self.savedArrayUser.value(forKey: "id") as! NSArray
                                                    
                                                    self.savedArrayUser.removeAllObjects()
                                                    var count : Int = 0
                                                    for userId in userIdArray
                                                    {
                                                        
                                                        let nameQuery : PFQuery = PFUser.query()!
                                                        nameQuery.whereKey("objectId", equalTo: userId)
                                                        nameQuery.findObjectsInBackground(block: { (objects, error) in
                                                            print(objects!)
                                                            count = count+1
                                                            let obj = objects![0] as? PFUser
                                                            let objId = PFUser.current()?.objectId
                                                            self.saveToNotifications(from_id: objId!, to_id: userId as! String, notification_type: "comment_tag", time: formatter.string(from: Date()), post_id: self.postId, parent_from: PFUser.current()!, parent_to: obj!,commentStr: "")
                                                        })
                                                    }
                                                    
                                                    
                                                    self.taggedUserPushNotification(userIdArray: userIdArray, postId: self.postId, pushType: "comment_tag", message: messages.userMentionedInComment)
                                                }
                                                self.commentBtn.returnToOriginalState()
                                                self.commentBtn.layer.cornerRadius = self.commentBtn.normalCornerRadius
                                                self.Post_comments_One_Obj()
                                            }
                                            else{
                                                self.showMessage((error?.localizedDescription)!)
                                                //  self.commentBtn.setImage(UIImage.init(named: "sendComment.png"), for: .normal)
                                                self.commentBtn.returnToOriginalState()
                                                self.commentBtn.layer.cornerRadius = self.commentBtn.normalCornerRadius
                                                print("error",error!)
                                                return
                                            }
                                        }
                                    }
                                }
                                else{
                                    //self.commentBtn.setImage(UIImage.init(named: "sendComment.png"), for: .normal)
                                    self.commentBtn.returnToOriginalState()
                                    self.commentBtn.layer.cornerRadius = self.commentBtn.normalCornerRadius
                                    let message = (error?.localizedDescription)
                                    print(message as Any)
                                    self.showMessage((error?.localizedDescription)!)
                                }
                            })
                            
                        }
                        else{
                            print("error",error!)
                            // self.commentBtn.setImage(UIImage.init(named: "sendComment.png"), for: .normal)
                            self.showMessage((error?.localizedDescription)!)
                            self.commentBtn.returnToOriginalState()
                            self.commentBtn.layer.cornerRadius = self.commentBtn.normalCornerRadius
                            return
                        }
                    }
                }
            }
            else {
                self.showMessage((error?.localizedDescription)!)
            }
        })
    }
    
    func commentPushNotification(userId:String,postId:String)
    {
        if !(userId == PFUser.current()!.objectId!)
        {
            let currentName = PFUser.current()?.value(forKey: "name") as! String
            self.sendPushNotification(postId: postId, userId: userId,settingType: "push_post_comment",message:"\(currentName) \(messages.commentPost)",type:"post_comment",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
        }
    }
    
    
    func taggedUserPushNotification(userIdArray:NSArray,postId:String,pushType:String,message:String)
    {
        var count = 0
        let arrayOfUsers = NSMutableArray()
        
        for (_, element) in userIdArray.enumerated() {
            
            let childPath = "chatPush/\(element)/tagComment"
            
            FIRDatabase.database().reference().child(childPath).observeSingleEvent(of: .value, with: { (snapshot) in
                
                print("...., \(element)")
                
                if let pushEnabled = snapshot.value as? Bool {
                    if pushEnabled {
                        arrayOfUsers.add(element)
                    }
                } else {
                    arrayOfUsers.add(element)
                }
                
                count += 1
                
                if count == userIdArray.count {
                    let currentName = PFUser.current()?.value(forKey: "name") as! String
                    self.sendPushNotification(postId: postId, userId: "",settingType: "push_comment_tag",message:"\(currentName) \(message)",type:pushType,userIdArray:arrayOfUsers,groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                }
            })
        }
    }
    
    
    // MARK: UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    
}

extension CommentViewController
{
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
}


extension CommentViewController : UITextViewDelegate
{
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        let text = textView.text as NSString
        let substring = text.substring(with: NSRange(location: 0, length: globalRange.location+globalInt))
        currentWord = substring.components(separatedBy: " ").last!
        print("currentWord==",currentWord)
        var wordWithSpace = substring.components(separatedBy: " ").last!
        wordWithSpace = " "+wordWithSpace
        let lastChar = String(currentWord.characters.suffix(1))
        print("savedArrayUser==",savedArrayUser)
        let usernameArr = savedArrayUser.value(forKey: "username") as! NSArray
        
        if (usernameArr.contains(currentWord) || wordWithSpace.contains(" @")) && !(lastChar==" ")
        {
            taggedUser = true
            updatedWord =  currentWord.replacingOccurrences(of: "@", with: "")
            searchResultArray = tagUserNameArray.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: updatedWord, options: NSString.CompareOptions.caseInsensitive)
                return range.location == 0
            })
            
            indexArray = [Int]()
            for i in 0..<searchResultArray.count
            {
                let indexOfValue = tagUserNameArray.index(of: searchResultArray[i])
                indexArray.append(indexOfValue!)
            }
            print("searchResultArray=====",searchResultArray)
            if(searchResultArray.count == 0){
                if wordWithSpace == " @"
                {
                    taggedContainer.isHidden = false
                }else{
                    taggedContainer.isHidden = true
                }
                searchActive = false;
            } else {
                searchActive = true;
                taggedContainer.isHidden = false
            }
            self.taggedUserTable.reloadData()
            
            
            
            if let range = textView.selectedTextRange {
                if range.start == range.end {
                    let beginning = textView.beginningOfDocument
                    let selectedRange = textView.selectedTextRange
                    let selectionStart = selectedRange?.start
                    let selectionEnd = selectedRange?.end
                    let location = textView.offset(from: beginning, to: selectionStart!)
                    let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                    print(length)
                    
                    print("currentWord======",currentWord)
                    rangeToAppend =  NSMakeRange((location-(currentWord.characters.count)), (currentWord.characters.count))
                    print("rangeToAppend location ==",rangeToAppend.location)
                    print("rangeToAppend length ==",rangeToAppend.length)
                }
            }
        }else
        {
            taggedContainer.isHidden = true
        }
    }
    
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        globalRange = range
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        var textWidth =  textView.frame.inset(by: textView.textContainerInset).width
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        let boundingRect = sizeOfString(string: newText, constrainedToWidth: Double(textWidth), font: textView.font!)
        let numberOfLines = boundingRect.height / textView.font!.lineHeight;
        
        
        let isBackSpace = strcmp(text, "\\b")
        if (isBackSpace == -92) {
            globalInt = 0
            //removeCount = removeCount-1
        }else
        {
            globalInt = 1
            //removeCount = removeCount+1
        }
        
        
        return numberOfLines <= 10;
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        taggedContainer.isHidden = true
    }
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSAttributedString.Key.font: font],
                                                 context: nil).size
    }
    
    
}


