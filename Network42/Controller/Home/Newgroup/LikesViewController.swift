//
//  LikesViewController.swift
//  Network42
//
//  Created by 42works on 18/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse

class LikesViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var tableViewObj: UITableView!
    
    var postId : String = ""
    var PFObjectArray : NSMutableArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.tabBarController?.tabBar.isHidden = true
        
        tableViewObj.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "SearchUserCell")
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 702
 
        self.Post_likes()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PFObjectArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchUserCell", for: indexPath as IndexPath) as! SearchUserCell
        
        if PFObjectArray.count > indexPath.row{
            if let user = (PFObjectArray.object(at: indexPath.row) as AnyObject).value(forKey: "parent_id") as? PFUser
            {
                cell.followBtn.isHidden = true
                cell.name.text = user.value(forKey: "name") as? String
                cell.userName.text = user.value(forKey: "user_name") as? String
                if let userPicture = user["profile_image"] as? PFFile {
                    userPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                        let image = UIImage(data: imageData!)
                        if image != nil {
                            cell.profileImage.image = (image)!
                        }
                    })
                }
                else
                {
                    cell.profileImage.image = UIImage.init(named: "dummyProfile")
                }
                
            }else{
                cell.followBtn.isHidden = true
                cell.name.text = "Anonymous"
                cell.userName.text = ""
                cell.profileImage.image = UIImage.init(named: "dummyProfile")
            }
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
      
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
       
        if let user = (PFObjectArray.object(at: indexPath.row) as AnyObject).value(forKey: "parent_id") as? PFUser{
            let vc = tabbarSB.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.fromSearch = true
            let objId = PFUser.current()?.objectId
            let id = user.objectId
            if !(id == objId){
                vc.userProfile = false
            }else
            {
                vc.userProfile = true
            }
            vc.friend = "FOLLOWING"
            vc.userFromSearch = user
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    
    //MARK:-Get Push FLags
    func Post_likes(){
        let query = PFQuery(className:"Post_likes")
        query.includeKey("parent_id")
        query.whereKey("post_id", equalTo: postId)
        query.findObjectsInBackground(block: { (objects, error) in
            self.indicator.stopAnimating()
            if error == nil {
                print(objects!)
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    PFObjectArr.add(object)
                }
                self.PFObjectArray = PFObjectArr
                self.tableViewObj.reloadData()
            }
            else {
                print(error!)
                self.showMessage(NSLocalizedDescriptionKey)
            }
            
        })
    }
    
}
