//
//  CommentCell.swift
//  Network42
//
//  Created by 42works on 24/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import ActiveLabel

class CommentCell: UITableViewCell {
    
    @IBOutlet weak var vw_profileBackground: UIView!
    //MARK:- IBOutlets
    @IBOutlet var profileImage: PFImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var timeObj: UILabel!
    @IBOutlet var comment: ActiveLabel!
    @IBOutlet var containerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- Config
    func config(_ commentObj: Comment) {
        comment?.text = commentObj.message
        timeObj?.text = commentObj.time
        name?.text = commentObj.user?.name ?? "Anonymous"
        vw_profileBackground.layer.borderColor = UIColor.init(red: 205/255, green: 205/255, blue: 205/255, alpha: 0.8).cgColor
        if let pic = commentObj.user?.profilePic {
            profileImage?.file = pic
            profileImage.loadInBackground()
        } else {
            profileImage.image = UIImage(named: "dummyProfile")
        }
    }
}
