//
//  UserInfoCell.swift
//  Network42
//
//  Created by 42works on 27/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol profileImgDelegate {
    @objc optional func editImgClickedDelegate(_ index:Int,cell:UserInfoCell,type:String)
}

@objc protocol followDelegateProfile {
    @objc optional func followClickedDelegateProfile(cell:UserInfoCell)
     
}

@objc protocol followListDelegate {
   
    @objc optional func followClickedtolistCount(cell:UserInfoCell,type:String)
    @objc optional func followingClickedtolistCount(cell:UserInfoCell,type:String)
     @objc optional func sendMsgClickedtoChatlist(cell:UserInfoCell)
}

class UserInfoCell: UITableViewCell {
    @IBOutlet weak var btn_follow: UIButton!
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var btn_message: UIButton!
    @IBOutlet weak var ht_constrnt_followOther: NSLayoutConstraint!
    @IBOutlet weak var vw_otherUserFollow: UIView!
    @IBOutlet weak var ht_singleLocation: NSLayoutConstraint!
    @IBOutlet weak var vw_stackAdditional: UIStackView!
    @IBOutlet weak var ht_locationView: NSLayoutConstraint!
    @IBOutlet var locationInsideView: UIView!
    @IBOutlet weak var editUserImgBtn: UIButton!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var locIcon: UIImageView!
   
    @IBOutlet var profileBackView: UIView!
    @IBOutlet var singleViewIcon: UIImageView!
    @IBOutlet var singleViewLocation: UIView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet var followingLbl: UILabel!
    @IBOutlet var followLbl: UILabel!
    @IBOutlet var singleViewTitle: UILabel!
    @IBOutlet var websiteIcon: UIImageView!
    @IBOutlet var websiteLbl: UILabel!
    @IBOutlet var singleView_height: NSLayoutConstraint!
   
    
    //delegate outlets
    weak var delegate: profileImgDelegate?
    weak var delegateProfile: followDelegateProfile?
    weak var delegateFollowlist: followListDelegate?
    
    
    @IBOutlet weak var followingCountLbl: UILabel!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var designation: UILabel!
    @IBOutlet var locationView: UIView!
    @IBOutlet var followerViewContainer: UIView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var editBgButtonObj: UIButton!
    @IBOutlet var bgImageHeight: NSLayoutConstraint!

    @IBOutlet var heightOfSingleViewLocation: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btn_follow.layer.borderColor = #colorLiteral(red: 0.4864336848, green: 0.2004309297, blue: 0.9574463964, alpha: 1).cgColor
        btn_message.layer.borderColor = #colorLiteral(red: 0.4864336848, green: 0.2004309297, blue: 0.9574463964, alpha: 1).cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func editUserImg(_ sender: Any) {
        self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "user")
    }
    
    @IBAction func followAction(_ sender: Any) {
        self.delegateProfile?.followClickedDelegateProfile!(cell: self)
    }
    
    @IBAction func editBgImage(_ sender: Any) {
        self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "background")
    }
    
    @IBAction func finalFollowingList(_ sender: Any) {
        self.delegateFollowlist?.followingClickedtolistCount!(cell: self,type: "Follow")
    }
    
    @IBAction func finalFollowerList(_ sender: Any) {
        self.delegateFollowlist?.followClickedtolistCount!(cell: self,type: "Following")
    }
    
    @IBAction func messageTapped(_ sender: Any) {
        self.delegateFollowlist?.sendMsgClickedtoChatlist!(cell:self)
    }
}
