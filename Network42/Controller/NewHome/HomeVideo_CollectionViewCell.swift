//
//  HomeVideo_CollectionViewCell.swift
//  Network42
//
//  Created by Apple on 20/02/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import AVFoundation
import Parse
import ParseUI
protocol muteDelegate {
    func clickMute(cell:HomeVideo_CollectionViewCell)
}

class HomeVideo_CollectionViewCell: UICollectionViewCell,ASAutoPlayVideoLayerContainer,removeVideoLayerDelegate {
    @IBOutlet weak var lbl_videoTimer: UILabel!
    var videoDuration = 0
    var totalDuration = 0
    var timer : Timer?
    @IBOutlet var btn_mute: UIButton!
    @IBOutlet var shotImageView: PFImageView!
    var playerController: ASVideoPlayerController?
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var delegate : muteDelegate?
    
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                ASVideoPlayerController.sharedVideoPlayer.mute = true
                
                ASVideoPlayerController.sharedVideoPlayer.currentLayer  = videoLayer
                
                ASVideoPlayerController.sharedVideoPlayer.videoURL  = videoURL
                
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        shotImageView.layer.cornerRadius = 5
        shotImageView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        shotImageView.clipsToBounds = true
        shotImageView.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
        shotImageView.layer.borderWidth = 0.5
        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.videoGravity = AVLayerVideoGravity.resize
        videoLayer.frame = CGRect.init(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        shotImageView.layer.addSublayer(videoLayer)
    }
    
    @objc func timeIntiate(){
        let durationSecs = videoDuration  - 1
        videoDuration = durationSecs
        if durationSecs > 0{
            self.lbl_videoTimer.text = "00:" + "\(durationSecs)"
        }else{
            self.lbl_videoTimer.text = "00:00"
        }
        if totalDuration - 3 == durationSecs{
            self.lbl_videoTimer.isHidden = true
            timer?.invalidate()
            timer = nil
        }else{
            self.lbl_videoTimer.isHidden = false
        }
    }
    
    func configureCell(imageUrl: String?,
                       description: String,
                       videoUrl: String?,file:PFFile?,duration:Int) {
    shotImageView.image = UIImage.init(named: "default.png")
        if imageUrl == "default.png"{
            shotImageView.image = UIImage.init(named: imageUrl ?? "")
        }else{
            shotImageView.file = file
            shotImageView.loadInBackground()
        }
        self.lbl_videoTimer.isHidden = true
        self.videoURL = videoUrl
        if duration > 0{
            videoDuration = duration
            totalDuration = duration
            if duration <= 60{
                self.lbl_videoTimer.text = "00:" + "\(duration)"
                self.lbl_videoTimer.isHidden = false
                if timer != nil{
                    timer?.invalidate()
                    timer = nil
                }else{
                    timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(timeIntiate), userInfo: nil, repeats: true)
                }
            }
        }
        else{
            self.lbl_videoTimer.isHidden = true
        }
    }
    
    @IBAction func click_mute(_ sender: UIButton) {
        //  ASVideoPlayerController.sharedVideoPlayer.mute = true
        
        if sender.isSelected == false{
            sender.isSelected = true
            videoLayer.player?.isMuted = false
        }else{
            sender.isSelected = false
            videoLayer.player?.isMuted = true
        }
    }
    
    override func prepareForReuse() {
        shotImageView.image = nil
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        videoLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
    }
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(shotImageView.frame, from: shotImageView)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
                return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
    
    func removeLayerFromCell() {
        ASVideoPlayerController.sharedVideoPlayer.mute = true
        videoLayer.player?.isMuted = true
        if self.videoURL != nil{
            ASVideoPlayerController.sharedVideoPlayer.pauseRemoveLayer(layer: videoLayer, url: self.videoURL!, layerHeight: 200.0)
        }
    }
    
}
