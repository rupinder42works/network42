//
//  HomeCell.swift
//  Network42
//
//  Created by 42works on 11/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import ActiveLabel
import AlamofireImage
import AVKit
import AVFoundation
import JPVideoPlayer

@objc protocol likeListDelegate {
    func likeListingClicked(tag:Int)
    func likeClicked(sender: UIButton)
    func commentClicked(tag:Int)
    func URLClicked(tag:Int)
    func moreClicked(sender:UIButton,tag:Int)
    func userProfileClicked(tag:Int)
    func imageClicked(tag:Int,object:PFObject)
}

class Home_UrlCell: UITableViewCell {
    
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var btn_comment: UIButton!
    @IBOutlet weak var htConsrnt_youtube: NSLayoutConstraint!
    @IBOutlet weak var imgVw_youtube: UIImageView!
    @IBOutlet weak var img_youtube: UIImageView!
    @IBOutlet var likeListingObj: UIButton!
    @IBOutlet var likeBtn: UIButton!
    @IBOutlet var commentListing: UIButton!
    @IBOutlet var URLBtn: UIButton!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet var userProfileBtn: UIButton!
    
    @IBOutlet weak var likeSelected: UIImageView!
    @IBOutlet weak var vw_profileImg: UIView!
    //var indicatorObject = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
  
    @IBOutlet var profileImage: PFImageView!
    @IBOutlet var time: UILabel!
    @IBOutlet var name: UILabel!
    
    @IBOutlet var descriptionLbl: ActiveLabel!
    
    @IBOutlet var urlView: UIView!
    
    @IBOutlet var urlImage: UIImageView!
    @IBOutlet var urlTitle: UILabel!
    @IBOutlet var urlDescription: UILabel!
    @IBOutlet var urlAgain: UILabel!
    @IBOutlet var widthOfUrlImage: NSLayoutConstraint!
    var objectForCollection : PFObject?

    weak var delegate: likeListDelegate?
    
    var avPlayer: AVPlayer!
    
    //var currentIndex : Int = 0
    
    var changeVideo : Bool = false
    
    var playerDictionary = [[:]]
    
    var muted : Bool = false
    var parent : UITableView?
    
    var isVisible = false
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
                // Configure the view for the selected state
    }
    
    @IBAction func like(_ sender: Any) {
        self.delegate?.likeClicked(sender: sender as? UIButton ?? UIButton())
    }
    
    @IBAction func comment(_ sender: Any) {
        self.delegate?.commentClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func URLAction(_ sender: Any) {
        self.delegate?.URLClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func more(_ sender: Any) {
        if let button = sender as? UIButton{
            self.delegate?.moreClicked(sender:button,tag:button.tag)
        }
    }

    @IBAction func likeListingAction(_ sender: Any) {
        self.delegate?.likeListingClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func userProfile(_ sender: Any) {
        self.delegate?.userProfileClicked(tag: (sender as AnyObject).tag)
    }
}


extension Home_UrlCell : MuteActionDelegate
{
    func mute() {
        //avPlayer.isMuted = !(avPlayer.isMuted)
        
     //   JPVideoPlayerManager.shared().muted = !JPVideoPlayerManager.shared().muted
        //        JPVideoPlayerManager.shared().setPlayerMute(!(JPVideoPlayerManager.shared().playerIsMute()))
    }
    
}

extension Home_UrlCell : muteDelegate{
    func clickMute(cell:HomeVideo_CollectionViewCell){
        
    }
}

