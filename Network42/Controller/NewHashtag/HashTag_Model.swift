//
//  HashTag_Model.swift
//  Network42
//
//  Created by Apple on 13/03/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import Parse

class HashTag_Model: NSObject {
    
    var parentPost : PFObject?
    var  hashtag_id = ""
    var parent_post : Post?
    var post_id = ""
    var post_user : User?
    
    func getHashtagDic(dic:PFObject) -> HashTag_Model{
        if let str = dic["hashtag_id"] as? String{
            self.hashtag_id = str
        }
        if let str = dic["parent_post"] as? PFObject{
            self.parentPost = str
            var post = Post()
            post = post.getPost(str)
            self.parent_post = post
        }
        if let str = dic["post_user"] as? PFUser{
            var user = User()
            user = user.getUser(str)
            self.post_user = user
        }
        if let str = dic["post_id"] as? String{
            self.post_id = str
        }
        return self
    }
}


