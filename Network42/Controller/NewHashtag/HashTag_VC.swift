//
//  HashTag_VCViewController.swift
//  Network42
//
//  Created by Apple on 13/03/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import Parse
import ActiveLabel

class HashTag_VC: BaseViewController,commentCountUpdateDelegate,likeListingDelegate {

    var arrObjects = [PFObject]()
    var arr_hashTags = [HashTag_Model]()
    
    @IBOutlet weak var lbl_heading: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var tagString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        tableView.register(UINib(nibName: "Home_UrlCell", bundle: nil), forCellReuseIdentifier: "Home_UrlCell")
        hashTags(hashTag:tagString)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func click_back(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- get hashtag
    
    func hashTags(hashTag:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }

        let query = PFQuery(className: "Hashtags")
        query.whereKey("name", equalTo: hashTag)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                var hashTagId : String = ""
                print(objects!)
                for object in objects! {
                    //print(object)
                    hashTagId = object.objectId!
                }
                let query = PFQuery(className: "Hashtag_posts")
                query.includeKey("parent_post")
                query.includeKey("post_user")
                query.order(byDescending: "createdAt")
                query.whereKey("hashtag_id", equalTo: hashTagId)
                query.findObjectsInBackground(block: { (objects, error) in
                    if error == nil {
                        for object in objects! {
                           // object.setObject(true, forKey: "mute")
                            var obj = HashTag_Model()
                            obj = obj.getHashtagDic(dic:object)
                            self.arr_hashTags.append(obj)
                        }
                        if objects != nil{
                            self.arrObjects = objects!
                        }
                        let postStr = self.arr_hashTags.count > 1 ? "Posts" : "Post"
                     //   self.lbl_heading.text = "\(hashTag) (\(self.arr_hashTags.count) \(postStr))"
                       // self.isPostExist()
                       self.tableView.reloadData()
                    }
                    else {
                        print(error!)
                    }
                })
            }
            else {
                print(error!)
            }
        })
    }
    
    
    // MARK: - Navigation/*///////////////////////

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

    //MARK:- home cell delegate methods
    
    func updateCommentCount(index:Int,value:String)
    {
        if let object = self.arr_hashTags[index].parent_post
        {
            var obj = object
            obj.commentCount = Int(value)
            self.arr_hashTags[index].parent_post = obj
            self.tableView.reloadData()
        }
    }
    
    func likeListingClicked(tag: Int) {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "LikesViewController") as! LikesViewController
        vc.postId = self.arr_hashTags[tag].parentPost?.objectId ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func likeClicked(sender: UIButton) {
        let tag = sender.tag
        sender.isSelected = !sender.isSelected
        var model = self.arr_hashTags[tag].parent_post
        var likeArrayObj  = [String]()
        if let likeArray = model?.likeArr as? [String]{
            likeArrayObj = likeArray
        }
        
        let objId = PFUser.current()?.objectId
        let id = model?.id
        var obj = self.arr_hashTags[tag].parentPost
        if sender.isSelected && !likeArrayObj.contains(objId ?? ""){
            likeArrayObj.append(objId ?? "")
        } else {
            
        }

        let likesQuery = PFQuery(className:"Post_likes")
        likesQuery.whereKey("post_id", equalTo:id)
        likesQuery.whereKey("user_id", equalTo:objId!)
        likesQuery.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                if objects!.count==0
                {
                    let compClass = PFObject(className: "Post_likes")
                    compClass["post_id"] =  id
                    compClass["user_id"] =  objId
                    compClass["parent_id"] =  PFUser.current()
                    compClass.saveInBackground(block: { (status, error) in
                        print(status)
                        self.view.isUserInteractionEnabled = true
                        if status ==  true{

                            if let arr = likeArrayObj as? [String]{
                                let s = arr
                                model?.likeArr = s
                                self.arr_hashTags[tag].parent_post = model
                            }
                            if let userId = model?.postedBy?.id
                            {
                                let formatter = DateFormatter()
                                formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                                let time = formatter.string(from: Date())
                                let objId = PFUser.current()?.objectId
                                
                                let nameQuery : PFQuery = PFUser.query()!
                                nameQuery.whereKey("objectId", equalTo: userId)
                                nameQuery.findObjectsInBackground(block: { (objects, error) in
                                    //print(objects!)
                                    for object in objects! {
                                        
                                       // self.saveToNotifications(from_id: objId!, to_id: userId, notification_type: "like", time: time, post_id: id, parent_from: PFUser.current()!, parent_to: (object as? PFUser)!,commentStr: "")
                                       //self.likePushNotification(userId: userId,postId: id)
                                    }
                                    //self.tableView.reloadData()
                                    let indexPath = IndexPath.init(row: sender.tag, section: 0)
                                    self.tableView.reloadRows(at: [indexPath], with: .none)
                                })
                                
                            }
                        }
                        else{
                            
                            let message = (error?.localizedDescription)
                            print(message as Any)
                        }
                    })
                }else
                {
                    print("liked already")
                    for object in objects! {
                        //print(object)
                        object.deleteInBackground(block: { (status, error) in
                            self.view.isUserInteractionEnabled = true
                            print(status)
                            if status == true{
                                //likeArrayObj.remove(objId!)
                                if let id = objId{
                                    let index = likeArrayObj.index{($0 == id)}
                                    if index != nil{
                                        likeArrayObj.remove(at: index!)
                                    }
                                }
                                
                                if let arr = likeArrayObj as? [String]{
                                    model?.likeArr = arr
                                    self.arr_hashTags[tag].parent_post = model
                                }
                                let indexPath = IndexPath.init(row: sender.tag, section: 0)
                                self.tableView.reloadRows(at: [indexPath], with: .none)
                                //self.tableView.reloadData()
                            }else{
                                let message = (error?.localizedDescription)
                                print(message as Any)
                            }
                        })
                    }
                }
            }else
            {
                self.view.isUserInteractionEnabled = true
            }})
    }
    
    func commentClicked(tag: Int) {
        
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController

        let id = self.arr_hashTags[tag].parent_post?.id ?? ""
        vc.objectFromHome = self.arr_hashTags[tag].parentPost
        vc.postId = id
        vc.delegate = self
        vc.indexOfpost = tag
        vc.fromProfile = false
        if let userId = self.arr_hashTags[tag].post_user?.id
        {
            vc.postUserId = userId
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func URLClicked(tag: Int) {
        
    }
    
    func moreClicked(sender: UIButton, tag: Int) {
        
    }
    
    func userProfileClicked(tag: Int) {
        
    }
    
    func imageClicked(tag: Int, object: PFObject) {
        
    }
    
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "Just now"
        } else {
            return "Just now"
        }
    }
    
}


extension HashTag_VC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arr_hashTags.count > 0{
            return self.arr_hashTags.count
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arr_hashTags.count > indexPath.row{
            let model = self.arr_hashTags[indexPath.row]
            if model.parent_post?.postType == "URL"{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Home_UrlCell", for: indexPath as IndexPath) as! Home_UrlCell
                cell.moreBtn.isHidden = true
                cell.parent = tableView
                cell.likeBtn.tag = indexPath.row
                cell.likeListingObj.tag = indexPath.row
                cell.commentListing.tag = indexPath.row
                cell.moreBtn.tag = indexPath.row
                cell.userProfileBtn.tag = indexPath.row
                cell.btn_comment.tag = indexPath.row
               // cell.delegate = self
                if let timeStampObj = model.parentPost?.createdAt as? String
                {
                    let newdateStr = timeStampObj.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    if newdateStr.lowercased().contains("pm") == true || newdateStr.lowercased().contains("am"){
                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss a"
                    }else{
                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    }
                    
                    let newDate = dateFormatter.date(from: newdateStr)
                    if let newdate1 = newDate as? Date {
                         let timeStr  = self.timeAgoSinceDate(date:newdate1,numericDates: true)
                        cell.time.text = timeStr
                    }
                }
                cell.name.text = model.post_user?.name ?? ""
                if model.post_user?.profilePic != nil
                {
                    cell.profileImage.file = model.post_user?.profilePic
                    cell.profileImage.loadInBackground()
                } else {
                    cell.profileImage.image =  UIImage.init(named: "dummyProfile")
                }
                cell.time.text = model.parent_post?.createdOn ?? ""
                
                if cell.descriptionLbl != nil{
                    if let obj = model.parentPost{
                        setDescriptionLbl(descriptionLbl:cell.descriptionLbl!,obj:obj,index:indexPath.row)
                    }
                }
                
                if let link_title = model.parent_post?.link_title
                {
                    if !(link_title == "")
                    {
                        cell.lineView.isHidden = true
                        var urlStr = ""
                        if let link_url = model.parent_post?.link_title
                        {
                            urlStr = link_url
                        }
                        if urlStr.contains("www.youtube.com/watch?v=") == true{
                            cell.img_youtube.isHidden = false
                            cell.imgVw_youtube.isHidden = false
                            cell.img_youtube.isHidden = false
                            cell.htConsrnt_youtube.constant = 110
                            
                        }else{
                            cell.imgVw_youtube.image = nil
                            cell.imgVw_youtube.isHidden = true
                            cell.img_youtube.isHidden = true
                            cell.htConsrnt_youtube.constant = 0
                        }
                        
                        cell.urlView.isHidden = false
                        cell.urlView.layer.shadowColor = UIColor.lightGray.cgColor
                        cell.urlView.layer.shadowOffset = CGSize(width: 3, height: 3)
                        cell.urlView.layer.shadowRadius = 3.0
                        cell.urlView.layer.shadowOpacity = 1
                        cell.urlView.clipsToBounds = false
                        cell.urlView.layer.masksToBounds = false
                        
                        cell.urlTitle.text = link_title
                        if let link_description = model.parent_post?.link_description
                        {
                            cell.urlDescription.text = link_description
                        }
                        if let link_url = model.parent_post?.link_title as? String
                        {
                            cell.urlAgain.text = link_url
                        }
                        
                        if let link_image_url = model.parent_post?.link_image_url
                        {
                            let url = URL(string: link_image_url)
                            if urlStr.contains("www.youtube.com/watch?v=") == true{
                                if let imageUrl = url {
                                    cell.imgVw_youtube.af_setImage(withURL: imageUrl)
                                }
                            }else{
                                cell.imgVw_youtube.image = nil
                            }
                            if !(url==nil)
                            {
                                
                                cell.urlImage.af_setImage(withURL: url!)
                                cell.widthOfUrlImage.constant = 50
                            }else
                            {
                                cell.urlImage.image = nil
                                cell.widthOfUrlImage.constant = 0
                            }
                        }
                    }
                }
                if let likeArray = model.parent_post?.likeArr as? [String]
                {
                    cell.likeListingObj.isEnabled = true
                    cell.likeListingObj.setTitle("\(likeArray.count)", for: .normal)
                    let objId = PFUser.current()?.objectId
                    if likeArray.contains(objId!)
                    {
                        cell.likeSelected.isHighlighted = true
                        cell.likeBtn.isSelected = true
                    }else
                    {
                        cell.likeSelected.isHighlighted = false
                        cell.likeBtn.isSelected = false
                    }
                }else
                {
                    cell.likeListingObj.isEnabled = false
                    cell.likeListingObj.setTitle("0", for: .normal)
                    cell.likeBtn.isSelected = false
                    cell.likeSelected.isHighlighted = false
                }
                
                cell.commentListing.setTitle("\(model.parent_post?.commentCount ?? 0)", for: .normal)
                cell.selectionStyle = .none
                return cell
            }else{
                // for post type images/videos/album load another cell
                let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath as IndexPath) as! HomeCell
                cell.moreBtn.isHidden = true
                cell.parent = tableView
                cell.likeBtn.tag = indexPath.row
                cell.likeListingObj.tag = indexPath.row
                cell.commentListing.tag = indexPath.row
                cell.moreBtn.tag = indexPath.row
                cell.userProfileBtn.tag = indexPath.row
                cell.btn_comment.tag = indexPath.row
                cell.delegate = self
             
                cell.name.text = model.post_user?.name ?? ""
                if model.post_user?.profilePic != nil
                {
                    cell.profileImage.file = model.post_user?.profilePic
                    cell.profileImage.loadInBackground()
                } else {
                    cell.profileImage.image =  UIImage.init(named: "dummyProfile")
                }
                
                cell.time.text = model.parent_post?.createdOn ?? ""
                if cell.descriptionLbl != nil{
                    if let obj = model.parentPost{
                        setDescriptionLbl(descriptionLbl:cell.descriptionLbl!,obj:obj,index:indexPath.row)
                    }
                }
                cell.collectionContainer.isHidden = true
                cell.collectionVIewHeight.constant = 0
                cell.heightOFPageController.constant = 0
                
                //post type basis cell reuse
                if model.parent_post?.postType == "ALBUM" || model.parent_post?.postType == "IMAGE" || model.parent_post?.postType == "VIDEO"
                {
                    cell.lineView.isHidden = true
                    cell.collectionContainer.isHidden = false
                    cell.collectionVIewHeight.constant = 450
                    cell.tag = indexPath.row
                    if let postmedia = model.parent_post?.postMedia
                    {
                        if postmedia.count>1
                        {
                            cell.heightOFPageController.constant = 20
                        }
                        
                        if let obj =  model.parentPost as? PFObject{
                            cell.objectForCollection = obj
                        }
                        

                        cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: cell, forRow: indexPath.row,changeVideo:true)
                    }
                }
                
                if let likeArray = model.parent_post?.likeArr as? [String]
                {
                    cell.likeListingObj.isEnabled = true
                    cell.likeListingObj.setTitle("\(likeArray.count)", for: .normal)
                    let objId = PFUser.current()?.objectId
                    if likeArray.contains(objId!)
                    {
                        cell.likeSelected.isHighlighted = true
                        cell.likeBtn.isSelected = true
                    }else
                    {
                        cell.likeSelected.isHighlighted = false
                        cell.likeBtn.isSelected = false
                        //   cell.likeBtn.setImage(UIImage.init(named: "likeUnSelected"), for: .normal)
                    }
                }else
                {
                    cell.likeListingObj.isEnabled = false
                    cell.likeListingObj.setTitle("0", for: .normal)
                    cell.likeBtn.isSelected = false
                    cell.likeSelected.isHighlighted = false
                }
                
                cell.commentListing.setTitle("\(model.parent_post?.commentCount ?? 0)", for: .normal)
                
                cell.selectionStyle = .none
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    //set description lbl data
    func setDescriptionLbl(descriptionLbl:ActiveLabel,obj:AnyObject,index:Int){
        if let descriptionObj = obj.object(forKey: "description") as? String
        {
            var btnTitle : String = ""
            if let users_tagged_withArray = obj.value(forKey: "users_tagged_with") as? NSArray
            {
                if users_tagged_withArray.count>0
                {
                    if users_tagged_withArray.count == 1
                    {
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String)"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {_ in
                           // self.withUser(index: index, name: $0)
                        }
                    }else if users_tagged_withArray.count == 2
                    {
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \((users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String)"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {_ in
                            //self.withUser(index: index, name: $0)
                        }
                        
                        let secondUser = (users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String
                        let customTypeSecond = ActiveType.custom(pattern: "\\s\(secondUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeSecond)
                        descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeSecond) {_ in
                            //self.withUser(index: index, name: $0)
                        }
                    }else
                    {
                        
                        btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \(users_tagged_withArray.count-1) others"
                        
                        let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                        let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                        descriptionLbl.enabledTypes.append(customTypeFirst)
                        descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeFirst) {_ in
                           // self.withUser(index: index, name: $0)
                        }
                        
                        let otherString = "\(users_tagged_withArray.count-1) others"
                        let customTypeSecond = ActiveType.custom(pattern: "\\s\(otherString)\\b")
                        descriptionLbl.enabledTypes.append(customTypeSecond)
                        descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                        descriptionLbl.handleCustomTap(for: customTypeSecond) {_ in
//
//                            self.popUp.fadeIn()
//                            self.popUpArray = users_tagged_withArray
//
//                            if self.popUpArray.count>6
//                            {
//                                self.popUpTableViewObj.isScrollEnabled = true
//                                self.heightOfPopUpTableView.constant = 300
//                            }else
//                            {
//                                let count = self.popUpArray.count
//                                self.heightOfPopUpTableView.constant = CGFloat(count * 50)
//                                self.popUpTableViewObj.isScrollEnabled = false
//                            }
//                            self.popUpTableViewObj.reloadData()
                        }
                    }
                }
            }
            
            descriptionLbl.customize { label in
                
                descriptionLbl.text = descriptionObj+btnTitle
                
                let arr = descriptionObj.components(separatedBy: "\n")
                
                if (arr.count > 3) || (descriptionObj.characters.count > 100)
                {
                    var lengthObj = 0
                    if descriptionObj.characters.count > 100
                    {
                        lengthObj = 100
                    }else
                    {
                        lengthObj = descriptionObj.characters.count
                    }
                    
                    let index = descriptionObj.index(descriptionObj.startIndex, offsetBy: lengthObj)
                    let substringDescription = descriptionObj.substring(to: index)
                    
                    label.text = substringDescription+" Continue Reading"
                    let continueReading = ActiveType.custom(pattern: "\\sContinue Reading\\b")
                    descriptionLbl.enabledTypes.append(continueReading)
                    descriptionLbl.customColor[continueReading] = ConstantColors.themeColor
                    descriptionLbl.handleCustomTap(for: continueReading) {_ in
                        let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
                        let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                        vc.secondTime = true
                        vc.isFromHashTag = false
                        vc.titleSecond = "network 42"
                        vc.showOnlySinglePost = true
                        let id = obj.value(forKey: "objectId") as! String
                        vc.postIdObject = id
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                }
                
                descriptionLbl.isHidden = false
                descriptionLbl.numberOfLines = 0
                descriptionLbl.lineSpacing = 4
                descriptionLbl.textColor = UIColor(red: 102.0/255, green: 117.0/255, blue: 127.0/255, alpha: 1)
                descriptionLbl.hashtagColor = ConstantColors.themeColor
                descriptionLbl.mentionColor = ConstantColors.themeColor
                descriptionLbl.URLColor = ConstantColors.themeColor
                descriptionLbl.URLSelectedColor = UIColor(red: 82.0/255, green: 190.0/255, blue: 41.0/255, alpha: 1)
                
                descriptionLbl.handleMentionTap {_ in
                   // self.descriptionUser(index: index, userName: $0)
                }
                descriptionLbl.handleHashtagTap {_ in
                    // no need to click hastag again
                }
                
                descriptionLbl.handleURLTap {
                    let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
                    let vc = storyboardObj.instantiateViewController(withIdentifier: "MTSWebViewController") as! MTSWebViewController
                    
                    print($0.absoluteString)
                    if let link_url = obj.object(forKey: "link_url") as? String
                    {
                        vc.urlStr = link_url
                    }
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
}
