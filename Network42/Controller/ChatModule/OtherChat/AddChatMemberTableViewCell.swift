//
//  AddChatMemberTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 18/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import ParseUI
class AddChatMemberTableViewCell: UITableViewCell {

    @IBOutlet var nameTF: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var user_ImageView: PFImageView!
    @IBOutlet var checkBtn: UIButton!
    @IBOutlet var containerImage: UIImageView!
    
    @IBAction func checkTapped(_ sender: Any) {
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkBtn.layer.cornerRadius = 5
        checkBtn.layer.borderColor = UIColor.gray.cgColor
        checkBtn.layer.borderWidth = 2 // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
