//
//  AddChatmembersViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 18/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

//
//  AddChatmembersViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 18/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import Firebase

class AddChatmembersViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate{
    
    @IBOutlet weak var btn_upperTick: UIButton!
    var fromGroupSettingsUserArray:NSMutableArray = []
    var groupIdStr:String = ""
    var fromViewType :String = ""
    @IBOutlet var particpantsLBl: UILabel!
    @IBOutlet var createActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var groupChatView: UIView!
    @IBOutlet var collectionView: UICollectionView!
    var tempUserArraytoPass:NSMutableArray = []
    var selectedIndexPath:Int = 0
    @IBOutlet var addPartyBtn: UIButton!
    @IBOutlet var groupNameTF: MyTextField!
    var selectedIndexArray:NSMutableArray = []
    var tempNameArray:NSMutableArray = []
    var subNameArray:NSMutableArray = []
    var nametoPass:String = ""
    @IBOutlet var tableView: UITableView!
    var matchedIndexArray:NSMutableArray = []
    var membersArray:NSMutableArray = []
    let newmemberArray:NSMutableArray = []
    var addMembersIntoGroupArray:NSMutableArray = []
    
     var responseChatArr: NSMutableArray = []
    var autoId:String = ""
    var useridArray:NSMutableArray = []
    var senderIdToPass:String = ""
    let DBReciverref = FIRDatabase.database().reference()
    var messageref = FIRDatabase.database().reference()
    
    var isAdminStr:String = ""
   
    var oldGroupMembersArray:NSMutableArray = []
    var chatDictionary = [String: Any]()
     var timeStrToPass:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "AddChatMemberTableViewCell", bundle: nil), forCellReuseIdentifier: "AddChatMemberTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        activityIndicator.stopAnimating()
        tempNameArray = ["Ulangnathan kathoriyannnnn","Mani","Sunny","Mamta","Anmol Rajdev"]
        subNameArray = ["Txt Hi","image","image","abc","abc"]
        
        
        collectionView.register(UINib(nibName: "AddChatMemebersCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "AddChatMemebersCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        addPartyBtn.layer.masksToBounds = true
        addPartyBtn.layer.cornerRadius = addPartyBtn.frame.width/2
        addPartyBtn.layer.masksToBounds = false
        addPartyBtn.layer.shadowColor = UIColor.darkGray.cgColor
        addPartyBtn.layer.shadowOffset = CGSize(width: 0, height: 1)
        addPartyBtn.layer.shadowOpacity = 1
        addPartyBtn.layer.shadowRadius = 1.0
        
        self.activityIndicator.startAnimating()
        self.getFollowingList()
        print(fromGroupSettingsUserArray)
        
        
        groupChatView.fadeOut()
  
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-TABLE VIEW DELEGATES
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numOfSections: Int = 0
        var totalSections:Int = 0
        totalSections =  membersArray.count
        
        print(totalSections)
        if totalSections > 0
        {
            tableView.separatorStyle = .none
            numOfSections            = totalSections
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel     = UILabel(frame: CGRect(x: 35, y: 0, width: tableView.bounds.size.width - 35, height: tableView.bounds.size.height))
            noDataLabel.font = UIFont.init(name: "SanFranciscoText-Regular", size: 18.0)
            noDataLabel.text          = messages.noFriendText
            noDataLabel.textColor     = UIColor.darkGray
            noDataLabel.numberOfLines = 0
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddChatMemberTableViewCell", for: indexPath as IndexPath) as! AddChatMemberTableViewCell
        
        
        cell.checkBtn.tag = indexPath.row
        var nameStr :String = ""
         if membersArray.count > 0{
          

            if matchedIndexArray.count > 0{
            if matchedIndexArray.contains(indexPath.row){
                nameStr = " (already added)"
               // selectedIndexArray.replaceObject(at: indexPath.row, with: "1")
                //cell.checkBtn.isUserInteractionEnabled = false
                cell.isUserInteractionEnabled = false
                
            }
            else{
                //cell.checkBtn.isUserInteractionEnabled = true
                cell.isUserInteractionEnabled = true
               // selectedIndexArray.replaceObject(at: indexPath.row, with: "0")
            }
            }
            cell.nameTF.text = ((membersArray.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String)! +  (nameStr)
         

            cell.userName.text = (membersArray.object(at: indexPath.row) as AnyObject).value(forKey: "user_name") as? String
            
            if let file = (membersArray.object(at: indexPath.row) as AnyObject).value(forKey: "profile_image") as? PFFile
            {
                cell.user_ImageView.file = file
                cell.user_ImageView.loadInBackground()
            }
            
            
            print(selectedIndexArray)
            if selectedIndexArray.count > 0{
                if  selectedIndexArray[indexPath.row] as! String == "1"{
                    cell.checkBtn.setImage(UIImage(named: "tick"), for: .normal)
                }
                    
                else{
                    cell.checkBtn.setImage(UIImage(named: "white"), for: .normal)
                }
            }
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath.row
        print(selectedIndexPath)
        if selectedIndexArray.object(at: indexPath.row) as! String == "0"
        {
            NSLog(" Not Selected");
            selectedIndexArray.replaceObject(at: indexPath.row, with: "1")
        }
        else
        {
            NSLog(" Selected");
            selectedIndexArray.replaceObject(at: indexPath.row, with: "0")
        }
     tableView.reloadData()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        _ =  self.navigationController?.popViewController(animated: true)
    }
    //MARK:COLLECTION VIEW DELEGATES
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return tempUserArraytoPass.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddChatMemebersCollectionViewCell",for: indexPath) as! AddChatMemebersCollectionViewCell
        if tempUserArraytoPass.count > 0{
            particpantsLBl.text = "Participants: 5/\(tempUserArraytoPass.count)"
            print(tempUserArraytoPass)
            cell.nameLbl.text = (tempUserArraytoPass.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
            if let file = (tempUserArraytoPass.object(at: indexPath.row) as AnyObject).value(forKey: "profile_image") as? PFFile
            {
                cell.userImageView.file = file
                cell.userImageView.loadInBackground()
            }
        }
        cell.layoutIfNeeded()
        cell.vw_imgBackground.layoutIfNeeded()
        cell.vw_imgBackground.layer.cornerRadius = cell.vw_imgBackground.frame.size.width/2
        cell.userImageView.layoutIfNeeded()
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        print("Index path==",indexPath.row)
        
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return CGSize(width: collectionView.bounds.size.width/5, height: 55)
    }
    
    
    //MARK:-ADD MEMBERS
    @IBAction func addTopNavTapped(_ sender: Any) {
        
        print(nametoPass)
        print(selectedIndexPath)
       tempUserArraytoPass.removeAllObjects()
        for i in 0 ..< selectedIndexArray.count {
            if selectedIndexArray[i] as! String == "1"{
                tempUserArraytoPass.add(membersArray.object(at: i))
                
            }
        }
        
        print(tempUserArraytoPass)
        if fromViewType == "groupSettings"{
             addUserIntoExistingGroup()
         }
        else{
        if tempUserArraytoPass.count > 0{
            if tempUserArraytoPass.count == 1{
                print(tempUserArraytoPass)
                let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                let vc = chatSB.instantiateViewController(withIdentifier: "SingleChatViewController")  as! SingleChatViewController
                vc.titleStr = ((tempUserArraytoPass.object(at: 0) as AnyObject).value(forKey: "name") as? String)!
                vc.pushUsrId = (PFUser.current()?.objectId!)!
                vc.responseChatArr = tempUserArraytoPass
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                
                self.groupNameTF.text = ""
                groupChatView.fadeIn()
                collectionView.reloadData()
            }
            
            
        }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //popUp.isHidden = true
        self.groupChatView.fadeOut()
    }
    //MARK:-CollectionView Add
    @IBAction func addPartiTapped(_ sender: Any) {
        
        print(selectedIndexPath)
        tempUserArraytoPass.removeAllObjects()
        for i in 0 ..< selectedIndexArray.count {
            if selectedIndexArray[i] as! String == "1"{
                
                tempUserArraytoPass.add(membersArray.object(at: i))
            }
        }
        
        tempUserArraytoPass.add(PFUser.current()!)
     
        if groupNameTF.text != ""{
            
            addDataintoGroupTableAPI()
          
        }
        else{
            self.showMessage("Group name cannot be blank.")
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 25
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    //Mark:Viewwill Appera
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.groupChatView.fadeOut()
    }
    
    func getFollowingList(){
        print("From Settings",self.fromGroupSettingsUserArray)
        let query = PFQuery(className: "User_follow")
        let currentUser = PFUser.current()
        query.whereKey("follower_id", equalTo:currentUser?.objectId! ?? "")
        query.includeKey("parent_following")
        query.whereKey("status", equalTo:"1")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print(objects!)
                self.membersArray.removeAllObjects()
                for object in objects! {
                    print(object)
                    if let singleUser = object.object(forKey: "parent_following") as? PFUser
                    {
                        print(singleUser)
                        
                        self.membersArray.add(singleUser)
                        self.selectedIndexArray.add("0")
                    }
                    
                }
                self.newmemberArray.removeAllObjects()
                self.matchedIndexArray.removeAllObjects()
                for  i in 0 ..< self.membersArray.count
                {
                    self.newmemberArray.add(((self.membersArray.object(at: i)) as AnyObject).value(forKey: "objectId")! as Any)
                }
                if self.fromGroupSettingsUserArray.count > 0{
                for  i in 0 ..< self.fromGroupSettingsUserArray.count
                {
                    let groupMembersid =  (self.fromGroupSettingsUserArray.object(at: i) as AnyObject).value(forKey: "user_id") as? String
                    print(self.newmemberArray)
                    if (self.newmemberArray.contains(groupMembersid!)){
                        let indexoF = self.newmemberArray.index(of: groupMembersid!)
                        print(indexoF)
                        self.matchedIndexArray.add(indexoF)
                        
                    }

                }
                }
                self.activityIndicator.stopAnimating()
                print("final dict as",self.membersArray)
                print("final dict as",self.membersArray.count)
              
                if self.membersArray.count > 0 {
                    self.btn_upperTick.isUserInteractionEnabled = true
                    self.btn_upperTick.alpha = 1.0
                }else{
                    self.btn_upperTick.isUserInteractionEnabled = false
                    self.btn_upperTick.alpha = 0.7
                }
                self.tableView.reloadData()
            }
            else {
                print(error!)
            }
        })
    }
    
    func addDataintoGroupTableAPI(){
        let DBref = FIRDatabase.database().reference()
        
        let usrRefrence = FIRDatabase.database().reference()
        var isAdminFlag:String = "0"
      
    
        let formatter = DateFormatter()
        
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        
       timeStrToPass = formatter.string(from: Date())
        
        
        
        //Sender data for group
        
        let groupDictionary = ["group_name":groupNameTF.text!,"last_message":"","msgType":"","sender_id":"","time":timeStrToPass,"sender_name":"","group_img":""] as [String : Any]
        
        
        
        DBref.child("groups").childByAutoId().setValue(groupDictionary){ (error, DBref) in
            print(DBref)
            self.autoId = DBref.key
            print(DBref.key)
            print(self.tempUserArraytoPass)
            
            if self.tempUserArraytoPass.count > 0{
                
                for i in 0 ..< self.tempUserArraytoPass.count {
                self.useridArray.add((self.tempUserArraytoPass[i] as AnyObject).value(forKey: "objectId") as! String) //to send push 
                    self.messageref = DBref
                   // self.activityChat.stopAnimating()
                    
                    if let adminIdAs = (self.tempUserArraytoPass[i] as AnyObject).value(forKey: "objectId") as? String{
                        
                        if adminIdAs == PFUser.current()?.objectId{
                            
                            isAdminFlag = "1"
                        }
                            
                        else{
                            
                            isAdminFlag = "0"
                            
                        }
                        
                        self.chatDictionary = (["name":(self.tempUserArraytoPass[i] as AnyObject).value(forKey: "name") as! String,"user_id":(self.tempUserArraytoPass[i] as AnyObject).value(forKey: "objectId") as! String,"is_active":"1","isAdmin":isAdminFlag,"add_participant":"1"] as [String : Any] as NSDictionary) as! [String : Any]
                        
                    }
                    
                    //User description in group
                    
                    DBref.child("users").childByAutoId().setValue(self.chatDictionary)
                    
                    //set Gruop value in User table
                    
                    let groupsDict = ["group_name":self.groupNameTF.text!,"last_message":"","msgType":"text","group_img":"","time":self.timeStrToPass]
                    
                    usrRefrence.child("users").child((self.tempUserArraytoPass[i] as AnyObject).value(forKey: "objectId") as! String).child("groups").child(self.autoId).setValue(groupsDict)
                    
                  //  self.activityChat.stopAnimating()
                    
                }
                
                let currentName = PFUser.current()?.value(forKey: "name") as! String
                self.createGroupPushNotification(userIdArray: self.useridArray, postId: "", pushType: "group_add", message: "\(currentName) added you in the group: \(self.groupNameTF.text!)",groupId: self.autoId,groupName: "\(self.groupNameTF.text!)")
                self.senderIdToPass =  self.autoId
                
            }
            
            print(self.chatDictionary)
         
        }
        
        
        
    }
    //MARK:-ADD user into group
    func addUserIntoExistingGroup(){
        let groupref = FIRDatabase.database().reference()
        
        print(self.tempUserArraytoPass)
        if self.tempUserArraytoPass.count > 0{
            var isAdminFlag = "1"
            var countObj = 0
            for i in 0 ..< self.tempUserArraytoPass.count {
                if let adminIdAs = (self.tempUserArraytoPass[i] as AnyObject).value(forKey: "objectId") as? String{
                    if adminIdAs == PFUser.current()?.objectId{
                        isAdminFlag = "1"
                        
                    }
                    else{
                        isAdminFlag = "0"
                     }
                    self.chatDictionary = (["name":(self.tempUserArraytoPass[i] as AnyObject).value(forKey: "name") as! String,"user_id":(self.tempUserArraytoPass[i] as AnyObject).value(forKey: "objectId") as! String,"is_active":"1","isAdmin":isAdminFlag,"add_participant":"1"] as [String : Any] as NSDictionary) as! [String : Any]
                    
                }
                
                groupref.child("groups").child(groupIdStr).child("users").childByAutoId().updateChildValues(self.chatDictionary) { (error, usrRefrence) in
                    if error == nil{
                        print(groupref) //User description in group
                        
                        
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                        self.timeStrToPass = formatter.string(from: Date())
                        
                        
                        let groupsDict = ["group_name":self.groupNameTF.text!,"last_message":"","msgType":"text","group_img":"","time":self.timeStrToPass]
                        usrRefrence.child("users").child((self.tempUserArraytoPass[i] as AnyObject).value(forKey: "objectId") as! String).child("groups").child(self.groupIdStr).setValue(groupsDict)
                        print(self.senderIdToPass)
                        countObj  = countObj + 1
                        
                       if self.tempUserArraytoPass.count == countObj
                        {
                        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                        let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatSettingViewController")  as! GroupchatSettingViewController
                        vc.groupMembersArray = self.tempUserArraytoPass
                        vc.fromView = "groupSettings"
                        vc.groupId = self.groupIdStr
                        vc.isAdminStr = self.isAdminStr
                        self.navigationController?.pushViewController(vc, animated: true)
                        }
                        
                        
                    }
                }
            }
        }
    }
    
    //MARK:Add
    //Send notification After group creation
    func createGroupPushNotification(userIdArray:NSMutableArray,postId:String,pushType:String,message:String,groupId:String,groupName:String)
    {
        print(PFUser.current()?.objectId! as Any)
        print(userIdArray)
//        if let index = userIdArray.index(of: PFUser.current()?.objectId! as Any) as? Int {
//            userIdArray.removeObject(at: index)
//        }
        userIdArray.remove(PFUser.current()?.objectId!)
        print(userIdArray)
        let pushuserIdArray = userIdArray as NSArray as! [String]
        print(groupId)
        
        self.sendPushNotification(postId: postId, userId: "",settingType: "",message:message,type:pushType,userIdArray:userIdArray,groupMembersId:groupId,MsgType:"GroupChat",reciverId:"",username:"\(groupNameTF.text!)",groupName:groupName)

        if fromViewType == "groupSettings"{
            print(self.senderIdToPass)
            let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
            let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatSettingViewController")  as! GroupchatSettingViewController
            vc.groupMembersArray = tempUserArraytoPass
            vc.fromView = "groupSettings"
            vc.groupId = groupId
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
        else{
        //After creting Group
        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
        let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatViewController")  as! GroupchatViewController
        globalgroupName.gName = groupNameTF.text!
       // vc.fromView = "AddMemebers"
        vc.fromView = "ChatList"
        vc.groupObjectsToPass = tempUserArraytoPass
        vc.senderIdToPass = groupId
        self.navigationController?.pushViewController(vc, animated: true)
        }
    }


}

extension UIView {
    func fadeIn() {
        // Move our fade out code from earlier
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0 // Instead of a specific instance of, say, birdTypeLabel, we simply set [thisInstance] (ie, self)'s alpha
        }, completion: nil)
    }
    
    func fadeOut() {
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            self.alpha = 0.0
        }, completion: nil)
    }
    
}

//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
//        let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatSettingViewController")  as! GroupchatSettingViewController
//        self.navigationController?.pushViewController(vc, animated: true)
//
//    }
//
//    @IBAction func backTapped(_ sender: Any) {
//
//        self.navigationController?.popViewController(animated: true)
//    }
//
//}
