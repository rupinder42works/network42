//
//  ChatListCell.swift
//  Network42
//
//  Created by jaspreet singh on 17/07/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class ChatListCell: UITableViewCell {
    
    @IBOutlet var profileImage: PFImageView!
    @IBOutlet var userName: MessageLbl!
    @IBOutlet var name: NameLabel!
    @IBOutlet var time: TimeLabel!
    @IBOutlet var containerImage: UIImageView!

    @IBOutlet weak var vw_profileBackground: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
