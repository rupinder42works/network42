//
//  GroupchatSettingViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 23/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Firebase
import  Parse

class GroupchatSettingViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate,nameGroupDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,FooterDelegates{
   @IBOutlet var groupActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    var fileUrl:String = ""
    var  userimageToSend = UIImage()
    var  bgimageToSend = UIImage()
    var pickerType:String = ""
    let imagePicker = UIImagePickerController()
    var groupId:String = ""
    var groupUserIdArray :NSMutableArray = []
    var groupMembersArray:NSMutableArray = []
    var uploadFlag:Bool = false
    var isAdminStr:String = ""
     var addParicipantsStr:String = ""
    var userChildIdArray:NSMutableArray = []
    var groupnameStr:String = ""
    var fromView:String = ""
    var getAdminId:String = ""
    var addMemberObjArray:NSMutableArray = []
    var groupAdminArray:NSMutableArray = []
    
    var groupImageStr : String = ""
    
    
    //MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let DBref = FIRDatabase.database().reference()
        print(groupId)
        DBref.child("groups").child(groupId).child("group_img").observe(.value, with: { snapshot in
            if !snapshot.exists() { return }
            
            print(snapshot)
            
            if let imageStr = snapshot.value as? String {
                self.groupImageStr =  imageStr
            }
        })
        

        tableView.register(UINib(nibName: "GroupSettingsHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "GroupSettingsHeaderFooterView")
      
        tableView.register(UINib(nibName: "GroupMembersTableViewCell", bundle: nil), forCellReuseIdentifier: "GroupMembersTableViewCell")
         tableView.register(UINib(nibName: "ExitgroupHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ExitgroupHeaderFooterView")
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 120
        userimageToSend = UIImage(named:"dummyProfile")!
        print(addMemberObjArray)
        print(groupMembersArray)
        
        print(fileUrl)
        
        groupUserIdArray.removeAllObjects()
        print(groupId)
        getGroupMembersUsersIdLIst(objgetGroupChat: groupId)
        

        //addImageGroupTableAPI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    @IBAction func back(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    var isAdminStrFlag:String = ""
    //MARK:TableView Delegates
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return UITableView.automaticDimension
        }
        else{
            return UITableView.automaticDimension
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupMembersArray.count
    }
    var adminWithOtherUserFlag:Bool = false
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "GroupMembersTableViewCell", for: indexPath as IndexPath) as! GroupMembersTableViewCell
         cell.selectionStyle = .none
        
        if indexPath.row != 0 {
            cell.members_Height.constant = 0
            cell.extraTopSpace.constant = 0
            cell.extraBottomSpace.constant = 0
        }
        
        if let user_image = (groupMembersArray.object(at: indexPath.row) as AnyObject).value(forKey: "user_img") as? String{
            let url = URL(string: user_image)
            if !(url==nil)
            {
                cell.userImage.af_setImage(withURL: url!)
            }else
            {
                cell.userImage.image = UIImage.init(named: "temp")
            }
        }
        
        if let nameStr = (groupMembersArray.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String{
            cell.nameLBl.text = nameStr
        }
        //check admin logged in or group created by him
        if PFUser.current()?.objectId! == self.getAdminId{
            
            isAdminStrFlag = "1"
        }
        else{
            isAdminStrFlag = "0"
        }
        
        
        //visibility of buttons
        if let adminStr = (groupMembersArray.object(at: indexPath.row) as AnyObject).value(forKey: "isAdmin") as? String{
            if adminStr == "1" {
                cell.removeBtn.isHidden = true
                cell.groupAdminBtn.isHidden = false
              }
            else{
                
                cell.groupAdminBtn.isHidden = true
                
                if isAdminStrFlag == "1"{
                    
                    cell.removeBtn.isHidden = false
                   
                }
                else{
                    cell.removeBtn.isHidden = true
                    
                }
                
            }
        }
       
        return cell
   }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       
        let cell = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "GroupSettingsHeaderFooterView") as! GroupSettingsHeaderFooterView
        cell.delegate = self
    
        if isAdminStr == "0"{
            cell.admin_height.constant = 0
        }
            print(fileUrl)
        
//        else{
//            let url = URL(string: fileUrl)
//            if !(url==nil)
//            {
//                print(url as Any!)
//                cell.userImage.af_setImage(withURL: url!)
//                cell.userImage.backgroundColor = UIColor.black
//                
//            }else
//            {
//                cell.userImage.image = UIImage(named: "default.png")!
//        }
//        }
        
        
        let url = URL(string: self.groupImageStr)
        if !(url==nil)
        {
            cell.groupBackImage.af_setImage(withURL: url!)
            cell.userImage.af_setImage(withURL: url!)
        }
        
        if uploadFlag ==  true{
            cell.userImage.image = self.userimageToSend
            cell.groupBackImage.image = self.userimageToSend
        }
        
      
        cell.groupBackImage.addBlurEffect()
        

        cell.nameTF.text =  groupnameStr
        
        cell.nameShadow_height.layer.shadowColor = UIColor.darkGray.cgColor
        cell.nameShadow_height.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.nameShadow_height.layer.shadowOpacity = 1
        cell.nameShadow_height.layer.shadowRadius = 1.0
        
        
        cell.adminView.layer.shadowColor = UIColor.darkGray.cgColor
        cell.adminView.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.adminView.layer.shadowOpacity = 1
        cell.adminView.layer.shadowRadius = 1.0
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        let currentCell = tableView.cellForRow(at: indexPath!) as! GroupMembersTableViewCell
        
        if  currentCell.removeBtn.titleLabel?.text == "Remove"{
            let alertController = UIAlertController(title: "Message", message: "Are you sure you want to delete user?", preferredStyle: .alert)
            
            let saveAction = UIAlertAction(title: Constants.okTitle, style: .default, handler: {
                alert -> Void in
              
                self.groupActivityIndicator.startAnimating()
                print(currentCell.removeBtn.titleLabel?.text as Any)
                print(self.groupMembersArray.object(at: (indexPath?.row)!) as AnyObject)
                if let userId = (self.groupMembersArray.object(at: (indexPath?.row)!) as AnyObject).value(forKey: "user_id") as? String{
                    
                    self.removegroupMember(memberId: userId,index:(indexPath?.row)!)
                }
            })
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
                (action : UIAlertAction!) -> Void in
                
            })
           
            alertController.addAction(saveAction)
            alertController.addAction(cancelAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      if isAdminStr == "0"{
        return 328
        }
        return 474

    }
    func  tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if addParicipantsStr == "0" {
            return 148
        }
        else{
            return 200
        }
       
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
         let cell1 = self.tableView.dequeueReusableHeaderFooterView(withIdentifier: "ExitgroupHeaderFooterView") as! ExitgroupHeaderFooterView
        cell1.delegate = self
        if addParicipantsStr == "0"{
           cell1.addPart_heght.constant = 0
        }
        else{
            cell1.addPart_heght.constant = 70
        }
        cell1.exitView1.layer.shadowColor = UIColor.darkGray.cgColor
        cell1.exitView1.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell1.exitView1.layer.shadowOpacity = 1
        cell1.exitView1.layer.shadowRadius = 1.0
        
        
        cell1.exitView2.layer.shadowColor = UIColor.darkGray.cgColor
        cell1.exitView2.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell1.exitView2.layer.shadowOpacity = 1
        cell1.exitView2.layer.shadowRadius = 1.0
        
        
        return cell1
    }
    //MARK:-UPDATE NAME
    func nameClicked(cell:GroupSettingsHeaderFooterView) {
        self.groupActivityIndicator.startAnimating()
        let alertController = UIAlertController(title: "Group Name", message: "", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: Constants.okTitle, style: .default, handler: {
            alert -> Void in
            
            let firstTextField = alertController.textFields![0] as UITextField
              print("firstName \(firstTextField.text!)")
            self.updategroupNameValue(groupName: firstTextField.text!,cellAS: cell)
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            if cell.nameTF.text! == ""{
                textField.placeholder = "Enter Group Name"
            }
            else{
                textField.text = cell.nameTF.text!
            }
        }
       
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
  
        
    }
    func userImageClicked(cell:GroupSettingsHeaderFooterView,type:String){
        pickerType = type
        self.cameraAuthorization()
        
        
    }
    func cameraAuthorization(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func chooseSheet(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            
        }
        
    }
    func openGallary() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertClose(_ gesture: UITapGestureRecognizer) {
        //        if imageEditBtnFlag == true {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    func alertClose1(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            self.userimageToSend = pickedImage
            self.uploadFlag = true
            self.groupActivityIndicator.startAnimating()
            addImageGroupTableAPI()
            tableView.reloadData()
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func addImageGroupTableAPI(){
       //        let chatDictionary1:NSDictionary = [:]
         var urlString: String = ""
        if uploadFlag ==  true{
            let imageData:NSData = self.userimageToSend.jpegData(compressionQuality: 0.5)! as NSData
            // Create a reference to the file you want to upload
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
            let  timeStrToPass = formatter.string(from: Date())
            
            let riversRef = FIRStorage.storage().reference().child("images/\(timeStrToPass).jpg")
            let metadata = FIRStorageMetadata()
            metadata.contentType = "image/jpeg"
            // Upload the file to the path "images/rivers.jpg"
            let uploadTask = riversRef.put(imageData as Data, metadata: metadata) { (metadata, error) in
                
            }
            
            uploadTask.observe(.success) { snapshot in
                print(snapshot) // Upload completed successfully
                let downloadURL = snapshot.metadata?.downloadURL()
                print(downloadURL!)
                //self.storeUserProfileInDB(fileurlAs:downloadURL! as NSURL)
                urlString = String(describing: downloadURL!)
                
                if urlString != ""{
                    let DBref = FIRDatabase.database().reference()
                    let usrRefrence = FIRDatabase.database().reference()
                    //Sender data for group
                    let groupDictionary = ["group_img":urlString] as [String : Any]
                    let myRef = DBref.child("groups/\(self.groupId)")
                    myRef.updateChildValues(groupDictionary) { (error, DBref) in
                        if error == nil {
                            //User description in group
                            self.groupActivityIndicator.stopAnimating()
                            for  i in 0 ..< self.groupUserIdArray.count{
                                let  userDict =  ["group_img":urlString] as [String : Any]
                                //usrRefrence.child("users\(self.groupUserIdArray[i])/groups\(self.groupId)")
                                usrRefrence.child("users").child(self.groupUserIdArray[i] as! String).child("groups").child(self.groupId)
                                    .updateChildValues(userDict) { (error, DBref) in
                                        print(DBref)
                                }
                            }
                        }
                    }
                }

                
            }
        }
    }
    func userAnyOneCanClicked(cell:GroupSettingsHeaderFooterView){
        cell.anyoneBTn.isSelected = !cell.anyoneBTn.isSelected
        if cell.anyoneBTn.isSelected == true{
            cell.anyOneSelectedBtn.isSelected = false
           
            cell.onlyadminBtn.setImage(UIImage(named: "uncheck.png"), for: .normal)
            cell.anyOneSelectedBtn.setImage(UIImage(named: "check.png"), for: .normal)
            self.setAddParicipantValue(addpartVal: "1")
        }
        else{
            cell.anyOneSelectedBtn.isSelected = true
            cell.onlyadminBtn.setImage(UIImage(named: "check.png"), for: .normal)
            cell.anyOneSelectedBtn.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
        
    }
    func adminClicked(cell:GroupSettingsHeaderFooterView){
        cell.onlyAdminBtn.isSelected = !cell.onlyAdminBtn.isSelected
        if cell.onlyAdminBtn.isSelected == true{
            cell.onlyAdminBtn.isSelected = false
            cell.anyOneSelectedBtn.setImage(UIImage(named: "uncheck.png"), for: .normal)
             cell.onlyadminBtn.setImage(UIImage(named: "check.png"), for: .normal)
            self.setAddParicipantValue(addpartVal: "0")
        }
        else{
            cell.onlyAdminBtn.isSelected = true
            cell.anyOneSelectedBtn.setImage(UIImage(named: "uncheck.png"), for: .normal)
            cell.onlyadminBtn.setImage(UIImage(named: "uncheck.png"), for: .normal)
        }
        
    }
    
    //MARK:-Update group name
    func updategroupNameValue(groupName:String,cellAS:GroupSettingsHeaderFooterView){
        
        let groupRefrence = FIRDatabase.database().reference()
        let usrRefrence = FIRDatabase.database().reference()
        //Sender data for group
        print(groupId)
        print(self.groupUserIdArray)
        let groupDictionary = ["group_name":groupName] as [String : Any]
        groupRefrence.child("groups").child(groupId).updateChildValues(groupDictionary) { (error, groupRefrence) in
            if error == nil{
                //User description in group
                for  i in 0 ..< self.groupUserIdArray.count{
                    let  userDict =  ["group_name":groupName] as [String : Any]
                    usrRefrence.child("users").child(self.groupUserIdArray[i] as! String).child("groups").child(self.groupId)
                        .updateChildValues(userDict) { (error, DBref) in
                            print(DBref)
                    }
                }
                cellAS.nameTF.text = groupName
                globalgroupName.gName = groupName
            }
            self.groupActivityIndicator.stopAnimating()
        }
        
    }
    //MARK:-Update User Value In Group
    func setAddParicipantValue(addpartVal:String){
        
        let usrRefrence = FIRDatabase.database().reference()
        //Sender data for group
        for  i in 0 ..< self.userChildIdArray.count{
            print(groupId)
          //  print(self.groupUserIdArray[i] as! String)
            let groupDictionary = ["add_participant":addpartVal] as [String : Any]
            usrRefrence.child("groups").child(groupId).child("users").child(self.userChildIdArray[i] as! String).updateChildValues(groupDictionary) { (error, usrRefrence) in
                if error == nil{
                   print(usrRefrence) //User description in group
                    
                }
            }
        }
    }
    //MARK:-Remove Group member From DB
    func removegroupMember(memberId:String,index:Int){
        print(memberId)
        print(index)
        let groupRefrence = FIRDatabase.database().reference()
        let usrRefrence = FIRDatabase.database().reference()
        //Sender data for group
           print(groupId)
        
            groupRefrence.child("groups").child(groupId).child("users").queryOrdered(byChild: "user_id").queryEqual(toValue: memberId).observe(.value, with: { snapshot in
                print("value : ", snapshot)
                for item in snapshot.children {
                    print(item)
                (item as AnyObject).ref.child((item as AnyObject).key!).parent?.removeValue()
                self.groupMembersArray.removeObject(at: index)
                }
            })
        
        
        let userRef = usrRefrence.child("users").child(memberId).child("groups").child(self.groupId)
        print(userRef)
        
        userRef.removeValue { (error, ref) in
            if error != nil {
                print("error \(error)")
            }
        }
        
        //Redirect to listing page.
        
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        
        var isChatListControllerExist = false
        for aViewController in viewControllers {
            if aViewController is ChatListingViewController {
                isChatListControllerExist = true
               _ = self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
        
        if !isChatListControllerExist {
            let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
            let vc = chatSB.instantiateViewController(withIdentifier: "ChatListingViewController")  as! ChatListingViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
        self.groupActivityIndicator.stopAnimating()
     }
    //MARK:-ADD  PARTICIPANTS
    func addPartyClicked(cell:ExitgroupHeaderFooterView){
        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
        let vc = chatSB.instantiateViewController(withIdentifier: "AddChatmembersViewController")  as! AddChatmembersViewController
        vc.fromGroupSettingsUserArray =  self.groupMembersArray
        vc.fromViewType = "groupSettings"
        vc.groupIdStr = self.groupId
        vc.isAdminStr = self.isAdminStr
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    //MARK:-Get Group Listing
    func getGroupMembersUsersIdLIst(objgetGroupChat:String){
        
        if  Reachability.isConnectedToNetwork() == false {
            
            self.showMessage(messages.internet)
            
            return
        }
        
        let DBref = FIRDatabase.database().reference()
        DBref.child("groups").child(objgetGroupChat).child("users").observe(.value, with: { snapshot in
            // print(snapshot)
            // print(DBref)
          self.groupMembersArray.removeAllObjects()
            var i = 0
            for rest in snapshot.children.allObjects as! [FIRDataSnapshot] {
                print(rest.key)
                
                guard let restDict = rest.value as? [String: Any] else { continue }
                // self.activityChat.stopAnimating()
                print("dict as",restDict)
                self.groupMembersArray.add(restDict)
                self.getUserImg(restDict, index: i)
                i += 1
            }
            print("After Sending Message",self.groupMembersArray)
            self.groupUserIdArray.removeAllObjects()
            for  i in 0 ..< self.groupMembersArray.count {
                if self.fromView != "groupSettings"{
                   self.groupUserIdArray.add((self.groupMembersArray.object(at: i) as AnyObject).value(forKey: "user_id") as? String ?? "")
                    
                    if (self.groupMembersArray.object(at: i) as AnyObject).value(forKey: "user_id") as? String ==  PFUser.current()?.objectId{
                        self.isAdminStr = ((self.groupMembersArray.object(at: i) as AnyObject).value(forKey: "isAdmin") as? String)!
                        if let participantsStr = (self.groupMembersArray.object(at: i) as AnyObject).value(forKey: "add_participant") as? String{
                            if participantsStr == "0"{
                                self.addParicipantsStr = participantsStr
                            }
                        }
                    }
                }
                
                if ((self.groupMembersArray.object(at: i) as AnyObject).value(forKey: "isAdmin") as? String)! == "1" {
                    self.getAdminId = ((self.groupMembersArray.object(at: i) as AnyObject).value(forKey: "user_id") as? String)!
                }
                
            }

            self.tableView.reloadData()
        })
     }
    
    
    func getUserImg(_ dict: [String: Any], index: Int) {
        let DBref = FIRDatabase.database().reference()
        var refDict = dict
        
        DBref.child("users").child(dict["user_id"] as? String ?? "").observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String:Any] {
                //print(dict)
                
                if let imageURLStr = (dict as AnyObject).value(forKey: "user_img") as? String {
                    refDict["user_img"] = imageURLStr
                }else {
                    refDict["user_img"] = ""
                }
                
                self.groupMembersArray[index] = refDict
            }
            
            self.tableView.reloadData()
        })
    }
    
    
    //MARK:-EXIT Group
    func exitGroup(cell:ExitgroupHeaderFooterView){
        
        let alertController = UIAlertController(title: "Message", message: "Are you sure you want to exit group?", preferredStyle: .alert)
        
        let saveAction = UIAlertAction(title: Constants.okTitle, style: .default, handler: {
            alert -> Void in
            print(self.isAdminStr)
           if self.isAdminStr == "1" {
                self.makeOtheruserAdmin()
            }
            
            //Assign Admin to Random User
            
            var index = 0
            for object in self.groupMembersArray {
                if let dic = object as? NSDictionary, dic.value(forKey: "user_id") as? String ?? "" == PFUser.current()?.objectId {
                    
                    let currentUserId = (PFUser.current()?.objectId!)!
                    
                    self.removegroupMember(memberId: currentUserId, index: index)
                    break
                }
                
                index += 1
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in
            
        })
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func makeOtheruserAdmin(){
        let groupMembersIdArray:NSMutableArray = []
        
        let getAllMembersIdArray:NSMutableArray = []
        print(groupMembersArray)
        if groupMembersArray.count > 0 {
            
         for  i in 0 ..< self.groupMembersArray.count{
           
            if let userIdStr = (groupMembersArray.object(at: i) as AnyObject).value(forKey: "user_id") as? String{
                getAllMembersIdArray.add(userIdStr)
                if userIdStr  != PFUser.current()?.objectId {
                    groupMembersIdArray.add(userIdStr)
                }
                }
            }
            print(groupMembersIdArray)
            let randomIndex = Int(arc4random_uniform(UInt32(groupMembersIdArray.count)))
            print(randomIndex)
             if let userIdStr = (groupMembersIdArray.object(at: randomIndex) as AnyObject) as? String{
                print(userIdStr)
                setAdminValue(adminVal: "1",userId: userIdStr,pushuserIdArray: groupMembersIdArray)
            }
        }
    }
    //MARK:-Update User Admin In Group
    func setAdminValue(adminVal:String,userId:String,pushuserIdArray:NSMutableArray){
        let groupRefrence = FIRDatabase.database().reference()
        //Sender data for group
        var selectedUserId :String = ""
        print(groupId)
        groupRefrence.child("groups").child(groupId).child("users").queryOrdered(byChild: "user_id").queryEqual(toValue: userId).observe(.value, with: { snapshot in
            print("value : ", snapshot)
            print("value Id : ", snapshot.key)
            for item in snapshot.children {
                print((item as AnyObject))
                print(snapshot.children)
               print((item as AnyObject).key!)
                selectedUserId = (item as AnyObject).key!
                let usrRefrence = FIRDatabase.database().reference()
                //Sender data for group
                
                print(self.groupId)
                //  print(self.groupUserIdArray[i] as! String)
                let groupDictionary = ["isAdmin":adminVal] as [String : Any]
                print( usrRefrence.child("groups").child(self.groupId).child("users").child(userId))
                usrRefrence.child("groups").child(self.groupId).child("users").child(selectedUserId).updateChildValues(groupDictionary) { (error, usrRefrence) in
                    if error == nil{
                        print(usrRefrence) //User description in group
                       
                         self.pushtoMakeOtherUserAdmin(userIdArray: pushuserIdArray, postId: "", pushType: "", message: messages.newAdminMessage, groupId: self.groupId)
                        
                    }
                }
            }
        })
    }
    
    
    //MARK:Send push on group Admin Exited
    func pushtoMakeOtherUserAdmin(userIdArray:NSMutableArray, postId:String, pushType:String, message:String, groupId:String)
    {
        print(userIdArray)
        print(PFUser.current()?.objectId! as Any)
        print(userIdArray)
        
        print(userIdArray)
        _ = userIdArray as NSArray as! [String]
        
        print(groupId)
        
        let currentName = PFUser.current()?.value(forKey: "name") as! String
        self.sendPushNotification(postId: postId, userId: "",settingType: "push_message_send",message:"\(currentName) \(message)",type:pushType,userIdArray:userIdArray,groupMembersId:groupId,MsgType:"GroupChat",reciverId:"",username:"\(groupnameStr)",groupName:"")
    }
}

extension UIImageView
{
    func addBlurEffect()
    {
        
//        var blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Light)
//        var effectView:UIVisualEffectView = UIVisualEffectView (effect: blur)
//        effectView.frame = frame
//        addSubview(effectView)
        
       
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = self.bounds
         blurEffectView.alpha = 0.3
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight] // for supporting device rotation
        self.addSubview(blurEffectView)
    }
}
