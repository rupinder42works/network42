//
//  GroupSettingsHeaderFooterView.swift
//  Network42
//
//  Created by 42works on 23/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol nameGroupDelegate {
    func nameClicked(cell:GroupSettingsHeaderFooterView)
    func userImageClicked(cell:GroupSettingsHeaderFooterView,type:String)
    func userAnyOneCanClicked(cell:GroupSettingsHeaderFooterView)
    func adminClicked(cell:GroupSettingsHeaderFooterView)

}
class GroupSettingsHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet var nameTF: MyTextField!
    weak var delegate: nameGroupDelegate?

    @IBOutlet var onlyadminBtn: UIButton!
    @IBOutlet var anyOneSelectedBtn: UIButton!
    @IBOutlet var onlyAdminBtn: UIButton!
    @IBAction func onlyAdminTapped(_ sender: Any) {
        self.delegate?.adminClicked(cell: self)
    }
    @IBOutlet var anyoneBTn: UIButton!
    @IBAction func anyOneTapped(_ sender: Any) {
        
        self.delegate?.userAnyOneCanClicked(cell: self)
    }
    @IBOutlet var admin_height: NSLayoutConstraint!
    @IBOutlet var groupBackImage: UIImageView!
    @IBOutlet var userImage: UIImageView!
    @IBAction func nameTapped(_ sender: Any) {
         self.delegate?.nameClicked(cell: self)
        
    }
    @IBAction func editBgImageTapped(_ sender: Any) {
        
         self.delegate?.userImageClicked(cell: self,type: "backgroud")
        
    }
    @IBOutlet var editBgImageBtn: UIButton!
    @IBOutlet var editBtn: UIButton!
    @IBAction func editTapped(_ sender: Any) {
        
        self.delegate?.userImageClicked(cell: self,type: "user")
    }
    @IBOutlet var member_height: NSLayoutConstraint!
    @IBOutlet var adminView: UIView!
    @IBOutlet var nameView: UIView!
    @IBOutlet var nameShadow_height: UIView!
 }
