//
//  ChatTableViewCell.swift
//  FuturEd
//
//  Created by Anmol Rajdev on 18/04/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var ht_constrntBK: NSLayoutConstraint!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var chatlBl: UILabel!
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var vw_bottom: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
