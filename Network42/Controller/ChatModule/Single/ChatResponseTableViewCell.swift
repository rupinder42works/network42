//
//  ChatResponseTableViewCell.swift
//  FuturEd
//
//  Created by Anmol Rajdev on 19/04/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class ChatResponseTableViewCell: UITableViewCell {

    @IBOutlet weak var userImageView: UIImageView!
 
    @IBOutlet var chatBKView_width: NSLayoutConstraint!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var chatlBl: UILabel!
    
    @IBOutlet weak var vw_bottom: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
    
}
