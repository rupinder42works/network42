//
//  ChatImagereciverTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 23/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class ChatImagereciverTableViewCell: UITableViewCell {

    @IBOutlet var timeLbl: UILabel!
    @IBOutlet var chatImgView: UIImageView!
    @IBOutlet var bkView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        bkView.layer.cornerRadius = 2.0
        bkView.clipsToBounds = true
        bkView.layer.masksToBounds = true // Initialization code
    }
    
    @IBOutlet var bkImage_Width: NSLayoutConstraint!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
