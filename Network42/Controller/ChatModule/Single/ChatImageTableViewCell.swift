//
//  ChatImageTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 22/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class ChatImageTableViewCell: UITableViewCell {

    @IBOutlet var bgImageVew_height: NSLayoutConstraint!
    @IBOutlet var bkView: UIView!
    @IBOutlet var bkView_Width: NSLayoutConstraint!
    @IBOutlet var chatImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        bkView.layer.cornerRadius = 2.0
        bkView.clipsToBounds = true
        bkView.layer.masksToBounds = true

    }
   
    @IBOutlet var name_height: NSLayoutConstraint!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var timeLbl: UILabel!
   

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
