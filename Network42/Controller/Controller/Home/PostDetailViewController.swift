//
//  PostDetailViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 12/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.addObserver(self, selector: #selector(PostDetailViewController.navigateToController(notification:)), name:NSNotification.Name(rawValue: "homeController"), object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(PostDetailViewController.pushNotifymethod(notification:)), name:NSNotification.Name(rawValue: "postDetailController"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    //MARK:- PUSH Functions
    func pushNotifymethod(notification: NSNotification) {
        print(notification)
       
    }
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
