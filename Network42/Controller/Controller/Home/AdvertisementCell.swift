//
//  AdvertisementCell.swift
//  Network42
//
//  Created by 42works on 20/12/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import GoogleMobileAds

class AdvertisementCell: UITableViewCell {

    @IBOutlet var bannerView: GADBannerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
