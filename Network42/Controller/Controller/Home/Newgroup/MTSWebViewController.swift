//
//  MTSWebViewController.swift
//  Magnum
//
//  Created by 42works on 17/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class MTSWebViewController: UIViewController,UIWebViewDelegate {

    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var webView: UIWebView!
    var urlStr : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        print(urlStr)
        indicator.startAnimating()

        let url = NSURL (string: urlStr)
        let requestObj = URLRequest(url: url! as URL)
        
        webView.loadRequest(requestObj)
        webView.delegate = self

    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        indicator.stopAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func backTapped(_ sender: Any) {
          _ =  self.navigationController?.popViewController(animated: true)
    }

   

}
