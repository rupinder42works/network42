//
//  SingleMediaViewController.swift
//  Network42
//
//  Created by 42works on 29/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import MediaPlayer
import AVKit
import AVFoundation
import Parse
import ParseUI



class SingleMediaViewController: UIViewController,UIScrollViewDelegate {

    var imagePrevious : UIImage = UIImage()
    var imageView: PFImageView!
    var imageViewSimple: UIImageView!
    var scrollV : UIScrollView!
    let playerController = AVPlayerViewController()
    var isImage : Bool = false
     var objectFormHome : PFObject?
    var index : Int = 0
    var indicatorObj: UIActivityIndicatorView!
    
    var fromChat : Bool = false
    var urlStr : String = ""

    
    override func viewDidLoad() {
        super.viewDidLoad()

         self.tabBarController?.tabBar.isHidden = true
        
      if fromChat == false
      {
        let arr = objectFormHome?.object(forKey: "postmedia") as! NSArray
        print("index======",index)
        if let fileObj = arr.object(at: index) as? PFFile
        {
            let urlStr = fileObj.url! as NSString
            let extensionStr = urlStr.pathExtension
            let extensionArray : NSMutableArray = ["mp4"]
            if extensionArray.contains(extensionStr)
            {
                let url = URL(string: fileObj.url!)
                let player = AVPlayer(url: url! as URL)
                
                player.actionAtItemEnd = .none
                playerController.player = player
                self.addChildViewController(playerController)
                self.view.addSubview(playerController.view)
                playerController.view.frame = self.view.frame
                player.play()
                
                let button = UIButton(frame: CGRect(x: self.view.bounds.size.width-30, y: 55, width: 29, height: 29))
                button.setImage(UIImage.init(named: "crossCreateProfile"), for: .normal)
                button.addTarget(self, action: #selector(SingleMediaViewController.dismissPlayer), for: .touchUpInside)
                playerController.view.addSubview(button)
            }
            else
            {
                
                self.scrollV=UIScrollView()
                self.scrollV.frame = CGRect(x: 0, y: 64, width: self.view.bounds.size.width, height:self.view.bounds.size.height-64)
                self.scrollV.minimumZoomScale=1
                self.scrollV.maximumZoomScale=3
                self.scrollV.bounces=false
                self.scrollV.delegate=self;
                self.view.addSubview(self.scrollV)
                
                self.imageView=PFImageView()
               // self.imageView.backgroundColor = UIColor.black
                self.imageView.frame = CGRect(x: 0, y: 0, width: self.scrollV.bounds.size.width, height:self.scrollV.bounds.size.height)
                self.imageView.contentMode = .scaleAspectFit
                self.scrollV.addSubview(self.imageView)
                
                self.indicatorObj = UIActivityIndicatorView()
                self.indicatorObj.frame = CGRect(x: 0, y: 0, width: 10, height:10)
                self.indicatorObj.center = imageView.center
                self.scrollV.addSubview(self.indicatorObj)
                
                
                imageView.file = fileObj
                self.indicatorObj.startAnimating()
                imageView.load { (image, error) in
                    self.indicatorObj.stopAnimating()
                }
            }
        }
        }else
        {
        
            self.scrollV=UIScrollView()
            self.scrollV.frame = CGRect(x: 0, y: 64, width: self.view.bounds.size.width, height:self.view.bounds.size.height-64)
            self.scrollV.minimumZoomScale=1
            self.scrollV.maximumZoomScale=3
            self.scrollV.bounces=false
            self.scrollV.delegate=self;
            self.view.addSubview(self.scrollV)
            
            self.imageViewSimple=UIImageView()
            self.imageViewSimple.frame = CGRect(x: 0, y: 0, width: self.scrollV.bounds.size.width, height:self.scrollV.bounds.size.height)
            self.imageViewSimple.contentMode = .scaleAspectFit
            self.scrollV.addSubview(self.imageViewSimple)
            
            self.indicatorObj = UIActivityIndicatorView()
            self.indicatorObj.frame = CGRect(x: 0, y: 0, width: 10, height:10)
            self.indicatorObj.center = imageViewSimple.center
            self.scrollV.addSubview(self.indicatorObj)
            
            
            let url = URL(string: urlStr)
            if !(url==nil)
            {
                imageViewSimple.af_setImage(withURL: url!)
            }else
            {
                imageViewSimple.image = nil
            }
        }
        
        if isImage
        {
        
        }else
        {
        
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }
    
   func dismissPlayer()
   {
     _ = self.navigationController?.popViewController(animated: true)
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        if fromChat == false
        {
        return self.imageView
        }else
        {
          return self.imageViewSimple
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    @IBAction func backButton(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
//        self.navigationController?.dismiss(animated: true, completion: { 
//            print("dismissed")
//        })
    }

}
