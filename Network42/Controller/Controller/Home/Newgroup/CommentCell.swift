//
//  CommentCell.swift
//  Network42
//
//  Created by 42works on 24/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import ActiveLabel

class CommentCell: UITableViewCell {
    @IBOutlet var profileImage: PFImageView!

    @IBOutlet var name: UILabel!
    @IBOutlet var timeObj: UILabel!
    @IBOutlet var comment: ActiveLabel!
    @IBOutlet var containerImage: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
