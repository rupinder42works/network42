//
//  HomeCollectionViewCell.swift
//  Network42
//
//  Created by 42works on 15/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import ParseUI
import JPVideoPlayer

@objc protocol MuteActionDelegate {
    func mute()
}

class HomeCollectionViewCell: UICollectionViewCell {

    @IBOutlet var muteButton: UIButton!
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var imageObj: PFImageView!
    @IBOutlet var heightOFImageView: NSLayoutConstraint!
    @IBOutlet var indicatorObj: UIActivityIndicatorView!
    weak var delegate : MuteActionDelegate?
    
     var updateData : PFObject?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func muteAction(_ sender: Any) {
        delegate?.mute()
        (sender as AnyObject).setBackgroundImage(UIImage.init(named: "\(muteButton.isSelected)"), for: .normal)
        muteButton.isSelected = !(muteButton.isSelected)
        let arr = ["muteUpdate",updateData ?? "no"] as NSArray
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "mute"), object: arr)
      }
    
}
