//
//  HomeCell.swift
//  Network42
//
//  Created by 42works on 11/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import ActiveLabel
import AlamofireImage
import AVKit
import AVFoundation
import JPVideoPlayer

@objc protocol likeListingDelegate {
    func likeListingClicked(tag:Int)
    func likeClicked(sender: UIButton)
    func commentClicked(tag:Int)
    func URLClicked(tag:Int)
    func moreClicked(tag:Int)
    func userProfileClicked(tag:Int)
    
    func imageClicked(tag:Int,object:PFObject)
}


class HomeCell: UITableViewCell {
    @IBOutlet var likeListingObj: UIButton!
    @IBOutlet var likeBtn: UIButton!
    @IBOutlet var commentListing: UIButton!
    @IBOutlet var URLBtn: UIButton!
    @IBOutlet var moreBtn: UIButton!
    @IBOutlet var userProfileBtn: UIButton!
    
    //var indicatorObject = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    @IBOutlet var indicatorObject: UIActivityIndicatorView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var profileImage: PFImageView!
    @IBOutlet var time: UILabel!
    @IBOutlet var name: UILabel!
    
    @IBOutlet var descriptionLbl: ActiveLabel!
    @IBOutlet var heightOfDescription: NSLayoutConstraint!
    
    @IBOutlet var like: UILabel!
    @IBOutlet var comment: UILabel!
    
    @IBOutlet var topHeightOfUrlView: NSLayoutConstraint!
    @IBOutlet var heightOFUrlView: NSLayoutConstraint!
    @IBOutlet var urlView: UIView!
    
    @IBOutlet var urlImage: UIImageView!
    @IBOutlet var urlTitle: UILabel!
    @IBOutlet var urlDescription: UILabel!
    @IBOutlet var urlAgain: UILabel!
    @IBOutlet var widthOfUrlImage: NSLayoutConstraint!
    
    
    @IBOutlet var collectionContainer: UIView!
    @IBOutlet var collectionVIewHeight: NSLayoutConstraint!
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var pageControlObj: UIPageControl!
    @IBOutlet var heightOFPageController: NSLayoutConstraint!
    //var collectionArray : NSMutableArray = []
    var objectForCollection : PFObject?
    @IBOutlet var topOfDescription: NSLayoutConstraint!
    
    @IBOutlet var singleVideo: UIView!
    @IBOutlet var singleVideoImage: PFImageView!
    @IBOutlet var heightOFSingleVideo: NSLayoutConstraint!
    
     weak var delegate: likeListingDelegate?
    
    var avPlayer: AVPlayer!
    
    //var currentIndex : Int = 0
    
    var changeVideo : Bool = false
    
    var playerDictionary = [[:]]
    
    var muted : Bool = false
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
//        self.indicatorObject.center = collectionViewObj.center
//        self.indicatorObject.hidesWhenStopped = false
//        self.indicatorObject.startAnimating()
        //collectionViewObj.addSubview(self.indicatorObject)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    @IBAction func like(_ sender: Any) {
        self.delegate?.likeClicked(sender: sender as? UIButton ?? UIButton())
    }
    @IBAction func comment(_ sender: Any) {
        self.delegate?.commentClicked(tag: (sender as AnyObject).tag)
    }
    @IBAction func URLAction(_ sender: Any) {
        
         self.delegate?.URLClicked(tag: (sender as AnyObject).tag)
    }
    @IBAction func more(_ sender: Any) {
        self.delegate?.moreClicked(tag: (sender as AnyObject).tag)
    }
    
   
    @IBAction func likeListingAction(_ sender: Any) {
        self.delegate?.likeListingClicked(tag: (sender as AnyObject).tag)
    }
    @IBAction func userProfile(_ sender: Any) {
         self.delegate?.userProfileClicked(tag: (sender as AnyObject).tag)
    }
}


extension HomeCell : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    
    
    func reloadSingleRow()
    {
       collectionViewObj.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let arr = objectForCollection?.object(forKey: "postmedia") as? NSArray ?? NSArray()
        pageControlObj.numberOfPages = arr.count
        return arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell",
                                                      for: indexPath) as! HomeCollectionViewCell
        let arr = objectForCollection?.object(forKey: "postmedia") as? NSArray ?? NSArray()
        if let fileObj = arr.object(at: indexPath.row) as? PFFile
        {
            print("currentIndex======",indexPath.row)
            let urlStr = fileObj.url! as NSString
            let extensionStr = urlStr.pathExtension
            let extensionArray : NSMutableArray = ["mp4"]
            
            cell.imageObj.file = nil
            cell.imageObj.stopPlay()
            cell.muteButton.isHidden = true
            if extensionArray.contains(extensionStr)
            {
                cell.muteButton.isHidden = false
                if let thumbnail = objectForCollection?.object(forKey: "thumbnail") as? NSArray
                {
                    if let fileObj = thumbnail.object(at: indexPath.row) as? PFFile
                    {
                        cell.imageObj.file = fileObj
                        cell.imageObj.loadInBackground()
                    }
                }
                else
                {
                    cell.imageObj.image = UIImage.init(named: "default.png")
                }
                
                let url = URL(string: fileObj.url!)
                cell.imageObj.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                
                if let mute = objectForCollection?.object(forKey: "mute") as? Bool
                {
                 cell.muteButton.setBackgroundImage(UIImage.init(named: "\(!mute)"), for: .normal)
                    if mute
                    {
                        cell.imageObj.jp_playVideoMuted(with: url!)
                    }else
                    {
                        cell.imageObj.jp_playVideo(with: url!)
                    }
                  cell.muteButton.isSelected = mute
                }
                cell.delegate = self
                cell.updateData = objectForCollection
             }
            else
            {

                cell.imageObj.file = fileObj
                cell.indicatorObj.startAnimating()
                cell.imageObj.load { (image, error) in
                    cell.indicatorObj.stopAnimating()
                }
            }
        }
        return cell
    }
    
    func timer(muteObj:Bool)
     {
        JPVideoPlayerManager.shared().setPlayerMute(muteObj)
     }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !(avPlayer == nil)
        {
          avPlayer.pause()
          avPlayer = nil
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       self.delegate?.imageClicked(tag: indexPath.row, object: objectForCollection!)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControlObj.currentPage = indexPath.row
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func setCollectionViewDataSourceDelegate
        <D: protocol<UICollectionViewDataSource,UICollectionViewDelegate>>
        (dataSourceDelegate: D, forRow row: Int,changeVideo:Bool) {
        
        collectionViewObj.register(UINib(nibName: "HomeCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "HomeCollectionViewCell")
        if #available(iOS 10.0, *)
        {
            collectionViewObj.isPrefetchingEnabled = false
        }
        
        self.changeVideo = changeVideo

        collectionViewObj.delegate = dataSourceDelegate
        collectionViewObj.dataSource = dataSourceDelegate
        collectionViewObj.tag = row
        collectionViewObj.reloadData()
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width: 355, height:400)
        collectionViewObj.isPagingEnabled = true
        collectionViewObj.setCollectionViewLayout(layout, animated: false)
    }
}

extension HomeCell : MuteActionDelegate
{
    func mute() {
        //avPlayer.isMuted = !(avPlayer.isMuted)
        JPVideoPlayerManager.shared().setPlayerMute(!(JPVideoPlayerManager.shared().playerIsMute()))
    }
    
}
