//
//  FollowListViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 12/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
class FollowListViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var getfinalArray : NSMutableArray = []
    @IBOutlet var followIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "SearchUserCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        followIndicator.stopAnimating()
        if type == "FOLLOWERS"{
            
            // getFollowList()
        }
        else if type == "FOLLOWING"{
            //  getFollowingList()
        }
        followHeadingLbl.text = type
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.view.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isHidden = true
    }
    var type:String = ""
    @IBOutlet var tableView: UITableView!
    @IBOutlet var followHeadingLbl: UILabel!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getfinalArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchUserCell", for: indexPath as IndexPath) as! SearchUserCell
        
        cell.containerImage.layer.cornerRadius = 5.0
        cell.containerImage.layer.borderWidth=1.0
        cell.containerImage.layer.borderColor=UIColor.clear.cgColor
        
        cell.followBtn.isHidden = true
        print((getfinalArray.object(at: indexPath.row) as AnyObject))
        cell.userName.text = (getfinalArray.object(at: indexPath.row) as AnyObject).value(forKey: "user_name") as? String
        cell.name.text = (getfinalArray.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
        
        if let file = (getfinalArray.object(at: indexPath.row) as AnyObject).value(forKey: "profile_image") as? PFFile
        {
            cell.profileImage.file = file
            cell.profileImage.loadInBackground()
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // nestedprofileArray =  [[],[],"","","",""]
        userProfileClicked(tag: indexPath.row)
        
    }
    
    
    
//    var shownFollowIndexes : [IndexPath] = []
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
//    {
//        if (shownFollowIndexes.contains(indexPath) == false) {
//            shownFollowIndexes.append(indexPath)
//            let frame = CGRect.init(x: cell.frame.origin.x - 200, y: cell.frame.origin.y , width: cell.frame.width, height: cell.frame.height)
//            cell.frame = frame
//            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
//                
//                let frame = CGRect.init(x: cell.frame.origin.x + 200, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height)
//                cell.frame = frame
//            }) { finished in
//            }
//        }
//    }
    
    
    func userProfileClicked(tag: Int)
    {
        self.view.isUserInteractionEnabled = false
        let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
        
        let vc = storyboardObj.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        let id = (getfinalArray.object(at: tag) as AnyObject).value(forKey: "objectId") as? String
        print(id!)
        let objId = PFUser.current()?.objectId
        if !(id == objId){
            
            vc.userProfile = false
        }
        vc.friend = "FOLLOWING"
        
        let nameQuery : PFQuery = PFUser.query()!
        nameQuery.whereKey("objectId", equalTo: id!)
        nameQuery.findObjectsInBackground(block: { (objects, error) in
            print(objects!)
            
            for object in objects! {
                vc.userFromSearch = object as? PFUser
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        })
        
    }
}
