//
//  PublicSearchViewController.swift
//  Network42
//
//  Created by Apple3 on 15/10/18.
//  Copyright © 2018 42works. All rights reserved.
//

import UIKit
import Parse

class PublicSearchViewController: UIViewController {

    //MARK:- Variables
    var arrayPost = [Post]()
    lazy var postViewModel = PostViewModel()
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonSortBy: UIButton!
    @IBOutlet weak var collectionViewSearch: UICollectionView! {
        didSet {
            collectionViewSearch.register(UINib(nibName: "HomeProductCell", bundle: nil), forCellWithReuseIdentifier: "homeProductCell")
            collectionViewSearch.contentInset = UIEdgeInsetsMake(5, 0, 5, 0)
        }
    }
    
    //MRK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        postInit()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Private
    private func postInit() {
        postViewModel.endRefreshClausre = {
            
        }
        
        postViewModel.reloadTableViewClosure = {
            self.collectionViewSearch.reloadData()
        }
        self.sortPost(1)
    }
    
    private func sortPost(_ index: Int) {
        postViewModel.sortBy = index
        var title = ""
        switch index {
        case 1:
            title = "Sort by likes"
            break
        case 2:
            title = "Sort by comments"
            break
        default:
            title = "Sort by recent published"
            break
        }
        buttonSortBy.setTitle(title, for: .normal)
        postViewModel.getAllPosts()
    }
    
    //MARK:- IBAction
    @IBAction func searchByHashTag() {
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVC, animated: true)
    }
    
    @IBAction func searchByRecentPublish() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Sort by likes", style: .default , handler:{ (UIAlertAction)in
            self.sortPost(1)
        }))
        
        alert.addAction(UIAlertAction(title: "Sort by comments", style: .default , handler:{ (UIAlertAction)in
            self.sortPost(2)
        }))
        
        alert.addAction(UIAlertAction(title: "Sort by recent published", style: .default , handler:{ (UIAlertAction)in
            self.sortPost(3)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
}

extension PublicSearchViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let obj = postViewModel.array?[indexPath.row] {
            let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
            let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePostViewController")  as! CreatePostViewController
            
            vc.objectForEdit  = obj.object
            vc.edit  = true
            vc.showProfileLogo = true
            vc.postForEdit = obj.id
            vc.indexEdit = indexPath.row
            vc.hideEditingOptions = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension PublicSearchViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postViewModel.array?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeProductCell", for: indexPath)as! HomeProductCell
        if let file = postViewModel.array?[indexPath.row].postMedia?.first {
            cell.imageProduct.file = file
            cell.imageProduct.loadInBackground()
        } else {
            cell.imageProduct.image = #imageLiteral(resourceName: "dummyProfile")
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3 - 5, height: collectionView.frame.width/3 - 5)
    }
}
