//
//  RequestCollectionViewCell.swift
//  Network42
//
//  Created by 42works on 01/06/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI

@objc protocol RequestDelegate {
    func acceptClicked(tag:Int,acceptIndicator:UIActivityIndicatorView,button:UIButton)
    func deleteClicked(tag:Int,deleteIndicator:UIActivityIndicatorView,button:UIButton)
}



class RequestCollectionViewCell: UICollectionViewCell {
    @IBOutlet var userImage: PFImageView!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet var userName: UILabel!
    @IBOutlet var acceptBtn: UIButton!
    @IBOutlet var deleteBtn: UIButton!
    weak var delegate: RequestDelegate?

    @IBOutlet var deleteIndicator: UIActivityIndicatorView!
    @IBOutlet var acceptIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBAction func acceptAction(_ sender: Any) {
        self.delegate?.acceptClicked(tag: (sender as AnyObject).tag,acceptIndicator: self.acceptIndicator,button: sender as! UIButton)
    }
    @IBAction func deleteAction(_ sender: Any) {
        self.delegate?.deleteClicked(tag: (sender as AnyObject).tag,deleteIndicator: self.deleteIndicator,button: sender as! UIButton)
    }

}
