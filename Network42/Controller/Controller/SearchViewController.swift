//
//  SearchViewController.swift
//  Network42
//
//  Created by 42works on 06/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse



class SearchViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var segmentObj: UISegmentedControl!
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var tableViewObj: UITableView!
    
    var searchResultArray : [String] = []
    var hashTagsGlobalIdsArray : [String] = []
    var hashTagsCountArray : [String] = []
    
    var NameArray : NSMutableArray = []
    var idsArray : NSMutableArray = []
    var UserNameArray : NSMutableArray = []
    var profileTypeArray : NSMutableArray = []
    var ProfileImageArray : NSMutableArray = []
    var PFObjectArray : NSMutableArray = []
    
    var PFObjForDeleteArray : NSMutableArray = []
    
    var friendsIdArray : NSMutableArray = []
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var postTableview: UITableView!
    
    
    @IBOutlet var scrollView: UIScrollView!
    
    var textFieldInsideSearchBar =  UITextField()
    
    var statusArray : NSMutableArray = []
    var refreshControl: UIRefreshControl!
    
    var selectedSegment : Int = 0
    
    var pointNow = CGPoint.init(x: 0.0, y: 0.0)
//    var shownUserIndexes : [IndexPath] = []
//    var shownPostIndexes : [IndexPath] = []

    
    @IBOutlet var notFound: UILabel!
    
    var isVisible = false
    
    func start() {
        NotificationCenter.default.addObserver(self, selector: #selector(didShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func didShow()
    {
        isVisible = true
    }
    
    func didHide()
    {
        isVisible = false
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.start()
        tableViewObj.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "SearchUserCell")
        tableViewObj.rowHeight = UITableViewAutomaticDimension
        tableViewObj.estimatedRowHeight = 702
        
        
        postTableview.register(UINib(nibName: "PostNewCell", bundle: nil), forCellReuseIdentifier: "PostNewCell")
        postTableview.rowHeight = UITableViewAutomaticDimension
        postTableview.estimatedRowHeight = 702
        
        textFieldInsideSearchBar = (self.searchBar.value(forKey: "searchField") as? UITextField)!
        textFieldInsideSearchBar.textColor = UIColor.white
        textFieldInsideSearchBar.font = UIFont.init(name: "SanFranciscoText-Regular", size: 14)!
        textFieldInsideSearchBar.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
        
        let glassIconView = textFieldInsideSearchBar.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        glassIconView.tintColor = UIColor.white
        
        
        let clearButton = textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
        clearButton.tintColor = UIColor.white
        
        self.getIds()
        
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(SearchViewController.getIds), name: NSNotification.Name(rawValue: "updateSearchStatus"), object: nil)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        tableViewObj.addGestureRecognizer(tapGesture)
        
        tapGesture.delegate = self
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.hideKeyboard))
        tapGesture2.cancelsTouchesInView = false
        postTableview.addGestureRecognizer(tapGesture2)
        
        tapGesture2.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(SearchViewController.callApi), for: UIControlEvents.valueChanged)
        tableViewObj.addSubview(refreshControl)
        
        
        let attr = NSDictionary(object: UIFont.init(name: "SanFranciscoText-Semibold", size: 15)!, forKey: NSFontAttributeName as NSCopying)
        segmentObj.setTitleTextAttributes(attr as [NSObject : AnyObject] , for: .normal)
        
        self.callSearchApi(currentWord: "")
        self.callHashTagsApi(currentWord: "#")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        if selectedSegment == 0
        {
            self.updateLabel(userTable: true)
            scrollView.setContentOffset(CGPoint(x:0, y:0), animated: false)
        }else
        {
            self.updateLabel(userTable: false)
            let x =  self.view.frame.size.width
         scrollView.setContentOffset(CGPoint(x:x, y:0), animated: false)
        }
        
        self.tabBarController?.tabBar.isHidden = false
        
        
    }
    
    
    func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func getIds()
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
         PFObjForDeleteArray = NSMutableArray()
         friendsIdArray = NSMutableArray()
         statusArray = NSMutableArray()
        let query = PFQuery(className: "User_follow")
        let objId = PFUser.current()?.objectId
        query.limit = 100
        query.whereKey("follower_id", equalTo:objId!)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                let ids : NSMutableArray = []
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    
                    PFObjectArr.add(object)
                    
                    print(object)
                    ids.add((object.object(forKey: "followed_id") as! String))
                    if let status = object.object(forKey: "status") as? String
                    {
                        if status == "0"
                        {
                            self.statusArray.add("Cancel")
                        }else
                        {
                          self.statusArray.add((object.object(forKey: "followed_id") as! String))
                        }
                     
                    }
                }
                self.PFObjForDeleteArray = PFObjectArr
                self.friendsIdArray = ids
                self.tableViewObj.reloadData()
            }
            else {
                self.tableViewObj.reloadData()
                print(error!)
            }
        })
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        scrollView.isScrollEnabled = true
        selectedSegment = sender.selectedSegmentIndex
        switch sender.selectedSegmentIndex
        {
        case 0:
            print(sender.selectedSegmentIndex)
            scrollView.setContentOffset(CGPoint(x:0, y:0), animated: true)
            self.updateLabel(userTable: true)
           
        case 1:
             print(sender.selectedSegmentIndex)
             let x =  self.view.frame.size.width
             scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
            self.updateLabel(userTable: false)
        default:
            break; 
        }
         scrollView.isScrollEnabled = false
    }
    
    
    //MARK:- Gesture Delegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tableViewObj))! || (touch.view?.isDescendant(of: postTableview))!{
            return true
        }
        return true
    }
}

extension SearchViewController:UITableViewDelegate,UITableViewDataSource,followDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewObj
        {
            self.updateLabel(userTable: true)
            return NameArray.count
        }else
        {
            self.updateLabel(userTable: false)
            return searchResultArray.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewObj {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchUserCell", for: indexPath as IndexPath) as! SearchUserCell
                        
            cell.containerImage.layer.cornerRadius = 5.0
            cell.containerImage.layer.borderWidth=1.0
            cell.containerImage.layer.borderColor=UIColor.clear.cgColor
            
            
            cell.name.text = (NameArray.object(at: indexPath.row) as? String)?.trim()
            cell.userName.text = (UserNameArray.object(at: indexPath.row) as? String)?.trim()
            if let file = ProfileImageArray.object(at: indexPath.row) as? PFFile
            {
                cell.profileImage.file = file
                cell.profileImage.loadInBackground()
            }else
            {
                cell.profileImage.image = UIImage.init(named: "dummyProfile")
            }
            cell.followBtn.layer.borderWidth = 1.0
            cell.followBtn.layer.cornerRadius = 5.0
            
            
            if friendsIdArray.contains(idsArray.object(at: indexPath.row))
            {
                if statusArray.contains(idsArray.object(at: indexPath.row))
                {
                cell.followBtn.setTitle("FOLLOWING", for: .normal)
                }else
                {
                 cell.followBtn.setTitle("CANCEL", for: .normal)
                }
                
                cell.followBtn.backgroundColor = ConstantColors.themeColor
                cell.followBtn.setTitleColor(UIColor.white, for: .normal)
                cell.followBtn.layer.borderColor = ConstantColors.themeColor.cgColor
            }else
            {
                cell.followBtn.setTitle("FOLLOW", for: .normal)
                cell.followBtn.backgroundColor = UIColor.clear
                cell.followBtn.setTitleColor(ConstantColors.themeColor, for: .normal)
                cell.followBtn.layer.borderColor = ConstantColors.lightGrayColor.cgColor
            }
            
            
            let objId = PFUser.current()?.objectId
            if idsArray.object(at: indexPath.row) as? String == objId {
                cell.followBtn.isHidden = true
            }else
            {
                cell.followBtn.isHidden = false
            }
            cell.followBtn.tag = indexPath.row
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostNewCell", for: indexPath as IndexPath) as! PostNewCell
            
            cell.containerImage.layer.cornerRadius = 5.0
            cell.containerImage.layer.borderWidth=1.0
            cell.containerImage.layer.borderColor=UIColor.clear.cgColor
            
            cell.titleObj.text = searchResultArray[indexPath.row]
            
            if hashTagsCountArray.count > indexPath.row {
                cell.postCountObj.text = hashTagsCountArray[indexPath.row]
            } else {
                cell.postCountObj.text = ""
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isVisible {
            searchBar.resignFirstResponder()
            return            
        }
        
        if tableView == tableViewObj
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.fromSearch = true
            let objId = PFUser.current()?.objectId
            if idsArray.object(at: indexPath.row) as? String == objId {
                vc.userProfile = true
            }else
            {
                vc.userProfile = false
            }
            
            vc.refreshSearchBlocked = true
            vc.callback = { self.callSearchApi(currentWord: self.searchBar.text!) }
            
            if friendsIdArray.contains(idsArray.object(at: indexPath.row))
            {
                if statusArray.contains(idsArray.object(at: indexPath.row))
                {
                    vc.friend = "FOLLOWING"
                }else
                {
                    vc.friend = "Cancel"
                }
            }else
            {
                vc.friend = "FOLLOW"
            }

            let PFUserVar = PFObjectArray.object(at: indexPath.row) as! PFUser
            vc.userFromSearch = PFUserVar
            self.navigationController?.pushViewController(vc, animated: true)
        }else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            vc.secondTime = true
            let leftBrace = "("
            let rightBrace = ")"
            let count = "\(leftBrace)\(hashTagsCountArray[indexPath.row])\(rightBrace)"
            vc.titleSecond = "\(searchResultArray[indexPath.row])\(count)"
            print(searchResultArray[indexPath.row])
            vc.titleSecond = searchResultArray[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
//    {
//        if selectedSegment == 0
//        {
//            if (shownUserIndexes.contains(indexPath) == false) {
//                shownUserIndexes.append(indexPath)
//                let frame = CGRect.init(x: cell.frame.origin.x - 200, y: cell.frame.origin.y , width: cell.frame.width, height: cell.frame.height)
//                cell.frame = frame
//                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
//                    
//                    let frame = CGRect.init(x: cell.frame.origin.x + 200, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height)
//                    cell.frame = frame
//                }) { finished in
//                }
//            }
//        }else
//        {
//            if (shownPostIndexes.contains(indexPath) == false) {
//                shownPostIndexes.append(indexPath)
//                let frame = CGRect.init(x: cell.frame.origin.x - 200, y: cell.frame.origin.y , width: cell.frame.width, height: cell.frame.height)
//                cell.frame = frame
//                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
//                    
//                    let frame = CGRect.init(x: cell.frame.origin.x + 200, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height)
//                    cell.frame = frame
//                }) { finished in
//                }
//            }
//        }
//    }
    
    
    func followClickedDelegate(index:Int,cell:SearchUserCell) {
        print("==",index)
        
        print(self.profileTypeArray)
        print(self.profileTypeArray.object(at: index))
        
        if cell.followBtn.titleLabel?.text == "FOLLOW"
        {
            cell.indicator.startAnimating()
            cell.followBtn.setTitle("", for: .normal)
            cell.isUserInteractionEnabled = false
            let objId = PFUser.current()?.objectId
            let User_followClass = PFObject(className: "User_follow")
            User_followClass["follower_id"] = objId
            User_followClass["followed_id"] = idsArray.object(at: index) as! String
            User_followClass["parent_follower"] = PFUser.current()
            User_followClass["parent_following"] = PFObjectArray.object(at: index)
            if self.profileTypeArray.object(at: index) as! String == "1"
            {
               User_followClass["status"] = "1"
            }else
            {
             User_followClass["status"] = "0"
            }
            
            
            User_followClass.saveInBackground { (success, error) in
                
                cell.isUserInteractionEnabled = true
                if(success)
                {
                    print("user follow")
                    self.getIds()
                    
                     let user = self.PFObjectArray.object(at: index) as? PFUser
                     let currentName = PFUser.current()?.value(forKey: "name") as! String
                    
                    if self.profileTypeArray.object(at: index) as! String == "1"
                    {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                        let time = formatter.string(from: Date())
                        
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                        
                        self.saveToNotifications(from_id: objId!, to_id: user!.objectId!, notification_type: "follow_public_user", time: time, post_id: "", parent_from: PFUser.current()!, parent_to: user!)
                        
                        self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.startedfollowing)",type:"follow_public_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                    }else
                    {
                        self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.requestSent)",type:"follow_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                    }
                }
                else{
                    print("error",error!)
                    cell.indicator.stopAnimating()
                    cell.followBtn.setTitle("FOLLOW", for: .normal)
                    let message = (error?.localizedDescription)
                    self.showMessage(message!)
                    return
                }
            }
        }else
        {
             var alert = UIAlertController()
           if cell.followBtn.titleLabel?.text == "FOLLOWING"
           {
            alert = UIAlertController(title: "Are you sure you want to unfollow \(NameArray.object(at: index))?", message: "", preferredStyle: UIAlertControllerStyle.alert)
           }else
           {
             alert = UIAlertController(title: "Are you sure you want to cancel this request?", message: "", preferredStyle: UIAlertControllerStyle.alert)
            }
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                let clearButton = self.textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
                clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
                clearButton.tintColor = UIColor.white
            }))
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                   
                        cell.indicator.startAnimating()
                        cell.isUserInteractionEnabled = false
                        cell.followBtn.setTitle("", for: .normal)
                        
                        let loggedIn = PFUser.current()?.objectId
                        let query = PFQuery(className:"User_follow")
                        query.whereKey("follower_id", equalTo: loggedIn!)
                        query.whereKey("followed_id", equalTo: self.idsArray.object(at: index) as! String)
                        query.findObjectsInBackground(block: { (objects, error) in
                            if error == nil {
                                for objectt in objects! {
                                    
                                    
                                    objectt.deleteInBackground(block: { (status, error) in
                                        print(status)
                                        cell.isUserInteractionEnabled = true
                                        if status == true{
                                            if cell.followBtn.titleLabel?.text == "FOLLOWING"
                                            {
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                                            }
                                            self.getIds()
                                        }else
                                        {
                                            self.getIds()
                                        }
                                    })
                                }
                            }
                            else {
                                let message = (error?.localizedDescription)
                                self.showMessage(message!)
                                print(error!)
                            }
                        })
                   
                    
                    
                    let clearButton = self.textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
                    clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
                    clearButton.tintColor = UIColor.white
                    
                    
                case .cancel:
                    print("cancel")
                    let clearButton = self.textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
                    clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate), for: .normal)
                    clearButton.tintColor = UIColor.white
                    
                case .destructive:
                    print("destructive")
                }
            }))

            self.present(alert, animated: true, completion: nil)
        }
    }
}


extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
       //print(searchText)
//        guard searchText.count != 0 else {
//
//            DispatchQueue.main.async {
//                searchBar.resignFirstResponder()
//                self.NameArray.removeAllObjects()
//                self.tableViewObj.reloadData()
//
//                self.searchResultArray.removeAll()
//                self.postTableview.reloadData()
//
//                if self.selectedSegment == 0
//                {
//                    self.updateLabel(userTable: true)
//                }else
//                {
//                    self.updateLabel(userTable: false)
//                }
//            }
//            return
//        }
        
        
        self.callSearchApi(currentWord: searchText)
        self.callHashTagsApi(currentWord: "#"+searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        guard searchBar.text!.count != 0 else {
//            
//            DispatchQueue.main.async {
//                //searchBar.resignFirstResponder()
//                self.NameArray.removeAllObjects()
//                self.tableViewObj.reloadData()
//                
//                self.searchResultArray.removeAll()
//                self.postTableview.reloadData()
//            }
//            return
//        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func callApi()
    {
        if !(searchBar.text! == "")
        {
            self.getIds()
            self.callSearchApi(currentWord: searchBar.text!)
        }else
        {
         self.refreshControl.endRefreshing()
        }
    }
    
    func callSearchApi(currentWord:String)
    {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
//userQuery4.whereNotContainedIn("objectId", ActivityHome.getInstance().listBlockedUsers);
        indicator.startAnimating()
        
        var blockedUserArray : [String] = []
        if let blockedUsers = PFUser.current()?.value(forKey: "blockedUsers") as? [String]
        {
            blockedUserArray = blockedUsers
            print(blockedUserArray)
        }
        
        let myIdArray : [String] = [(PFUser.current()?.objectId)!]
        print(myIdArray)
        
        print(currentWord)
        let nameQuery : PFQuery = PFUser.query()!
        if currentWord != "" {
            nameQuery.whereKey("name", hasPrefix: currentWord)
        }
        
        nameQuery.whereKey("objectId", notContainedIn: blockedUserArray)
        nameQuery.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let nameQuerylowercased : PFQuery = PFUser.query()!
        nameQuerylowercased.whereKey("name", hasPrefix: currentWord.smallFirstLetter())
        nameQuerylowercased.whereKey("objectId", notContainedIn: blockedUserArray)
        nameQuerylowercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let nameQueryuppercased : PFQuery = PFUser.query()!
        nameQueryuppercased.whereKey("name", hasPrefix: currentWord.capitalizingFirstLetter())
        nameQueryuppercased.whereKey("objectId", notContainedIn: blockedUserArray)
        nameQueryuppercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        
        let userNameQuery : PFQuery = PFUser.query()!
        userNameQuery.whereKey("user_name", hasPrefix: currentWord)
        userNameQuery.whereKey("objectId", notContainedIn: blockedUserArray)
        userNameQuery.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let userNameQuerylowercased : PFQuery = PFUser.query()!
        userNameQuerylowercased.whereKey("user_name", hasPrefix: currentWord.smallFirstLetter())
        userNameQuerylowercased.whereKey("objectId", notContainedIn: blockedUserArray)
        userNameQuerylowercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let userNameQueryuppercased : PFQuery = PFUser.query()!
        userNameQueryuppercased.whereKey("user_name", hasPrefix: currentWord.capitalizingFirstLetter())
        userNameQueryuppercased.whereKey("objectId", notContainedIn: blockedUserArray)
        userNameQueryuppercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let query = PFQuery.orQuery(withSubqueries: [nameQuery,nameQuerylowercased,nameQueryuppercased,
                                                     userNameQuery,userNameQuerylowercased,userNameQueryuppercased])
        if currentWord != "" {
            query.order(byAscending: "name")
        }
        
        if currentWord == "" {
            query.limit = 10
        } else {
            query.limit = 100
        }
        
        query.findObjectsInBackground(block: { (objects, error) in
            self.indicator.stopAnimating()
             self.refreshControl.endRefreshing()
            if error == nil {
                
                let name : NSMutableArray = []
                let userName : NSMutableArray = []
                let ids : NSMutableArray = []
                let profile : NSMutableArray = []
                let profileType : NSMutableArray = []
                let PFObjectArr : NSMutableArray = []
                
                for object in objects! {
                    //print(object)
                    PFObjectArr.add(object)
                    name.add(object.object(forKey: "name") as? String ?? "")
                    userName.add(object.object(forKey: "user_name") as? String ?? "")
                    
                    if let type = object.object(forKey: "profile_type") as? String
                    {
                         profileType.add(type)
                    }else
                    {
                        profileType.add("1")
                    }
                    
                    ids.add(object.objectId!)
                    if let profileObj = object.object(forKey: "profile_image") as? PFFile
                    {
                        profile.add(profileObj)
                    }
                    else
                    {
                        profile.add("")
                    }
                }
                self.NameArray = name
                self.UserNameArray = userName
                self.idsArray = ids
                self.profileTypeArray = profileType
                self.ProfileImageArray = profile
                self.PFObjectArray = PFObjectArr
                self.tableViewObj.reloadData()
            }
            else {
                print(error!)
            }
        })
    }
    
    
    func callHashTagsApi(currentWord:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        print(currentWord)
        let query = PFQuery(className: "Hashtags")
        
        if currentWord == "" {
            query.limit = 10
        } else {
            query.limit = 100
            query.whereKey("name", hasPrefix: currentWord.lowercased())
        }
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                print(objects!)
                
                var name : [String] = []
                var ids : [String] = []
                for object in objects! {
                    print(object)
                    name.append(object.object(forKey: "name") as! String)
                    ids.append(object.objectId!)
                }
                self.searchResultArray = name
                self.hashTagsGlobalIdsArray = ids
                
                if self.searchResultArray.count == 0
                {
                    self.postTableview.reloadData()
                    return
                }
                
                self.hashTagsCountArray = []
                self.callHashTagPostApi(i: 0)
                
            }
            else {
                print(error!)
            }
        })
    }
    
    
    func callHashTagPostApi(i:Int)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "Hashtag_posts")
        
        query.whereKey("hashtag_id", equalTo: hashTagsGlobalIdsArray[i])
        query.limit = 100
        query.countObjectsInBackground { (count, error) in
            if error == nil {
                if count>1 {
                    self.hashTagsCountArray.append("\(count) Posts")
                } else {
                    self.hashTagsCountArray.append("\(count) Post")
                }
                
                if self.hashTagsGlobalIdsArray.count > i + 1 {
                    self.callHashTagPostApi(i: i + 1)
                } else {
                    self.postTableview.reloadData()
                }
            }
            else {
                print(error!)
                if self.hashTagsGlobalIdsArray.count > i + 1 {
                    self.callHashTagPostApi(i: i + 1)
                } else {
                    self.postTableview.reloadData()
                }
                
            }
        }
    }
}

extension SearchViewController: UIScrollViewDelegate
{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        let pageNo = round(scrollView.contentOffset.x / scrollView.frame.size.width);
//        print(scrollView.contentOffset.x)
//        if  pageNo < 1 {
//            segmentObj.selectedSegmentIndex = 0
//        }else
//        {
//            segmentObj.selectedSegmentIndex = 1
//        }

     }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let pageNo = round(scrollView.contentOffset.x / scrollView.frame.size.width);
//        print(scrollView.contentOffset.x)
//        if  pageNo < 1 {
//            segmentObj.selectedSegmentIndex = 0
//        }else
//        {
//            segmentObj.selectedSegmentIndex = 1
//        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let pageNo = round(scrollView.contentOffset.x / scrollView.frame.size.width);
//        print(scrollView.contentOffset.x)
//        if  pageNo < 1 {
//            segmentObj.selectedSegmentIndex = 0
//        }else
//        {
//            segmentObj.selectedSegmentIndex = 1
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        let pageNo = round(scrollView.contentOffset.x / scrollView.frame.size.width);
//        print(scrollView.contentOffset.x)
//        if  pageNo < 1 {
//            segmentObj.selectedSegmentIndex = 0
//        }else
//        {
//            segmentObj.selectedSegmentIndex = 1
//        }
    }
    
    func updateLabel(userTable:Bool)
    {
        if selectedSegment == 0 && NameArray.count == 0 {
            notFound.text = "No users found"
        } else if selectedSegment == 1 && searchResultArray.count == 0 {
                notFound.text = "No posts found"
        } else {
            notFound.text = ""
        }
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    
    func smallFirstLetter() -> String {
        let first = String(characters.prefix(1)).lowercased()
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func smalleFirstLetter() {
        self = self.smallFirstLetter()
    }
}

