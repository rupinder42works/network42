//
//  NotificationsViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 09/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
class NotificationsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,notificationDelegate{
//    let ids : NSMutableArray = []
//    let userNameArr : NSMutableArray = []
//    let timeArr:NSMutableArray = []
//    let imageArray:NSMutableArray = []
//    let otherPFUser:NSMutableArray = []
    @IBOutlet var tableView: UITableView!
    
//    let notificationDict : NSMutableDictionary = [:]
//    var typeOfNotification : NSMutableArray = []
    var followedStr:String = ""
    var pageCount = 0
    var pagingSpinner = UIActivityIndicatorView()
    var attributednameArray:NSMutableArray = []
    
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var collectionViewContainerHeight: NSLayoutConstraint!
    var PFObjectArray : NSMutableArray = []
    var refreshControl: UIRefreshControl!
    @IBOutlet var heightOfLabel: NSLayoutConstraint!
    
    @IBOutlet var pageControlObj: UIPageControl!
    @IBOutlet var heightOFCollection: NSLayoutConstraint!
    
    var notificationListing = [PFObject]()
    
    @IBOutlet var collectionContainer: UIView!
    @IBOutlet var topLayoutWithCollection: NSLayoutConstraint!
    @IBOutlet var topLayoutTV: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "cell1")
        
        tableView.estimatedRowHeight = 72
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.delegate = self
        tableView.dataSource = self
        notifyactivityIndicator.startAnimating()
        
        
        collectionViewObj.register(UINib(nibName: "RequestCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "RequestCollectionViewCell")
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(NotificationsViewController.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.tabBarController?.tabBar.isHidden = false
        
        self.topLayoutWithCollection.isActive = false
        self.topLayoutTV.isActive = true
        self.collectionContainer.isHidden = true
        
        self.getRequests()
        self.getAllNotificationAPI()
    }
    
    //MARK:-tableView
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath as IndexPath) as! NotificationTableViewCell
        // create attributed string
        
        cell.delegate = self
        cell.userProfileBtn.tag = indexPath.row
        
        cell.containerImage.layer.cornerRadius = 5.0
        cell.containerImage.layer.borderWidth=1.0
        cell.containerImage.layer.borderColor=UIColor.clear.cgColor
        
        if let parent_from = self.notificationListing[indexPath.row].value(forKey: "parent_from") as? PFUser{
            if let notifytype = self.notificationListing[indexPath.row].value(forKey: "notification_type") as? String{
                print(notifytype)
                if let usernameStr = parent_from.value(forKey: "name") as? String{
                    if notifytype == "like"{
                        followedStr = "\(usernameStr) liked your post. "
                    }
                    else if notifytype == "comment_tag"{
                        followedStr = "\(usernameStr) \(messages.userMentionedInComment)"
                    }
                    else if notifytype == "post_comment"{
                        followedStr = "\(usernameStr) \(messages.commentPost)"
                        
                    }
                        
                    else if notifytype == "user_tag"{
                        
                        followedStr = "\(usernameStr) \(messages.userTagged)"
                        
                    }
                    else if notifytype == "post_tag"{
                        
                        followedStr = "\(usernameStr) \(messages.userTagged)"
                        
                    }
                    else if notifytype == "follow_request_accept"{
                        followedStr = "\(usernameStr) \(messages.requestAccept)"
                        
                    }
                    else if notifytype == "follow_public_user"{
                        followedStr = "\(usernameStr) \(messages.startedfollowing)"
                        
                    }
                    
                    //complete name
                    let attrString: NSMutableAttributedString = NSMutableAttributedString(string: followedStr)
                    attrString.addAttribute(NSFontAttributeName, value: UIFont.init(name: "SanFranciscoText-Regular", size: 14)!, range: NSMakeRange(0, attrString.length))
                    
                    attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: NSMakeRange(0, attrString.length))
                    //set name colour
                    let range2 = (followedStr as NSString).range(of: usernameStr)
                    attrString.addAttribute(NSForegroundColorAttributeName, value:ConstantColors.themeColor, range: range2)
                    attrString.addAttribute(NSFontAttributeName, value:UIFont.init(name: "SanFranciscoText-Semibold", size: 14)!, range: range2)
                    
                    cell.notificationTitleLbl.attributedText = attrString
                }
            }
            
            
            if parent_from.object(forKey: "profile_image") != nil {
                let ImageFile = parent_from.object(forKey: "profile_image") as! PFFile
                cell.userImageView.file = ImageFile
                cell.userImageView.loadInBackground()
            } else {
                cell.userImageView.image = UIImage.init(named: "dummyProfile")
            }
            
        }
        
        
        if let  timeStampStr = self.notificationListing[indexPath.row].value(forKey: "time") as? String{
            
            print(timeStampStr)
            let newdateStr = timeStampStr.replacingOccurrences(of: "_", with: " ")
            print(newdateStr)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            let newdate = dateFormatter.date(from: newdateStr)
            let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
            print(timeStr)
            cell.timeLbl.text! = timeStr
        }
        
        
        cell.selectionStyle = .none
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let id = self.notificationListing[indexPath.row].value(forKey: "post_id") as? String
        
        if id == ""
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.fromSearch = true
            vc.userProfile = false
            vc.friend = "FOLLOWING"
             if let parent_from = self.notificationListing[indexPath.row].value(forKey: "parent_from") as? PFUser{
                vc.userFromSearch = parent_from
            }
            
            self.navigationController?.pushViewController(vc, animated: true)
            
            
        }else
        {
            let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
            let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
            vc.secondTime = true
            vc.titleSecond = "Post Detail"
            vc.showOnlySinglePost = true
            vc.postIdObject = id!
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func otherUserProfileClicked(tag: Int)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        vc.friend = "FOLLOWING"
        if let parent_from = self.notificationListing[tag].value(forKey: "parent_from") as? PFUser{
            vc.userFromSearch = parent_from
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBOutlet var notifyactivityIndicator: UIActivityIndicatorView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:-get Notification API
    
    func getAllNotificationAPI(){
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        self.tabBarController?.tabBar.isUserInteractionEnabled = false
        
        
        let objId = PFUser.current()?.objectId
        print(objId!)
        let query = PFQuery(className:"Notifications")
        query.whereKey("to_id", equalTo:objId!)
        query.limit =  100
        query.skip = self.notificationListing.count
        query.includeKey("parent_from")
        query.order(byDescending: "createdAt")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                for object in objects! {
                    if object.value(forKey: "notification_type") as? String ?? "" == "follow_user" || object.value(forKey: "notification_type") as? String ?? "" == "follow_request" {
                        continue
                    }
                    
                    self.notificationListing.append(object)
                }
                
                self.notifyactivityIndicator.stopAnimating()
//                self.notificationListing.append(contentsOf: objects!)
                print(self.notificationListing)
                self.tableView.reloadData()
                self.pagingSpinner.stopAnimating()
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
            }
            else {
                print(error!)
                self.tabBarController?.tabBar.isUserInteractionEnabled = true
                self.showMessage(NSLocalizedDescriptionKey)
                self.pagingSpinner.stopAnimating()
                self.notifyactivityIndicator.stopAnimating()
            }
        })
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        if scrollView == tableView {
            
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                pagingSpinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                pagingSpinner.startAnimating()
                pagingSpinner.color = UIColor(red: 22.0/255.0, green: 106.0/255.0, blue: 176.0/255.0, alpha: 1.0)
                pagingSpinner.hidesWhenStopped = true
                
                if self.notificationListing.count>5 {
                    
                    tableView.tableFooterView = pagingSpinner
                    self.getAllNotificationAPI()
                    
                }
            }
        }
    }
    //MARK:-Conver Timestamp
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        
        
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }

}







extension NotificationsViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,RequestDelegate
{
    func refresh()
    {
        self.getRequests()
        notificationListing.removeAll()
        self.tableView.reloadData()
        self.getAllNotificationAPI()
    }
    
    func getRequests()
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "User_follow")
        let objId = PFUser.current()?.objectId
        query.whereKey("followed_id", equalTo:objId!)
        query.whereKey("status", equalTo: "0")
        query.order(byDescending: "createdAt")
        query.includeKey("parent_follower")
        query.findObjectsInBackground(block: { (objects, error) in
            self.refreshControl.endRefreshing()
            if error == nil {
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    print(object)
                    PFObjectArr.add(object)
                }
                
                if PFObjectArr.count>0
                {
                    self.topLayoutWithCollection.isActive = true
                    self.collectionContainer.isHidden = false
                    self.topLayoutTV.isActive = false
                    self.heightOfLabel.constant = 41
                }else
                {
                    self.topLayoutWithCollection.isActive = false
                    self.collectionContainer.isHidden = true
                    self.topLayoutTV.isActive = true
                    self.heightOfLabel.constant = 0
                }
                

                self.PFObjectArray = PFObjectArr
                self.collectionViewObj.reloadData()
            }
            else {
                print(error!)
            }
        })
        
        
        
    }
    
    
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        pageControlObj.numberOfPages = PFObjectArray.count
        print("collection count=",PFObjectArray.count)
        return PFObjectArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestCollectionViewCell",
                                                      for: indexPath) as! RequestCollectionViewCell
        
        
        cell.delegate = self
        cell.acceptBtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        cell.nameLbl.textColor = ConstantColors.themeColor
        
        cell.acceptBtn.layer.borderWidth = 2.0
        cell.acceptBtn.layer.cornerRadius = 5.0
        cell.acceptBtn.setTitleColor(ConstantColors.themeColor, for: .normal)
        cell.acceptBtn.layer.borderColor = ConstantColors.lightGrayColor.cgColor
        
        cell.deleteBtn.layer.borderWidth = 2.0
        cell.deleteBtn.layer.cornerRadius = 5.0
        cell.deleteBtn.setTitleColor(ConstantColors.lightGrayColor, for: .normal)
        cell.deleteBtn.layer.borderColor = ConstantColors.lightGrayColor.cgColor
        
        
        let obect = (PFObjectArray.object(at: indexPath.row) as AnyObject).object(forKey: "parent_follower") as? PFObject
        
        if let profileImageFile = obect?.value(forKey: "profile_image") as? PFFile
        {
            cell.userImage.file = profileImageFile
            cell.userImage.loadInBackground()
        }else
        {
            cell.userImage.image = UIImage.init(named: "dummyProfile")
        }
        
        if let nameObj = obect?.value(forKey: "name") as? String
        {
            cell.nameLbl.text = nameObj
        }
        
        if let nameObj = obect?.value(forKey: "user_name") as? String
        {
            cell.userName.text = nameObj
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        vc.friend = "FOLLOW"
        vc.notificationStatus = true
        let user = (self.PFObjectArray.object(at: indexPath.row) as AnyObject).object(forKey: "parent_follower") as? PFUser
        vc.userFromSearch = user
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellSize:CGSize =  CGSize(width: self.view.bounds.width, height: 71)
        return cellSize
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        self.pageControlObj.currentPage = indexPath.row
    }
    
    
    
    
    
    
    
    
    
    
    func acceptClicked(tag: Int,acceptIndicator:UIActivityIndicatorView,button: UIButton) {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        print("accept")
        acceptIndicator.startAnimating()
        button.setTitle("", for: .normal)
        self.collectionViewObj.isUserInteractionEnabled = false
        let obect = PFObjectArray.object(at: tag) as! PFObject
        obect["status"] = "1"
        obect.saveInBackground { (success, error) in
            
            acceptIndicator.stopAnimating()
            button.setTitle("ACCEPT", for: .normal)
            self.collectionViewObj.isUserInteractionEnabled = true
            if(success)
            {
                print("user updated")
                
                
                let user = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "parent_follower") as? PFUser
                
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                self.saveToNotifications(from_id: PFUser.current()!.objectId!, to_id: user!.objectId!, notification_type: "follow_request_accept", time: formatter.string(from: Date()), post_id: "", parent_from: PFUser.current()!, parent_to: user!)
                
                
                self.acceptedPushNotification(userId: user!.objectId!)
                
                
                self.PFObjectArray.removeObject(at: tag)
                self.collectionViewObj.reloadData()
                
                
                if self.PFObjectArray.count>0
                {
                    self.topLayoutWithCollection.isActive = true
                    self.collectionContainer.isHidden = false
                    self.topLayoutTV.isActive = false
                    self.heightOfLabel.constant = 41
                }else
                {
                    self.topLayoutWithCollection.isActive = false
                    self.collectionContainer.isHidden = true
                    self.topLayoutTV.isActive = true
                    self.heightOfLabel.constant = 0
                }
                
                
            }
            else{
                print("error",error!)
                let message = (error?.localizedDescription)
                self.showMessage(message!)
                return
            }
        }
    }
    
    
    func acceptedPushNotification(userId:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        if !(userId == PFUser.current()!.objectId!)
        {
            //            let alertDictionary = ["body": "\(PFUser.current()!.username!) \(messages.requestAccept)",
            //                "pushType": "follow_request_accept",
            //                "userId": "\(PFUser.current()!.objectId!)",
            //                "userName": "\(PFUser.current()!.username!)"]
            //            let innerDictionary  = ["alert": alertDictionary,
            //                                    "badge": "1",
            //                                    "sound": "chime.aiff"
            //                ] as [String : Any]
            //            let mainDictionary = ["aps" : innerDictionary]
            //            let pushQuery = PFInstallation.query()
            //            pushQuery?.whereKey("userId", equalTo: userId)
            //            pushQuery?.whereKey("isActive", equalTo: "1")
            //            let push = PFPush()
            //            push.setQuery(pushQuery as! PFQuery<PFInstallation>?)
            //            push.setData(mainDictionary)
            //            push.sendInBackground()
            
            let currentName = PFUser.current()?.value(forKey: "name") as! String
            self.sendPushNotification(postId: "", userId: userId,settingType: "push_accept_follow_request",message:"\(currentName) \(messages.requestAccept)",type:"follow_request_accept",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
        }
    }
    
    
    
    func deleteClicked(tag: Int,deleteIndicator:UIActivityIndicatorView,button: UIButton) {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        deleteIndicator.startAnimating()
        button.setTitle("", for: .normal)
        self.collectionViewObj.isUserInteractionEnabled = false
        let obect = PFObjectArray.object(at: tag) as! PFObject
        obect.deleteInBackground(block: { (status, error) in
            print(status)
            deleteIndicator.stopAnimating()
            button.setTitle("DELETE", for: .normal)
            self.collectionViewObj.isUserInteractionEnabled = true
            if status == true{
                self.PFObjectArray.removeObject(at: tag)
                self.collectionViewObj.reloadData()
                
                if self.PFObjectArray.count>0
                {
                    self.topLayoutWithCollection.isActive = true
                    self.collectionContainer.isHidden = false
                    self.topLayoutTV.isActive = false
                    self.heightOfLabel.constant = 41
                }else
                {
                    self.topLayoutWithCollection.isActive = false
                    self.collectionContainer.isHidden = true
                    self.topLayoutTV.isActive = true
                    self.heightOfLabel.constant = 0
                }
            }else{
                print("error",error!)
                let message = (error?.localizedDescription)
                self.showMessage(message!)
                return
            }
            
        })
        
        
    }
    
}

