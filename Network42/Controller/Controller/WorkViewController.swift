//
//  WorkViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 02/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
var workSelectedStr:String = ""
class WorkViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    @IBOutlet var tableView: UITableView!
    var fromView:String = ""
    var responseCompArr:NSMutableArray = []
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var cityTf: MyTextField!
    var isSearchStart:Bool = false
    let newFilterArray:NSMutableArray = []
    //MARK:-Did load lyfecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        cityTf.borderStyle = UITextBorderStyle.none
        cityTf.layer.cornerRadius = 5
        cityTf.layer.borderWidth = 1
        cityTf.layer.borderColor = UIColor.clear.cgColor
        cityTf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        tableView.register(UINib(nibName: "searchCell", bundle: Bundle.main), forCellReuseIdentifier: "Cell")
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        
        cityTf.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBOutlet var cancelBtn: UIButton!
    @IBAction func cancelTapped(_ sender: Any) {
        if cancelBtn.titleLabel?.text == "Add"{
            workSelectedStr = cityTf.text!
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.activityIndicator.startAnimating()
        if fromView == "school"{
            cityTf.placeholder = "Search for a university or school"
            fetchSchoolList(getTextfeild: cityTf.text! as NSString)
            
        }
        else{
             cityTf.placeholder = "Search for a Company"
            fetchCompaniesAPI(getTextfeild: cityTf.text! as NSString)
        }
        
    }
    
    //MARK:Table View Delegates
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let arraycount = isSearchStart == true ? newFilterArray.count : responseCompArr.count
        return arraycount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let filteredArray = isSearchStart == true ? newFilterArray : responseCompArr
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            as! searchCell
        if let locationName = (filteredArray[indexPath.row] as AnyObject).value(forKey: "description") as? String {
            cell.label.text  = locationName
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let filteredArray = isSearchStart == true ? newFilterArray : responseCompArr
        
        if let cName = (filteredArray[indexPath.row] as AnyObject).value(forKey: "description") as? String {
            workSelectedStr = cName
            cityTf.resignFirstResponder()
            dismiss(animated: true, completion: nil)
        }
        
    }
    //MARK:-Textfeild to search
    func textFieldDidChange(_ textField: UITextField) {
        let searchedText = textField.text!
        if textField.text == "" {
            cancelBtn.setTitle("Cancel", for: .normal)
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
            fetchCompaniesAPI(getTextfeild: searchedText as NSString)
        }
        else{
            cancelBtn.setTitle("Add", for: .normal)
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            
        }
        
        workSelectedStr = textField.text!
        guard cityTf.text?.characters.count != 0 else {
            cityTf.resignFirstResponder()
            tableView.reloadData()
            return
        }
        print(responseCompArr)
        let pre:NSPredicate = NSPredicate(format:"SELF CONTAINS[c] %@",searchedText)
        print(pre)
        let array = responseCompArr.filtered(using: pre) as NSArray
        print(array)
        newFilterArray.removeAllObjects()
        if array.count > 0{
            for i in 0..<array.count {
                newFilterArray.add(array[i])
            }
            
        }
        else{
            newFilterArray.removeAllObjects()
        }
        print(newFilterArray)
        if (newFilterArray.count) > 0 {
            
            isSearchStart = true
            
            
        }
        else{
            isSearchStart = false
            newFilterArray.removeAllObjects()
            responseCompArr.removeAllObjects()
        }
        
        tableView.reloadData()
        
    }
    var getCompaniesArray:NSMutableArray = []
    //MARK:-Fetch Companies API
    func fetchCompaniesAPI(getTextfeild:NSString){
        
        let query = PFQuery(className: "Companies")
        //  print(PFUser.current()?.objectId!)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                self.getCompaniesArray.removeAllObjects()
                self.responseCompArr.removeAllObjects()
                print(objects?.count as Any)
                for object in objects! {
                    print(object)
                    self.activityIndicator.stopAnimating()
                 self.getCompaniesArray.add(object)
                    
                }
               let descriptor: NSSortDescriptor = NSSortDescriptor(key: "company_name", ascending: true)
                print(self.getCompaniesArray)
                let sortedResults = self.getCompaniesArray.sortedArray(using: [descriptor])
                print(sortedResults)
                let filterCompArray = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
                for object in filterCompArray {
                    if let compStr = (object as AnyObject).value(forKey: "company_name") as? String{
                        print(compStr)
                        if compStr != ""{
                            self.responseCompArr.add((object as AnyObject).value(forKey: "company_name")!)
                            
                        }
                    }
                }
                
                print(self.responseCompArr)
                self.tableView.reloadData()
                
                
            }
            else {
                print(error!)
            }
        })
        
        
    }
    //MARK:-fetch school API
    func fetchSchoolList(getTextfeild:NSString){
        let query = PFQuery(className: "schools")
        //  print(PFUser.current()?.objectId!)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                self.getCompaniesArray.removeAllObjects()
                self.responseCompArr.removeAllObjects()
                print(objects?.count as Any)
                for object in objects! {
                    print(object)
                    self.activityIndicator.stopAnimating()
                    self.getCompaniesArray.add(object)
                }
         
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "school_name", ascending: true)
            print(self.getCompaniesArray)
            let sortedResults = self.getCompaniesArray.sortedArray(using: [descriptor])
            print(sortedResults)
            let filterCompArray = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
            for object in filterCompArray {
                if let compStr = (object as AnyObject).value(forKey: "school_name") as? String{
                    print(compStr)
                    if compStr != ""{
                        self.responseCompArr.add((object as AnyObject).value(forKey: "school_name")!)
                        
                    }
                }
            }
            print(self.responseCompArr)
            self.tableView.reloadData()
        }
            else {
            print(error!)
        }
    })
    
}
func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    let newString = NSString(string: textField.text!).replacingCharacters(in: range, with: string)
    print(newString)
    if newString == ""
    {
        isSearchStart = false
        self.activityIndicator.stopAnimating()
        self.activityIndicator.isHidden = true
        tableView.reloadData()
    }
    
    return true
}
}

