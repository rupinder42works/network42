//
//  SettingTopTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 06/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class SettingTopTableViewCell: UITableViewCell {
    @IBOutlet var bottom_height: NSLayoutConstraint!
    @IBOutlet weak var headingTitle: UILabel!
    @IBOutlet weak var subheadingTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
