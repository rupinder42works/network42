//
//  SettingsTableViewCell.swift
//  Network42
//
//  Created by Anmol Rajdev on 21/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {
    @IBOutlet var bottom_height: NSLayoutConstraint!
    @IBOutlet weak var headingTitle: UILabel!
    @IBOutlet weak var subheadingTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet var contentInnerView: UIView!
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
