//
//  SectionTableViewCell.swift
//  Network42
//
//  Created by Anmol Rajdev on 22/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol rowHeaderActionDelegate {
    func deleterowActionClicked(cell:SectionTableViewCell,tag:Int)
}
class SectionTableViewCell: UITableViewCell {
     weak var delegate: rowHeaderActionDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet var actIndicator: UIActivityIndicatorView!
    @IBAction func deleteTapped(_ sender: Any) {
        self.delegate?.deleterowActionClicked(cell: self,tag:(sender as AnyObject).tag)
    }
    @IBOutlet var deleteView_width: NSLayoutConstraint!
    @IBOutlet var deleteBtn: UIButton!
    @IBOutlet var titleLbl: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
