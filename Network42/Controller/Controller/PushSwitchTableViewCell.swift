//
//  PushSwitchTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 05/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol SettingsDelegate {
    @objc optional func pushtoSettingsView(index:Int,cell:PushSwitchTableViewCell)
}
class PushSwitchTableViewCell: UITableViewCell {
    weak var delegate: SettingsDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var switchBtn: UISwitch!
    @IBAction func switchTapped(_ sender: Any) {
        
        let button: UISwitch = (sender as! UISwitch)
        self.delegate!.pushtoSettingsView!(index: button.tag,cell: self)

    }
    @IBOutlet var titleLbl: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
