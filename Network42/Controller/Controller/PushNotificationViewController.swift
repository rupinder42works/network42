//
//  PushNotificationViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 05/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import Firebase

class PushNotificationViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,SettingsDelegate{
    var subTitleArray:NSMutableArray = []
    var titleArray:NSMutableArray = []
    
    @IBOutlet var pushActivityIndicator: UIActivityIndicatorView!
    var setLikeFlag = "1"
    var setCommentflag = "1"
    var setShareflag = "1"
    var setTagComment = "1"
    var setTagPost = "1"
    
 //   @IBOutlet var activity: UIActivityIndicatorView!
  
    var setPushMsgSend = "1"
    var setSendFollowRequest = "1"
    var setAcceptFollowRequest = "1"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "SettingsTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
         tableView.register(UINib(nibName: "TitleHeaderTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell1")
        tableView.register(UINib(nibName: "PushSwitchTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell2")
       
        tableView.estimatedRowHeight = 72
        tableView.rowHeight = UITableViewAutomaticDimension
        subTitleArray = ["","","Likes your post","Comment on your post","Share your post","Tags you in a comment","Tags you in a post","","Sends you a message","","Sends you a follow Request","Accepts your follow Request"]
        titleArray = ["","Me","","","","","","Messaging","","My Network","","",""]
        
        self.pushActivityIndicator.startAnimating()
        self.getPushFlagsAPI()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    @IBOutlet var tableView: UITableView!
    override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBOutlet var backBtn: UIButton!
    @IBAction func backTapped(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 12
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 1 || indexPath.row == 7 || indexPath.row == 9{
//            return 44.0
//        }
//        else{
//            return UITableViewAutomaticDimension
//        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        if indexPath.row == 0{
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
                as! TitleHeaderTableViewCell
            cell1.selectionStyle = .none
           // cell1.headerTitle.text = hedingArray[indexPath.row] as? String
            return cell1
        }
        else if indexPath.row == 1 || indexPath.row == 7 || indexPath.row == 9 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                as! SettingsTableViewCell
            cell.selectionStyle = .none
//            cell.headingTitle.text = "Me"
            cell.subheadingTitle.text = "We will notify you when someone"
            cell.bottom_height.constant = 0
          
            cell.headingTitle.text = titleArray[indexPath.row] as? String
            
            cell.contentInnerView.layer.shadowColor = UIColor.darkGray.cgColor
            cell.contentInnerView.layer.shadowOffset = CGSize(width: 0, height: 1)
            cell.contentInnerView.layer.shadowOpacity = 1
            cell.contentInnerView.layer.shadowRadius = 1.0
            
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath)
                as! PushSwitchTableViewCell
            cell.delegate = self
            cell.titleLbl.text = subTitleArray[indexPath.row] as? String
            
            if cell.titleLbl.text == "Likes your post" {
                print(setLikeFlag)
                if setLikeFlag == "1" {
                    ////print("UISwitch is ON")
                     cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                    cell.switchBtn.isOn = false
                }
                
                
            }
            else if cell.titleLbl.text == "Comment on your post"{
                if setCommentflag == "1"  {
                    ////print("UISwitch is ON")
                    cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                    cell.switchBtn.isOn = false
                }
                
                
            }
            else if cell.titleLbl.text == "Share your post"{
                if setShareflag == "1"  {
                    ////print("UISwitch is ON")
                    cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                   cell.switchBtn.isOn = false
                }
            }
            else if cell.titleLbl.text == "Tags you in a comment"{
                if setTagComment == "1"  {
                    ////print("UISwitch is ON")
                    cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                   cell.switchBtn.isOn = false
                }
                
                
            }
            else if cell.titleLbl.text == "Tags you in a post"{
                if setTagPost == "1"  {
                    ////print("UISwitch is ON")
                    cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                    cell.switchBtn.isOn = false
                }
                
                
            }
            else if cell.titleLbl.text == "Sends you a message"{
                if setPushMsgSend == "1"  {
                    ////print("UISwitch is ON")
                    cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                    cell.switchBtn.isOn = false
                }
                
                
            }
            else if cell.titleLbl.text == "Sends you a follow Request"{
                if setSendFollowRequest == "1"  {
                    ////print("UISwitch is ON")
                    cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                    cell.switchBtn.isOn = false
                }
                
                
            }
            else if cell.titleLbl.text == "Accepts your follow Request"{
                if setAcceptFollowRequest == "1"  {
                    ////print("UISwitch is ON")
                    cell.switchBtn.isOn = true
                } else {
                    ////print("UISwitch is off")
                    cell.switchBtn.isOn = false
                }
                
                
            }
            return cell

            
        }
    }
    
    
    //    func didselectrow
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//            as! SettingsTableViewCell
//        cell.contentView.backgroundColor = UIColor.clear
//        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y: 10, width: self.view.frame.size.width, height: 100))
//        
//        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
//        whiteRoundedView.layer.masksToBounds = false
//        whiteRoundedView.layer.cornerRadius = 2.0
//        // whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
//        whiteRoundedView.layer.shadowOpacity = 0.2
//        
//        cell.contentView.addSubview(whiteRoundedView)
//        cell.contentView.sendSubview(toBack: whiteRoundedView)
//    }
    //MARK:-Put notification Flags
    func notificationAPI(cellAs:PushSwitchTableViewCell){
        print(setLikeFlag)
        cellAs.activityIndicator.startAnimating()
        cellAs.switchBtn.isHidden = true
        let objId = PFUser.current()?.objectId
        print(objId!)
        let query = PFQuery(className:"Settings")
        query.whereKey("user_id", equalTo:objId!)
         query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                if (objects?.count != 0) {
                    //company Exists already
                    print(objects as Any)
                    for compClass in objects! {
                        compClass["push_post_tag"] = self.setTagPost
                        compClass["push_message_send"] = self.setPushMsgSend
                        compClass["push_post_share"] = self.setShareflag
                        compClass["push_post_comment"] = self.setCommentflag
                        compClass["profile_visibility"] = "1"
                        compClass["push_post_like"] = self.setLikeFlag
                        compClass["push_accept_follow_request"] = self.setAcceptFollowRequest
                        compClass["push_send_follow_request"] = self.setSendFollowRequest
                        compClass["push_comment_tag"] = self.setTagComment
                        compClass.saveInBackground(block: { (status, error) in
                            print(status)
                            if status ==  true{
                                print(status)
                                cellAs.activityIndicator.stopAnimating()
                                cellAs.switchBtn.isHidden = false
                            }
                            
                        })
                    }
                }
                }
            else{
                let message = (error?.localizedDescription)
                print(message as Any)
            }
        })
    }
    //SwitchClicked Delegate
    func pushtoSettingsView(index:Int,cell:PushSwitchTableViewCell){
        
        if cell.titleLbl.text == "Likes your post"{
            if cell.switchBtn.isOn {
                ////print("UISwitch is ON")
                setLikeFlag = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
                setLikeFlag = "0"
            }
        }
        else if cell.titleLbl.text == "Comment on your post"{
            if cell.switchBtn.isOn  {
                ////print("UISwitch is ON")
                setCommentflag = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
               setCommentflag = "0"
            }
        }
        else if cell.titleLbl.text == "Share your post"{
            if cell.switchBtn.isOn  {
                ////print("UISwitch is ON")
                setShareflag = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
                setShareflag = "0"
            }
        }
        else if cell.titleLbl.text == "Tags you in a comment" {
            if cell.switchBtn.isOn  {
                ////print("UISwitch is ON")
                setTagComment = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
                setTagComment = "0"
            }
            //@ in comment
            updatePushOnFirebase(value: cell.switchBtn.isOn, index: 4)
        }
        else if cell.titleLbl.text == "Tags you in a post" {
            
            if cell.switchBtn.isOn  {
                ////print("UISwitch is ON")
                setTagPost = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
                 setTagPost = "0"
            }
            //@ and with user
            updatePushOnFirebase(value: cell.switchBtn.isOn, index: 5)
        }
        else if cell.titleLbl.text == "Sends you a message"{
            
            if cell.switchBtn.isOn {
                ////print("UISwitch is ON")
                setPushMsgSend = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
               setPushMsgSend = "0"
            }
            //Both chat
            updatePushOnFirebase(value: cell.switchBtn.isOn, index: 6)
            
        }
        else if cell.titleLbl.text == "Sends you a follow Request"{
            if cell.switchBtn.isOn {
                ////print("UISwitch is ON")
                setSendFollowRequest = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
                setSendFollowRequest = "0"
            }
            
        }
        else if cell.titleLbl.text == "Accepts your follow Request"{
            if cell.switchBtn.isOn {
                ////print("UISwitch is ON")
                setAcceptFollowRequest = "1"
            } else if !cell.switchBtn.isOn{
                ////print("UISwitch is off")
                setAcceptFollowRequest = "0"
            }
        }

        //ApiCallFor Settings
        
        self.notificationAPI(cellAs: cell)
    }

    //MARK:-Get Push FLags
    func getPushFlagsAPI(){
       
        let objId = PFUser.current()?.objectId
        print(objId!)
        let query = PFQuery(className:"Settings")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                self.pushActivityIndicator.stopAnimating()
                print(objects as Any)
                for object in objects! {
                    
                    print(object)
                    if let pushLikeFlag = object.value(forKey: "push_post_like") as? String{
                        self.setLikeFlag = pushLikeFlag
                    }
                    if let pushCommentFlag = object.value(forKey: "push_post_comment") as? String{
                        self.setCommentflag = pushCommentFlag
                    }
                    if let pushShareFlag = object.value(forKey: "push_post_share") as? String{
                        self.setShareflag = pushShareFlag
                    }
                    if let setTagCommentFlag = object.value(forKey: "push_comment_tag") as? String{
                        self.setTagComment = setTagCommentFlag
                    }
                    if let setTagPostFlag = object.value(forKey: "push_post_tag") as? String{
                        self.setTagPost = setTagPostFlag
                    }
                    if let setMsgFlag = object.value(forKey: "push_message_send") as? String{
                        self.setPushMsgSend = setMsgFlag
                    }
                    if let setFollowFlag = object.value(forKey: "push_send_follow_request") as? String{
                        self.setSendFollowRequest = setFollowFlag
                    }
                    if let setAcceptFlag = object.value(forKey: "push_accept_follow_request") as? String{
                        self.setAcceptFollowRequest = setAcceptFlag
                    }
                }
               
            self.tableView.reloadData()
            }
            else {
                print(error!)
                self.showMessage(NSLocalizedDescriptionKey)
                 self.pushActivityIndicator.stopAnimating()
            }
            
        })
    }
    
    
    //MARK:- Firebase
    
    func updatePushOnFirebase(value: Bool, index: Int) {
        let ref = Database.database().reference()
        
        let objectId = PFUser.current()?.objectId!
        var path = "chatPush/\(objectId ?? "")"
        
        switch index {
        case 4:
            path = "\(path)/tagComment"
            break
            
        case 5:
            path = "\(path)/withUser"
            break
            
        case 6:
            path = "\(path)/groupChat"
            break
            
        default:
            break
        }
        
        ref.child(path).setValue(value)
    }
}
