//
//  ChatListingViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 18/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

import Firebase
import Parse

class ChatListingViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    var tempNameArray:NSMutableArray = []
    var typeArray:NSMutableArray = []
    
    var subNameArray:NSMutableArray = []
    var messageArray:NSMutableArray = []
    var lastMsgArray:NSMutableArray = []
    var valueDict:NSMutableDictionary = [:]
    var groupmsgsDict:NSDictionary = [:]
    var msgtype: NSMutableArray = []
    var usernameArray: NSMutableArray = []
    
    var fileArray: NSMutableArray = []
    var timeArray: NSMutableArray = []
    
    var userIdArray:NSMutableArray = []
    var userImageArray:NSMutableArray = []
    var msgAs:NSMutableArray = []
    var wholeUserGroupDict:NSMutableDictionary = [:]
    
    var mainArray:[NSMutableDictionary] = []
    var singleChat:NSMutableDictionary = [:]
    var singleChatArray : [NSMutableDictionary] = []
    var groupChat:NSMutableDictionary = [:]
    var groupChatArray : [NSMutableDictionary] = []
    var singleUserCount = 0
    
    
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var noChatFoundLabel: UILabel!
    
    @IBOutlet var chatActivityIndicator: UIActivityIndicatorView!
    @IBAction func addMembers(_ sender: Any) {
        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
        let vc = chatSB.instantiateViewController(withIdentifier: "AddChatmembersViewController")  as! AddChatmembersViewController
        self.navigationController?.pushViewController(vc, animated: true)        
    }
    
    //MARK:- Private Methods
    private func isChatExist() {
        if mainArray.count == 0 {
            noChatFoundLabel.isHidden = false
        } else {
            noChatFoundLabel.isHidden = true
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ChatListCell", bundle: nil), forCellReuseIdentifier: "ChatListCell")
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        
        tempNameArray = ["Sachin","Mani","Sunny","Mamta"]
        subNameArray = ["Txt Hi","image","image","abc"]
        
        self.tabBarController?.tabBar.isHidden = true
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        chatActivityIndicator.startAnimating()
        self.fetchChatUsersListAPI()
        self.logController()
        
        self.bookMark()
    }
    
    
    
    
    func bookMark()
    {
        if  Reachability.isConnectedToNetwork() == false {
            
            self.showMessage(messages.internet)
            return
        }
        
        let DBref = Database.database().reference()
        let bookmarkRef =  DBref.child("bookmarkMessage").child((PFUser.current()?.objectId)!)
        bookmarkRef.updateChildValues(["bookmarkStatus": "1"]) { (error, Refrence) in
            print(error ?? "no")
            if error == nil{
                print(Refrence)
            }
        }
       
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        let nav = self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.mainArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(self.wholeUserGroupDict)
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatListCell", for: indexPath as IndexPath) as! ChatListCell
        
        
        cell.containerImage.layer.cornerRadius = 5.0
        cell.containerImage.layer.borderWidth=1.0
        cell.containerImage.layer.borderColor=UIColor.clear.cgColor
        
        cell.selectionStyle = .none
        
        cell.profileImage.image = UIImage.init(named: "temp")
        

        
        if let nameStr = self.mainArray[indexPath.row].value(forKey: "name") as? String{
            cell.name.text = nameStr
        }
        
        if let timeStr = self.mainArray[indexPath.row].value(forKey: "time") as? String{
            if timeStr == ""
            {
                 cell.time.text = timeStr
            }else
            {
            let newdateStr = timeStr.replacingOccurrences(of: "_", with: " ")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            let newdate = dateFormatter.date(from: newdateStr)
            let timeS  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
            cell.time.text = timeS
            }
        }
        
        
        if let user_image = self.mainArray[indexPath.row].value(forKey: "user_image") as? String{
            let url = URL(string: user_image)
            if !(url==nil)
            {
                cell.profileImage.af_setImage(withURL: url!)
            }else
            {
                cell.profileImage.image = UIImage.init(named: "temp")
            }
        }
        
        
        if let msgType = self.mainArray[indexPath.row].value(forKey: "msgType") as? String{
            if msgType == "media"
            {
                cell.userName.text = "Image"
            }else
            {
                if let message = self.mainArray[indexPath.row].value(forKey: "message") as? String{
                    cell.userName.text = message
                }
            }
            print(self.mainArray)
            
            if let status = self.mainArray[indexPath.row].value(forKey: "status") as? String{
                if status == "0"
                {
                    cell.userName.font = UIFont.init(name: "SanFranciscoTex-Bold", size: 12)
                }else
                {
                    cell.userName.font = UIFont.init(name: "SanFranciscoText-Regular", size: 12)
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let object = self.mainArray[indexPath.row].value(forKey: "objId") as? String{
            
            
            if object == ""
            {
                let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatViewController")  as! GroupchatViewController
                if let nameStr = self.mainArray[indexPath.row].value(forKey: "name") as? String{
                   globalgroupName.gName = nameStr
                }
                if let groupId = self.mainArray[indexPath.row].value(forKey: "groupId") as? String{
                    vc.senderIdToPass = groupId
                }
                vc.fromView = "ChatList"
                self.navigationController?.pushViewController(vc, animated: true)
            }else
            {
                
                    let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
                    let vc = chatSB.instantiateViewController(withIdentifier: "SingleChatViewController")  as! SingleChatViewController
                    if let nameStr = self.mainArray[indexPath.row].value(forKey: "name") as? String{
                     vc.titleStr = nameStr
                    }
                    vc.senderIdToPass = object
                    vc.pushUsrId = (PFUser.current()?.objectId)!
                    self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
//        if msgsCount > indexPath.row{
//           
//        }
//        else if let groupidObj = (self.groupmsgsDict.value(forKey: "groupId") as AnyObject).object(at: indexPath.row-msgsCount) as? String{
//            print(groupidObj)
//           
//        }
        
    }
    
    
    //MARK:-Fetch Users Chat Listing
    func fetchChatUsersListAPI(){
        
        if  Reachability.isConnectedToNetwork() == false {
            
            self.showMessage(messages.internet)
            return
        }
        chatActivityIndicator.stopAnimating()
        
        let DBref = Database.database().reference()
        DBref.child("messages").child((PFUser.current()?.objectId!)!).observe(.value, with: { snapshot in
            self.msgtype.removeAllObjects()
            self.msgAs.removeAllObjects()
            self.userIdArray.removeAllObjects()
            self.usernameArray.removeAllObjects()
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                //print(rest.key)
                print(snapshot.children.allObjects.count)
                self.singleUserCount = snapshot.children.allObjects.count
                
                var restDict = [String:Any]()
                for rest1 in rest.children.allObjects as! [DataSnapshot] {
                    restDict = rest1.value as! [String: Any]
                    //print(restDict)
                }
                
                self.getUserDetails(id: rest.key, msgType: (restDict as AnyObject).value(forKey: "msgType")! as! String, message: (restDict as AnyObject).value(forKey: "message")! as! String, name: (restDict as AnyObject).value(forKey: "name")! as! String, file: (restDict as AnyObject).value(forKey: "file")! as! String, time: (restDict as AnyObject).value(forKey: "time")! as! String,status: (restDict as AnyObject).value(forKey: "status") as? String ?? "1")
            }
            
            if snapshot.children.allObjects.count == 0
            {
              self.fetchGroupchatListAPI(singleArray: self.singleChatArray)
            }
        })
        
    }
    //MARK:-Get User Detail
    func getUserDetails(id:String,msgType:String,message:String,name:String,file:String,time:String,status:String){
        
        self.userImageArray.removeAllObjects()
        let DBref = Database.database().reference()
        DBref.child("users").child(id).observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String:Any] {
                //print(dict)
                if dict["user_img"] != nil {
                    
                    if let imageURLStr = (dict as AnyObject).value(forKey: "user_img") as? String{
                        self.singleChat.setValue(imageURLStr, forKey: "user_image")
                    }
                }else
                {
                    self.singleChat.setValue("", forKey: "user_image")
                }
                
                
                if let nameStr = (dict as AnyObject).value(forKey: "name") as? String{
                     self.singleChat.setValue(nameStr, forKey: "name")
                }
            }
            
            self.singleChat.setValue(msgType, forKey: "msgType")
            self.singleChat.setValue(message, forKey: "message")
            self.singleChat.setValue(file, forKey: "file")
            self.singleChat.setValue(time, forKey: "time")
            self.singleChat.setValue(id, forKey: "objId")
            self.singleChat.setValue(status, forKey: "status")
            
            self.singleChatArray.append(self.singleChat)
            self.singleChat = NSMutableDictionary()
            
            
            if self.singleUserCount == self.singleChatArray.count
            {
                self.fetchGroupchatListAPI(singleArray: self.singleChatArray)
            }
        })
        
    }
    
    
    //MARK:-Fetch Group Chat Listing
    func fetchGroupchatListAPI(singleArray:[NSMutableDictionary]){
       
        //print(self.groupmsgsDict)
        let DBref = Database.database().reference()
        DBref.child("users").child((PFUser.current()?.objectId!)!).observe(.value, with: { snapshot in
            //print(snapshot.key)
            
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
               // print(rest)
                print(snapshot.children.allObjects.count)
                 var restDict = [String:Any]()
                for rest1 in rest.children.allObjects as! [DataSnapshot] {
                    //print(rest1.key)
                     restDict = rest1.value as! [String: Any]
                   // print(restDict)
                    
                    
                    if restDict.count > 0 {
                        //print(restDict)
                        self.groupChat.setValue(rest1.key, forKey: "groupId")
                        self.groupChat.setValue((restDict as AnyObject).value(forKey: "group_name")! as? String, forKey: "name")
                        self.groupChat.setValue((restDict as AnyObject).value(forKey: "last_message")! as? String, forKey: "message")
                        self.groupChat.setValue((restDict as AnyObject).value(forKey: "time")! as? String, forKey: "time")
                        
                        if let status = (restDict as AnyObject).value(forKey: "status") as? String
                            {
                             self.groupChat.setValue(status, forKey: "status")
                            }else
                        {
                            self.groupChat.setValue("1", forKey: "status")
                        }
                        
                        self.groupChat.setValue((restDict as AnyObject).value(forKey: "msgType")! as? String, forKey: "msgType")
                        
                        
                        if restDict["group_img"] != nil {
                            
                            if let group_img_Str = (restDict as AnyObject).value(forKey: "group_img")! as? String{
                                self.groupChat.setValue(group_img_Str, forKey: "user_image")
                            }
                        }else
                        {
                            self.groupChat.setValue("", forKey: "user_image")
                        }
                        
                        self.groupChat.setValue("", forKey: "file")
                        self.groupChat.setValue("", forKey: "objId")
                        self.groupChatArray.append(self.groupChat)
                        self.groupChat = NSMutableDictionary()
                    }
                }
            }
            
            self.mainArray = singleArray + self.groupChatArray

            let sortedArray = self.mainArray.sorted{ ($0["time"] as? String)! > ($1["time"] as? String)! }
            
            self.mainArray = sortedArray
            
            print(self.mainArray)
            
            self.isChatExist()
            self.tableView.reloadData()
            self.groupChatArray.removeAll()
            self.singleChatArray.removeAll()
        })
    
    }
}


extension Dictionary {
    mutating func update(other:Dictionary) {
        for (key,value) in other {
            self.updateValue(value, forKey:key)
        }
    }
}


extension ChatListingViewController
{
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        
        
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }
}
