//
//  DateTableViewCell.swift
//  Demo app
//
//  Created by 42works on 09/08/16.
//  Copyright © 2016 42works. All rights reserved.
//

import UIKit

class DateTableViewCell: UITableViewCell {

    @IBOutlet var date: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
