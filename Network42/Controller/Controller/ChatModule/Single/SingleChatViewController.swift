//
//  SingleChatViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 22/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Firebase
import IQKeyboardManagerSwift
import Parse
import AlamofireImage
class SingleChatViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate{
    @IBOutlet var sendBtn: UIButton!
    var pushUsrId:String = ""
    var  imageToSend = UIImage()
    @IBOutlet var addToolbar: UIView!
    var titleStr:String = ""
    
    @IBOutlet var activityChat: UIActivityIndicatorView!
    @IBOutlet var topLbl: UILabel!
    var messageArray: NSMutableArray = []
    var imageArray: NSMutableArray = []
    @IBOutlet var chatTableView: UITableView!
    var responseChatArr: NSMutableArray = []
    @IBOutlet var textViewBottom: NSLayoutConstraint!
    
    @IBOutlet var sendActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var chatTextView: UITextView!
    var nameStrtoPass:String = ""
    var userNameStrtopass:String = ""
    var timeStrToPass:String = ""
    var senderIdToPass:String = ""
    var chatDictionary = [String: Any]()
    var uploadFlag:Bool = false
    var DBref = Database.database().reference()
    let DBReciverref = Database.database().reference()
    var msgTypeStr:String = ""
    var currentUserId:String = ""
    var senderArray:NSMutableArray = []
    
    var enableScroll = true
    
    var callLister = false
    
    @IBOutlet var chatViewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        chatTextView.textContainerInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        chatTableView.register(UINib(nibName: "ChatResponseTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatResponseTableViewCell")
        chatTableView.register(UINib(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatTableViewCell")
        chatTableView.register(UINib(nibName: "ChatImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatImageTableViewCell")
        chatTableView.register(UINib(nibName: "ChatImagereciverTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatImagereciverTableViewCell")
        
        chatTableView.rowHeight = UITableViewAutomaticDimension
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.estimatedRowHeight = 55
        automaticallyAdjustsScrollViewInsets = true
        self.tabBarController?.tabBar.isHidden = true
        
        chatViewContainer.layer.cornerRadius = 5.0
        chatViewContainer.layer.borderWidth=1.0
        chatViewContainer.layer.borderColor=UIColor.groupTableViewBackground.cgColor
        
        sendBtn.layer.cornerRadius = 5.0
        sendBtn.layer.borderWidth=1.0
        sendBtn.layer.borderColor=UIColor.clear.cgColor
        
        //set top label Title
        
        topLbl.text = titleStr
        
        //Keboard methods
        NotificationCenter.default.addObserver(self, selector: #selector(SingleChatViewController.keyboardWasShown(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(SingleChatViewController.keyboardWillHide(notification:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
        IQKeyboardManager.sharedManager().enable = false
        //print(responseChatArr)
        if responseChatArr.count > 0{
            if let nameStr = (responseChatArr[0] as AnyObject).value(forKey: "name") as? String{
                nameStrtoPass = nameStr
            }
            if let unameStr = (responseChatArr[0] as AnyObject).value(forKey: "user_name") as? String{
                userNameStrtopass = unameStr
            }
            if let timeStr = (responseChatArr[0] as AnyObject).value(forKey: "last_login") as? String{
                timeStrToPass = timeStr
            }
            if let senderIdStr = (responseChatArr[0] as AnyObject).value(forKey: "objectId") as? String{
                //print(senderIdStr)
                senderIdToPass = senderIdStr
                
            }
            self.activityChat.startAnimating()
            
        }
        else{
            self.activityChat.stopAnimating()
        }
        self.getValuefromFirDB()
        
        let offset = CGPoint(x: 0, y: (self.chatTableView.contentSize.height -  self.chatTableView.frame.size.height))
        self.chatTableView.setContentOffset(offset, animated: false)
        
        self.bookMarkForNotification()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        callLister = true
        self.logController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        callLister = false
        appDelegate.activeUser = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    //    func textViewDidChangeHeight(_ textView: textView, height: CGFloat) {
    //        UIView.animate(withDuration: 0.2) {
    //            self.view.layoutIfNeeded()
    //        }
    //    }
    
    @IBAction func back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:Tableview Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let msgtype = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "msgType") as? String{
            if msgtype == "media"{
                return 180
            }
            else{
                return UITableViewAutomaticDimension
            }
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // print(messageArray)
        let cell = UITableViewCell()
        if let msgtype = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "msgType") as? String{
            msgTypeStr = msgtype
        }
        if msgTypeStr == "text"{
            
            if let currentUser = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "userId") as? String{
                currentUserId = currentUser
            }
            
            //print(currentUserId)
            //print(PFUser.current()?.objectId as Any)
            let loggedinUserid = PFUser.current()?.objectId!
            //print(loggedinUserid!)
            if currentUserId == loggedinUserid! {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatResponseTableViewCell", for: indexPath as IndexPath) as! ChatResponseTableViewCell
                cell.selectionStyle = .none
                cell.chatlBl.text = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "message") as? String
                if let timeVal = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "time") as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                }
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(SingleChatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
                cell.chatlBl.text=(messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "message") as? String
                if let timeVal = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "time") as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                    cell.name_height.constant = 0
                    cell.msgTopmargin.constant = 0
                }
                cell.selectionStyle = .none
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(SingleChatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                return cell
            }
        }
        else{
            
            
            if let currentUser = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "userId") as? String{
                currentUserId = currentUser
            }
            if currentUserId == PFUser.current()?.objectId{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImagereciverTableViewCell", for: indexPath as IndexPath) as! ChatImagereciverTableViewCell
                cell.selectionStyle = .none
                
                if let link_image_url = (self.messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "file") as? String
                {
                    let url = URL(string: link_image_url)
                    if !(url==nil)
                    {
                        cell.chatImgView.af_setImage(withURL: url!)
                        uploadFlag = false
                        
                    }else
                    {
                        cell.chatImgView.image = nil
                        
                    }
                }
                
                
                if let timeVal = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "time") as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                    
                }
                cell.bkImage_Width.constant = cell.contentView.frame.size.width / 2 - 10
                cell.bkView_height.constant = cell.contentView.frame.size.height / 2 - 10
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(SingleChatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                return cell
            }
            else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageTableViewCell", for: indexPath as IndexPath) as! ChatImageTableViewCell
                
                if let link_image_url = (self.messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "file") as? String
                {
                    let url = URL(string: link_image_url)
                    if !(url==nil)
                    {
                        if let data = NSData(contentsOf: url! as URL) {
                            self.imageToSend = UIImage(data: data as Data)!
                        }
                        cell.chatImage.af_setImage(withURL: url!)
                        uploadFlag = false
                    }else
                    {
                        cell.chatImage.image = nil
                    }
                }
                if let timeVal = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "time") as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                    
                }
                cell.selectionStyle = .none
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(SingleChatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                return cell
            }
        }
        return cell
    }
    
    func longPress(sender: UITapGestureRecognizer) {
        let tag = sender.view!.tag
        print(tag)
        if let id = (self.messageArray.object(at: tag) as AnyObject).value(forKey: "id") as? String
        {
            self.alertMethod(messageId: id)
        }
    }
    
    
    func alertMethod(messageId:String)
    {
        let alert = UIAlertController(title: "Are you sure you want to delete this message?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.deleteChat(removeMessageId: messageId)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chatTextView.resignFirstResponder()
        
        if let msgtype = (messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "msgType") as? String{
            msgTypeStr = msgtype
        }
        if !(msgTypeStr == "text"){
            let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
            let vc = CreatePostSB.instantiateViewController(withIdentifier: "SingleMediaViewController")  as! SingleMediaViewController
            vc.fromChat = true
            
            if let link_image_url = (self.messageArray.object(at: indexPath.row) as AnyObject).value(forKey: "file") as? String
            {
                vc.urlStr = link_image_url
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else
        {
            
        }
    }
    
    
    @IBAction func sendMessage(_ sender: Any) {
        
        let trimmedString = chatTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        //print(trimmedString)
        
        chatTVFixedHeight.isActive = false
        chatTVHeight.isActive = true
        chatTextView.isScrollEnabled = false
        
        guard trimmedString.characters.count>0  else{
            return
        }
        self.activityChat.startAnimating()
        self.storeDatainDB()
        guard !(trimmedString == Constants.placeHolder) else{
            return
        }
        chatTextView.text=""
        chatTableView.scrollsToTop = true
        
    }
    //Mark:-Save database
    func storeDatainDB() {
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        
        timeStrToPass = formatter.string(from: Date())
        
        if uploadFlag ==  true{
            let imageData:NSData = self.imageToSend.mediumQualityJPEGNSData as NSData
            // Create a reference to the file you want to upload
            let riversRef = Storage.storage().reference().child("images/image.jpg")
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            // Upload the file to the path "images/rivers.jpg"
            let uploadTask = riversRef.putData(imageData as Data, metadata: metadata) { (metadata, error) in
                
            }
            uploadTask.observe(.success) { snapshot in
                // print(snapshot) // Upload completed successfully
                let downloadURL = snapshot.metadata?.downloadURL()
                print(downloadURL!)
                //self.storeUserProfileInDB(fileurlAs:downloadURL! as NSURL)
                let urlString: String = String(describing: downloadURL!)
                self.storeUserProfileInDB(fileurlAs:urlString,messageType: "media")
            }
        }
        else{
            self.storeUserProfileInDB(fileurlAs:"",messageType:"text")
        }
    }
    func storeUserProfileInDB(fileurlAs:String,messageType:String) {
        
        //print(nameStrtoPass)
        let DBref = Database.database().reference()
        let nameStr = PFUser.current()?.value(forKey: "name")
        // print(nameStr!)
        //Sender data
        
        var trimmedString = chatTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        //print(trimmedString)
        
        let chatDictionary = ["file":fileurlAs,"message":trimmedString,"msgType":messageType,"name":topLbl.text!,"time":timeStrToPass,"userId":(PFUser.current()?.objectId!)!,"username":(PFUser.current()?.username)!] as [String : Any]
        DBref.child("messages").child((PFUser.current()?.objectId!)!).child(senderIdToPass).childByAutoId().setValue(chatDictionary)
        { (error, DBref) in
            
            if !(error==nil)
            {
                print(error as! String)
            }
            else
            {
                self.activityChat.stopAnimating()
                self.sendBtn.isHidden = false
                self.sendActivityIndicator.stopAnimating()
                self.getValuefromFirDB()
            }
        }
        //Receiver data
        
        
        
        let chatDictionary1 = ["status":"0","file":fileurlAs,"message":trimmedString,"msgType":messageType,"name":nameStr!,"time":timeStrToPass,"userId":(PFUser.current()?.objectId!)!,"username":self.userNameStrtopass] as [String : Any]
        self.DBReciverref.child("messages").child(self.senderIdToPass).child((PFUser.current()?.objectId!)!).childByAutoId().setValue(chatDictionary1)
        { (error, DBReciverref) in
            
            if !(error==nil)
            {
                print(error as! String)
            }
            else{
                // print(DBReciverref)
                self.bookMark()
            }
        }
        
        
        if messageType == "media"
        {
            trimmedString = "Image"
        }
        self.messagePushNotification(userId:self.senderIdToPass,message:trimmedString)
        
    }
    
    
    
    
    func bookMark()
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        let DBref = Database.database().reference().child("bookmarkMessage").child(self.senderIdToPass)
        DBref.updateChildValues(["bookmarkStatus": "0"]) { (error, Refrence) in
            print(error ?? "no")
            if error == nil{
                print(Refrence)
            }
        }
    }
    
    func bookMarkForNotification()
    {
        if  Reachability.isConnectedToNetwork() == false {
            
            self.showMessage(messages.internet)
            return
        }
        
        let DBref = Database.database().reference()
        let bookmarkRef =  DBref.child("bookmarkMessage").child((PFUser.current()?.objectId)!)
        bookmarkRef.updateChildValues(["bookmarkStatus": "1"]) { (error, Refrence) in
            print(error ?? "no")
            if error == nil{
                print(Refrence)
            }
        }
        
    }
    
    //MARK:-Delete Chat
    func deleteChat(removeMessageId:String){
        self.enableScroll = false
        let DBref = Database.database().reference().child("messages").child((PFUser.current()?.objectId!)!).child(senderIdToPass).child(removeMessageId)
        DBref.removeValue(completionBlock:
            { (error, DBref) in
                
                if !(error==nil)
                {
                    print(error as! String)
                }
                else
                {
                    
                    self.activityChat.stopAnimating()
                    self.sendBtn.isHidden = false
                    self.sendActivityIndicator.stopAnimating()
                }
        })
    }
    
    
    
    
    
    
    
    
    
    @IBAction func cameraClicked(_ sender: Any) {
        
        cameraAuthorization()
        
    }
    
    func cameraAuthorization(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        //  imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func chooseSheet(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            
        }
        
    }
    func openGallary() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertClose(_ gesture: UITapGestureRecognizer) {
        //        if imageEditBtnFlag == true {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    func alertClose1(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadFlag = true
            imageToSend = pickedImage
            //imageArray.add(pickedImage)
            self.sendBtn.isHidden = true
            self.sendActivityIndicator.startAnimating()
            self.storeDatainDB()
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.textViewBottom.constant = keyboardFrame.size.height
            
        })
        let offset = CGPoint(x: 0, y: (self.chatTableView.contentSize.height -  keyboardFrame.size.height) + 200)
        self.chatTableView.setContentOffset(offset, animated: false)
        
    }
    
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.textViewBottom.constant = 0
        })
    }
    //MARK:-Get value from firebase
    func getValuefromFirDB(){
        
        if  Reachability.isConnectedToNetwork() == false {
            self.sendMessage(messages.internet)
            return
        }
        appDelegate.activeUser = senderIdToPass
        
        var key = ""
        let DBref = Database.database().reference()
        DBref.child("messages").child(pushUsrId).child(senderIdToPass).observe(.value, with: { snapshot in
            self.messageArray.removeAllObjects()
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
               // print(rest)
                //print(rest.value as Any)
                guard var restDict = rest.value as? [String: Any] else { continue }
                restDict["id"] = "\(rest.key)"
                key = "\(rest.key)"
                self.activityChat.stopAnimating()
                self.messageArray.add(restDict)
            }
            
            if self.callLister
            {
              if let userId = (self.messageArray.lastObject as AnyObject).value(forKey: "userId") as? String{
              if !(userId == PFUser.current()!.objectId!)
                {
                    let messageref =  DBref.child("messages").child(self.pushUsrId).child(self.senderIdToPass).child(key)
                    messageref.updateChildValues(["status": "1"]) { (error, Refrence) in
                        print(error ?? "no")
                        if error == nil{
                            print(Refrence)
                        }
                    }
                }
               }
            }
            
            
            
            // print("After Sending Message",self.messageArray.count)
            if self.messageArray.count == 0{
                self.activityChat.stopAnimating()
            }
            self.chatTableView.reloadData()
            
            if self.messageArray.count>0
            {
                if self.enableScroll
                {
                    let indexPathObj = NSIndexPath.init(row: self.messageArray.count-1, section: 0)
                    self.chatTableView.scrollToRow(at: indexPathObj as IndexPath, at: .bottom, animated: false)
                }
            }
            self.enableScroll = true
        })
        
    }
    
    
    
    
    @IBOutlet var chatTVFixedHeight: NSLayoutConstraint!
    @IBOutlet var chatTVHeight: NSLayoutConstraint!
    var dummy = 1
    func textViewDidChange(_ textView: UITextView) {
        print(textView.contentSize.height)
        if chatTextView.contentSize.height>50
        {
            chatTVFixedHeight.isActive = true
            chatTVHeight.isActive = false
            chatTextView.isScrollEnabled = true
            dummy = 0
            
        }else
        {
            chatTVFixedHeight.isActive = false
            chatTVHeight.isActive = true
            chatTextView.isScrollEnabled = false
            
            if dummy == 0
            {
                chatTextView.text = chatTextView.text + " "
            }
            dummy = 1
        }
    }
    
    
    
    
    //MARK:-SEND MESSAGE PUSH
    func messagePushNotification(userId:String,message:String)
    {
        print(userId)
        if !(userId == PFUser.current()!.objectId!)
        {
            //            let alertDictionary = ["body": "\(PFUser.current()!.username!) \(message)",
            //                "pushType": "\(PFUser.current()!.username!)",
            //                "postId":"",
            //                "reciverId": userId,
            //                "userId": "\(PFUser.current()!.objectId!)",
            //                "username": "\(PFUser.current()!.username!)",
            //                "MsgType":"SingleUserChat"]
            //            let innerDictionary  = ["alert": alertDictionary,
            //                                    "badge": "1",
            //                                    "sound": "chime.aiff"
            //                ] as [String : Any]
            //            let mainDictionary = ["aps" : innerDictionary]
            //
            //            print(mainDictionary)
            //
            //
            //            let pushQuery = PFInstallation.query()
            //            pushQuery?.whereKey("userId", equalTo: userId)
            //            pushQuery?.whereKey("isActive", equalTo: "1")
            //
            //            let push = PFPush()
            //            push.setQuery(pushQuery as! PFQuery<PFInstallation>?)
            //            push.setData(mainDictionary)
            //            push.sendInBackground()
            
            
            let currentName = PFUser.current()?.value(forKey: "name") as! String
            self.sendPushNotification(postId: "", userId: userId,settingType: "push_message_send",message:"\(currentName): \(message)",type:"chat_message",userIdArray:[],groupMembersId:"",MsgType:"SingleUserChat",reciverId:userId,username:"",groupName:"")
            
        }
    }
    
}
extension SingleChatViewController
{
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        
        
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }
}
