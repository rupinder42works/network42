//
//  ChatTableViewCell.swift
//  FuturEd
//
//  Created by Anmol Rajdev on 18/04/16.
//  Copyright © 2016 Anmol Rajdev. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet var name_height: NSLayoutConstraint!
    @IBOutlet var nameLbl: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var chatlBl: UILabel!
    @IBOutlet weak var bkView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet var msgTopmargin: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        bkView.layer.cornerRadius = 2.0
        bkView.clipsToBounds = true
        bkView.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
