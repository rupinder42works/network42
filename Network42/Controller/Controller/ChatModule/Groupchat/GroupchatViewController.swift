//
//  GroupchatViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 01/06/17.
//  Copyright © 2017 42works. All rights reserved.
//


import UIKit
import Firebase
import IQKeyboardManagerSwift
import Parse
class GroupchatViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate{
    
    @IBOutlet var muteBtn: UIButton!
    var currentUserIndex = -1
    var groupIdForPush:String = ""
    var  imageToSend = UIImage()
    @IBOutlet var addToolbar: UIView!
    var titleStr:String = ""
    var fileUrlStr:String = ""
    var fromView:String = ""
    @IBOutlet var activityChat: UIActivityIndicatorView!
    @IBOutlet var topLbl: UILabel!
    var messageArray: NSMutableArray = []
    var imageArray: NSMutableArray = []
    @IBOutlet var chatTableView: UITableView!
    
    @IBOutlet var textViewBottom: NSLayoutConstraint!
    
    @IBOutlet var sendActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var sendBtn: UIButton!
    @IBOutlet var chatTextView: UITextView!
    var nameStrtoPass:String = ""
    var userNameStrtopass:String = ""
    var timeStrToPass:String = ""
    var senderIdToPass:String = ""
    var chatDictionary = [String: Any]()
    var uploadFlag:Bool = false
    var DBref = Database.database().reference()
    let DBReciverref = Database.database().reference()
    var messageref = Database.database().reference()
    var msgTypeStr:String = ""
    var currentUserId:String = ""
    var senderArray:NSMutableArray = []
    var autoId:String = ""
    var useridArray:NSMutableArray = []
    var useridPushArray:NSMutableArray = []
    var userChildKeyArray:NSMutableArray = []
    var groupObjectsToPass:NSMutableArray = []
    var timeArray:NSMutableArray = []
    var usernameArray:NSMutableArray = []
    var userMsgArray:NSMutableArray = []
    var msgTypeArray:NSMutableArray = []
    var fileArray:NSMutableArray = []
    var preserveMessageDict:NSDictionary = [:]
    
    var user_statusArray:NSMutableArray = []
    var messageIDArray:NSMutableArray = []
    
    
    var enableScroll = true
    
    var callLister = false
    
    
    @IBOutlet var chatViewContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        chatTextView.textContainerInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        
        
        
        
        chatTableView.register(UINib(nibName: "ChatResponseTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatResponseTableViewCell")
        
        chatTableView.register(UINib(nibName: "ChatTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatTableViewCell")
        chatTableView.register(UINib(nibName: "ChatImageTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatImageTableViewCell")
        chatTableView.register(UINib(nibName: "ChatImagereciverTableViewCell", bundle: nil), forCellReuseIdentifier: "ChatImagereciverTableViewCell")
        
        chatTableView.rowHeight = UITableViewAutomaticDimension
        chatTableView.delegate = self
        chatTableView.dataSource = self
        chatTableView.estimatedRowHeight = 55
        
        
        automaticallyAdjustsScrollViewInsets = true
        
        //        chatTextView.translatesAutoresizingMaskIntoConstraints = false
        //        addToolbar.translatesAutoresizingMaskIntoConstraints = false
        //        addToolbar.addSubview(chatTextView)
        // chatTextView.delegate = self
        //  self.view.addSubview(addToolbar)
        
        self.tabBarController?.tabBar.isHidden = true
        
        chatViewContainer.layer.cornerRadius = 5.0
        chatViewContainer.layer.borderWidth=1.0
        chatViewContainer.layer.borderColor=UIColor.groupTableViewBackground.cgColor
        
        sendBtn.layer.cornerRadius = 5.0
        sendBtn.layer.borderWidth=1.0
        sendBtn.layer.borderColor=UIColor.clear.cgColor
        
        
        
        //Keboard methods
        NotificationCenter.default.addObserver(self, selector: #selector(SingleChatViewController.keyboardWasShown(notification:)), name:NSNotification.Name.UIKeyboardWillShow, object: nil);
        
        NotificationCenter.default.addObserver(self, selector: #selector(SingleChatViewController.keyboardWillHide(notification:)), name:NSNotification.Name.UIKeyboardWillHide, object: nil);
        
        IQKeyboardManager.sharedManager().enable = false
        //  print(responseChatArr)
        //  print(senderIdToPass)
        self.activityChat.startAnimating()
        if fromView == "AddMemebers"{ //to create group from Add Members
            //self.addDataintoGroupTableAPI()
        }
        else {              //To view group messages from chat listing
            self.getGroupMessagesLIst(objgetGroupChat: senderIdToPass)
            self.getGroupMembersUsersIdLIst(objgetGroupChat: senderIdToPass)
        }
        self.activityChat.stopAnimating()
        
        self.bookMarkForNotification()
    }
    
    
    func bookMarkForNotification()
    {
        if  Reachability.isConnectedToNetwork() == false {
            
            self.showMessage(messages.internet)
            return
        }
        
        let DBref = Database.database().reference()
        let bookmarkRef =  DBref.child("bookmarkMessage").child((PFUser.current()?.objectId)!)
        bookmarkRef.updateChildValues(["bookmarkStatus": "1"]) { (error, Refrence) in
            print(error ?? "no")
            if error == nil{
                print(Refrence)
            }
        }
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        callLister = true
        self.logController()
        //set top label Title
        topLbl.text =  globalgroupName.gName
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        callLister = false
        appDelegate.activeGroupId = ""
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    
    
    //    func textViewDidChangeHeight(_ textView: textView, height: CGFloat) {
    //        UIView.animate(withDuration: 0.2) {
    //            self.view.layoutIfNeeded()
    //        }
    //    }
    
    @IBAction func back(_ sender: Any) {
        let nav = self.navigationController?.popViewController(animated: true)
        
    }
    
    //MARK:Tableview Delegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let msgtype = (preserveMessageDict.object(forKey: "msgType") as AnyObject).object(at: indexPath.row) as? String{
            if msgtype == "media"{
                return 180
            }
            else{
                return UITableViewAutomaticDimension
            }
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.useridArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // print(preserveMessageDict)
        let cell = UITableViewCell()
        if let msgtype = (preserveMessageDict.object(forKey: "msgType") as AnyObject).object(at: indexPath.row) as? String{
            msgTypeStr = msgtype 
        }
        // print(msgTypeStr)
        if msgTypeStr == "text"{
            
            if let currentUser = (preserveMessageDict.object(forKey: "userId") as AnyObject).object(at: indexPath.row) as? String{
                currentUserId = currentUser
            }
            
            
            let loggedinUserid = PFUser.current()?.objectId!
            if currentUserId == loggedinUserid! {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatResponseTableViewCell", for: indexPath as IndexPath) as! ChatResponseTableViewCell
                cell.chatBKView_width.constant = cell.contentView.frame.size.width / 2
                
                cell.selectionStyle = .none
                cell.chatlBl.text = (preserveMessageDict.object(forKey: "message") as AnyObject).object(at: indexPath.row) as? String
                if let timeVal =  (preserveMessageDict.object(forKey: "time") as AnyObject).object(at: indexPath.row) as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                    
                }
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(GroupchatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                return cell
            }
            else{
                //Reciver code
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatTableViewCell", for: indexPath) as! ChatTableViewCell
                if let usrNameStr = (preserveMessageDict.object(forKey: "username") as AnyObject).object(at: indexPath.row) as? String{
                    cell.name_height.constant = 18
                    cell.nameLbl.text = usrNameStr
                }
                else{
                    cell.name_height.constant = 0
                }
                cell.chatlBl.text=(preserveMessageDict.object(forKey: "message") as AnyObject).object(at: indexPath.row) as? String
                if let timeVal = (preserveMessageDict.object(forKey: "time") as AnyObject).object(at: indexPath.row) as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                    
                }
                cell.selectionStyle = .none
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(GroupchatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                
                return cell
            }
            
        }
        else{
            //Image Sender
            //  print(preserveMessageDict)
            if let currentUser = (preserveMessageDict.object(forKey: "userId") as AnyObject).object(at: indexPath.row) as? String{
                currentUserId = currentUser
            }
            if currentUserId == PFUser.current()?.objectId{
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImagereciverTableViewCell", for: indexPath as IndexPath) as! ChatImagereciverTableViewCell
                cell.selectionStyle = .none
                
                if let link_image_url = (preserveMessageDict.object(forKey: "file") as AnyObject).object(at: indexPath.row) as? String
                {
                    let url = URL(string: link_image_url)
                    if !(url==nil)
                    {
                        
                        cell.chatImgView.af_setImage(withURL: url!)
                        uploadFlag = false
                        
                    }else
                    {
                        cell.chatImgView.image = nil
                        
                    }
                }
                if let timeVal = (preserveMessageDict.object(forKey: "time") as AnyObject).object(at: indexPath.row) as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                    
                }
                cell.bkImage_Width.constant = cell.contentView.frame.size.width / 2 - 10
                cell.bkView_height.constant = cell.contentView.frame.size.height / 2 - 10
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(GroupchatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                
                return cell
            }
            else{
                //Image Reciver
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatImageTableViewCell", for: indexPath as IndexPath) as! ChatImageTableViewCell
                if let usrNameStr = (preserveMessageDict.object(forKey: "username") as AnyObject).object(at: indexPath.row) as? String{
                    cell.name_height.constant = 18
                    cell.nameLbl.text = usrNameStr
                }
                else{
                    cell.name_height.constant = 0
                }
                
                if let link_image_url = (preserveMessageDict.object(forKey: "file") as AnyObject).object(at: indexPath.row) as? String
                {
                    let url = URL(string: link_image_url)
                    if !(url==nil)
                    {
                        
                        cell.chatImage.af_setImage(withURL: url!)
                        uploadFlag = false
                        
                    }else
                    {
                        cell.chatImage.image = nil
                        
                    }
                }
                if let timeVal = (preserveMessageDict.object(forKey: "time") as AnyObject).object(at: indexPath.row) as? String{
                    let newdateStr = timeVal.replacingOccurrences(of: "_", with: " ")
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                    let newdate = dateFormatter.date(from: newdateStr)
                    let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                    cell.timeLbl.text = timeStr
                    
                }
                
                
                let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(GroupchatViewController.longPress(sender:))) //Long
                cell.bkView.tag = indexPath.row
                longGesture.cancelsTouchesInView = true
                cell.bkView.addGestureRecognizer(longGesture)
                
                return cell
            }
            
        }
        
        return cell
        
    }
    
    
    
    
    
    func longPress(sender: UITapGestureRecognizer) {
        let tag = sender.view!.tag
        print(tag)
        var userStatus : [[String:Any]] = [["status": "0",
                                            "user_id": PFUser.current()?.objectId!
            ]]
        
        print(tag)
        
        if let user_status = (preserveMessageDict.object(forKey: "user_status") as AnyObject).object(at: tag) as? [[String:Any]]
        {
            userStatus.append(contentsOf: user_status)
        }
        
        if let id = (preserveMessageDict.object(forKey: "id") as AnyObject).object(at: tag) as? String
        {
            self.alertMethod(messageId: id,user_status: userStatus,tag: tag)
        }
    }
    
    
    func alertMethod(messageId:String,user_status:[[String:Any]],tag:Int)
    {
        let alert = UIAlertController(title: "Are you sure you want to delete this message?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.deleteChat(removeMessageId: messageId,user_status:user_status,tag: tag)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK:-Delete Chat
    func deleteChat(removeMessageId:String,user_status:[[String:Any]],tag:Int){
         self.enableScroll = false
        if useridArray.count-2 >= -1 && (useridArray.count == tag+1)
        {
           
            if useridArray.count-2 == -1
            {

                self.updateLastMessage(tag: tag,index: 0,userID: (PFUser.current()?.objectId!)!,groupIdStr: senderIdToPass,onlyOneMessage: true)
            }else
            {
                 self.updateLastMessage(tag: tag,index: useridArray.count-2,userID: (PFUser.current()?.objectId!)!,groupIdStr: senderIdToPass,onlyOneMessage: false)
            }
        }
        let messageref = Database.database().reference().child("groups").child(senderIdToPass).child("messages").child(removeMessageId)
        messageref.updateChildValues(["user_status": user_status]) { (error, Refrence) in
            print(error ?? "no")
            if error == nil{
               print(Refrence)
            }
        }
        
        
    }
    
    func updateLastMessage(tag:Int,index:Int,userID:String,groupIdStr:String,onlyOneMessage:Bool)
    {
            if let msgtype = (preserveMessageDict.object(forKey: "msgType") as AnyObject).object(at: index) as? String{
                if let lastMessage =  (preserveMessageDict.object(forKey: "message") as AnyObject).object(at: index) as? String
                {
                    if let timeVal = (preserveMessageDict.object(forKey: "time") as AnyObject).object(at: index) as? String{
                        let groupsDict = ["last_message":onlyOneMessage ? "" : lastMessage,"msgType":msgtype,"time":onlyOneMessage ? "" : timeVal,"status":"1"]
                        let groupref = Database.database().reference()
                        groupref.child("users").child(userID).child("groups").child(groupIdStr).updateChildValues(groupsDict) { (error, usrRefrence) in
                            if error == nil{
                                print(groupref)
                            }
                        }
                        
                    }
                }
            }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        chatTextView.resignFirstResponder()
        
        if let msgtype = (preserveMessageDict.object(forKey: "msgType") as AnyObject).object(at: indexPath.row) as? String{
            msgTypeStr = msgtype
        }
        
        if !(msgTypeStr == "text"){
            let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
            let vc = CreatePostSB.instantiateViewController(withIdentifier: "SingleMediaViewController")  as! SingleMediaViewController
            vc.fromChat = true
            
            if let link_image_url = (preserveMessageDict.object(forKey: "file") as AnyObject).object(at: indexPath.row) as? String
            {
                vc.urlStr = link_image_url
            }
            
            
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    @IBAction func sendMessage(_ sender: Any) {
        
        let trimmedString = chatTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        print(trimmedString)
        
        chatTVFixedHeight.isActive = false
        chatTVHeight.isActive = true
        chatTextView.isScrollEnabled = false
        
        guard trimmedString.characters.count>0  else{
            return
        }
        
        
        
        self.storeDatainDB()
        
        
        guard !(trimmedString == Constants.placeHolder) else{
            return
        }
        
        chatTextView.text=""
    }
    //Mark:-Save database
    func storeDatainDB(){
        self.messageArray.removeAllObjects()
        
        let trimmedString = chatTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        print(trimmedString)
        
        
        if trimmedString != ""{
            print("adsf")
            self.messageArray.add(trimmedString)
        }
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        
        timeStrToPass = formatter.string(from: Date())
        
        if uploadFlag ==  true{
            
            let imageData:NSData = self.imageToSend.mediumQualityJPEGNSData as NSData
            // Create a reference to the file you want to upload
            let riversRef = Storage.storage().reference().child("images/image.jpg")
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            // Upload the file to the path "images/rivers.jpg"
            let uploadTask = riversRef.putData(imageData as Data, metadata: metadata) { (metadata, error) in
                
            }
            uploadTask.observe(.success) { snapshot in
                //  print(snapshot) // Upload completed successfully
                let downloadURL = snapshot.metadata?.downloadURL()
                let urlString: String = String(describing: downloadURL!)
                self.storeUserProfileInDB(fileurlAs:urlString,messageType: "media")
                
                
            }
        }
        else{
            self.activityChat.startAnimating()
            self.storeUserProfileInDB(fileurlAs:"",messageType:"text")
        }
    }
    
    //Mark:--
    //MARK:-Store Database Messages
    var msgDict = [String: Any]()
    func storeUserProfileInDB(fileurlAs:String,messageType:String) {
        if fromView == "ChatList"{ //add Messages on Viewing
            messageref = DBref.child("groups").child(senderIdToPass)
        }
        print(messageref)
        //Sender data
        if fileurlAs == ""{
            for i in 0 ..< self.messageArray.count {
                let msgDict = (["file":fileurlAs,"message":self.messageArray[i],"userId":PFUser.current()?.objectId! as Any,"msgType":messageType,"username":PFUser.current()?.username! as Any,"time":timeStrToPass] as [String : Any] as NSDictionary) as! [String : Any]
                messageref.child("messages")
                    .childByAutoId().setValue(msgDict)
                print(msgDict)
            }
        }
        else{
            let msgDict = (["file":fileurlAs,"message":"","userId":PFUser.current()?.objectId! as Any,"msgType":messageType,"username":PFUser.current()?.username! as Any,"time":timeStrToPass] as [String : Any] as NSDictionary) as! [String : Any]
            messageref.child("messages")
                .childByAutoId().setValue(msgDict)
            self.sendBtn.isHidden = false
            self.sendActivityIndicator.stopAnimating()
            uploadFlag = false
            
        }
        var lastMessage = "Image"
        
        for i in 0..<self.useridPushArray.count
        {
            
            if self.messageArray.count>0
            {
                lastMessage = self.messageArray[0] as! String
            }
            self.groupUpdateWithType(userID: self.useridPushArray.object(at: i) as! String,groupIdStr: senderIdToPass,lastMessage: lastMessage,type: messageType)
        }
        
        sendGroupmessagePushNotification(userIdArray: self.useridPushArray, postId: "", pushType: "group_chat_message", message:lastMessage)
    }
    
    
    func groupUpdateWithType(userID:String,groupIdStr:String,lastMessage:String,type:String)
    {
        let groupsDict = ["last_message":lastMessage,"msgType":type,"time":timeStrToPass,"status":"0"]
        let groupref = Database.database().reference()
        groupref.child("users").child(userID).child("groups").child(groupIdStr).updateChildValues(groupsDict) { (error, usrRefrence) in
            if error == nil{
                print(groupref)
            }
        }
        
       print(self.useridPushArray)
        if !(userID==PFUser.current()?.objectId!)
            {
            if  Reachability.isConnectedToNetwork() == false {
                self.showMessage(messages.internet)
                return
            }
                print(userID)
            let DBref = Database.database().reference().child("bookmarkMessage").child(userID)
            DBref.updateChildValues(["bookmarkStatus": "0"]) { (error, Refrence) in
                print(error ?? "no")
                if error == nil{
                    print(Refrence)
                }
            }
    }
        
    }
    
    
    func groupUpdate(userID:String,groupIdStr:String,status:String)
    {
        let groupsDict = ["status":status]
        let groupref = Database.database().reference()
        groupref.child("users").child(userID).child("groups").child(groupIdStr).updateChildValues(groupsDict) { (error, usrRefrence) in
            if error == nil{
                print(groupref)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    @IBAction func cameraClicked(_ sender: Any) {
        
        cameraAuthorization()
        
    }
    
    func cameraAuthorization(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        //  imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func chooseSheet(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            
        }
        
    }
    func openGallary() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            let imagePicker = UIImagePickerController()
            // Add the actions
            imagePicker.delegate = self
            //            imageEditBtnFlag = false
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertClose(_ gesture: UITapGestureRecognizer) {
        //        if imageEditBtnFlag == true {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    func alertClose1(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadFlag = true
            imageToSend = pickedImage
            self.sendBtn.isHidden = true
            self.sendActivityIndicator.startAnimating()
            self.storeDatainDB()
            
            //imageArray.add(pickedImage)
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func keyboardWasShown(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.textViewBottom.constant = keyboardFrame.size.height
            
        })
        let offset = CGPoint(x: 0, y: (self.chatTableView.contentSize.height -  keyboardFrame.size.height) + 200)
        self.chatTableView.setContentOffset(offset, animated: false)
    }
    
    
    func keyboardWillHide(notification: NSNotification) {
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            self.textViewBottom.constant = 0
        })
    }
    
    
    //MARK:-Get Group Listing
    func getGroupMessagesLIst(objgetGroupChat:String){
        
        if  Reachability.isConnectedToNetwork() == false {
            
            self.sendMessage(messages.internet)
            
            return
        }
        
        
        groupIdForPush = objgetGroupChat
        appDelegate.activeGroupId = groupIdForPush
        let DBref = Database.database().reference()
        DBref.child("groups").child(objgetGroupChat).child("messages").observe(.value, with: { snapshot in
            // print(snapshot)
            // print(DBref)
            
            self.useridArray.removeAllObjects()
            self.messageArray.removeAllObjects()
            self.userMsgArray.removeAllObjects()
            self.msgTypeArray.removeAllObjects()
            self.fileArray.removeAllObjects()
            self.timeArray.removeAllObjects()
            self.usernameArray.removeAllObjects()
            self.user_statusArray.removeAllObjects()
            self.messageIDArray.removeAllObjects()
            
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                print(rest)
                
                guard let restDict = rest.value as? [String: Any] else { continue }
                // self.activityChat.stopAnimating()
                print("dict as",restDict)
                
                var skip = false
                if let user_status  = (restDict as AnyObject).value(forKey: "user_status") as? [[String:Any]]
                {
                    for object in user_status
                    {
                        if object["user_id"] as? String == PFUser.current()?.objectId!
                        {
                            skip = true
                            break
                        }
                    }
                }
                if skip == false
                {
                    self.useridArray.add((restDict as AnyObject).value(forKey: "userId")! as! String)
                    self.userMsgArray.add((restDict as AnyObject).value(forKey: "message")! as! String)
                    self.msgTypeArray.add((restDict as AnyObject).value(forKey: "msgType")! as! String)
                    self.fileArray.add((restDict as AnyObject).value(forKey: "file")! as! String)
                    self.timeArray.add((restDict as AnyObject).value(forKey: "time")! as! String)
                    self.usernameArray.add((restDict as AnyObject).value(forKey: "username")! as! String)
                    self.messageIDArray.add(rest.key)
                    if let user_status  = (restDict as AnyObject).value(forKey: "user_status") as? [[String:Any]]
                    {
                        self.user_statusArray.add(user_status)
                    }else
                    {
                        self.user_statusArray.add("")
                    }
                }
            }
            print("After Sending Message",self.usernameArray)
            if self.fromView == "ChatList"{
                self.messageref = DBref
            }
            self.preserveMessageDict = ["userId":self.useridArray,"message":self.userMsgArray,"msgType":self.msgTypeArray,"file":self.fileArray,"time":self.timeArray,"username":self.usernameArray,"user_status":self.user_statusArray,"id":self.messageIDArray]
            self.useridArray = self.preserveMessageDict.value(forKey: "userId") as! NSMutableArray
            //   print(self.preserveMessageDict )
            self.activityChat.stopAnimating()
            
            
            if self.callLister
            {
              self.groupUpdate(userID: PFUser.current()!.objectId!, groupIdStr: self.senderIdToPass, status: "1")
            }
            
            self.chatTableView.reloadData()
            
            if self.useridArray.count>0
            {
                if self.enableScroll
                {
                 let indexPathObj = NSIndexPath.init(row: self.useridArray.count-1, section: 0)
                 self.chatTableView.scrollToRow(at: indexPathObj as IndexPath, at: .bottom, animated: false)
                }
            }
            self.enableScroll = true
            if self.messageArray.count == 0{
                self.activityChat.stopAnimating()
            }
            
            
        })
        
        
    }
    //MARK:-Get Group Listing
    func getGroupMembersUsersIdLIst(objgetGroupChat:String){
        let DBref = Database.database().reference()
        DBref.child("groups").child(objgetGroupChat).child("users").observe(.value, with: { snapshot in
            // print(snapshot)
            print(DBref)
            
            self.useridPushArray.removeAllObjects()
            self.groupObjectsToPass.removeAllObjects()
            self.userChildKeyArray.removeAllObjects()
            var index = 0
            for rest in snapshot.children.allObjects as! [DataSnapshot] {
                print(rest.key)
                
                guard let restDict = rest.value as? [String: Any] else { continue }
                // self.activityChat.stopAnimating()
                print("dict as",restDict)
                self.groupObjectsToPass.add(restDict)
                self.useridPushArray.add((restDict as AnyObject).value(forKey: "user_id")! as! String)
                self.userChildKeyArray.add(rest.key)
                
                if let userID = (restDict as AnyObject).value(forKey: "user_id") as? String, userID == PFUser.current()?.objectId! {
                    self.currentUserIndex = index
                    if let pushEnable = (restDict as AnyObject).value(forKey: "notification") as? Bool, pushEnable == false {
                        self.muteBtn.isSelected = true
                    } else {
                        self.muteBtn.isSelected = false
                    }
                }
                
                index += 1
                
            }
            print("After Sending Message",self.useridPushArray)
        })
    }
    //MARK:-GROUP SETTINGS TAPPED
    @IBAction func groupSettingsTapped(_ sender: Any) {
        if groupObjectsToPass.count > 0{
            let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
            let vc = chatSB.instantiateViewController(withIdentifier: "GroupchatSettingViewController")  as! GroupchatSettingViewController
            vc.groupId =  self.senderIdToPass
            vc.groupnameStr = self.topLbl.text!
            vc.fileUrl = fileUrlStr
            vc.groupUserIdArray = self.useridPushArray
            vc.userChildIdArray = self.userChildKeyArray
            vc.groupMembersArray = self.groupObjectsToPass
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    //MARK:- IBAction
    @IBAction func muteTap(_ sender: UIButton) {
        
        let keyToUpdate = userChildKeyArray[currentUserIndex] as? String ?? ""
        DBref.child("groups").child(senderIdToPass).child("users").child(keyToUpdate).updateChildValues(["notification": sender.isSelected])
        
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBOutlet var chatTVFixedHeight: NSLayoutConstraint!
    @IBOutlet var chatTVHeight: NSLayoutConstraint!
    var dummy = 1
    func textViewDidChange(_ textView: UITextView) {
        print(textView.contentSize.height)
        if chatTextView.contentSize.height>50
        {
            chatTVFixedHeight.isActive = true
            chatTVHeight.isActive = false
            chatTextView.isScrollEnabled = true
            dummy = 0
            
        }else
        {
            chatTVFixedHeight.isActive = false
            chatTVHeight.isActive = true
            chatTextView.isScrollEnabled = false
            
            if dummy == 0
            {
                chatTextView.text = chatTextView.text + " "
            }
            dummy = 1
        }
    }
    
    
    
    
    //MARK:-ON Message Sending in group
    func sendGroupmessagePushNotification(userIdArray:NSMutableArray,postId:String,pushType:String,message:String)
    {
        print(PFUser.current()?.objectId! as Any)
        
        userIdArray.remove(PFUser.current()?.objectId!)
        
        
        let arrayOfUsers = NSMutableArray()
        var count = 0
        for (_, element) in userIdArray.enumerated() {
            
            let childPath = "chatPush/\(element)/groupChat"
            
            Database.database().reference().child(childPath).observeSingleEvent(of: .value, with: { (snapshot) in
                
                print("...., \(element)")
                
                if let pushEnabled = snapshot.value as? Bool {
                    if pushEnabled {
                        arrayOfUsers.add(element)
                    }
                } else {
                    arrayOfUsers.add(element)
                }
                
                count += 1
                
                if count == userIdArray.count {
                    
                    //Push Check completed
                    let newArr = NSMutableArray()
                    Database.database().reference().child("groups").child(self.senderIdToPass).child("users").observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        for userSnapshot in snapshot.children.allObjects as! [DataSnapshot] {
                            
                            if let user = userSnapshot.value as? NSDictionary {
                                if let userID = user.value(forKey: "user_id") as? String, arrayOfUsers.contains(userID) {
                                    if let pushEnable = user.value(forKey: "notification") as? Bool {
                                        if pushEnable {
                                            newArr.add(userID)
                                        }
                                    } else {
                                        //Notification is on
                                        newArr.add(userID)
                                    }
                                }
                            }
                        }
                        
                        
                        let currentName = PFUser.current()?.value(forKey: "name") as! String
                        
                        self.sendPushNotification(postId: "", userId: "",settingType: "push_message_send",message:currentName+" @\(self.topLbl.text!): "+message,type:pushType,userIdArray:newArr,groupMembersId:self.groupIdForPush,MsgType:"GroupChat",reciverId:"",username:"\(self.topLbl.text!)",groupName:self.topLbl.text!)
                    })
                }
            })
        }
        
    }
}


extension GroupchatViewController
{
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        
        
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1) {
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1) {
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1) {
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1) {
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1) {
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1) {
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }
}
