//
//  GroupMembersTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 23/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class GroupMembersTableViewCell: UITableViewCell {

  @IBOutlet var removeBtn: UIButton!
    @IBAction func removeTapped(_ sender: Any) {
    }
   
    @IBAction func GroupAdminTapped(_ sender: Any) {
    }
    @IBOutlet var groupAdminBtn: UIButton!
    @IBOutlet var bottomShadowView: UIView!
    @IBOutlet var shadowView_bottomHeight: NSLayoutConstraint!
    @IBOutlet var memberBgView: UIView!
    
    @IBOutlet var extraBottomSpace: NSLayoutConstraint!
    @IBOutlet var extraTopSpace: NSLayoutConstraint!
    @IBOutlet var removeBtn_height: NSLayoutConstraint!
    @IBOutlet var members_Height: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
         removeBtn.layer.borderColor = ConstantColors.themeColor.cgColor
        removeBtn.layer.borderWidth = 2
        removeBtn.layer.cornerRadius = 2.0
        removeBtn.clipsToBounds = true
        removeBtn.layer.masksToBounds = true
       
        
        groupAdminBtn.layer.borderColor = ConstantColors.themeColor.cgColor
        groupAdminBtn.layer.borderWidth = 2
        groupAdminBtn.layer.cornerRadius = 2.0
        groupAdminBtn.clipsToBounds = true
        groupAdminBtn.layer.masksToBounds = true
        // Initialization code
    }
    @IBOutlet var nameLBl: UILabel!

    @IBOutlet var userImage: PFImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
