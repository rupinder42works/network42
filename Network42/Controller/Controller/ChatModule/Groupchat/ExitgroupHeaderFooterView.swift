//
//  ExitgroupHeaderFooterView.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 23/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol FooterDelegates {
    func addPartyClicked(cell:ExitgroupHeaderFooterView)
  @objc optional  func exitGroup(cell:ExitgroupHeaderFooterView)
    
    
}
class ExitgroupHeaderFooterView: UITableViewHeaderFooterView {

    weak var delegate: FooterDelegates?
    @IBOutlet var exitBtn: UIButton!
    @IBAction func exitGroupTapped(_ sender: Any) {
        
        self.delegate?.exitGroup!(cell:self)
    }
    @IBOutlet var addparticipantBtn: UIButton!
    @IBAction func addParicipant(_ sender: Any) {
        self.delegate?.addPartyClicked(cell:self)
        
    }
    @IBOutlet var addPart_heght: NSLayoutConstraint!
     @IBOutlet var exitView2: UIView!
     @IBOutlet var exitView1: UIView!
 

}
