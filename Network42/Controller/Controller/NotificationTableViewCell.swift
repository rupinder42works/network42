//
//  NotificationTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 09/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI


@objc protocol notificationDelegate {
   
    func otherUserProfileClicked(tag:Int)
}


class NotificationTableViewCell: UITableViewCell {
    @IBOutlet var userProfileBtn: UIButton!

    @IBOutlet var notificationTitleLbl: UILabel!
    @IBOutlet var timeLbl: UILabel!
    weak var delegate: notificationDelegate?
    
    @IBOutlet var containerImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet var userImageView: PFImageView!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func otherUserProfile(_ sender: Any) {
        self.delegate?.otherUserProfileClicked(tag: (sender as AnyObject).tag)
    }
    
}
