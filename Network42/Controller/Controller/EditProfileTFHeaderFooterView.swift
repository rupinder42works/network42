//
//  EditProfileTFHeaderFooterView.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 24/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol sectionHeaderActionDelegate {
    func sectionActionClicked(cell:EditProfileTFHeaderFooterView)
    func  otherScreenClicked(cell:EditProfileTFHeaderFooterView,type:String)
    func deleteSectionActionClicked(cell:EditProfileTFHeaderFooterView)
}
class EditProfileTFHeaderFooterView: UITableViewHeaderFooterView {
  
    @IBOutlet var actIndicator: UIActivityIndicatorView!
    @IBOutlet var bgButton_height: NSLayoutConstraint!
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var addBtn: UIButton!
    weak var delegate: sectionHeaderActionDelegate?
    @IBOutlet var bgBtn: UIButton!
    @IBAction func bgButtonTapped(_ sender: Any) {
        self.delegate?.otherScreenClicked(cell: self,type:"")
    }
    @IBOutlet var newTf: MyTextField!
    @IBOutlet var deleteBtn: UIButton!
   // @IBOutlet var textfeilds: MyTextField!
    @IBOutlet var Add_width: NSLayoutConstraint!
    @IBAction func AddTapped(_ sender: Any) {
        self.delegate?.sectionActionClicked(cell: self)
    }
    @IBOutlet var deleteicon_height: NSLayoutConstraint!
    @IBAction func deleteTapped(_ sender: Any) {
        self.delegate?.deleteSectionActionClicked(cell: self)
    }

}
