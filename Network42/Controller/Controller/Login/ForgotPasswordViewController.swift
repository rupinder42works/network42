//
//  ForgotPasswordViewController.swift
//  Network42
//
//  Created by 42works on 09/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import TKSubmitTransitionSwift3
import Parse

class ForgotPasswordViewController: BaseViewController, UIViewControllerTransitioningDelegate {
    
    // MARK:- Outlets
    @IBOutlet var resetButton: TKTransitionSubmitButton!
    @IBOutlet var email: MyTextField!
    
    // MARK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- IBAction
    @IBAction func submitAction(_ sender: AnyObject)
    {
        
        if !self.isValidEmail(email.text!)
        {
            self.showMessage(messages.validEmail)
            
        } else {
            resetButton.startLoadingAnimation()
            self.activityIndicatorBK.isHidden = false
            //        let userEmail = email.text
            var message = ""
            PFUser.requestPasswordResetForEmail(inBackground: self.email.text!,
                                                block: {(user, error) -> Void in
                                                    if let error = error as? NSError{
                                                        let errorString = error.userInfo["error"] as? NSString
                                                        print(errorString!)// In case something went wrong...
                                                        print("ended")
                                                        message = (error.localizedDescription)
                                                    }else{
                                                        message = messages.resetPassword
                                                        self.email.text = ""
                                                    }
                                                    self.email.resignFirstResponder()
                                                    self.showMessage(message)
                                                    self.resetButton.returnToOriginalState()
                                                    self.activityIndicatorBK.isHidden = true
                                                    self.resetButton.layer.cornerRadius = self.resetButton.normalCornerRadius!
                                                    
            })
        }
    }
    
    @IBAction func back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    // MARK:- UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    
    
    
}
