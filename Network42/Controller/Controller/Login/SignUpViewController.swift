//
//  SignUpViewController.swift
//  Network42
//
//  Created by 42works on 09/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import TKSubmitTransitionSwift3
import  Parse
import ChimpKit


class SignUpViewController: BaseViewController, UIViewControllerTransitioningDelegate,UITextFieldDelegate {
    
    @IBOutlet var name: MyTextField!
    @IBOutlet var userName: MyTextField!
    @IBOutlet var email: MyTextField!
    @IBOutlet var password: MyTextField!
    @IBOutlet var confirmPassword: MyTextField!
    @IBOutlet var scrollViewObj: UIScrollView!
    @IBOutlet var termsConditionSelectImg: UIImageView!
    @IBOutlet var signUpButton: TKTransitionSubmitButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SignUpViewController.down))
        self.scrollViewObj.addGestureRecognizer(tapGesture)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        name.text = ""
        userName.text = ""
        email.text = ""
        password.text = ""
        confirmPassword.text = ""
        password.text = ""
    }
    
    @IBAction func createAccountAction(_ sender: AnyObject)
    {
        self.view.endEditing(true)
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        if name.text == "" || userName.text == "" || email.text == "" || password.text == "" || confirmPassword.text == "" {
            self.showMessage(messages.allRequired)
        }
        else if !self.isValidEmail(email.text!)
        {
            self.showMessage(messages.validEmail)
        }
        else if (password.text!.characters.count==0) || !(password.text!==password.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines))
        {
            self.showMessage(messages.incorrectPassword)
        }
        else if (password.text!.characters.count<6)
        {
            self.showMessage(messages.passwordLength)
        }
        else if !(password.text!==confirmPassword.text!)
        {
            self.showMessage(messages.notSame)
        }
        
        else if !termsConditionSelectImg.isHighlighted {
            self.showMessage("Please accept Terms and Conditions.")
        }
            
        else {
             signUpButton.startLoadingAnimation()
             self.activityIndicatorBK.isHidden = false
             self.signUpApi()
        }
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if (string == " ") {
            return false
        }
        return true
    }
    
    
    func signUpApi()
    {
        let currentDate = Date()
        print(currentDate)
        // Defining the user object
        let user = PFUser()
        user.username = userName.text!
        user.password = password.text!
        user.email = email.text!
        
        // Signing up using the Parse API
        user.signUpInBackground {
            (success, error) -> Void in
            if let error = error as NSError? {
                _ = error.userInfo["error"] as? NSString
                print("ended")
                
                self.showMessage((error.localizedDescription))// In case something went wrong, use errorString to get the error
                
                self.signUpButton.returnToOriginalState()
                self.activityIndicatorBK.isHidden = true
                self.signUpButton.layer.cornerRadius = self.signUpButton.normalCornerRadius!
            } else {
                print("User",user)
                if let currentUser = PFUser.current(){
                    currentUser["name"] = self.name.text!.capitalized
                    currentUser["last_login"] = "\(currentDate)"
                    currentUser["set_password"] = "1"
                    currentUser["login_type"] = "email"
                    currentUser["account_status"] = "1"
                    currentUser["account_created_at"] = "\(currentDate)"
                    currentUser["user_name"] = self.userName.text!
                    currentUser["username"] = self.userName.text!
                    currentUser["profile_updated"] = "0"
                    currentUser["email"] = self.email.text!
                    currentUser["profile_type"] = "1"
                    currentUser.saveInBackground()
                    
                    self.installer()
                    self.notificationAPI()
                }
                self.registerUserusingFir()
                
            }
        }

    }
    
    
    func installer()
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let installationClass = PFInstallation.current()
        installationClass?["userId"] = PFUser.current()?.objectId!
        installationClass?["isActive"] = "1"
        installationClass?.saveInBackground { (success, error) in
            if(success)
            {
                print("installation saved")
                
                let arr = (self.name.text ?? "").components(separatedBy: " ")
                
                let fname = arr.first
                let lName = arr.count > 1 ? arr.last : ""
                
                
                let mailToSubscribe: [String: AnyObject] = ["email": self.email.text as AnyObject? ?? "" as AnyObject]
                let params: [String: AnyObject] = ["id": "9044b6b254" as AnyObject, "email": mailToSubscribe as AnyObject, "double_optin": false as AnyObject, "merge_vars": ["FNAME": fname, "LName":lName] as AnyObject]
                
                ChimpKit.shared().callApiMethod("lists/subscribe", withParams: params, andCompletionHandler: {(response, data, error) -> Void in
                    if let httpResponse = response as? HTTPURLResponse {
                        NSLog("Reponse status code: %d", httpResponse.statusCode)
                        let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                        print(datastring) // printing result of response
                    }
                })
            }
            else{
                print("error",error!)
                return
            }
        }
    }
    
    
    //MARK:-Put notification Flags
    func notificationAPI(){
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let objId = PFUser.current()?.objectId
        print(objId!)
        
        let Settings = PFObject(className: "Settings")
                        Settings["push_post_tag"] = "1"
                        Settings["push_message_send"] = "1"
                        Settings["push_post_share"] = "1"
                        Settings["push_post_comment"] = "1"
                        Settings["profile_visibility"] = "1"
                        Settings["push_post_like"] = "1"
                        Settings["push_accept_follow_request"] = "1"
                        Settings["push_send_follow_request"] = "1"
                        Settings["push_comment_tag"] = "1"
                        Settings["user_id"] = objId!
                        Settings.saveInBackground(block: { (status, error) in
                            print(status)
                            if status ==  true{
                                print(status)
                                
                            }
                        })
    }
    
    
    func sendVarificationEmail(_ getUser:User)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        getUser.sendEmailVerification { (error) in
            if error==nil
            {
                print("email sent")
            }
            else
            {
                print("some error")
            }
        }
    }
    
    
    func down()
    {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func termsCondtionTap() {
        termsConditionSelectImg.isHighlighted = !termsConditionSelectImg.isHighlighted
    }
    
    
    @IBAction func back(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    // MARK: UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    //MARK:REGISTERING USING FIR
    func registerUserusingFir(){
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        Auth.auth().createUser(withEmail: email.text!, password: password.text!) { (user, error) in
            
            if error == nil {
                print("You have successfully signed up")
                let parameters = ["name":self.name.text!.capitalized,"email":self.email.text!,"userName":self.userName.text!] as [String : Any]
                let ref = Database.database().reference()
                ref.child("users").child((PFUser.current()?.objectId!)!).setValue(parameters)
                { (error, DBref) in
                    print(DBref)
                    if !(error==nil)
                    {
                        print(error as! String)
                    }
                }
                
                self.signUpButton.setTitle("", for: .normal)
                self.signUpButton.startFinishAnimation(0.5, completion: {
                    let tabBarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                    let vc = tabBarSB.instantiateViewController(withIdentifier: "TabBarViewController")
                    vc.transitioningDelegate = self
                    self.activityIndicatorBK.isHidden = true
                    self.signUpButton.layer.cornerRadius = self.signUpButton.normalCornerRadius!
                    self.present(vc, animated: true, completion: nil)
                })
                
            } else {
                
                self.signUpButton.returnToOriginalState()
                self.activityIndicatorBK.isHidden = true
                self.signUpButton.layer.cornerRadius = self.signUpButton.normalCornerRadius!
                
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
            
        }

    }
}
