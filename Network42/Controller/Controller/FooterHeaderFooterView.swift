//
//  FooterHeaderFooterView.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 27/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import TKSubmitTransitionSwift3

@objc protocol updateDelegate {
    @objc optional func updateDelegate(cell:FooterHeaderFooterView)
}
class FooterHeaderFooterView: UITableViewHeaderFooterView {
    @IBOutlet var updateBtn: TKTransitionSubmitButton!
    weak var delegate: updateDelegate?
    @IBAction func updateTapped(_ sender: Any) {
           self.delegate?.updateDelegate!(cell: self)
        
    }
   
    
}
