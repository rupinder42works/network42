//
//  UserInfoCell.swift
//  Network42
//
//  Created by 42works on 27/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol profileImgDelegate {
    @objc optional func editImgClickedDelegate(_ index:Int,cell:UserInfoCell,type:String)
}

@objc protocol followDelegateProfile {
    @objc optional func followClickedDelegateProfile(cell:UserInfoCell)
     
}

@objc protocol followListDelegate {
   
    @objc optional func followClickedtolistCount(cell:UserInfoCell,type:String)
    @objc optional func followingClickedtolistCount(cell:UserInfoCell,type:String)
     @objc optional func sendMsgClickedtoChatlist(cell:UserInfoCell)
}
class UserInfoCell: UITableViewCell {
   
   
    @IBOutlet var disableImage: UIImageView!
    @IBOutlet var TView: UIView!
    
    @IBOutlet var locationInsideView: UIView!
    @IBOutlet weak var editUserImgBtn: UIButton!
    @IBOutlet var locationLbl: UILabel!
    @IBOutlet var locIcon: UIImageView!
   
    @IBOutlet var messageBtnWidhtConstraint: NSLayoutConstraint!
    @IBOutlet var leadingMsgBtnConstraint: NSLayoutConstraint!
    @IBOutlet var profileBackView: UIView!
    @IBOutlet var locicon_width: NSLayoutConstraint!
    @IBOutlet var singleViewIcon: UIImageView!
    @IBOutlet var singleViewLocation: UIView!
    @IBOutlet weak var userImgView: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBAction func editUserImg(_ sender: Any) {
        self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "user")
    }
    @IBOutlet var followingLbl: UILabel!
    @IBOutlet var followLbl: UILabel!
    @IBOutlet var singleViewTitle: UILabel!
    @IBOutlet var websiteIcon: UIImageView!
    @IBOutlet var websiteLbl: UILabel!
    @IBOutlet var singleView_height: NSLayoutConstraint!
    @IBOutlet var website_iconwidth: NSLayoutConstraint!
    @IBAction func editBgImage(_ sender: Any) {
        self.delegate?.editImgClickedDelegate!((sender as AnyObject).tag,cell: self,type: "background")
    }
     weak var delegate: profileImgDelegate?
    weak var delegateProfile: followDelegateProfile?
    weak var delegateFollowlist: followListDelegate?
    @IBOutlet weak var followingCountLbl: UILabel!
    @IBOutlet var userInfoView: UIView!
    @IBOutlet weak var followersCount: UILabel!
    @IBOutlet weak var designation: UILabel!
    @IBOutlet var infoTableBgView: UIView!
    @IBOutlet var heightOFTopImage: NSLayoutConstraint!
    @IBOutlet var locationView: UIView!
    @IBOutlet var followerViewContainer: UIView!
    @IBOutlet var whiteShadowImage: UIImageView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var editBgButtonObj: UIButton!
    @IBOutlet var bgImageHeight: NSLayoutConstraint!

    @IBOutlet weak var location_height: NSLayoutConstraint!
    @IBOutlet weak var description_height: NSLayoutConstraint!
    @IBOutlet var heightOfFollowView: NSLayoutConstraint!
    @IBOutlet var heightOfSingleViewLocation: NSLayoutConstraint!
    
    @IBOutlet var followBtnObj: UIButton!
    @IBOutlet var messageBtnObj: UIButton!
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var followerContainer: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func followAction(_ sender: Any) {
        self.delegateProfile?.followClickedDelegateProfile!(cell: self)
    }
    @IBAction func finalFollowingList(_ sender: Any) {
        
        self.delegateFollowlist?.followingClickedtolistCount!(cell: self,type: "Follow")
        
    }
    @IBAction func finalFollowerList(_ sender: Any) {
        
        self.delegateFollowlist?.followClickedtolistCount!(cell: self,type: "Following")
    }
    
    @IBAction func messageTapped(_ sender: Any) {
        
        self.delegateFollowlist?.sendMsgClickedtoChatlist!(cell:self)
    }
}
