//
//  InfoTableViewCell.swift
//  Network42
//
//  Created by 42works on 27/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class InfoTableViewCell: UITableViewCell {

    @IBOutlet var infoLbl: UILabel!
    @IBOutlet var upShadowHeightConstraint: NSLayoutConstraint!
    @IBOutlet var bottomShadowHeightConstraint: NSLayoutConstraint!
    @IBOutlet var bottomMarginViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var topMarginViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var bottomSpaceLabelConstriant: NSLayoutConstraint!
    @IBOutlet var bgInfoImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    @IBOutlet var info_icon: UIImageView!

    @IBOutlet var bottom_margin: NSLayoutConstraint!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
