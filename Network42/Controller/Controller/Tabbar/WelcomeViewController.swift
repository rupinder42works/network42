//
//  WelcomeViewController.swift
//  Network42
//
//  Created by 42works on 14/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    @IBOutlet var welcomeMessage: UILabel!
    var name : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UserDefaults.standard.set(true, forKey: "LoggedIn")
       welcomeMessage.text = "Hey \(name), we missed you!"

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func `continue`(_ sender: Any) {
        
        let tabBarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = tabBarSB.instantiateViewController(withIdentifier: "TabBarViewController")
        self.present(vc, animated: true, completion: nil)
    }

    @IBAction func back(_ sender: Any) {
        _ = self.navigationController?.popToRootViewController(animated: true)
    }

}
