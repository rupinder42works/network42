//
//  TabBarViewController.swift
//  Network42
//
//  Created by 42works on 14/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
class TabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let currentUser = PFUser.current()
        print(currentUser!)
        currentUser?.fetchInBackground { (object, error) -> Void in
            print("Refreshed")
            currentUser?.fetchIfNeededInBackground { (result, error) -> Void in
                let astatus = currentUser?.object(forKey: "profile_updated") as? String ??
                    ""
                print("Updated")
                print("values are",astatus)
            }
        }
        
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
