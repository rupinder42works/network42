//
//  ProfileViewController.swift
//  Network42
//
//  Created by 42works on 27/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import AVFoundation
import ActiveLabel
import SwiftyGif
import JPVideoPlayer
import Firebase
import GoogleMobileAds

var nestedprofileArray:NSMutableArray =  [[],[],"","","",""]
@objc protocol updateImageonProfile {
    @objc optional func imageupdateDelegate(img:UIImage)
}
var refreshControl: UIRefreshControl!
class ProfileViewController: BaseViewController,profileImgDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,followDelegateProfile,followListDelegate,likeListingDelegate{
    
    var isVideoSeen = false
    fileprivate var getAllData = false
    var objectId: String = ""
    var followingUserCountDict:NSMutableDictionary = [:]
    weak var delegate: updateImageonProfile?
    let followArray : NSMutableArray = []
    let followingArray : NSMutableArray = []
    var refreshControl: UIRefreshControl!
    var profileArray:NSMutableArray = []
    var fololowingcountArray:NSMutableArray = []
    @IBOutlet var profileAcIndicator: UIActivityIndicatorView!
    
    let imagePicker = UIImagePickerController()
    @IBOutlet var profileTable: UITableView!
    var  userimageToSend = UIImage.init(named: "dummyProfile")!
    var  bgimageToSend = UIImage()
    var uploadFlag:Bool = false
    var pickerTypeStr:String = ""
    var tempCartArray = NSMutableArray()
    var profileFlag :String = ""
    var profileImageArray:NSMutableArray = []
    var filterImgArray :NSMutableArray = []
    let companyArray:NSMutableArray = []
    
    let schoolArray:NSMutableArray = []
    var comapnyAPIresponse:Bool = false
    var getLocVal:String = ""
    var websiteVal:String = ""
    var bioVal:String = ""
    let companyobj:NSMutableArray = []
    let compobjIds:NSMutableArray = []
    let schoolobj:NSMutableArray = []
    let sobjId:NSMutableArray = []
    var dummyinfoTitle:NSMutableArray = []
    var dummyImgArray:NSMutableArray = []
    var wholeCompArray:NSMutableArray = []
    var objectidtoPass :String = ""
    
    @IBOutlet var backButtonObj: UIButton!
    @IBOutlet var editButtonObj: UIButton!
    
    var fromSearch : Bool = false
    var userProfile : Bool = true
    var friend : String = ""
    var userFromSearch : PFUser?
    var currentUser = PFUser.current()
    
    
    var refreshSearchBlocked : Bool = false
    var blocked : Bool = false
    
    var postExist : Bool = false
    var PFObjectArray : NSMutableArray = []
    
    
    @IBOutlet var popUpTableViewObj: UITableView!
    @IBOutlet var popUp: UIView!
    @IBOutlet var progressVIew: UIView!
    @IBOutlet var progressImage: UIImageView!
    @IBOutlet var heightOFProgressView: NSLayoutConstraint!
    @IBOutlet var heightOfPopUpTableView: NSLayoutConstraint!
    var popUpArray : NSArray = []
    
    var descriptionIndex : Int = 0
    
    @IBOutlet var headingLabelObj: UILabel!
    
    var notificationStatus:Bool = false
    
    var firstTime : Bool = true
    
    @IBOutlet var blockbtn: UIButton!
    var otherUserBL : [String] = []
     var myBL : [String] = []
    
    var videoIndexCell = 0
    
    
    var callback: (() -> Void)?
    
    //MARK:-Life cycles
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        nestedprofileArray =  [[],[],"","","",""]
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(ProfileViewController.refreshTable), for: UIControlEvents.valueChanged)
        profileTable.addSubview(refreshControl) // not required when using UITableViewController
        
        profileTable.register(UINib(nibName: "SettingsotherTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell1")
        profileTable.register(UINib(nibName: "HomeCell", bundle: nil), forCellReuseIdentifier: "HomeCell")
        profileTable.register(UINib(nibName: "AdvertisementCell", bundle: nil), forCellReuseIdentifier: "AdvertisementCell")
        profileTable.register(UINib(nibName: "UserInfoCell", bundle: nil), forCellReuseIdentifier: "cell")
        profileTable.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "infoCell")
        profileTable.register(UINib(nibName: "InfoTableViewCell", bundle: nil), forCellReuseIdentifier: "infoCellLast")

        profileTable.rowHeight = UITableViewAutomaticDimension
        profileTable.estimatedRowHeight = 702
        
        
        popUpTableViewObj.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "PostTableViewCell")
        popUpTableViewObj.rowHeight = UITableViewAutomaticDimension
        popUpTableViewObj.estimatedRowHeight = 702
        
//        profileTable.rowHeight = UITableViewAutomaticDimension
//        profileTable.estimatedRowHeight = 416
        
//        profileTable.rowHeight = UITableViewAutomaticDimension
//        profileTable.estimatedRowHeight = 30
        
//        profileTable.rowHeight = UITableViewAutomaticDimension
//        profileTable.estimatedRowHeight = 30
        
        profileImageArray = [[],[],"","","",""]
        dummyinfoTitle = ["developer at Aditya Birla Group","Studied at VIT University","ankit@42works.net","3111554499","Single","April 8, 2005"]
        dummyImgArray = ["work.png","education.png","email.png","phone.png","relationHeart.png","dob.png"]
        
        
//        if userProfile == true {
//            currentUser = PFUser.current()
//            serviceCalling()
//            postListUI()
//            
//        } else {
//            
//            if objectId == PFUser.current()?.objectId {
//                userProfile = true
//            } else {
//                userProfile = false
//            }
//            
//            if self.userFromSearch == nil {
//                self.getProfileOfUser()
//                
//            } else {
//                currentUser = userFromSearch
//                serviceCalling()
//                postListUI()
//            }
//        }
        
 //       self.firstFunction()
    }
    
    
    
    func firstFunction()
    {
        if userProfile == true {
            currentUser = PFUser.current()
            serviceCalling()
            postListUI()
            
        } else {
            
            if objectId == PFUser.current()?.objectId {
                userProfile = true
            } else {
                userProfile = false
            }
            
            if self.userFromSearch == nil {
                self.getProfileOfUser()
                
            } else {
                currentUser = userFromSearch
                serviceCalling()
                postListUI()
            }
        }
    }
    
    
    
        
    //MARK:- Private methods
    
    private func serviceCalling() {
        self.getImagesFromDb()
        
        //FOLLOWER COUNT AS
        self.getFollowList()
        self.getFollowingList()
    }
    
    
    private func postListUI() {
        print(currentUser?.username ?? "Network 42")
        self.headingLabelObj.text = currentUser?.object(forKey: "user_name") as? String ?? "Network 42"
        
        if notificationStatus == true{
            
            let followedId = userFromSearch?.value(forKey: "objectId") as! String
            getNotificationstatusApi(followedidStr: followedId)
        }
        
        
        let currentProbileId = self.currentUser?.objectId
        let loggedInId = PFUser.current()?.objectId
        var status : String = ""
        if let profileStatus = self.currentUser?.value(forKey: "profile_type") as? String
        {
            status = profileStatus
        }
        
        if let otherBlockedUsers = currentUser?.value(forKey: "blockedUsers") as? [String]
        {
            otherUserBL = otherBlockedUsers
        }
        
        if let myBlockedUsers = PFUser.current()?.value(forKey: "blockedUsers") as? [String]
        {
            myBL = myBlockedUsers
        }
        
        let loggedIn = PFUser.current()?.objectId
        let otherUserId = currentUser?.objectId
        if self.friend == "FOLLOWING" ||  currentProbileId == loggedInId || status == "1"
        {
            firstTime = false
            
            if  (!(otherUserBL.contains(loggedIn!)) && !(myBL.contains(otherUserId!))) {
                self.postsListApi(isRefresh: true)
            }
        }
    }
    
    
    private func loadDataToView() {
        globalBool.showProgess = false
        
        if fromSearch {
            self.tabBarController?.tabBar.isHidden = true
            self.backButtonObj.isHidden = false
            
        } else  {
            self.tabBarController?.tabBar.isHidden = false
            self.backButtonObj.isHidden = true
        }
        
        if userProfile {
            editButtonObj.isHidden = false
            blockbtn.isHidden = true
            
        } else {
            editButtonObj.isHidden = true
            blockbtn.isHidden = false
        }
        
        //self.getImagesFromDb()
        
        UIApplication.shared.statusBarStyle = .lightContent
        
        // Profile is updated
        if userProfile == true
        {
            currentUser = PFUser.current()
        }else
        {
            currentUser = userFromSearch
        }
        
        if let pstatus = currentUser?.object(forKey: "profile_updated") as? String{
            //print("Updated user",currentUser!)
            profileImageArray = [[],[],"","","",""]
            //print("images array as",profileImageArray)
            //print("values are",pstatus)
            if pstatus == "1"{
                if let email = currentUser?.object(forKey: "email") as? String{
                    if email != ""{
                        nestedprofileArray.replaceObject(at: 2, with: email)
                        profileImageArray.replaceObject(at: 2, with: "email.png")
                    }
                }
                if let phone = currentUser?.object(forKey: "mobile_number") as? String{
                    if phone != ""{
                        nestedprofileArray.replaceObject(at: 3, with: phone)
                        profileImageArray.replaceObject(at: 3, with: "phone.png")
                    }
                }
                if let rstatus = currentUser?.object(forKey: "relationship_status") as? String{
                    if rstatus != ""{
                        nestedprofileArray.replaceObject(at: 4, with: rstatus)
                        profileImageArray.replaceObject(at: 4, with: "relationHeart.png")
                    }
                }
                if let rstatus = currentUser?.object(forKey: "dob") as? String{
                    if rstatus != ""{
                        nestedprofileArray.replaceObject(at: 5, with: rstatus)
                        profileImageArray.replaceObject(at: 5, with: "dob.png")
                    }
                }
                bioVal = ""
                
                if let bioStr = currentUser?.object(forKey: "bio") as? String{
                    if bioStr != ""{
                        bioVal = bioStr
                    }
                    
                    //  profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                //Pending Hide one feild show
                if let locStr = currentUser?.object(forKey: "location") as? String{
                    if locStr != ""{
                        //print(locStr)
                        getLocVal = locStr
                    }
                    // profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                websiteVal = ""
                if let websiteStr = currentUser?.object(forKey: "website") as? String{
                    if websiteStr != ""{
                        //print(websiteStr)
                        websiteVal = websiteStr
                    }
                    // profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                }
                //print(profileArray)
                //Get User companies list from User Work Experience Table
                self.getWorkExperience()
                
                let indexPath = NSIndexPath.init(row: 0, section: 0)
                profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                
                
                NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.methodOfReceivedNotification(notification:)), name: Notification.Name("ImageNotify"), object: nil)
                
            }
            else{
                self.getWorkExperience()
                nestedprofileArray =  [[],[],"","","",""]
                profileImageArray = [[],[],"","","",""]
            }
        }
    }
    
    
    func showBgImage(cell: UserInfoCell) {
        cell.bgImageHeight.constant = cell.TView.frame.maxY
    }
    
    //Mark:---From CollectionView Notification call
    //MARK:-
    func getNotificationstatusApi(followedidStr:String){
        
        let query = PFQuery(className: "User_follow")
        query.whereKey("follower_id", equalTo:PFUser.current()?.objectId! ?? "")
        query.whereKey("followed_id", equalTo:followedidStr)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print(objects!)
                if objects?.count != 0{
                    for object in objects! {
                        print(object)
                        if let statusFlag = object.object(forKey: "status") as? String
                        {
                            print(statusFlag)
                            if statusFlag == "0"{
                                self.friend = "CANCEL"
                            }
                            else if statusFlag == "1"{
                                
                                self.friend = "FOLLOWING"
                            }
                        }
                    }
                }
                else {
                    self.friend = "FOLLOW"
                }
                self.profileTable.reloadData()
                
                
                let currentProbileId = self.currentUser?.objectId
                let loggedInId = PFUser.current()?.objectId
                var status : String = ""
                if let profileStatus = self.currentUser?.value(forKey: "profile_type") as? String
                {
                    status = profileStatus
                }
                if self.friend == "FOLLOWING" ||  currentProbileId == loggedInId || status == "1"
                {
                    if self.firstTime
                    {
                        self.postsListApi(isRefresh: true)
                    }
                }
                
            }
            else {
                //print(error!)
            }
        })
    }
    
    
    
    func refreshTable() {
        //Get User companies list from User Work Experience Table
        self.serviceCalling()

        
        if let status = currentUser?.value(forKey: "profile_type") as? String, status == "1" {
            self.getWorkExperience()
            self.postsListApi(isRefresh: true)
        } else {
            self.refreshControl.endRefreshing()
        }
        
    }
    
    
    @IBAction func edit(_ sender: Any) {
        let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
        let vc = settingsSB.instantiateViewController(withIdentifier: "EditViewController") as! EditViewController
        vc.bgimageToSend = self.bgimageToSend
        vc.profileCallback = {
            self.getImagesFromDb()
        }
        vc.userimageToSend = self.userimageToSend
        if companyArray.count > 0 || schoolArray.count > 0{
            vc.schoolArray = self.schoolArray
            
            vc.deleteObjId = self.objectidtoPass
            vc.getobjIdsArray = self.compobjIds
            vc.getSchoolobjIds = self.sobjId
            
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:-BlockAction
    @IBAction func blockAction(_ sender: Any) {
        
        
        var alert = UIAlertController()
        let user = currentUser
        let currentName = user?.value(forKey: "name") as! String
        alert = UIAlertController(title: "Block \(currentName)", message: "Are you sure you want to block \(currentName)?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
            
        }))
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            let objId = self.currentUser?.objectId
            print(objId!)
            if let blockedUsers = PFUser.current()?.value(forKey: "blockedUsers") as? [String]
            {
                if blockedUsers.contains(objId!)
                {
                    self.showMessage(messages.alreadyBlocked)
                    return
                }else
                {
                    var blockedUserArray : [String] = blockedUsers
                    blockedUserArray.append(objId!)
                    print(blockedUserArray)
                    if let currentUser = PFUser.current(){
                        currentUser["blockedUsers"] = blockedUserArray
                        self.blocked = true
                        currentUser.saveInBackground(block: { (status, error) in
                            self.backMethod()
                            self.showMessage(messages.blockedUser)
                        })
                    }
                    
                    self.unfollowBlockedUser()
                    self.unfollowMyself()
                    
                }
                
            }else
            {
                if let currentUser = PFUser.current(){
                    let arrayIds : [String] = [objId!]
                    currentUser["blockedUsers"] = arrayIds
                    self.blocked = true
                    currentUser.saveInBackground(block: { (status, error) in
                        self.backMethod()
                        self.showMessage(messages.blockedUser)
                    })
                }
                
                self.unfollowBlockedUser()
                self.unfollowMyself()
            }
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func unfollowBlockedUser()
    {
       if self.friend == "FOLLOWING"
       {
        let objectId = self.currentUser?.objectId
        let loggedIn = PFUser.current()?.objectId
        let query = PFQuery(className:"User_follow")
        query.whereKey("follower_id", equalTo: loggedIn!)
        query.whereKey("followed_id", equalTo: objectId!)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                for objectt in objects! {
                    objectt.deleteInBackground(block: { (status, error) in
                        if status == true{
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                            self.friend = "FOLLOW"
                            self.profileTable.reloadData()
                        }
                    })
                }
            }
            else {
                //let message = (error?.localizedDescription)
                //self.showMessage(message!)
                print(error!)
            }
        })
        }
    }
    
    
    func unfollowMyself()
    {
        if self.friend == "FOLLOWING"
        {
            let objectId = self.currentUser?.objectId
            let loggedIn = PFUser.current()?.objectId
            print(objectId!)
            let query = PFQuery(className:"User_follow")
            query.whereKey("follower_id", equalTo:objectId!)
            query.whereKey("followed_id", equalTo:loggedIn!)
            query.findObjectsInBackground(block: { (objects, error) in
                if error == nil {
                    for objectt in objects! {
                        objectt.deleteInBackground(block: { (status, error) in
                            if status == true{
                                print(status)
                            }
                        })
                    }
                }
                else {
                    //let message = (error?.localizedDescription)
                    //self.showMessage(message!)
                    print(error!)
                }
            })
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.firstFunction()
        self.logController()
        if !isVideoSeen {
            
            popUp.isHidden = false
            popUp.fadeOut()
            
            let gif = UIImage(gifName: "loading.gif")
            let gifmanager = SwiftyGifManager(memoryLimit:20)
            self.progressImage.setGifImage(gif, manager: gifmanager)
            self.heightOFProgressView.constant = 0
            self.progressVIew.isHidden = true
            
            if userProfile == true || self.userFromSearch != nil {
                self.loadDataToView()
            }
        }
        
        isVideoSeen = false
        
         NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.updateMuted), name: NSNotification.Name(rawValue: "mute"), object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.postsListFirstRecord(indexObj:)), name: NSNotification.Name(rawValue: "postSuccessMyProfile"), object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        JPVideoPlayerManager.shared().stopPlay()
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "postSuccessMyProfile"), object: nil);
    }
        
    @IBAction func backButton(_ sender: Any) {
       self.backMethod()
    }
    
    
    func backMethod()
    {
        nestedprofileArray =  [[],[],"","","",""]
        
        if refreshSearchBlocked && blocked
        {
            callback!()
        }
        
        _ =  self.navigationController?.popViewController(animated: true)
    }
    
    
    func getProfileOfUser() {
        let nameQuery : PFQuery = PFUser.query()!
        nameQuery.whereKey("objectId", equalTo: objectId)
        nameQuery.findObjectsInBackground(block: { (objects, error) in
            print(objects!)
            for object in objects! {
                self.userFromSearch = object as? PFUser
            }
            self.currentUser = self.userFromSearch
            self.serviceCalling()
            self.postListUI()
            self.loadDataToView()
        })
    }
    
    
    //MARK:-For image updation at Edit profile
    func methodOfReceivedNotification(notification: Notification){
        self.getImagesFromDb() //Take Action on Notification
    }
    //MARK:-Get Work Experience
    func getWorkExperience(){
        //Get work experience from User_work_history table
        self.profileAcIndicator.startAnimating()
        let img:NSMutableArray = []
        //print(self.profileImageArray)
        self.companyArray.removeAllObjects()
        self.compobjIds.removeAllObjects()
        self.companyobj.removeAllObjects()
        
        let objId = currentUser?.objectId
        let query = PFQuery(className:"User_work_history")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                cworkExpWholeobj.removeAllObjects()
                for object in objects! {
                    self.objectidtoPass = object.objectId! as String
                    cworkExpWholeobj.add(object)
                }
                let descriptor: NSSortDescriptor = NSSortDescriptor(key: "from_year", ascending: false)
                let sortedResults = cworkExpWholeobj.sortedArray(using: [descriptor])
                //print(sortedResults)
                cworkExpWholeobj.removeAllObjects()
                self.companyArray.removeAllObjects()
                self.compobjIds.removeAllObjects()
                cworkExpWholeobj = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
                //print("After Sort",cworkExpWholeobj)
                for object in cworkExpWholeobj{
                    self.compobjIds.add((object as AnyObject).value(forKey: "objectId")!)
                    self.companyArray.add((object as AnyObject).value(forKey: "company_name")!)
                    img.add("work.png")
                    self.comapnyAPIresponse = true
                }
                //print(self.companyArray)
                //print(self.compobjIds)
                self.refreshControl.endRefreshing()
            }
            else {
                //print(error!)
                self.profileAcIndicator.stopAnimating()
                self.refreshControl.endRefreshing()
            }
            self.profileImageArray.replaceObject(at: 0, with: img)
            nestedprofileArray.replaceObject(at: 0, with: self.companyArray)
                        
            self.getSchoolNameList()
        })
        
    }
    //MARK:-Fetch school data
    func getSchoolNameList(){
        
        //print(self.profileImageArray)
        //Get work education from User_edu table
        
        schoolArray.removeAllObjects()
        self.sobjId.removeAllObjects()
        self.schoolobj.removeAllObjects()
        let schoolImgArray:NSMutableArray=[]
        self.sobjId.removeAllObjects()
        
        let objId = currentUser?.objectId
        let query = PFQuery(className:"User_education")
        query.whereKey("user_id", equalTo:objId!)
        // query.whereKey("user_id", equalTo:"Dqqz2juv5M")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                schollWholeobj.removeAllObjects()
                for object in objects! {
                    schollWholeobj.add(object)
                    //print(object.value(forKey: "school_name")!)
                    self.profileAcIndicator.stopAnimating()
                    self.refreshControl.endRefreshing()
                }
            } else {
                //print(error!)
                self.profileAcIndicator.stopAnimating()
                //                self.refreshControl.endRefreshing()
            }
            
            let descriptor: NSSortDescriptor = NSSortDescriptor(key: "batch_from", ascending: false)
            let sortedResults = schollWholeobj.sortedArray(using: [descriptor])
            ////print(sortedResults)
            schollWholeobj.removeAllObjects()
            self.schoolArray.removeAllObjects()
            self.sobjId.removeAllObjects()
            schollWholeobj = (sortedResults as NSArray).mutableCopy() as! NSMutableArray
            ////print("After Sort SChool List",schollWholeobj)
            for object in schollWholeobj{
                self.schoolArray.add((object as AnyObject).value(forKey: "school_name")!)
                self.sobjId.add((object as AnyObject).value(forKey: "objectId")!);
                schoolImgArray.add("education.png")
            }
            
            
            nestedprofileArray.replaceObject(at: 1, with: self.schoolArray)
            self.profileImageArray.replaceObject(at: 1, with: schoolImgArray)
            //print("After school objects",self.schoolArray)
            //print(self.schoolArray)
            
            self.profileArray.removeAllObjects()
            self.filterImgArray.removeAllObjects()
            for index in 0 ..< nestedprofileArray.count {
                if nestedprofileArray.object(at: index) as? String != "" && (nestedprofileArray.object(at: index) as? NSArray)?.count != 0{
                    if let objArray = (nestedprofileArray.object(at: index) as? NSArray){
                        self.profileArray.addObjects(from: objArray as! [Any])
                        
                        
                    }
                    else{
                        self.profileArray.add(nestedprofileArray[index])
                    }
                }
                if self.profileImageArray.object(at: index) as? String != "" && (self.profileImageArray.object(at: index) as? NSArray)?.count != 0{
                    if let objImgArray = (self.profileImageArray.object(at: index) as? NSArray){
                        self.filterImgArray.addObjects(from: objImgArray as! [Any])
                        
                    }
                    else{
                        self.filterImgArray.add(self.profileImageArray[index])
                    }
                }
            }
            //print(self.profileArray)
            //print(self.filterImgArray)
            self.profileAcIndicator.stopAnimating()
            
            //            let indexPath = NSIndexPath.init(row: 0, section: 0)
            //            self.profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
            self.profileTable.reloadData()
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK:-Delegate followClickedDelegateProfile
    func followClickedDelegateProfile(cell:UserInfoCell) {
        
        //print("==",index)
//
        if cell.followBtnObj.titleLabel?.text == "FOLLOW"
        {
            cell.indicator.startAnimating()
            cell.followBtnObj.setTitle("", for: .normal)
            let objId = PFUser.current()?.objectId
            let User_followClass = PFObject(className: "User_follow")
            User_followClass["follower_id"] = objId
            User_followClass["followed_id"] = currentUser?.objectId
            User_followClass["parent_follower"] = PFUser.current()
            User_followClass["parent_following"] = currentUser
            
            var typeObj = "0"
            if let type = currentUser?.object(forKey: "profile_type") as? String
            {
                if type == "1"
                {
                    User_followClass["status"] = "1"
                    typeObj = "1"
                }else
                {
                    User_followClass["status"] = "0"
                }
            }else
            {
                User_followClass["status"] = "1"
            }
            
            User_followClass.saveInBackground { (success, error) in
                cell.indicator.stopAnimating()
                if(success)
                {
                    //print("user follow")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateSearchStatus"), object: nil)
                    
                    
                    if typeObj == "1" {
                        self.friend = "FOLLOWING"
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                        let time = formatter.string(from: Date())
                        let user = self.currentUser
                        
                        self.saveToNotifications(from_id: objId!, to_id: user!.objectId!, notification_type: "follow_public_user", time: time, post_id: "", parent_from: PFUser.current()!, parent_to: user!)
                                                
                        let currentName = PFUser.current()!.value(forKey: "name") as! String
                        self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.startedfollowing)",type:"follow_public_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                    } else {
                        self.friend = "CANCEL"
                        self.pushNotification()
                    }
                    
                    self.profileTable.reloadData()
                }
                else{
                    //print("error",error!)
                    self.friend = "FOLLOW"
                    cell.followBtnObj.setTitle("FOLLOW", for: .normal)
                    let message = (error?.localizedDescription)
                    self.showMessage(message ?? "")
                    return
                }
            }
        }else
        {
            var alert = UIAlertController()
            if cell.followBtnObj.titleLabel?.text == "FOLLOWING"
            {
                let user = currentUser
                let currentName = user?.value(forKey: "name") as! String
                alert = UIAlertController(title: "Are you sure you want to unfollow \(currentName)?", message: "", preferredStyle: UIAlertControllerStyle.alert)
            }else
            {
                alert = UIAlertController(title: "Are you sure you want to cancel this request?", message: "", preferredStyle: UIAlertControllerStyle.alert)
            }
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                cell.indicator.stopAnimating()
            }))
            
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                switch action.style{
                case .default:
                    cell.indicator.startAnimating()
                    cell.isUserInteractionEnabled = false
                    cell.followBtnObj.setTitle("FOLLOW", for: .normal)
                    cell.followBtnObj.backgroundColor = UIColor.clear
                    cell.followBtnObj.setTitleColor(ConstantColors.themeColor, for: .normal)
                    cell.followBtnObj.layer.borderColor = UIColor.lightGray.cgColor
                    
                    let loggedIn = PFUser.current()?.objectId
                    let objectId = self.currentUser?.objectId
                    let query = PFQuery(className:"User_follow")
                    query.whereKey("follower_id", equalTo: loggedIn!)
                    query.whereKey("followed_id", equalTo: objectId!)
                    query.findObjectsInBackground(block: { (objects, error) in
                        cell.isUserInteractionEnabled = true
                        cell.indicator.stopAnimating()
                        if error == nil {
                            for objectt in objects! {
                                objectt.deleteInBackground(block: { (status, error) in
                                    if status == true {
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateSearchStatus"), object: nil)
                                        self.friend = "FOLLOW"
                                        self.profileTable.reloadData()
                                    }
                                })
                            }
                        }
                        else {
                            cell.isUserInteractionEnabled = true
                            cell.followBtnObj.setTitle("FOLLOWING", for: .normal)
                            self.friend = "FOLLOWING"
                            let message = (error?.localizedDescription)
                            self.showMessage(message!)
                            print(error!)
                        }
                    })
                    
                    
                    
                case .cancel: break
                print("cancel")
                    
                case .destructive:
                    print("destructive")
                }
            }))
            self.present(alert, animated: true, completion: nil)
            
            
            
        }
    }
    
    //Mark:message send
    func sendMsgClickedtoChatlist(cell:UserInfoCell){
        
        if let profileStatus = self.currentUser?.value(forKey: "profile_type") as? String, profileStatus == "1"  || cell.followBtnObj.titleLabel?.text == "FOLLOWING"  {
            let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
            let vc = chatSB.instantiateViewController(withIdentifier: "SingleChatViewController")  as! SingleChatViewController
            vc.senderIdToPass = (currentUser?.objectId)!
            vc.titleStr = cell.name.text!
            vc.pushUsrId = (PFUser.current()?.objectId)!
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
            
        else{
            print("not a friend to chat")
        }
        
    }
    
    func pushNotification()
    {
        let user = currentUser
        let currentName = PFUser.current()!.value(forKey: "name") as! String
        self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.requestSent)",type:"follow_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
    }
    
    //to list Follow count
    func followClickedtolistCount(cell:UserInfoCell,type:String){
        if cell.followersCount.text != "0"{
            if self.followArray.count != 0 {
                let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                let vc = tabbarSB.instantiateViewController(withIdentifier: "FollowListViewController")  as! FollowListViewController
                vc.type = "FOLLOWERS"
                vc.getfinalArray = self.followArray
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func followingClickedtolistCount(cell:UserInfoCell,type:String){
        //print("Hi")
        if cell.followingCountLbl.text != "0"{
            if self.followingArray.count != 0 {
                let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                let vc = tabbarSB.instantiateViewController(withIdentifier: "FollowListViewController")  as! FollowListViewController
                vc.type = "FOLLOWING"
                vc.getfinalArray = self.followingArray
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
    }
    
    
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == profileTable {
            if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
                
                if self.PFObjectArray.count>5
                {
                    self.postsListApi(isRefresh: false)
                }else
                {
                    if self.PFObjectArray.count>=1 && self.PFObjectArray.count<5
                    {
                        self.showMessage(messages.noPost)
                        return
                    }
                }
                
                
            }
           
        }
    } 
    
}
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource{
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableViewAutomaticDimension
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == profileTable {
            return UITableViewAutomaticDimension
        }
//        else  if tableView == popUpTableViewObj
//        {
            return 50
//        }
//        else
//        {
//            return UITableViewAutomaticDimension
//        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == profileTable {
            return self.PFObjectArray.count + profileArray.count + 1
            
        } else if tableView == popUpTableViewObj {
            return popUpArray.count
            
        } else {
            return profileArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == profileTable {
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! UserInfoCell
                
                let loggedIn = PFUser.current()?.objectId
                let otherUserId = currentUser?.objectId ?? ""
                if otherUserBL.contains(loggedIn!) || myBL.contains(otherUserId) {
                    cell.disableImage.isHidden = false
                } else {
                    cell.disableImage.isHidden = true
                }
                
                if let pstatus = currentUser?.object(forKey: "profile_updated") as? String{
                    self.profileFlag = pstatus
                }
                
                cell.delegate = self
                cell.delegateFollowlist = self
                cell.editUserImgBtn.tag = indexPath.row
                
                if userProfile
                {
                    cell.heightOfFollowView.constant = 5
                    cell.followerContainer.isHidden = true
                    
                }else
                {
                    cell.heightOfFollowView.constant = 40
                    cell.followerContainer.isHidden = false
                    cell.delegateProfile = self
                    
                    cell.messageBtnObj.layer.borderWidth = 1.0
                    cell.messageBtnObj.layer.cornerRadius = 5.0
                    cell.messageBtnObj.layer.borderColor = UIColor.lightGray.cgColor
                    cell.messageBtnObj.setTitleColor(ConstantColors.themeColor, for: .normal)
                    
                    cell.followBtnObj.layer.borderWidth = 1.0
                    cell.followBtnObj.layer.cornerRadius = 5.0
                    
                    cell.leadingMsgBtnConstraint.constant = 20.0
                    cell.messageBtnWidhtConstraint.constant = 100.0
                    
                    if friend == "FOLLOWING"
                    {
                        cell.followBtnObj.setTitle(friend, for: .normal)
                        cell.followBtnObj.backgroundColor = ConstantColors.themeColor
                        cell.followBtnObj.setTitleColor(UIColor.white, for: .normal)
                        cell.followBtnObj.layer.borderColor = ConstantColors.themeColor.cgColor
                        cell.messageBtnObj.backgroundColor = UIColor.clear
                        cell.messageBtnObj.setTitleColor(ConstantColors.themeColor, for: .normal)
                    }   else if friend == "CANCEL" || friend == "Cancel" {
                            cell.followBtnObj.setTitle("CANCEL", for: .normal)
                            cell.followBtnObj.backgroundColor = ConstantColors.themeColor
                            cell.followBtnObj.setTitleColor(UIColor.white, for: .normal)
                            cell.followBtnObj.layer.borderColor = ConstantColors.themeColor.cgColor
                        
                        cell.leadingMsgBtnConstraint.constant = 0.0
                        cell.messageBtnWidhtConstraint.constant = 0.0
                        }
                        
                        else {
                        cell.followBtnObj.setTitle(friend, for: .normal)
                        cell.followBtnObj.backgroundColor = UIColor.clear
                        cell.followBtnObj.setTitleColor(ConstantColors.themeColor, for: .normal)
                        cell.followBtnObj.layer.borderColor = UIColor.lightGray.cgColor
                        
                        if let profileStatus = self.currentUser?.value(forKey: "profile_type") as? String, profileStatus == "1" {
                            cell.messageBtnObj.backgroundColor = UIColor.clear
                            cell.messageBtnObj.setTitleColor(ConstantColors.themeColor, for: .normal)
                            
                        } else {
                            cell.messageBtnObj.backgroundColor = UIColor(red: 171/255.0, green: 171/255.0, blue: 171/255.0, alpha: 1.0)
                            cell.messageBtnObj.setTitleColor(UIColor.white, for: .normal)
                            
                            cell.leadingMsgBtnConstraint.constant = 0.0
                            cell.messageBtnWidhtConstraint.constant = 0.0
                        }
                    }
                }
                
                
                if userProfile == true
                {
                    cell.editUserImgBtn.isHidden = false
                    cell.editBgButtonObj.isHidden = false
                }else
                {
                    cell.editUserImgBtn.isHidden = true
                    cell.editBgButtonObj.isHidden = true
                }
                
                cell.whiteShadowImage.layer.shadowColor = ConstantColors.lightGrayColor.cgColor
                cell.whiteShadowImage.layer.shadowOffset = CGSize(width: 0, height: 1)
                cell.whiteShadowImage.layer.shadowOpacity = 1
                cell.whiteShadowImage.layer.shadowRadius = 1.0
                cell.whiteShadowImage.layer.shadowRadius = 3.0
                cell.whiteShadowImage.layer.masksToBounds = false
                
                cell.followerViewContainer.layer.shadowColor = ConstantColors.lightGrayColor.cgColor
                cell.followerViewContainer.layer.shadowOffset = CGSize(width: 0, height: 1)
                cell.followerViewContainer.layer.shadowOpacity = 1
                cell.followerViewContainer.layer.shadowRadius = 1.0
                cell.followerViewContainer.layer.masksToBounds = false
                cell.followerViewContainer.layer.cornerRadius = 3.0
                
                
                //set userimage view border
                cell.profileBackView.layer.shadowColor = ConstantColors.lightGrayColor.cgColor
                cell.profileBackView.layer.shadowOffset = CGSize(width: 0, height: 1)
                cell.profileBackView.layer.shadowOpacity = 1
                cell.profileBackView.layer.shadowRadius = 1.0
                cell.profileBackView.layer.masksToBounds = false
                
                //FOLLOWER COUNT AS
                
                if followArray.count == 1 {
                    cell.followLbl.text = "FOLLOWER"
                    
                }
                else{
                    cell.followLbl.text = "FOLLOWERS"
                }
                cell.followersCount.text! = "\(self.followArray.count)"
                
                
                //Following count as
                
                cell.followingCountLbl.text = "\(self.followingArray.count)"
                cell.heightOfSingleViewLocation.constant = 0
                
                if profileFlag != "0"{
                    cell.designation.text = bioVal
                    
                    if getLocVal != "" && websiteVal != "" {
                        cell.heightOfSingleViewLocation.constant = 18
                        cell.singleViewLocation.isHidden = true
                        cell.locationInsideView.isHidden = false
                        cell.locationView.isHidden = false
                        cell.websiteIcon.isHidden = false
                        cell.locIcon.isHidden = false
                        cell.locationLbl.text = getLocVal
                        cell.locIcon.image = UIImage(named: "location.png")!
                        cell.websiteLbl.text = websiteVal
                        cell.websiteIcon.image = UIImage(named: "website.png")!
                        
                    } else if getLocVal != "" {
                        cell.heightOfSingleViewLocation.constant = 18
                        cell.singleViewLocation.isHidden = false
                        cell.locationView.isHidden = true
                        cell.singleViewTitle.text = getLocVal
                        cell.singleViewIcon.image = UIImage(named: "location.png")!
                    }
                    else if websiteVal != "" {
                        cell.locationView.isHidden = true
                        cell.singleViewLocation.isHidden = false
                        cell.heightOfSingleViewLocation.constant = 18
                        cell.singleViewTitle.text = websiteVal
                        cell.singleViewIcon.image = UIImage(named: "website.png")!
                    }
                }
                
                if let nameStr = currentUser?.object(forKey: "name") as? String{
                    //print(nameStr.capitalized)
                    cell.name.text = nameStr.capitalized
                }
                
                ///image taping
                
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                cell.backgroundImage.isUserInteractionEnabled = true
                cell.backgroundImage.addGestureRecognizer(tapGestureRecognizer)
                
                
                let tapGestureRecognizerUser = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizerUser:)))
                cell.userImgView.isUserInteractionEnabled = true
                cell.userImgView.addGestureRecognizer(tapGestureRecognizerUser)
                
                self.profileAcIndicator.stopAnimating()
                cell.userImgView.image = self.userimageToSend
                cell.backgroundImage.image = self.bgimageToSend
                
                if getAllData {
                    cell.bgImageHeight.constant = cell.TView.frame.maxY
                    self.perform(#selector(showBgImage), with: cell, afterDelay: 0.1)
                } else {
                    cell.bgImageHeight.constant = 0
                }
                
                return cell
                
            } else if indexPath.row > 0 && indexPath.row <= profileArray.count {
                var designationStr:String = ""
                
                print("Former obj Profile values",profileArray)
                print("Former obj Filter Images values",self.filterImgArray)
                print("Former obj values",cworkExpWholeobj)
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath as IndexPath) as! InfoTableViewCell
                
                let index = indexPath.row - 1
                
                cell.upShadowHeightConstraint.constant = 0
                cell.bottomShadowHeightConstraint.constant = 0
                cell.topMarginViewHeightConstraint.constant = 6
                
                if index == profileArray.count - 1 {
                    cell.bottomMarginViewHeightConstraint.constant = 5
                    cell.bottomSpaceLabelConstriant.constant = 15
                    cell.bottomShadowHeightConstraint.constant = 30
                    
                    
                } else {
                    cell.bottomMarginViewHeightConstraint.constant = 0
                    cell.bottomSpaceLabelConstriant.constant = 2
                    
                    if index == 0 {
                        cell.upShadowHeightConstraint.constant = 30
                        cell.topMarginViewHeightConstraint.constant = 12
                    }
                }
                
                if index >= profileArray.count && index < 0 {
                    print("index:..",index)
                    return cell
                }
                
                cell.selectionStyle = .none
                if let valueStr = profileArray.object(at: index) as? String {
                    var newVal:String = ""
                    if self.filterImgArray.object(at: index) as? String == "work.png" {
                        
                        if index < cworkExpWholeobj.count {
                            if let designation = (cworkExpWholeobj.object(at: index) as AnyObject).value(forKey: "designation") as? String {
                                if designation != "" {
                                    designationStr = "\(designation) at "
                                }
                            }
                            
                            if let toyearStr = (cworkExpWholeobj.object(at: index) as AnyObject).value(forKey: "to_year") as? String{
                                //print(toyearStr)
                                
                                let date = Date()
                                let calendar = Calendar.current
                                let components = calendar.dateComponents([.year, .month, .day], from: date)
                                let year =  components.year
                                //print(year as Any)
                                let currentYear = String(describing: year!)
                                
                                if toyearStr == currentYear || toyearStr == "Present" {
                                    newVal = "\(designationStr) \(valueStr)"
                                }
                                else  {
                                    newVal = "Former \(designationStr) \(valueStr)"
                                }
                            }
                        }
                    }
                        
                    else if self.filterImgArray.object(at: index) as? String == "education.png"{
                        newVal = "Studied at \(valueStr)"
                    } else {
                        newVal = valueStr
                    }
                    cell.infoLbl.text = newVal
                    
                }
                if let imageStr = self.filterImgArray.object(at: index) as? String{
                    cell.info_icon.image =  UIImage(named: imageStr)
                }
                
                return cell
            }
            else {
                //MARK:-POSTS
                
                if postExist
                {
                    let index = indexPath.row - 1 - profileArray.count
                    
                    if PFObjectArray.object(at: index) as? String == Constants.advertisement
                    {
                        let cell = tableView.dequeueReusableCell(withIdentifier: "AdvertisementCell", for: indexPath as IndexPath) as! AdvertisementCell
                        cell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
                        cell.bannerView.rootViewController = self
                        cell.bannerView.load(GADRequest())
                        return cell
                        
                    }else
                    {
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath as IndexPath) as! HomeCell
                    
                    cell.innerView.layer.shadowColor = ConstantColors.lightGrayColor.cgColor
                    cell.innerView.layer.shadowOffset = CGSize.zero
                    cell.innerView.layer.shadowRadius = 3.0
                    cell.innerView.layer.shadowOpacity = 1
                    cell.innerView.clipsToBounds = false
                    cell.innerView.layer.masksToBounds = false
                    cell.innerView.layer.cornerRadius = 5.0
                    
                    
                    cell.likeBtn.tag = index
                    cell.likeListingObj.tag = index
                    cell.commentListing.tag = index
                    cell.URLBtn.tag = index
                    cell.moreBtn.tag = index
                    cell.userProfileBtn.tag = index
                    cell.userProfileBtn.isHidden = true
                    cell.delegate = self
                    
                    
                    if let timeStampObj = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "time_stamp") as? String
                    {
                        let newdateStr = timeStampObj.replacingOccurrences(of: "_", with: " ")
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                        let newdate = dateFormatter.date(from: newdateStr)
                        let timeStr  = self.timeAgoSinceDate(date:newdate!,numericDates: true)
                        cell.time.text = timeStr
                    }
                    
                   // print(PFObjectArray)
                    
                    if let user = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "parent_id") as? PFUser
                    {
                        // print(user)
                        print(user.value(forKey: "name") as? String ?? "")
                        
                        cell.name.text = user.value(forKey: "name") as? String ?? ""
                        
                        if let profileImageFile = user.object(forKey: "profile_image") as? PFFile
                        {
                            cell.profileImage.file = profileImageFile
                            cell.profileImage.loadInBackground()
                        }else
                        {
                            cell.profileImage.image =  UIImage.init(named: "dummyProfile")
                        }
                    }

                    
                    if let descriptionObj = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "description") as? String
                    {
                        var btnTitle : String = ""
                        if let users_tagged_withArray = (PFObjectArray.object(at: index) as AnyObject).value(forKey: "users_tagged_with") as? NSArray
                        {
                            if users_tagged_withArray.count>0
                            {
                                if users_tagged_withArray.count == 1
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String)"
                                    
                                    let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                                    let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                                    cell.descriptionLbl.enabledTypes.append(customTypeFirst)
                                    cell.descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                                    cell.descriptionLbl.handleCustomTap(for: customTypeFirst) {
                                        self.withUser(index: index, name: $0)
                                    }
                                }else if users_tagged_withArray.count == 2
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \((users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String)"
                                    
                                    let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                                    let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                                    cell.descriptionLbl.enabledTypes.append(customTypeFirst)
                                    cell.descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                                    cell.descriptionLbl.handleCustomTap(for: customTypeFirst) {
                                        self.withUser(index: index, name: $0)
                                    }
                                    
                                    let secondUser = (users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String
                                    let customTypeSecond = ActiveType.custom(pattern: "\\s\(secondUser)\\b")
                                    cell.descriptionLbl.enabledTypes.append(customTypeSecond)
                                    cell.descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                                    cell.descriptionLbl.handleCustomTap(for: customTypeSecond) {
                                        self.withUser(index: index, name: $0)
                                    }
                                    
                                }else
                                {
                                    
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \(users_tagged_withArray.count-1) others"
                                    
                                    let firstUser = (users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String
                                    let customTypeFirst = ActiveType.custom(pattern: "\\s\(firstUser)\\b")
                                    cell.descriptionLbl.enabledTypes.append(customTypeFirst)
                                    cell.descriptionLbl.customColor[customTypeFirst] = ConstantColors.themeColor
                                    cell.descriptionLbl.handleCustomTap(for: customTypeFirst) {
                                        self.withUser(index: index, name: $0)
                                    }
                                    
                                    let otherString = "\(users_tagged_withArray.count-1) others"
                                    let customTypeSecond = ActiveType.custom(pattern: "\\s\(otherString)\\b")
                                    cell.descriptionLbl.enabledTypes.append(customTypeSecond)
                                    cell.descriptionLbl.customColor[customTypeSecond] = ConstantColors.themeColor
                                    cell.descriptionLbl.handleCustomTap(for: customTypeSecond) {_ in
                                        
                                        
                                        self.popUp.fadeIn()
                                        self.popUpArray = users_tagged_withArray
                                        
                                        if self.popUpArray.count>6
                                        {
                                            self.popUpTableViewObj.isScrollEnabled = true
                                            self.heightOfPopUpTableView.constant = 300
                                        }else
                                        {
                                            let count = self.popUpArray.count
                                            self.heightOfPopUpTableView.constant = CGFloat(count * 50)
                                            self.popUpTableViewObj.isScrollEnabled = false
                                        }
                                        self.popUpTableViewObj.reloadData()
                                    }
                                }
                            }
                        }
                        
                        cell.descriptionLbl.customize { label in
                            label.text = descriptionObj+btnTitle
                            
                            
                            
                            
                            
                            let arr = descriptionObj.components(separatedBy: "\n")
                            print("arr==",arr)
                            if ( arr.count > 3) || (descriptionObj.characters.count > 100)
                            {
                                var lengthObj = 0
                                if descriptionObj.characters.count > 100
                                {
                                    lengthObj = 100
                                }else
                                {
                                    lengthObj = descriptionObj.characters.count
                                }
                                
                                let indexObj = descriptionObj.index(descriptionObj.startIndex, offsetBy: lengthObj)
                                let substringDescription = descriptionObj.substring(to: indexObj)
                                
                                label.text = substringDescription+" Continue Reading"
                                let continueReading = ActiveType.custom(pattern: "\\sContinue Reading\\b")
                                cell.descriptionLbl.enabledTypes.append(continueReading)
                                cell.descriptionLbl.customColor[continueReading] = ConstantColors.themeColor
                                cell.descriptionLbl.handleCustomTap(for: continueReading) {_ in
                                    print("hello")
                                    let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
                                    let vc = storyboardObj.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                    vc.secondTime = true
                                    vc.isFromHashTag = false
                                    vc.titleSecond = "network 42"
                                    vc.showOnlySinglePost = true
                                    var obj = (self.PFObjectArray.object(at: index) as AnyObject)
                                    let id = obj.value(forKey: "objectId") as! String
                                    print(id)
                                    vc.postIdObject = id
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            cell.descriptionLbl.numberOfLines = 0
                            cell.descriptionLbl.lineSpacing = 4
                            
                            cell.descriptionLbl.textColor = UIColor(red: 102.0/255, green: 117.0/255, blue: 127.0/255, alpha: 1)
                            cell.descriptionLbl.hashtagColor = ConstantColors.themeColor
                            cell.descriptionLbl.mentionColor = ConstantColors.themeColor
                            cell.descriptionLbl.URLColor = ConstantColors.themeColor
                            cell.descriptionLbl.URLSelectedColor = UIColor(red: 82.0/255, green: 190.0/255, blue: 41.0/255, alpha: 1)
                            
                            
                            cell.descriptionLbl.handleMentionTap {
                                self.descriptionUser(index: index, userName: $0)
                            }
                            
                            cell.descriptionLbl.handleHashtagTap {
                                //self.alert("Hashtag", message: $0)
                                // MARK: - Hashtag
                                //                                    if self.secondTime == false
                                //                                    {
                                let vc = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
                                vc.secondTime = true
                                vc.titleSecond = $0
                                //print($0)
                                self.navigationController?.pushViewController(vc, animated: true)
                                //                                    }
                            }
                            
                            
                            cell.descriptionLbl.handleURLTap {_ in
                                let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
                                let vc = storyboardObj.instantiateViewController(withIdentifier: "MTSWebViewController") as! MTSWebViewController
                                
                                //print($0.absoluteString)
                                if let link_url = (self.PFObjectArray.object(at: index) as AnyObject).object(forKey: "link_url") as? String
                                {
                                    vc.urlStr = link_url
                                }
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                    }
                    
                    
                    if cell.descriptionLbl.text?.characters.count==0
                    {
                        cell.heightOfDescription.isActive = true
                        cell.topOfDescription.constant = 0
                    }else
                    {
                        cell.heightOfDescription.isActive = false
                        cell.topOfDescription.constant = 12
                    }
                    
                    
                    cell.collectionContainer.isHidden = true
                    cell.collectionVIewHeight.constant = 0
                    cell.heightOFPageController.constant = 0
                    cell.heightOFSingleVideo.constant = 0
                    
                    
                    // MARK: - videos
                    
                    if let post_type = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "post_type") as? String
                    {
                        if post_type == "ALBUM" || post_type == "IMAGE" || post_type == "VIDEO"
                        {
                            cell.singleVideoImage.isHidden = true
                            cell.collectionContainer.isHidden = false
                            cell.collectionVIewHeight.constant = 400
                            cell.tag = index
                            if let postmedia = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "postmedia") as? NSArray
                            {
                                if postmedia.count>1
                                {
                                    cell.heightOFPageController.constant = 20
                                }
                                cell.objectForCollection = PFObjectArray.object(at: index) as? PFObject
                                cell.setCollectionViewDataSourceDelegate(dataSourceDelegate: cell, forRow: index,changeVideo:true)
                                videoIndexCell = indexPath.row
                            }
                        }else
                        {
                            if !(videoIndexCell+1 == indexPath.row || videoIndexCell-1 == indexPath.row)
                            {
                            JPVideoPlayerManager.shared().stopPlay()
                            }
                        }
                    }
                    
                    
                    cell.heightOFUrlView.constant = 0
                    cell.topHeightOfUrlView.constant = 0
                    cell.urlView.isHidden = true
                    
                    let post_type = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "post_type") as? String ?? ""
                    if let link_title = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "link_title") as? String
                    {
                        if !(link_title == "")  && post_type == "URL"
                        {
                            cell.heightOFUrlView.constant = 70
                            cell.topHeightOfUrlView.constant = 15
                            cell.urlView.isHidden = false
                            cell.urlView.layer.shadowColor = UIColor.lightGray.cgColor
                            cell.urlView.layer.shadowOffset = CGSize(width: 3, height: 3)
                            cell.urlView.layer.shadowRadius = 3.0
                            cell.urlView.layer.shadowOpacity = 1
                            cell.urlView.clipsToBounds = false
                            cell.urlView.layer.masksToBounds = false
                            
                            cell.urlTitle.text = link_title
                            if let link_description = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "link_description") as? String
                            {
                                cell.urlDescription.text = link_description
                            }
                            if let link_url = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "link_url") as? String
                            {
                                cell.urlAgain.text = link_url
                            }
                            
                            
                            if let link_image_url = (PFObjectArray.object(at: index) as AnyObject).object(forKey: "link_image_url") as? String
                            {
                                let url = URL(string: link_image_url)
                                if !(url==nil)
                                {
                                    cell.urlImage.af_setImage(withURL: url!)
                                    cell.widthOfUrlImage.constant = 50
                                }else
                                {
                                    cell.urlImage.image = nil
                                    cell.widthOfUrlImage.constant = 0
                                }
                            }
                        }
                    }
                    
                    
                    // cell.setCollectionViewDataSourceDelegate
                    
                    
                    if let likeArray = (PFObjectArray.object(at: index) as AnyObject).value(forKey: "user_like_post") as? NSArray
                    {
                        cell.likeListingObj.isEnabled = true
                        //print(likeArray)
                        cell.like.text = "\(likeArray.count) likes"
                        let objId = PFUser.current()?.objectId
                        if likeArray.contains(objId!)
                        {
                            cell.likeBtn.isSelected = true
                            cell.likeBtn.setBackgroundImage(UIImage.init(named: "likeSelected"), for: .normal)
                        }else
                        {
                            cell.likeBtn.isSelected = false
                            cell.likeBtn.setBackgroundImage(UIImage.init(named: "likeUnSelected"), for: .normal)
                        }
                        
                    }else
                    {
                        cell.likeListingObj.isEnabled = false
                        cell.like.text = "0 likes"
                        cell.likeBtn.isSelected = false
                        cell.likeBtn.setBackgroundImage(UIImage.init(named: "likeUnSelected"), for: .normal)
                    }
                    
                    if let comment = (PFObjectArray.object(at: index) as AnyObject).value(forKey: "comment_count") as? String
                    {
                        cell.comment.text = "\(comment) comments"
                    }else
                    {
                        cell.comment.text = "0 comments"
                    }
                    
                    
                    cell.selectionStyle = .none
                    return cell
                    }
                    
                }else
                {
                    let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
                        as! SettingsotherTableViewCell
                    cell1.selectionStyle = .none
                    cell1.backgroundColor = ConstantColors.GrayBgColor
                    cell1.headerTitle.text = "No Posts Found."
                    return cell1
                }
            }
        } else if tableView == popUpTableViewObj
        {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath as IndexPath) as! PostTableViewCell
            cell.bottomLayoutObj.isActive = false
            
            if popUpArray.contains("CANCEL")
            {
                cell.topLayout.constant = 15
                cell.titleObj.text = popUpArray.object(at: indexPath.row) as? String
                cell.postCountObj.isHidden = true
            }else
            {
                cell.topLayout.constant = 10
                cell.titleObj.text = (popUpArray.value(forKey: "name") as AnyObject).object(at: indexPath.row) as? String
                cell.postCountObj.text = (popUpArray.value(forKey: "username") as AnyObject).object(at: indexPath.row) as? String
                cell.postCountObj.isHidden = false
            }
            
            cell.selectionStyle = .none
            return cell
        }
        else
        {        }
        
        return UITableViewCell()
    }
    
    func updateMuted(object:NSArray)
    {
        let array = object.value(forKey: "object") as? NSArray
        var i = 0
        for obj in PFObjectArray
        {
            let  objectdata = array?.object(at: 1) as? PFObject
            if objectdata?.value(forKey: "objectId") as? String == (obj as? PFObject)?.objectId
            {
               if let muteValue = objectdata?.value(forKey: "mute") as? Bool
               {
                objectdata?.setObject(!(muteValue), forKey: "mute")
                PFObjectArray.replaceObject(at: i, with: objectdata ?? obj)
                }
            }
            i=i+1
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == popUpTableViewObj
        {
            
            if popUpArray.contains("CANCEL")
            {
                if popUpArray.object(at: indexPath.row) as! String == "CANCEL"
                {
                    self.popUp.fadeOut()
                }else if popUpArray.object(at: indexPath.row) as! String == "SHARE"
                {
                    if let descriptionObj = (PFObjectArray.object(at: descriptionIndex) as AnyObject).object(forKey: "description") as? String
                    {
                        
                        var btnTitle : String = ""
                        if let users_tagged_withArray = (PFObjectArray.object(at: descriptionIndex) as AnyObject).value(forKey: "users_tagged_with") as? NSArray
                        {
                            if users_tagged_withArray.count>0
                            {
                                if users_tagged_withArray.count == 1
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String)"
                                }else if users_tagged_withArray.count == 2
                                {
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \((users_tagged_withArray.object(at: 1) as AnyObject).value(forKey: "name") as! String)"
                                }else
                                {
                                    
                                    btnTitle = " - with \((users_tagged_withArray.object(at: 0) as AnyObject).value(forKey: "name") as! String) and \(users_tagged_withArray.count-1) others"
                                }
                            }
                        }
                        
                        
                        
                        var urlStr : String = ""
                        if let postmedia = (PFObjectArray.object(at: descriptionIndex) as AnyObject).object(forKey: "postmedia") as? NSArray
                        {
                            if let fileObj = postmedia.object(at: 0) as? PFFile
                            {
                                urlStr = fileObj.url ?? ""
                            }
                        }
                        
                        print(descriptionObj)
                        let vc = UIActivityViewController(activityItems: [descriptionObj+btnTitle,urlStr], applicationActivities: [])
                        present(vc, animated: true, completion: nil)
                    }
                    self.popUp.fadeOut()
                }
                else if popUpArray.object(at: indexPath.row) as! String == "DELETE"
                {
                    self.popUp.fadeOut()
                    
                    let alert = UIAlertController(title: "Are you sure you want to delete this post?", message: "", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                        
                        let object = self.PFObjectArray.object(at: self.descriptionIndex) as! PFObject
                        object.deleteInBackground(block: { (status, error) in
                            print(status)
                            if status == true{
                                self.PFObjectArray.removeObject(at: self.descriptionIndex)
                                self.profileTable.reloadData()
                                let arr = ["delete",object] as NSArray
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
                            }else{
                                let message = (error?.localizedDescription)
                                print(message as Any)
                            }
                        })
                        
                        
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                    
                    
                }else if popUpArray.object(at: indexPath.row) as! String == "EDIT"
                {
                    self.popUp.fadeOut()
                    let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
                    let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePostViewController")  as! CreatePostViewController
                    vc.objectForEdit  = PFObjectArray.object(at: descriptionIndex) as? PFObject
                    vc.edit  = true
                    let id = (PFObjectArray.object(at: descriptionIndex) as AnyObject).value(forKey: "objectId") as! String
                    vc.postForEdit = id
                    vc.indexEdit = descriptionIndex
                    vc.fromMyProfile = true
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else if popUpArray.object(at: indexPath.row) as! String == "REPORT ABUSE"
                {
                    self.popUp.fadeOut()
                    let objId = PFUser.current()?.objectId
                    let postId = (PFObjectArray.object(at: descriptionIndex) as AnyObject).value(forKey: "objectId") as! String
                    
                    let compClass = PFObject(className: "Report_abuse")
                    compClass["post_id"] =  postId
                    compClass["user_id"] =  objId
                    compClass.saveInBackground(block: { (status, error) in
                        print(status)
                        if status ==  true{
                            self.showMessage("This post is marked as spam.")
                        }
                        else{
                            let message = (error?.localizedDescription)
                            print(message as Any)
                        }
                    })
                }
            }else
            {
                self.popUp.fadeOut()
                self.otherUser(index: indexPath.row, name: ((popUpArray.value(forKey: "name") as AnyObject).object(at: indexPath.row) as? String)!)
            }
            
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
       
        return CGFloat.leastNormalMagnitude
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
        
    }
    //MARK:-Back Image
    func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        //print(tappedImage)
        if !(tappedImage.image?.size)!.equalTo(CGSize.zero)
        {
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "FullScreenImgViewController") as! FullScreenImgViewController
            vc.pickerTypeStr = "background"
            vc.userProfile = userProfile
            vc.bgimageToSend = tappedImage.image!
            vc.profileCallback = {
                self.getImagesFromDb()
                self.profileTable.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        // Your action
    }
    
    //MARK:-User Image Tapped
    func imageTapped(tapGestureRecognizerUser: UITapGestureRecognizer)
    {
        
        let tappedImage = tapGestureRecognizerUser.view as! UIImageView
        
        if tappedImage.image != UIImage(named: "dummyProfile") {
            let settingsSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = settingsSB.instantiateViewController(withIdentifier: "FullScreenImgViewController") as! FullScreenImgViewController
            vc.pickerTypeStr = "user"
            vc.userProfile = userProfile
            vc.bgimageToSend = tappedImage.image!
            vc.profileCallback = {
                self.getImagesFromDb()
                self.profileTable.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
    
    
    //MARK:-Delegate
    //Image Picker
    
    func editImgClickedDelegate(_ index:Int,cell:UserInfoCell,type: String){
        
        pickerTypeStr = type
        cameraAuthorization()
    }
    func cameraAuthorization(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func chooseSheet(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertActionStyle.default) {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            
        }
        
    }
    func openGallary() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary)) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertClose(_ gesture: UITapGestureRecognizer) {
        //        if imageEditBtnFlag == true {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    func alertClose1(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        profileAcIndicator.startAnimating()
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            uploadFlag = true
            if pickerTypeStr == "user"{
                
                userimageToSend = pickedImage
                let avatar = PFFile(data: UIImageJPEGRepresentation(userimageToSend, 1.0)!)
                PFUser.current()!.setObject(avatar!, forKey: "profile_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    //print(status)
                    if status ==  true{
                        self.addImageAPI()
                        self.profileAcIndicator.stopAnimating()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                })
            }
            if pickerTypeStr == "background"{
                
                bgimageToSend = pickedImage
                
                let avatar = PFFile(data: UIImageJPEGRepresentation(bgimageToSend, 1.0)!)
                PFUser.current()!.setObject(avatar!, forKey: "cover_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    //print(status)
                    if status ==  true{
                        self.profileAcIndicator.stopAnimating()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                    
                })
            }
            
        }
        profileTable.reloadData()
        dismiss(animated: true, completion: nil)
    }    
    
    
    func addImageAPI(){
        //        let chatDictionary1:NSDictionary = [:]
        var urlString: String = ""
       
            let imageData:NSData = self.userimageToSend.mediumQualityJPEGNSData as NSData
            // Create a reference to the file you want to upload
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
            let  timeStrToPass = formatter.string(from: Date())
            
            let riversRef = Storage.storage().reference().child("images/\(timeStrToPass).jpg")
            let metadata = StorageMetadata()
            metadata.contentType = "image/jpeg"
            // Upload the file to the path "images/rivers.jpg"
            let uploadTask = riversRef.putData(imageData as Data, metadata: metadata) { (metadata, error) in
                
            }
            
            uploadTask.observe(.success) { snapshot in
                print(snapshot) // Upload completed successfully
                let downloadURL = snapshot.metadata?.downloadURL()
                print(downloadURL!)
                urlString = String(describing: downloadURL!)
                
                if urlString != ""{
                    let DBref = Database.database().reference()
                    //Sender data for group
                    
                    let userId = PFUser.current()!.objectId!
                    
                    let userDictionary = ["user_img":urlString] as [String : Any]
                    let myRef = DBref.child("users/\(userId)")
                    myRef.updateChildValues(userDictionary) { (error, DBref) in
                        if error == nil{
                            print("error == ",error)
                        }
                    }
                }
            }
        
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK:-Get Images from server
    func getImagesFromDb(){
        
        if let userPicture = currentUser?["profile_image"] as? PFFile {
            userPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                let image = UIImage(data: imageData!)
                if image != nil {
                    //print("image value",image!)
                    self.userimageToSend = (image)!
                    self.uploadFlag = true
                    DispatchQueue.main.async {
                        
                        let indexPath = NSIndexPath.init(row: 0, section: 0)
                        self.profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                    }
                }
            })
        }
        
        if let coverPicture = currentUser?["cover_image"] as? PFFile {
            coverPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                let image = UIImage(data: imageData!)
                if image != nil {
                    //print("image value",image!)
                    self.uploadFlag = true
                    self.bgimageToSend = (image)!
                    self.getAllData = true
                    DispatchQueue.main.async {
                        let indexPath = NSIndexPath.init(row: 0, section: 0)
                        self.profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
                    }
                }
            })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver("ImageNotify")
        self.tabBarController?.tabBar.isHidden = false
    }
    
    
    
    func getFollowList(){
        
        if let status = currentUser?.value(forKey: "profile_type") as? String, status == "1" {
            self.getWorkExperience()
            self.postsListApi(isRefresh: true)
        }
        
        let query = PFQuery(className: "User_follow")
        query.whereKey("followed_id", equalTo:currentUser?.objectId! ?? "")
        query.includeKey("parent_follower")
        query.whereKey("status", equalTo:"1")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                //print(objects!)
                self.followArray.removeAllObjects()
                for object in objects! {
                    //print(object)
                    if let singleUser = object.object(forKey: "parent_follower") as? PFUser
                    {
                        //print(singleUser)
                        self.followArray.add(singleUser)
                    }
                }
                
                self.profileAcIndicator.stopAnimating()
                self.profileTable.reloadData()
            }
            else {
                //print(error!)
            }
        })
        
    }
    func getFollowingList(){
        
        let query = PFQuery(className: "User_follow")
        query.whereKey("follower_id", equalTo:currentUser?.objectId! ?? "")
        query.includeKey("parent_following")
        query.whereKey("status", equalTo:"1")
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                //print(objects!)
                self.followingArray.removeAllObjects()
                for object in objects! {
                    //print(object)
                    if let singleUser = object.object(forKey: "parent_following") as? PFUser
                    {
                        print(singleUser)
                        
                        self.followingArray.add(singleUser)
                    }
                }
                self.profileAcIndicator.stopAnimating()
                
                self.profileTable.reloadData()
                
            }
            else {
                //print(error!)
            }
        })
    }
}


extension ProfileViewController
{
    func postsListApi(isRefresh: Bool)
    {
        let objId = currentUser?.objectId ?? ""
        self.profileAcIndicator.startAnimating()
        let query = PFQuery(className: "Post")
        query.order(byDescending: "createdAt")
        query.whereKey("user_id",equalTo:objId)
        query.includeKey("parent_id")
        query.limit = 20
        query.skip = isRefresh ? 0 : self.PFObjectArray.count
        query.findObjectsInBackground(block: { (objects, error) in
            self.refreshControl.endRefreshing()
            self.profileAcIndicator.stopAnimating()
            if error == nil {
                
                if isRefresh {
                    self.PFObjectArray.removeAllObjects()
                }
                
                for object in objects! {
                    object.setObject(false, forKey: "mute")
                    self.PFObjectArray.add(object)
                    self.postExist = true
                    
                    if self.PFObjectArray.count%Constants.addAfter==0
                    {
                        self.PFObjectArray.add(Constants.advertisement)
                    }
                }
                self.profileTable.reloadData()
                
                if objects!.count == 0 && self.PFObjectArray.count > 1 {
                    self.showMessage(messages.noPost)
                    return
                }
            }
        })
    }
    
    //MARK:- postsListFirstRecord
    
    func postsListFirstRecord(indexObj: NSNotification)
    {
        //print(indexObj)
        var arr : NSMutableArray = []
        arr = indexObj.object as! NSMutableArray
        //print(arr)
        let query = PFQuery(className: "Post")
        query.whereKey("objectId", equalTo:arr.object(at: 1))
        query.includeKey("parent_id")
        query.findObjectsInBackground(block: { (objects, error) in
            self.heightOFProgressView.constant = 0
            self.progressVIew.isHidden = true
            //globalBool.showProgess = false
            if error == nil {
                
                for object in objects! {
                    print(object)
                    if arr.object(at: 2) as! Bool == false
                    {
                        self.PFObjectArray.insert(object, at: 0)
                    }else
                    {
                        self.PFObjectArray.replaceObject(at: arr.object(at: 0) as! Int, with: object)
                        
                        let arr = ["Edited",object] as NSArray
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
                    }
                    
                    let indexPath = NSIndexPath.init(row: arr.object(at: 0) as! Int, section: 0)
                    self.profileTable.scrollToRow(at: indexPath as IndexPath , at: .top, animated: true)
                    self.profileTable.reloadData()
                    
                    
                }
            }
            else {
                //print(error!)
            }
        })
    }
    
    
    
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }
    
    
    
    func withUser(index:Int,name:String)
    {
        //print(name)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        if let users_tagged_withArray = (PFObjectArray.object(at: index) as AnyObject).value(forKey: "users_tagged_with") as? NSArray
        {
            if users_tagged_withArray.count>0
            {
                //print(users_tagged_withArray)
                let index = (users_tagged_withArray.value(forKey: "name")  as AnyObject).index(of: name)
                let id = (users_tagged_withArray.object(at: index)as AnyObject).value(forKey: "id") as! String
                //print(id)
                
                var idsArray : [String] = []
                idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
                if idsArray.contains(id)
                {
                    vc.friend = "FOLLOWING"
                }else
                {
                    vc.friend = "FOLLOW"
                }
                
                vc.objectId = id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    
    func otherUser(index:Int,name:String)
    {
        //print(name)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        
        let id = (popUpArray.object(at: index)as AnyObject).value(forKey: "id") as! String
        //print(id)
        
        
        var idsArray : [String] = []
        idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
        if idsArray.contains(id)
        {
            vc.friend = "FOLLOWING"
        }else
        {
            vc.friend = "FOLLOW"
        }
        
        vc.objectId = id
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.popUp.fadeOut()
    }
    
    
    
    
    func descriptionUser(index:Int,userName:String)
    {
        //print(userName)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        if let users_tagged_withArray = (self.PFObjectArray.object(at: index) as AnyObject).value(forKey: "tagged_users_in_desc") as? NSArray
        {
            //print(users_tagged_withArray)
            let index = (users_tagged_withArray.value(forKey: "username")  as AnyObject).index(of: userName)
            let id = (users_tagged_withArray.object(at: index)as AnyObject).value(forKey: "id") as! String
            //print(id)
            
            var idsArray : [String] = []
            idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! [String]
            if idsArray.contains(id)
            {
                vc.friend = "FOLLOWING"
            }else
            {
                vc.friend = "FOLLOW"
            }
            
            vc.objectId = id
            self.navigationController?.pushViewController(vc, animated: true)
        }else
        {
            self.showMessage(messages.notFound)
            return
        }
    }
    
    
    
    func alert(_ title: String, message: String) {
        let vc = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        vc.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        present(vc, animated: true, completion: nil)
    }
    
    
}


extension ProfileViewController
{
    
    func likeListingClicked(tag: Int) {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "LikesViewController") as! LikesViewController
        let id = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "objectId") as! String
        //print(id)
        vc.postId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func likeClicked(sender: UIButton) {
        
        let tag = sender.tag
        sender.isSelected = !sender.isSelected
        var likeArrayObj : NSMutableArray = []
        if let likeArray = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "user_like_post") as? NSMutableArray
        {
            likeArrayObj = likeArray
        }
        let objId = PFUser.current()?.objectId
        let id = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "objectId") as! String
        
        if sender.isSelected && !likeArrayObj.contains(objId ?? ""){
            likeArrayObj.add(objId ?? "")
        } else {
            likeArrayObj.remove(objId ?? "")
        }
        
        let postObj = PFObjectArray.object(at: tag) as! PFObject
        postObj.setValue(likeArrayObj, forKey: "user_like_post")
        PFObjectArray.replaceObject(at: tag, with: postObj)
        self.profileTable.reloadData()
        
        
        let arr = ["Liked",postObj] as NSArray
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
        
        
        let likesQuery = PFQuery(className:"Post_likes")
        likesQuery.whereKey("post_id", equalTo:id)
        likesQuery.whereKey("user_id", equalTo:objId!)
        likesQuery.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                if objects!.count==0
                {
                    let compClass = PFObject(className: "Post_likes")
                    compClass["post_id"] =  id
                    compClass["user_id"] =  objId
                    compClass["parent_id"] =  PFUser.current()
                    compClass.saveInBackground(block: { (status, error) in
                        //print(status)
                        if status ==  true{
                           // likeArrayObj.add(objId!)
                            self.likeArrayUpdate(id: id, likeArr: likeArrayObj, index: tag)
                            
                            if let userId = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "user_id") as? String
                            {
                                
                                let formatter = DateFormatter()
                                formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                                let time = formatter.string(from: Date())
                                let objId = PFUser.current()?.objectId
                                let nameQuery : PFQuery = PFUser.query()!
                                nameQuery.whereKey("objectId", equalTo: userId)
                                nameQuery.findObjectsInBackground(block: { (objects, error) in
                                    for object in objects! {
                                        
                                        self.saveToNotifications(from_id: objId!, to_id: userId, notification_type: "like", time: time, post_id: id, parent_from: PFUser.current()!, parent_to: (object as? PFUser)!)
                                        self.likePushNotification(userId: userId,postId: id)
                                    }
                                })
                                
                            }
                        }
                        else{
                            let message = (error?.localizedDescription)
                            //print(message as Any)
                        }
                    })
                }else
                {
                    //print("liked already")
                    for object in objects! {
                        //print(object)
                        object.deleteInBackground(block: { (status, error) in
                            //print(status)
                            if status == true{
                                likeArrayObj.remove(objId!)
                                self.likeArrayUpdate(id: id, likeArr: likeArrayObj, index: tag)
                            }else{
                                let message = (error?.localizedDescription)
                                //print(message as Any)
                            }
                        })
                    }
                }
            }})
    }
    
    
    func likePushNotification(userId:String,postId:String)
    {
        if !(userId == PFUser.current()!.objectId!)
        {
            let currentName = PFUser.current()?.value(forKey: "name") as! String
            self.sendPushNotification(postId: postId, userId: userId,settingType: "push_post_like",message:"\(currentName) \(messages.likePost)",type:"post_like",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
        }
    }
    
    
    func likeArrayUpdate(id:String,likeArr:NSArray,index:Int)
    {
        ////////////////////////
        let query = PFQuery(className:"Post")
        query.whereKey("objectId", equalTo:id)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                for object in objects! {
                    
                    //print(object)
                    object["user_like_post"] =  likeArr
                    object.saveInBackground(block: { (status, error) in
                        //print(status)
                        if status ==  true{
                            self.updateAPIArray(index: index, likeArr: likeArr)
                        }
                        else{
                            let message = (error?.localizedDescription)
                            //print(message as Any)
                        }
                    })
                }
            }
            else {
                //print(error!)
            }
            
        })
    }
    
    func updateAPIArray(index:Int,likeArr:NSArray)
    {
        //print(likeArr)
        let obj = PFObjectArray.object(at: index) as! PFObject
        obj.setValue(likeArr, forKey: "user_like_post")
        //print(obj)
        PFObjectArray.replaceObject(at: index, with: obj)
        let indexPath = NSIndexPath.init(row: index+1, section: 0)
        self.profileTable.reloadRows(at: [indexPath as IndexPath], with: .none)
        
    }
    
    
    func commentClicked(tag: Int) {
        
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        let id = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "objectId") as! String
        vc.objectFromHome = PFObjectArray.object(at: tag) as! PFObject
        vc.postId = id
        vc.delegate = self
        
        if let userId = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "user_id") as? String
        {
            vc.postUserId = userId
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
//    func commentClicked(tag: Int) {
//        
//        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
//        let vc = storyboardObj.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
//        let id = (PFObjectArray.object(at: tag) as AnyObject).value(forKey: "objectId") as! String
//        vc.objectFromHome = PFObjectArray.object(at: tag) as? PFObject
//        vc.postId = id
//        vc.delegate = self
//        vc.indexOfpost = tag
//        vc.fromProfile = false
//        if let userId = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "user_id") as? String
//        {
//            vc.postUserId = userId
//        }
//        self.navigationController?.pushViewController(vc, animated: true)
//        
//    }
    
    
    
    
    
    
    
    
    
    func URLClicked(tag: Int)
    {
        let storyboardObj = UIStoryboard(name: "HomeSB",bundle: nil)
        let vc = storyboardObj.instantiateViewController(withIdentifier: "MTSWebViewController") as! MTSWebViewController
        
        if let link_url = (self.PFObjectArray.object(at: tag) as AnyObject).object(forKey: "link_url") as? String
        {
            vc.urlStr = link_url
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func moreClicked(tag: Int)
    {
        descriptionIndex = tag
        self.popUp.fadeIn()
//        self.popUpArray = ["SHARE","DELETE","EDIT","CANCEL"]
        
        if let id = (PFObjectArray.object(at: tag) as AnyObject).object(forKey: "user_id") as? String
        {
            let objId = PFUser.current()?.objectId
            if id == objId!
            {
                self.popUpArray = ["SHARE","DELETE","EDIT","CANCEL"]
            }else
            {
                self.popUpArray = ["REPORT ABUSE","SHARE","CANCEL"]
            }
            
        }
        
        if self.popUpArray.count>6
        {
            self.popUpTableViewObj.isScrollEnabled = true
            self.heightOfPopUpTableView.constant = 300
        }else
        {
            let count = self.popUpArray.count
            self.heightOfPopUpTableView.constant = CGFloat(count * 50)
            self.popUpTableViewObj.isScrollEnabled = false
        }
        self.popUpTableViewObj.reloadData()
    }
    
    
    @IBAction func chatAction(_ sender: Any) {
        let chatSB = UIStoryboard.init(name: "ChatSB", bundle: nil)
        let vc = chatSB.instantiateViewController(withIdentifier: "ChatListingViewController")  as! ChatListingViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func userProfileClicked(tag: Int)
    {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        vc.userProfile = false
        //print(index)
        let id = (PFObjectArray.object(at: tag)as AnyObject).value(forKey: "user_id") as! String
        //print(id)
        
        vc.objectId = id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    func imageClicked(tag: Int,object:PFObject)
    {
        if let postmedia = object.object(forKey: "postmedia") as? NSArray
        {
            if postmedia.count>1
            {
                let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
                let vc = CreatePostSB.instantiateViewController(withIdentifier: "CreatePostViewController")  as! CreatePostViewController
                vc.objectForEdit  = object
                vc.edit  = true
                vc.showProfileLogo = true
                let id = object.value(forKey: "objectId") as! String
                vc.postForEdit = id
                vc.indexEdit = tag
                vc.hideEditingOptions = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else if postmedia.count==1
            {
                let CreatePostSB = UIStoryboard.init(name: "CreatePostSB", bundle: nil)
                let vc = CreatePostSB.instantiateViewController(withIdentifier: "SingleMediaViewController")  as! SingleMediaViewController
                vc.objectFormHome = object
                vc.index = 0
                self.isVideoSeen = true
//                self.navigationController?.present(vc, animated: true, completion: { 
//                    print("d")
//                })
               // self.present(vc, animated: true, completion: nil)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}


extension ProfileViewController : commentCountUpdateDelegate
{
    func updateCommentCount(index:Int,value:String)
    {
        let obj = (PFObjectArray.object(at: index) as AnyObject)
        if let object = obj as? PFObject
        {
            object.setValue(value, forKey: "comment_count")
            print(object)
            self.profileTable.reloadData()
            
            let arr = ["CommentCount",object] as NSArray
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateList"), object: arr)
        }
    }
}

extension UIView {
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
