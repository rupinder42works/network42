//
//  RelationShipStatusViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 24/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
var rStatusValueStr:String = ""
class RelationShipStatusViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,MarkRStatusDelegate{

    var statusArray:NSMutableArray = []
    var selectedIndexPath:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "RStatusTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
        statusArray = ["Single","In a relationship","Engaged","Married","In a civil union","In a domestic patnership","In an open relationship","It's complicated","Separated","Divorced","Widowed"]
        tableView.estimatedRowHeight = 70
        tableView.rowHeight = UITableViewAutomaticDimension
    }

    @IBOutlet var tableView: UITableView!
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return statusArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            as! RStatusTableViewCell
        cell.delegate = self
        cell.sRStatusBtn.tag = indexPath.row
         print(selectedIndexPath)
        if let selectedIndexPath = UserDefaults.standard.value(forKey: "indexKey") as? Int{
        if selectedIndexPath == indexPath.row{
             cell.sRStatusBtn.setImage(UIImage(named: "check"), for: .normal)
        }
        }
        cell.titleLbl.text = statusArray.object(at: indexPath.row) as? String
        return cell
        
    }
      func sectionActionClicked(cell:RStatusTableViewCell){
        // Save Data
        cell.sRStatusBtn.isSelected  = !cell.sRStatusBtn.isSelected
        selectedIndexPath = cell.sRStatusBtn.tag
        if (!cell.sRStatusBtn.isSelected)
        {
            NSLog(" Not Selected");
             cell.sRStatusBtn.isSelected = true
            cell.sRStatusBtn.setImage(UIImage(named: "uncheck"), for: .normal)
            
        }
        else
        {
            NSLog(" Selected");
            cell.sRStatusBtn.isSelected = false
             UserDefaults.standard.set(cell.sRStatusBtn.tag,forKey: "indexKey")
            rStatusValueStr = cell.titleLbl.text!
            cell.sRStatusBtn.setImage(UIImage(named: "check"), for: .normal)
            dismiss(animated: true, completion: nil)
        }
        print(selectedIndexPath)
    }
    
    //    func didselectrow
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
    }
    //Cancel Tapped
    @IBAction func cancelTapped(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
}
