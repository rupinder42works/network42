//
//  PostDecriptionCell.swift
//  Network42
//
//  Created by 42works on 31/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI

@objc protocol tagUserNewDelegate {
    func tagUserNewClicked()
}


class PostDescriptionNewCell: UITableViewCell {
    
    
    @IBOutlet var userProfile: PFImageView!
    @IBOutlet var tagBtn: UIButton!
    @IBOutlet var postTextView: UITextView!
    @IBOutlet var heightOfTagButton: NSLayoutConstraint!
    @IBOutlet var nameLbl: NameLabel!
    @IBOutlet var timeLbl: TimeLabel!
    @IBOutlet var heightOfTextView: NSLayoutConstraint!
    @IBOutlet var topOfTag: NSLayoutConstraint!
     weak var delegate: tagUserNewDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        print(self.contentView.subviews)
        
        let when = DispatchTime.now() + 2
        DispatchQueue.main.asyncAfter(deadline: when) {
            for subview in self.contentView.subviews as [UIView] {
                if let UITextView = subview as? UITextView {
                    UITextView.becomeFirstResponder()
                }
            }
        }
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func tagUserAction(_ sender: Any) {
        self.delegate?.tagUserNewClicked()
    }
    
    
}
