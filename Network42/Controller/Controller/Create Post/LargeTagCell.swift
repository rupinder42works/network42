//
//  TagTableViewCell.swift
//  Network42
//
//  Created by 42works on 10/04/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI



class LargeTagCell: UITableViewCell {
    @IBOutlet var profileImage: PFImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var tick: UIImageView!
    @IBOutlet var containerImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
