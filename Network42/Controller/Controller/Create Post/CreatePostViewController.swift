//
//  CreatePostViewController.swift
//  Network42
//
//  Created by 42works on 23/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Photos
import AVKit
import DKImagePickerController
import Parse
import AVFoundation
import Alamofire
import TKSubmitTransitionSwift3
import JPVideoPlayer
import Firebase


class CreatePostViewController: BaseViewController,UIViewControllerTransitioningDelegate {
    
    @IBOutlet var postTitleHeading: HeadingLabel!
    @IBOutlet var profileLogo: UIImageView!
    @IBOutlet var tableViewTop: NSLayoutConstraint!
    @IBOutlet var userButton: UIButton!
    @IBOutlet var cameraButton: UIButton!
    @IBOutlet var heightOfPostTypeTable: NSLayoutConstraint!
    
    @IBOutlet var postButton: TKTransitionSubmitButton!
    
    
    var dropdownArray : NSMutableArray = [[]]
    var iconArray : NSMutableArray = []
    
    var selectedType : String = "Friends"
    
    var hashTagsFromTextview = [String]()
    
    
    var youtubeArray : NSMutableArray = []
    var imagesArray : NSMutableArray = []
    var captionHeightArray : NSMutableArray = []
    var captionArray : NSMutableArray = []
    
    var savedArrayUser : NSMutableArray = []
    
    var newHashTags : NSMutableArray = []
    
    var savedArrayHashTags : NSMutableArray = []
    
    
    @IBOutlet var taggedContainer: UIView!
    
    @IBOutlet var taggedUserTable: UITableView!
    
    @IBOutlet var tableViewObj: UITableView!
    
    @IBOutlet var imageTableView: UITableView!
    
    var forLastRow : Int = 0
    var selectedRow : Int = 0
    
    var minusValue = CGFloat()
    
    var assetsObj: [DKAsset]?
    
    var selectedUser : String = "selectedUser.png"
    var unselectedUser : String = "userUnselected.png"
    
    var taggedArrayUserDefault : NSArray = []
    
    
    var tagNameArray : NSMutableArray = []
    var tagUserNameArray : [String] = []
    var tagProfileImageArray : NSMutableArray = []
    var idsArray : NSMutableArray = []
    
    
    var NewAndOldHashTagIdsArray : NSMutableArray = []
    
    //    var hashTagsGlobalNameArray : [String] = []
    var hashTagsGlobalIdsArray : [String] = []
    
    var searchResultArray :[String] = []
    var searchActive : Bool = false
    
    var replaceString : String = ""
    
    var noVideo : Bool = false
    
    var rangeToAppend = NSRange()
    var removeCount : Int = 0
    
    var currentWord : String = ""
    var updatedWord : String = ""
    
    var globalRange = NSRange()
    var globalInt : Int = 1
    
    
    var taggedUser : Bool = false
    
    //MARK:- Get View Url
    var isImageExist = false
    var isUrlExist = false
    var titleToSend : String! = ""
    var descriptionToSend : String! = ""
    var urlToSend : String! = ""
    var imageToSend : String = ""
    var imageUrlAsString = ""
    
    var postId : String = ""
    var postIdPFObject : PFObject?
    
    var objectForEdit : PFObject?
    var postForEdit : String = ""
    var edit : Bool = false
    var showProfileLogo : Bool = false
    var indexEdit : Int = 0
    
    var showLoader = false
    
    var hideEditingOptions : Bool = false
    @IBOutlet var heightOfBottomBox: NSLayoutConstraint!
    @IBOutlet var bottomBox: UIView!
    var scrollRow : Int = 2
    
    
    
    
    var indexArray : [Int] = []
    
    
    var profilePic = UIImage()
    
    
    var videoFile : NSMutableArray = []
    
    var existHashTag : Bool = false
    
    var fromMyProfile : Bool = false
    
    
    var avPlayer: AVPlayer!
    
    var globalMediaType : NSMutableArray = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //var data =
        idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! NSMutableArray
        tagNameArray =  globalDic.tagUserDictionary.value(forKey: "name") as! NSMutableArray
        tagUserNameArray =  globalDic.tagUserDictionary.value(forKey: "userName")  as! [String]
        tagProfileImageArray =  globalDic.tagUserDictionary.value(forKey: "image") as! NSMutableArray
        
        
        
        self.tabBarController?.tabBar.isHidden = true
        
        if edit
        {
            if let users_tagged_with = objectForEdit?.object(forKey: "users_tagged_with") as? NSArray
            {
                UserDefaults.standard.set(users_tagged_with, forKey: "taggedUser")
                UserDefaults.standard.synchronize()
            }
        }else
        {
            UserDefaults.standard.set(NSMutableArray(), forKey: "taggedUser")
            UserDefaults.standard.synchronize()
        }
        
        
        tableViewObj.register(UINib(nibName: "PostTypeCell", bundle: nil), forCellReuseIdentifier: "cell")
        tableViewObj.register(UINib(nibName: "PostTypeHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "header")
        
        taggedUserTable.register(UINib(nibName: "TagTableViewCell", bundle: nil), forCellReuseIdentifier: "TagTableViewCell")
        
        taggedUserTable.register(UINib(nibName: "HashTagCell", bundle: nil), forCellReuseIdentifier: "HashTagCell")
        
        taggedUserTable.rowHeight = UITableViewAutomaticDimension
        taggedUserTable.estimatedRowHeight = 702
        
        
        
        imageTableView.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
        imageTableView.register(UINib(nibName: "AddImageAndVideos", bundle: nil), forCellReuseIdentifier: "AddImageAndVideos")
        imageTableView.register(UINib(nibName: "PostDecriptionCell", bundle: nil), forCellReuseIdentifier: "PostDecriptionCell")
        imageTableView.register(UINib(nibName: "PostDescriptionNewCell", bundle: nil), forCellReuseIdentifier: "PostDescriptionNewCell")
        imageTableView.register(UINib(nibName: "URLTableViewCell", bundle: nil), forCellReuseIdentifier: "URLTableViewCell")
        
        
        iconArray = [UIImage.init(named: "friend.png")! as UIImage,UIImage.init(named: "globe.png")! as UIImage]
        
        
        imageTableView.rowHeight = UITableViewAutomaticDimension
        imageTableView.estimatedRowHeight = 702
        
        
        //heightOfPostTypeTable.constant = 50
        heightOfPostTypeTable.constant = 0
        if edit
        {
            print(objectForEdit!)
            if let descriptionObj = objectForEdit?.object(forKey: "description") as? String
            {
                self.captionArray.add(descriptionObj)
            }
            self.captionArray.add("")
            self.imagesArray.add("dummyProfile")
            self.youtubeArray.add("dummyProfile")
            self.imagesArray.add("dummyProfile")
            self.youtubeArray.add("dummyProfile")
            
            
            
            if let post_type = objectForEdit?.object(forKey: "post_type") as? String
            {
                if post_type == "ALBUM" || post_type == "IMAGE" || post_type == "VIDEO"
                {
                    
                    if let postmedia = objectForEdit?.object(forKey: "postmedia") as? NSArray
                    {
                        for i in 0..<postmedia.count
                        {
                            if let fileObj = postmedia.object(at: i) as? PFFile
                            {
                                if let media_typesArr = objectForEdit?.object(forKey: "media_types") as? NSArray
                                {
                                    
                                    globalMediaType = media_typesArr as! NSMutableArray
                                    if let media_types = media_typesArr.object(at: i) as? String
                                    {
                                        if media_types == "VIDEO"
                                        {
                                            videoFile.add(fileObj)
                                            //                                            let url = NSURL (string: fileObj.url!)
                                            //                                            let img = generateThumbImage(url: url!)
                                            
                                            
                                            if let thumbnail = objectForEdit?.object(forKey: "thumbnail") as? NSArray
                                            {
                                                if let fileObj = thumbnail.object(at: i) as? PFFile
                                                {
                                                    do {
                                                        let image = try UIImage(data:fileObj.getData())
                                                        self.imagesArray.add(image!)
                                                    }
                                                    catch let error as NSError
                                                    {
                                                        print("Image generation failed with error \(error)")
                                                    }
                                                }
                                            }else
                                            {
                                                let image = UIImage.init(named: "black.png")
                                                self.imagesArray.add(image!)
                                            }
                                            
                                            
                                            
                                            if self.imagesArray.count == postmedia.count+2
                                            {
                                                self.imageTableView.reloadData()
                                            }
                                        }else
                                        {
                                            videoFile.add("")
                                            do {
                                                let image = try UIImage(data:fileObj.getData())
                                                self.imagesArray.add(image!)
                                            }
                                            catch let error as NSError
                                            {
                                                print("Image generation failed with error \(error)")
                                            }
                                            
                                            if self.imagesArray.count == postmedia.count+2
                                            {
                                                self.imageTableView.reloadData()
                                            }
                                        }
                                        
                                        self.youtubeArray.add("dummyProfile")
                                        if let media_description = self.objectForEdit?.object(forKey: "media_description") as? NSArray
                                        {
                                            self.captionArray.add(media_description.object(at: i) as! String)
                                        }
                                        
                                    }
                                }
                            }
                        }
                    }
                }else if post_type == "URL"
                {
                    
                    self.isUrlExist = true
                    if let link_titleObj = objectForEdit?.object(forKey: "link_title") as? String
                    {
                        titleToSend = link_titleObj
                    }
                    if let link_descriptionObj = objectForEdit?.object(forKey: "link_description") as? String
                    {
                        descriptionToSend = link_descriptionObj
                    }
                    if let link_urlObj = objectForEdit?.object(forKey: "link_url") as? String
                    {
                        urlToSend = link_urlObj
                    }
                    if let link_image_urlObj = objectForEdit?.object(forKey: "link_image_url") as? String
                    {
                        isImageExist = true
                        imageToSend = link_image_urlObj
                    }
                    self.imageTableView.reloadData()
                    
                }
            }
            
            
            
        }else
        {
            self.captionArray.add("")
            self.imagesArray.add("dummyProfile")
            self.youtubeArray.add("dummyProfile")
            self.captionArray.add("")
            self.imagesArray.add("dummyProfile")
            self.youtubeArray.add("dummyProfile")
        }
        
        self.captionHeightArray.add(CGFloat(75))
        self.captionHeightArray.add(CGFloat(75))
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(CreatePostViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = true
        imageTableView.addGestureRecognizer(tapGesture)
        
        
        
        
        if let userPicture = PFUser.current()?["profile_image"] as? PFFile {
            userPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                let image = UIImage(data: imageData!)
                if image != nil {
                    self.profilePic = (image)!
                }
            })
        }else
        {
            self.profilePic = UIImage.init(named: "dummyProfile")!
        }
        
        
        
        if hideEditingOptions
        {
            postButton.isHidden = true
            bottomBox.isHidden = true
            tableViewTop.constant = 0
            self.tableViewObj.isHidden = true
        }
        
        
        
        
        if edit
        {
            playVideoInVisiableCells()
        }
        
        if showProfileLogo
        {
         profileLogo.isHidden = false
            postTitleHeading.isHidden = true
        }
        
    }
    
    //MARK:- Thumbnail
    func generateThumbImage(url : NSURL) -> UIImage{
        let asset : AVAsset = AVAsset.init(url: url as URL)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time        : CMTime = CMTimeMake(1, 30)
        do {
            let imageRef = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        }
        catch let error as NSError
        {
            print("Image generation failed with error \(error)")
            return UIImage()
        }
    }
    
    
    
    func hideKeyboard() {
        dropdownArray.replaceObject(at: 0, with: [])
        //heightOfPostTypeTable.constant = 50
        heightOfPostTypeTable.constant = 0
        self.tableViewObj.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let completeArray = UserDefaults.standard.object(forKey: "taggedUser") as! NSArray
        print(completeArray)
        taggedArrayUserDefault = completeArray.value(forKey: "name") as! NSArray
        print(taggedArrayUserDefault)
        if taggedArrayUserDefault.count>0
        {
            self.userButton.setImage(UIImage.init(named: selectedUser), for: .normal)
        }else
        {
            self.userButton.setImage(UIImage.init(named: unselectedUser), for: .normal)
        }
        
        imageTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        taggedContainer.isHidden = true
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        JPVideoPlayerManager.shared().stopPlay()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func sectionAction(_ sender: Any)
    {
        if (dropdownArray.object(at: 0) as AnyObject).count == 0
        {
            self.view.endEditing(true)
            heightOfPostTypeTable.constant = 150
            dropdownArray.replaceObject(at: 0, with: ["Friends","Public"])
        }else
        {
            //heightOfPostTypeTable.constant = 50
            heightOfPostTypeTable.constant = 0
            dropdownArray.replaceObject(at: 0, with: [])
        }
        self.tableViewObj.reloadData()
    }
    
    @IBAction func tagUser(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagUserViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    @IBAction func showImagePicker(_ sender: UIButton) {
        
        self.showGallery()
        
    }
    
    
    func showGallery()
    {
        self.imagesArray.removeAllObjects()
        print(self.captionArray)
        
        let postText = captionArray.object(at: 0) as! String
        
        
        self.captionArray.removeAllObjects()
        self.youtubeArray.removeAllObjects()
        
        self.captionArray.add(postText)
        self.imagesArray.add("dummyProfile")
        self.youtubeArray.add("dummyProfile")
        
        self.captionArray.add(postText)
        self.imagesArray.add("dummyProfile")
        self.youtubeArray.add("dummyProfile")
        
        
        let pickerController = DKImagePickerController()
        pickerController.maxSelectableCount = 12-imagesArray.count
        pickerController.defaultSelectedAssets = self.assetsObj
        pickerController.didSelectAssets = { [unowned self] (assets: [DKAsset]) in
            
            
            print("Finish: \(assets)")
            self.assetsObj = assets
            
            for i in 0..<assets.count
            {
                self.titleToSend = ""
                
                let asset = self.assetsObj![i]
                
                asset.fetchOriginalImage(true, completeBlock: { (image, info) in
                    print("video===",asset.isVideo)
                    if asset.isVideo {
                        print("===",i)
                        self.youtubeArray.add(UIImage.init(named: "youtube.png")!)
                    }else
                    {
                        self.youtubeArray.add("")
                    }
                    
                    self.imagesArray.add(image!)
                    self.captionArray.add("")
                    self.captionHeightArray.add(CGFloat(33))
                    
                    if assets.count < self.imagesArray.count
                    {
                        if self.imagesArray.count>2
                        {
                            self.isUrlExist = false
                            self.forLastRow = 1
                            self.cameraButton.setImage(UIImage.init(named: "cameraSelected.png"), for: .normal)
                        }else
                        {
                            self.forLastRow = 0
                            self.cameraButton.setImage(UIImage.init(named: "cameraUnselected.png"), for: .normal)
                        }
                        
                        
                        if self.imagesArray.count==12
                        {
                            self.forLastRow = 0
                        }
                        self.imageTableView.reloadData()
                    }
                    
                })
                
            }
            
            
            if assets.count==0
            {
                self.forLastRow = 0
                self.cameraButton.setImage(UIImage.init(named: "cameraUnselected.png"), for: .normal)
                self.imageTableView.reloadData()
            }
            
        }
        
        if UI_USER_INTERFACE_IDIOM() == .pad {
            pickerController.modalPresentationStyle = .formSheet
        }
        
        self.present(pickerController, animated: true) {}
    }
    
    
    
    @IBAction func backButtonAction(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    //MARK:- savePost
    @IBAction func savePost(_ sender: Any) {
        self.view.endEditing(true)
        
        print("caption = ",captionArray.object(at: 0) as! String)
        print("titleToSend = ",titleToSend)
        print("imagesArray.count = ",imagesArray.count)
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        if taggedArrayUserDefault.count==0 && captionArray.object(at: 0) as! String == "" && titleToSend=="" && imagesArray.count==2{
            print("no post")
        }
        else{
            print("proceed")
            postButton.startLoadingAnimation()
            self.activityIndicatorBK.isHidden = false
            
            
            
            
            var postClass = PFObject(className: "Post")
            if edit
            {
                let queryComp = PFQuery(className: "Post")
                queryComp.whereKey("objectId", equalTo:postForEdit)
                queryComp.findObjectsInBackground(block: { (objects, error) in
                    if error == nil {
                        print(objects?.count as Any)
                        
                        for object in objects! {
                            postClass = object
                            self.post(postClass: postClass)
                            self.saveHashTags()
                        }
                    }
                    else{
                        let message = (error?.localizedDescription)
                        print(message as Any)
                    }
                })
            }else
            {
                postClass = PFObject(className: "Post")
                self.post(postClass: postClass)
                self.saveHashTags()
            }
        }
    }
    
    func post(postClass:PFObject)
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        let objId = PFUser.current()?.objectId
        
        let description = captionArray.object(at: 0) as! String
        let trimmeddescription = description.trimmingCharacters(in: .whitespacesAndNewlines)
        print(trimmeddescription)
        
        if !edit {
            postClass["time_stamp"] = formatter.string(from: Date())
        }
        postClass["like_count"] = "0"
        postClass["comment_count"] = "0"
        postClass["description"] = trimmeddescription
        postClass["user_id"] = objId
        postClass["post_privacy"] = selectedType
        
        postClass["name"] = PFUser.current()?.value(forKey: "name") as? String
        postClass["user_name"] = PFUser.current()?.value(forKey: "user_name") as! String
        
        postClass["parent_id"] = PFUser.current()
        
        
        
        
        
        let completeArray = UserDefaults.standard.object(forKey: "taggedUser") as! NSArray
        if completeArray.count>0
        {
            postClass["users_tagged_with"] = completeArray
        }else
        {
            let tempArr : NSArray = []
            postClass["users_tagged_with"] = tempArr
        }
        
        if savedArrayUser.count>0
        {
            postClass["tagged_users_in_desc"] = savedArrayUser
        }
        
        
        
        if !(titleToSend=="") {
            postClass["link_title"] = titleToSend
            postClass["link_description"] = descriptionToSend
            postClass["link_url"] = urlToSend
            postClass["link_image_url"] = imageToSend
            postClass["post_type"] = "URL"
            self.savePostInBG(postClassObj: postClass)
        }else if(imagesArray.count>=3)
        {
            //This whole block is for posting images and videos
            
            if (imagesArray.count==3)
            {
                var mediaExist : Bool = false
                if let media_typesArr = objectForEdit?.object(forKey: "media_types") as? NSArray
                {
                    mediaExist = true
                }
                // print(self.assetsObj!)
                if !(self.assetsObj == nil) || mediaExist == false
                {
                    let asset = self.assetsObj![0]
                    // This block is for posting either single image or single video
                    if asset.isVideo
                    {
                        postClass["post_type"] = "VIDEO"
                        postClass["media_description"] = [captionArray.object(at: 0) as! String]
                        postClass["media_types"] = ["VIDEO"]
                        print(assetsObj!)
                        asset.fetchAVAssetWithCompleteBlock { (avAsset, info) in
                            DispatchQueue.main.async(execute: { () in
                                print(avAsset!)
                                let asset = avAsset! as? AVURLAsset
                                let data = NSData(contentsOf: (asset?.url)!)
                                print("size = ",Double(data!.length/1048576))
                                let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
                                self.compressVideo(inputURL: (asset?.url)!, outputURL: compressedURL, handler: { (session) in
                                    print(session!)
                                    let data = NSData(contentsOf: compressedURL)
                                    print("size = ",Double(data!.length/1048576))
                                    let imageFile = PFFile(name: "image.mp4", data: data! as Data)
                                    postClass["postmedia"] = [imageFile]
                                    
                                    let image = self.imagesArray[2] as! UIImage
                                    let imageData = image.mediumQualityJPEGNSData
                                    let thumbnail = PFFile(name: "image.png", data: imageData as Data)
                                    postClass["thumbnail"] = [thumbnail]
                                    
                                    self.savePostInBG(postClassObj: postClass)
                                })
                            })
                        }
                        
                    }else
                    {
                        postClass["post_type"] = "IMAGE"
                        postClass["media_description"] = [captionArray.object(at: 0) as! String]
                        postClass["media_types"] = ["IMAGE"]
                        let image = imagesArray[2] as! UIImage
                        let imageData = image.mediumQualityJPEGNSData
                        let imageFile = PFFile(name: "image.png", data: imageData as Data)
                        postClass["postmedia"] = [imageFile]
                        self.savePostInBG(postClassObj: postClass)
                    }
                }else
                {
                    print(objectForEdit!)
                    
                    if let media_typesArr = objectForEdit?.object(forKey: "media_types") as? NSArray
                    {
                        if let media_types = media_typesArr.object(at: 0) as? String
                        {
                            if media_types == "VIDEO"
                            {
                                postClass["post_type"] = "VIDEO"
                                postClass["media_description"] = [captionArray.object(at: 0) as! String]
                                postClass["media_types"] = ["VIDEO"]
                                postClass["postmedia"] = videoFile
                                
                                
                                let image = self.imagesArray[2] as! UIImage
                                let imageData = image.mediumQualityJPEGNSData
                                let thumbnail = PFFile(name: "image.png", data: imageData as Data)
                                postClass["thumbnail"] = [thumbnail]
                                
                                
                                self.savePostInBG(postClassObj: postClass)
                            }else
                            {
                                postClass["post_type"] = "IMAGE"
                                postClass["media_description"] = [captionArray.object(at: 0) as! String]
                                postClass["media_types"] = ["IMAGE"]
                                let image = imagesArray[2] as! UIImage
                                let imageData = image.mediumQualityJPEGNSData
                                let imageFile = PFFile(name: "image.png", data: imageData as Data)
                                postClass["postmedia"] = [imageFile]
                                self.savePostInBG(postClassObj: postClass)
                            }
                            
                        }
                        
                    }
                }
                
                
            }else{
                // This block is for posting an album
                postClass["post_type"] = "ALBUM"
                var media_typeArray = [String]()
                var media_descriptionArray = [String]()
                var postmediaArray = [PFFile]()
                
                var thumbnailArray = [PFFile]()
                
                for i in 0..<(imagesArray.count-2)
                {
                    media_descriptionArray.append(captionArray.object(at: (i+2)) as! String)
                    if !(self.assetsObj == nil)
                    {
                        let asset = self.assetsObj![i]
                        if asset.isVideo
                        {
                            asset.fetchAVAsset(true, options: nil, completeBlock: { (avAsset, info) in
                                print(avAsset!)
                                let asset = avAsset! as? AVURLAsset
                                let data = NSData(contentsOf: (asset?.url)!)
                                print("original size = ",Double(data!.length/1048576))
                                let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
                                self.compressVideo(inputURL: (asset?.url)!, outputURL: compressedURL, handler: { (session) in
                                    print(session!)
                                    let data = NSData(contentsOf: compressedURL)
                                    print("compressed size = ",Double(data!.length/1048576))
                                    let imageFile = PFFile(name: "image.mp4", data: data! as Data)
                                    postmediaArray.append(imageFile!)
                                    media_typeArray.append("VIDEO")
                                    
                                    
                                    let image = self.imagesArray[(i+2)] as! UIImage
                                    let imageData = image.mediumQualityJPEGNSData
                                    let thumbnail = PFFile(name: "image.png", data: imageData as Data)
                                    thumbnailArray.append(thumbnail!)
                                    
                                    
                                    if (self.imagesArray.count-2)==postmediaArray.count
                                    {
                                        postClass["media_description"] = media_descriptionArray
                                        postClass["media_types"] = media_typeArray
                                        postClass["postmedia"] = postmediaArray
                                        postClass["thumbnail"] = thumbnailArray
                                        self.savePostInBG(postClassObj: postClass)
                                    }
                                })
                            })
                        }else
                        {
                            media_typeArray.append("IMAGE")
                            let image = imagesArray[(i+2)] as! UIImage
                            let imageData = image.mediumQualityJPEGNSData
                            let imageFile = PFFile(name: "image.png", data: imageData as Data)
                            postmediaArray.append(imageFile!)
                            thumbnailArray.append(imageFile!)
                            
                            if (self.imagesArray.count-2)==postmediaArray.count
                            {
                                postClass["media_description"] = media_descriptionArray
                                postClass["media_types"] = media_typeArray
                                postClass["postmedia"] = postmediaArray
                                postClass["thumbnail"] = thumbnailArray
                                self.savePostInBG(postClassObj: postClass)
                            }
                        }
                        
                    }else
                    {
                        
                        //                        if let media_typesArr = objectForEdit?.object(forKey: "media_types") as? NSArray
                        //                        {
                        if let media_types = globalMediaType.object(at: i) as? String
                        {
                            let image = self.imagesArray[(i+2)] as! UIImage
                            let imageData = image.mediumQualityJPEGNSData
                            let thumbnail = PFFile(name: "image.png", data: imageData as Data)
                            thumbnailArray.append(thumbnail!)
                            
                            if media_types == "VIDEO"
                            {
                                media_typeArray.append("VIDEO")
                                postmediaArray.append(videoFile.object(at: i) as! PFFile)
                            }else
                            {
                                media_typeArray.append("IMAGE")
                                let image = imagesArray[(i+2)] as! UIImage
                                let imageData = image.mediumQualityJPEGNSData
                                let imageFile = PFFile(name: "image.png", data: imageData as Data)
                                postmediaArray.append(imageFile!)
                            }
                        }
                        //                        }
                        
                        
                        
                        if (self.imagesArray.count-2)==postmediaArray.count
                        {
                            postClass["media_description"] = media_descriptionArray
                            postClass["media_types"] = media_typeArray
                            postClass["postmedia"] = postmediaArray
                            postClass["thumbnail"] = thumbnailArray
                            self.savePostInBG(postClassObj: postClass)
                        }
                        
                    }
                }
            }
        }
        else
        {
            postClass["post_type"] = "TEXT"
            self.savePostInBG(postClassObj: postClass)
        }
        globalBool.showProgess = true
        _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    
    func taggedUserPushNotification(userIdArray:NSArray,postId:String,pushType:String,message:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        var count = 0
        let arrayOfUsers = NSMutableArray()
        
        for (_, element) in userIdArray.enumerated() {
            
            let childPath = "chatPush/\(element)/withUser"
            
            Database.database().reference().child(childPath).observeSingleEvent(of: .value, with: { (snapshot) in
                
                print("...., \(element)")
                
                if let pushEnabled = snapshot.value as? Bool {
                    if pushEnabled {
                        arrayOfUsers.add(element)
                    }
                } else {
                    arrayOfUsers.add(element)
                }
                
                count += 1
                
                if count == userIdArray.count {
                    self.sendPushNotification(postId: postId, userId: "",settingType: "push_post_tag",message:message,type:pushType,userIdArray:arrayOfUsers,groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                }
            })
        }
    }
    
    
    func saveHashTags()
    {
        
        let str : NSMutableString =   NSMutableString(string: captionArray.object(at: 0) as! String)
        var strArray = str.components(separatedBy: " ")
        
        for i in 0..<strArray.count
        {
            let word = strArray[i]
            if word.contains("#")
            {
                hashTagsFromTextview.append(word)
            }
        }
        
        hashTagsFromTextview = Array(Set(hashTagsFromTextview))
        
        NewAndOldHashTagIdsArray = NSMutableArray()
        for i in 0..<hashTagsFromTextview.count
        {
            let word = hashTagsFromTextview[i]
            self.hashTagExist(name: word)
        }
    }
    
    
    func hashTagExist(name:String) {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "Hashtags")
        query.whereKey("name", equalTo: name)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                print(objects!)
                
                if objects!.count>0
                {
                    self.existHashTag = true
                    
                    //                    let indexOfWord = self.searchResultArray.index(of: name )
                    //                    self.NewAndOldHashTagIdsArray.add(self.hashTagsGlobalIdsArray[indexOfWord!])
                    let objectId = objects?[0].value(forKey: "objectId")
                    self.NewAndOldHashTagIdsArray.add(objectId!)
                    //self.compareHashTags()   // new hash tag will not added
                    
                }else
                {
                    self.existHashTag = false
                    
                    let hashtagsClass = PFObject(className: "Hashtags")
                    hashtagsClass["name"] = name
                    hashtagsClass.saveInBackground { (success, error) in
                        if(success)
                        {
                            print("hashtag saved")
                            self.NewAndOldHashTagIdsArray.add(hashtagsClass.objectId!)
                            self.compareHashTags()
                        }
                        else{
                            print("error",error!)
                            return
                        }
                    }
                    
                    
                }
                
                
            }else {
                print(error!)
                self.existHashTag = false
            }
        })
        
    }
    
    
    
    
    
    func compareHashTags()
    {
        if hashTagsFromTextview.count == self.NewAndOldHashTagIdsArray.count
        {
            if !(self.postId == "")
            {
                self.saveHashTagPost(postId: self.postId, postPFObject: self.postIdPFObject!, hashTagIds: self.NewAndOldHashTagIdsArray)
            }
        }
    }
    
    func saveHashTagPost(postId:String, postPFObject:PFObject ,hashTagIds:NSMutableArray)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        for i in 0..<hashTagIds.count
        {
            let hashTagPostsClass = PFObject(className: "Hashtag_posts")
            hashTagPostsClass["post_id"] = postId
            hashTagPostsClass["parent_post"] = postPFObject
            hashTagPostsClass["post_user"] = PFUser.current()
            print(hashTagIds.object(at: i))
            hashTagPostsClass["hashtag_id"] = hashTagIds.object(at: i) as! String
            hashTagPostsClass.saveInBackground { (success, error) in
                if(success)
                {
                    print("hashtag post saved")
                }
                else{
                    print("error",error!)
                    return
                }
            }
        }
        
    }
    
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileTypeQuickTimeMovie
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
    
    func savePostInBG(postClassObj:PFObject)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        postClassObj.saveInBackground { (success, error) in
            if(success)
            {
                print("Post saved")
                
                let completeArray = UserDefaults.standard.object(forKey: "taggedUser") as! NSArray
                if completeArray.count>0
                {
                    let userIdArray = completeArray.value(forKey: "id") as! NSArray
                    
                    var count : Int = 0
                    for userId in userIdArray
                    {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                        
                        let nameQuery : PFQuery = PFUser.query()!
                        nameQuery.whereKey("objectId", equalTo: userId)
                        nameQuery.findObjectsInBackground(block: { (objects, error) in
                            print(objects!)
                            count = count+1
                            let obj = objects![0] as? PFUser
                            let objId = PFUser.current()?.objectId
                            self.saveToNotifications(from_id: objId!, to_id: userId as! String, notification_type: "user_tag", time: formatter.string(from: Date()), post_id: self.postId, parent_from: PFUser.current()!, parent_to: obj!)
                        })
                    }
                    
                    
                    let currentName = PFUser.current()?.value(forKey: "name") as? String ?? ""
                    self.taggedUserPushNotification(userIdArray: userIdArray, postId: postClassObj.objectId!, pushType: "user_tag", message: "\(currentName) \(messages.userTagged)" )
                }
                
                if self.savedArrayUser.count>0
                {
                    let userIdArray = self.savedArrayUser.value(forKey: "id") as! NSArray
                    
                    var count : Int = 0
                    for userId in userIdArray
                    {
                        let formatter = DateFormatter()
                        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                        
                        let nameQuery : PFQuery = PFUser.query()!
                        nameQuery.whereKey("objectId", equalTo: userId)
                        nameQuery.findObjectsInBackground(block: { (objects, error) in
                            print(objects!)
                            count = count+1
                            let obj = objects![0] as? PFUser
                            let objId = PFUser.current()?.objectId
                            self.saveToNotifications(from_id: objId!, to_id: userId as! String, notification_type: "post_tag", time: formatter.string(from: Date()), post_id: self.postId, parent_from: PFUser.current()!, parent_to: obj!)
                        })
                    }
                    
                    let currentName = PFUser.current()?.value(forKey: "name") as? String ?? ""
                    self.taggedUserPushNotification(userIdArray: userIdArray, postId: postClassObj.objectId!, pushType: "post_tag", message: "\(currentName) \(messages.userTagged)")
                    
                }
                
                let arr : NSMutableArray = [self.indexEdit,postClassObj.objectId!,self.edit]
                
                if self.fromMyProfile
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postSuccessMyProfile"), object: arr)
                }else
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "postSuccess"), object: arr)
                }
                self.showMessage("Post saved successfully.")
                self.postId = postClassObj.objectId!
                self.postIdPFObject = postClassObj
                self.compareHashTags()
            }
            else{
                print("error",error!)
                self.showMessage((error?.localizedDescription)!)
                self.postButton.returnToOriginalState()
                self.activityIndicatorBK.isHidden = true
                self.postButton.layer.cornerRadius = self.postButton.normalCornerRadius!
                return
            }
        }
    }
    
    
    
    // MARK: UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
}


extension UIImage {
    var uncompressedPNGData: NSData      { return     UIImagePNGRepresentation(self)! as NSData        }
    var highestQualityJPEGNSData: NSData { return UIImageJPEGRepresentation(self, 1.0)! as NSData  }
    var highQualityJPEGNSData: NSData    { return UIImageJPEGRepresentation(self, 0.75)! as NSData }
    var mediumQualityJPEGNSData: NSData  { return UIImageJPEGRepresentation(self, 0.5)! as NSData  }
    var lowQualityJPEGNSData: NSData     { return UIImageJPEGRepresentation(self, 0.25)! as NSData }
    var lowestQualityJPEGNSData:NSData   { return
        UIImageJPEGRepresentation(self, 0.0)! as NSData  }
}



extension CreatePostViewController:UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,crossButtonDelegate,videoPhotoDelegate,tagUserDelegate,crossUrlDelegate,tagUserNewDelegate
{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if tableView==tableViewObj
        {
            let PostTypeHeaderObj = self.tableViewObj.dequeueReusableHeaderFooterView(withIdentifier: "header") as! PostTypeHeader
            PostTypeHeaderObj.titleLbl.text = selectedType
            PostTypeHeaderObj.titleLbl.textColor = ConstantColors.themeColor
            return PostTypeHeaderObj
        }else
        {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView==tableViewObj
        {
            return 50
        }else
        {
            return 0
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if tableView==tableViewObj
        {
            return 10
        }else
        {
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView==tableViewObj
        {
            return 50
        }
        else
        {
            return UITableViewAutomaticDimension
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView==tableViewObj
        {
            return (dropdownArray.object(at: 0) as AnyObject).count
        }else if tableView==taggedUserTable
        {
            if searchActive
            {
                return searchResultArray.count
            }
            return tagNameArray.count
        }
        else
        {
            let value = (imagesArray.count+forLastRow) as Int
            print(value)
            print(imagesArray.count)
            return value
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView==tableViewObj
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath as IndexPath) as! PostTypeCell
            cell.titleLbl.text = (dropdownArray.object(at: 0) as AnyObject).objectAt(indexPath.row) as? String
            cell.titleLbl.textColor = ConstantColors.themeColor
            cell.typeImage.image = iconArray.object(at: indexPath.row) as? UIImage
            if indexPath.row == selectedRow
            {
                cell.tickImage.image = UIImage.init(named: "tick.png")! as UIImage
            }else
            {
                cell.tickImage.image = UIImage.init(named: "white.png")! as UIImage
            }
            cell.selectionStyle = .none
            return cell
        }else if tableView == taggedUserTable
        {   
            if searchActive
            {
                if taggedUser
                {
                    let   cell = tableView.dequeueReusableCell(withIdentifier: "TagTableViewCell", for: indexPath as IndexPath) as! TagTableViewCell
                    let indexObj = indexArray[indexPath.row]
                    cell.name.text = tagNameArray.object(at: indexObj) as? String
                    cell.userName.text = tagUserNameArray[indexObj]
                    if let file = tagProfileImageArray.object(at: indexObj) as? PFFile
                    {
                        cell.profileImage.file = file
                        cell.profileImage.loadInBackground()
                    }else
                    {
                        cell.profileImage.image = UIImage.init(named: "dummyProfile")
                    }
                    cell.selectionStyle = .none
                    return cell
                    
                }else
                {
                    let  cell = tableView.dequeueReusableCell(withIdentifier: "HashTagCell", for: indexPath as IndexPath) as! HashTagCell
                    cell.name.text = searchResultArray[indexPath.row]
                    cell.selectionStyle = .none
                    return cell
                    
                }
            }
            else
            {
                let   cell = tableView.dequeueReusableCell(withIdentifier: "TagTableViewCell", for: indexPath as IndexPath) as! TagTableViewCell
                cell.name.text = tagNameArray.object(at: indexPath.row) as? String
                cell.userName.text = tagUserNameArray[indexPath.row]
                if let file = tagProfileImageArray.object(at: indexPath.row) as? PFFile
                {
                    cell.profileImage.file = file
                    cell.profileImage.loadInBackground()
                }else
                {
                    cell.profileImage.image = UIImage.init(named: "dummyProfile")
                }
                cell.selectionStyle = .none
                return cell
            }
        }
            
        else
        {
            if  indexPath.row == 0 {
                 //MARK:- Post Description
                if hideEditingOptions == false
                {
                    let PostDecriptionCell = tableView.dequeueReusableCell(withIdentifier: "PostDecriptionCell", for: indexPath as IndexPath) as! PostDecriptionCell
                    
                    if let userPicture = PFUser.current()?["profile_image"] as? PFFile {
                        userPicture.getDataInBackground({ (imageData: Data?, error: Error?) -> Void in
                            let image = UIImage(data: imageData!)
                            if image != nil {
                                PostDecriptionCell.userProfile.image = (image)!
                            }
                        })
                    }else
                    {
                        PostDecriptionCell.userProfile.image = UIImage.init(named: "dummyProfile")
                    }
                    
                    PostDecriptionCell.postTextView.delegate = self
                    PostDecriptionCell.postTextView.tag = (indexPath.row+10)
                    PostDecriptionCell.postTextView.text = captionArray.object(at: indexPath.row) as! String
                    PostDecriptionCell.selectionStyle = .none
                    if taggedArrayUserDefault.count>0
                    {
                        PostDecriptionCell.heightOfTagButton.constant = 15
                        PostDecriptionCell.delegate = self
                        var btnTitle : String = ""
                        if taggedArrayUserDefault.count == 1
                        {
                            btnTitle = "- with \(taggedArrayUserDefault.object(at: 0) as! String)"
                        }else if taggedArrayUserDefault.count == 2
                        {
                            btnTitle = "- with \(taggedArrayUserDefault.object(at: 0) as! String) and \(taggedArrayUserDefault.object(at: 1) as! String)"
                        }else
                        {
                            btnTitle = "- with \(taggedArrayUserDefault.object(at: 0) as! String) and \(taggedArrayUserDefault.count-1) others"
                        }
                        
                        let attrString: NSMutableAttributedString = NSMutableAttributedString(string: btnTitle)
                        attrString.addAttribute(NSFontAttributeName, value: UIFont.init(name: "SanFranciscoText-Regular", size: 14)!, range: NSMakeRange(0, attrString.length))
                        attrString.addAttribute(NSForegroundColorAttributeName, value: ConstantColors.themeColor, range: NSMakeRange(0, attrString.length))
                        
                        let range2 = (btnTitle as NSString).range(of: "- with")
                        attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: range2)
                        
                        let range3 = (btnTitle as NSString).range(of: "and")
                        attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: range3)
                        PostDecriptionCell.tagBtn.setAttributedTitle(attrString, for: .normal)
                    }else
                    {
                        PostDecriptionCell.heightOfTagButton.constant = 0
                        let attrString: NSMutableAttributedString = NSMutableAttributedString(string: "")
                        PostDecriptionCell.tagBtn.setAttributedTitle(attrString, for: .normal)
                    }
                    if hideEditingOptions
                    {
                        PostDecriptionCell.postTextView.isEditable = false
                    }
                    return PostDecriptionCell
                    
                } else {
                    let PostDecriptionCell = tableView.dequeueReusableCell(withIdentifier: "PostDescriptionNewCell", for: indexPath as IndexPath) as! PostDescriptionNewCell
                    
                    if let user = objectForEdit?.object(forKey: "parent_id") as? PFUser {
                        if let nameObj = user.object(forKey: "name") as? String
                        {
                            PostDecriptionCell.nameLbl.text = nameObj
                        }
                        
                        if let profileImageFile = user.object(forKey: "profile_image") as? PFFile
                        {
                            PostDecriptionCell.userProfile.file = profileImageFile
                            PostDecriptionCell.userProfile.loadInBackground()
                        }else
                        {
                            PostDecriptionCell.userProfile.image =  UIImage.init(named: "dummyProfile")
                        }
                    }
                    
                    
                    if let timeStampObj = objectForEdit?.object(forKey: "time_stamp") as? String
                    {
                        let newdateStr = timeStampObj.replacingOccurrences(of: "_", with: " ")
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
                        let newdate = dateFormatter.date(from: newdateStr)
                        if let newdate = newdate {
                            let timeStr  = self.timeAgoSinceDate(date:newdate,numericDates: true)
                             PostDecriptionCell.timeLbl.text = timeStr
                        }
                    }
                    
                   
                    PostDecriptionCell.postTextView.delegate = self
                    PostDecriptionCell.postTextView.tag = (indexPath.row+10)
                    PostDecriptionCell.postTextView.text = captionArray.object(at: indexPath.row) as! String
                    
                    let desObj = captionArray.object(at: indexPath.row) as! String
                    
                    if desObj == ""
                    {
                      PostDecriptionCell.heightOfTextView.isActive = true
                      PostDecriptionCell.topOfTag.constant = 0
                    }
                    
                    PostDecriptionCell.selectionStyle = .none
                    if taggedArrayUserDefault.count>0
                    {
                        PostDecriptionCell.heightOfTagButton.constant = 15
                        PostDecriptionCell.delegate = self
                        var btnTitle : String = ""
                        if taggedArrayUserDefault.count == 1
                        {
                            btnTitle = "- with \(taggedArrayUserDefault.object(at: 0) as! String)"
                        }else if taggedArrayUserDefault.count == 2
                        {
                            btnTitle = "- with \(taggedArrayUserDefault.object(at: 0) as! String) and \(taggedArrayUserDefault.object(at: 1) as! String)"
                        }else
                        {
                            btnTitle = "- with \(taggedArrayUserDefault.object(at: 0) as! String) and \(taggedArrayUserDefault.count-1) others"
                        }
                        
                        let attrString: NSMutableAttributedString = NSMutableAttributedString(string: btnTitle)
                        attrString.addAttribute(NSFontAttributeName, value: UIFont.init(name: "SanFranciscoText-Regular", size: 14)!, range: NSMakeRange(0, attrString.length))
                        attrString.addAttribute(NSForegroundColorAttributeName, value: ConstantColors.themeColor, range: NSMakeRange(0, attrString.length))
                        
                        let range2 = (btnTitle as NSString).range(of: "- with")
                        attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: range2)
                        
                        let range3 = (btnTitle as NSString).range(of: "and")
                        attrString.addAttribute(NSForegroundColorAttributeName, value: UIColor.black, range: range3)
                        PostDecriptionCell.tagBtn.setAttributedTitle(attrString, for: .normal)
                    }else
                    {
                        PostDecriptionCell.heightOfTagButton.constant = 0
                        let attrString: NSMutableAttributedString = NSMutableAttributedString(string: "")
                        PostDecriptionCell.tagBtn.setAttributedTitle(attrString, for: .normal)
                    }
                    
                    if hideEditingOptions
                    {
                        PostDecriptionCell.postTextView.isEditable = false
                    }
                    return PostDecriptionCell
                    
                }
                
            }else if indexPath.row==1
            {
                //MARK:- URL
                let URLTableViewCell = tableView.dequeueReusableCell(withIdentifier: "URLTableViewCell", for: indexPath as IndexPath) as! URLTableViewCell
                
                if showLoader
                {
                    URLTableViewCell.indicatorObj.startAnimating()
                }
                URLTableViewCell.selectionStyle = .none
                
                URLTableViewCell.innerView.layer.shadowColor = UIColor.lightGray.cgColor
                URLTableViewCell.innerView.layer.shadowOffset = CGSize(width: 3, height: 3)
                URLTableViewCell.innerView.layer.shadowRadius = 2.0
                URLTableViewCell.innerView.layer.shadowOpacity = 1
                URLTableViewCell.innerView.clipsToBounds = false
                URLTableViewCell.innerView.layer.masksToBounds = false
                URLTableViewCell.urlTitle.text = self.titleToSend
                URLTableViewCell.urlDescription.text = self.descriptionToSend
                URLTableViewCell.urlAgain.text = self.urlToSend
                print(self.imageToSend)
                let url = URL(string: self.imageToSend)
                if !(url==nil)
                {
                    URLTableViewCell.urlImage.af_setImage(withURL: url!)
                }else
                {
                    URLTableViewCell.urlImage.image = nil
                }
                
                
                if isImageExist == true
                {
                    URLTableViewCell.widthOfUrlImage.constant = 50
                }else
                {
                    URLTableViewCell.widthOfUrlImage.constant = 0
                }
                URLTableViewCell.delegate = self
                if isUrlExist &&  imagesArray.count==2
                {
                    
                    URLTableViewCell.heightOfUrlInnerView.constant = 70
                    URLTableViewCell.outterView.isHidden = false
                }else
                {
                    URLTableViewCell.heightOfUrlInnerView.constant = 0
                    URLTableViewCell.outterView.isHidden = true
                }
                
                return URLTableViewCell
            }
            else if indexPath.row==imagesArray.count
            {
                let AddImageAndVideos = tableView.dequeueReusableCell(withIdentifier: "AddImageAndVideos", for: indexPath as IndexPath) as! AddImageAndVideos
                AddImageAndVideos.addButtonObj.layer.borderWidth = 2
                AddImageAndVideos.addButtonObj.layer.borderColor = UIColor.init(red: 0/255.0, green: 122/255.0, blue: 255/255.0, alpha: 1.0).cgColor
                AddImageAndVideos.delegate = self
                return AddImageAndVideos
            }
            else
            {
                //MARK:- ImageCell
                let ImageCell = tableView.dequeueReusableCell(withIdentifier: "ImageCell", for: indexPath as IndexPath) as! ImageCell
                
                let image = imagesArray.object(at: (indexPath.row)) as? UIImage
                ImageCell.delegate = self
                ImageCell.setPostedImage(image: image!)
                ImageCell.imageObj.image = image
                ImageCell.captionTextView.delegate = self
                ImageCell.crossButton.tag = ((indexPath.row))
                ImageCell.imageClikedBtn.tag = ((indexPath.row))-2
                ImageCell.captionTextView.tag = (indexPath.row+10)
                ImageCell.captionTextView.text = captionArray.object(at: (indexPath.row)) as! String
                ImageCell.selectionStyle = .none
                
                
                ImageCell.tag = indexPath.row
                
                if imagesArray.count==3
                {
                    ImageCell.captionTextView.isHidden = true
                    
                    ImageCell.height45.isActive = true
                    ImageCell.heightOfCaptionTextView.isActive = false
                }else
                {
                    ImageCell.captionTextView.isHidden = false
                    ImageCell.height45.isActive = false
                    ImageCell.heightOfCaptionTextView.isActive = true
                }
                
                if let youtubeImage = youtubeArray.object(at: (indexPath.row)) as? UIImage
                {
                    ImageCell.playImage.image = youtubeImage
                    ImageCell.playImage.isHidden = false
                }else
                {
                    ImageCell.playImage.isHidden = true
                }
                
                if hideEditingOptions
                {
                    ImageCell.crossButton.isHidden = true
                    ImageCell.captionTextView.isEditable = false
                    ImageCell.imageClikedBtn.isUserInteractionEnabled = true
                    
                    let captionStr = captionArray.object(at: (indexPath.row)) as! String
                    if captionStr == ""
                    {
                        ImageCell.height45.isActive = true
                        ImageCell.heightOfCaptionTextView.isActive = false
                    }else
                    {
                        ImageCell.height45.isActive = false
                        ImageCell.heightOfCaptionTextView.isActive = true
                    }
                }
                
                return ImageCell
            }
        }
    }
    
    
    //MARK:- playVideoInVisiableCells
    
    func playVideoInVisiableCells() {
        if edit
        {
            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when) {
                let visiableCells = self.imageTableView.visibleCells
                
                print(visiableCells)
                
                var targetCell : ImageCell?
                var cellNo : Int = 0
                for c in visiableCells {
                    let cell = c as? ImageCell
                    targetCell = cell
                    
                    cellNo = cellNo+1
                    if cellNo > 2 || visiableCells.count==1
                    {
                        print("cellNo==",cellNo)
                        let arr = self.objectForEdit?.object(forKey: "postmedia") as! NSArray
                        //print(arr)
                        
                        print("targetCell==",targetCell?.tag ?? 15)
                        
                        
                        if !(targetCell?.tag == 15)
                        {
                            let index = (targetCell?.tag)!-2
                            
                            let media_types = self.objectForEdit?.object(forKey: "media_types") as! NSArray
                            print(media_types)
                            let type = media_types.object(at: index) as! String
                            
                            if !(type == "IMAGE")
                            {
                                if let fileObj = arr.object(at: index) as? PFFile
                                {
                                    let url = URL(string: fileObj.url!)
                                    targetCell?.imageObj.jp_playVideo(with:url)
                                }
                                break
                            }
                        }
                        
                        
                        
                    }
                }
            }
        }
    }
    
    
    
    
    
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    //        print(indexPath.row)
    //    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if !(self.avPlayer == nil)
        {
            if indexPath.row>1
            {
                self.avPlayer.pause()
                self.avPlayer = nil
            }
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Post Type table view
        if tableView==tableViewObj
        {
            selectedRow = indexPath.row
            selectedType = ((dropdownArray.object(at: 0) as AnyObject).objectAt(indexPath.row) as? String)!
            dropdownArray.replaceObject(at: 0, with: [])
            //heightOfPostTypeTable.constant = 50
            heightOfPostTypeTable.constant = 0
            self.tableViewObj.reloadData()
        }
        
        
        if tableView == taggedUserTable
        {
            
            print(replaceString)
            
            let str : NSMutableString =   NSMutableString(string: captionArray.object(at: 0) as! String)
            // var updatedstr : String =   ""
            //   print("updatedWord===","@"+updatedWord)
            if searchActive == true
            {
                if taggedUser
                {
                    let indexObj = indexArray[indexPath.row]
                    
                    
                    let dic : NSMutableDictionary = [:]
                    dic.setValue(idsArray.object(at: indexObj) as! String, forKey: "id")
                    dic.setValue(tagNameArray.object(at: indexObj) as! String, forKey: "name")
                    dic.setValue(tagUserNameArray[indexObj], forKey: "username")
                    
                    savedArrayUser.add(dic)
                    str.replaceCharacters(in: rangeToAppend, with: "@"+tagUserNameArray[indexObj])
                }else
                {
                    savedArrayHashTags.add(searchResultArray[indexPath.row])
                    str.replaceCharacters(in: rangeToAppend, with: searchResultArray[indexPath.row])
                }
            }else
            {
                let dic : NSMutableDictionary = [:]
                dic.setValue(idsArray.object(at: indexPath.row) as! String, forKey: "id")
                dic.setValue(tagNameArray.object(at: indexPath.row) as! String, forKey: "name")
                dic.setValue(tagUserNameArray[indexPath.row], forKey: "username")
                
                
                // savedArrayUser.add("@"+tagUserNameArray[indexPath.row])
                savedArrayUser.add(dic)
                str.replaceCharacters(in: rangeToAppend, with: "@"+tagUserNameArray[indexPath.row])
            }
            
            let   mutableStr = NSMutableString(string: str)
            print(mutableStr)
            captionArray.replaceObject(at: 0 , with: mutableStr)
            
            taggedContainer.isHidden = true
            imageTableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
            
            let indexPath = NSIndexPath.init(row: 0, section: 0)
            //imageTableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
            let cell = imageTableView.cellForRow(at: indexPath as IndexPath) as! PostDecriptionCell
            cell.postTextView.text = captionArray.object(at: indexPath.row) as! String
            
            UIView.setAnimationsEnabled(false)
            imageTableView.beginUpdates()
            imageTableView.endUpdates()
        }
        
    }
    
    
    
    func crossButtonClicked(tag: Int) {
        print(tag)
        //captionHeightArray.removeObject(at: tag)
        if videoFile.count>0
        {
            videoFile.removeObject(at: tag-2)
        }
        if edit
        {
            print(globalMediaType)
            globalMediaType.removeObject(at: tag-2)
            print(globalMediaType)
        }
        
        
        JPVideoPlayerManager.shared().stopPlay()
        imagesArray.removeObject(at: tag)
        captionArray.removeObject(at: tag)
        self.youtubeArray.removeObject(at: tag)
        if imagesArray.count==2 {// for removing the last row of imagestableview "Add photos/Videos"
            forLastRow = 0
            self.cameraButton.setImage(UIImage.init(named: "cameraUnselected.png"), for: .normal)
            imageTableView.reloadData()
            
        }else
        {
            imageTableView.reloadData()
            
        }
        
    }
    
    
    func imageButtonClicked(tag: Int) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SingleMediaViewController")  as! SingleMediaViewController
        vc.objectFormHome = objectForEdit
        vc.index = tag
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func videoPhotoClicked() {
        self.showGallery()
    }
    func tagUserClicked() {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagUserViewController")
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func tagUserNewClicked() {
//        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TagUserViewController")
//        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    func crossUrlClicked() {
        self.isUrlExist = false
        let indexPath = NSIndexPath.init(row: 1, section: 0)
        self.imageTableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == imageTableView && taggedContainer.isHidden == false
        {
            if(scrollView.contentOffset.y>0){
                taggedContainer.isHidden = true
                imageTableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
            }
        }
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if edit
        {
            self.playVideoInVisiableCells()
            let firstVisibleIndexPath = self.imageTableView.indexPathsForVisibleRows
            scrollRow = firstVisibleIndexPath?[0][1] ?? 50
            print("scrollRow=======",scrollRow)
        }
    }
    
    
    //MARK:- textView
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        
        // captionHeightArray.replaceObject(at: (textView.tag-10), with: textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height)
        captionArray.replaceObject(at: (textView.tag-10), with: textView.text)
        
        
        if  textView.tag == 10
        {
            let text = textView.text as NSString
            let substring = text.substring(with: NSRange(location: 0, length: globalRange.location+globalInt))
            currentWord = substring.components(separatedBy: " ").last!
            var wordWithSpace = substring.components(separatedBy: " ").last!
            wordWithSpace = " "+wordWithSpace
            let lastChar = String(currentWord.characters.suffix(1))
            print("savedArrayUser==",savedArrayUser)
            let usernameArr = savedArrayUser.value(forKey: "username") as! NSArray
            
            if (usernameArr.contains(currentWord) || wordWithSpace.contains(" @")) && !(lastChar==" ")
            {
                taggedUser = true
                var textWidth = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
                textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
                let boundingRect = sizeOfString(string: substring, constrainedToWidth: Double(textWidth), font: textView.font!)
                let numberOfLines = boundingRect.height / textView.font!.lineHeight;
                
                updatedWord =  currentWord.replacingOccurrences(of: "@", with: "")
                searchResultArray = tagUserNameArray.filter({ (text) -> Bool in
                    let tmp: NSString = text as NSString
                    let range = tmp.range(of: updatedWord, options: NSString.CompareOptions.caseInsensitive)
                    return range.location == 0
                })
                
                indexArray = [Int]()
                for i in 0..<searchResultArray.count
                {
                    let indexOfValue = tagUserNameArray.index(of: searchResultArray[i])
                    indexArray.append(indexOfValue!)
                }
                print("searchResultArray=====",searchResultArray)
                if(searchResultArray.count == 0){
                    if wordWithSpace == " @"
                    {
                        taggedContainer.isHidden = false
                        if numberOfLines>2
                        {
                            imageTableView.setContentOffset(CGPoint.init(x: 0, y: (numberOfLines*12)), animated: true)
                        }
                    }else{
                        taggedContainer.isHidden = true
                        imageTableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
                    }
                    
                    searchActive = false;
                    
                } else {
                    searchActive = true;
                    taggedContainer.isHidden = false
                    if numberOfLines>2
                    {
                        imageTableView.setContentOffset(CGPoint.init(x: 0, y: (numberOfLines*12)), animated: true)
                    }
                }
                self.taggedUserTable.reloadData()
                
                
                
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.characters.count)), (currentWord.characters.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                    }
                }
            }
            else if (savedArrayHashTags.contains(currentWord) || wordWithSpace.contains(" #")) && !(lastChar==" ")
            {
                
                
                taggedUser = false
                var textWidth = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
                textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
                let boundingRect = sizeOfString(string: substring, constrainedToWidth: Double(textWidth), font: textView.font!)
                let numberOfLines = boundingRect.height / textView.font!.lineHeight;
                
                updatedWord =  currentWord
                print("====",updatedWord)
                
                self.callSearchApi(currentWord: updatedWord, numberOfLines: Int(numberOfLines))
                
                if let range = textView.selectedTextRange {
                    if range.start == range.end {
                        let beginning = textView.beginningOfDocument
                        let selectedRange = textView.selectedTextRange
                        let selectionStart = selectedRange?.start
                        let selectionEnd = selectedRange?.end
                        let location = textView.offset(from: beginning, to: selectionStart!)
                        let length = textView.offset(from: selectionStart!, to: selectionEnd!)
                        print(length)
                        if removeCount<0
                        {
                            removeCount = 0
                        }
                        //print("currentWord======",currentWord)
                        rangeToAppend =  NSMakeRange((location-(currentWord.characters.count)), (currentWord.characters.count))
                        print("rangeToAppend location ==",rangeToAppend.location)
                        print("rangeToAppend length ==",rangeToAppend.length)
                    }
                }
            }
            else
            {
                taggedContainer.isHidden = true
                imageTableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
            }
            
            if taggedContainer.isHidden == true
            {
                UIView.setAnimationsEnabled(false)
                imageTableView.beginUpdates()
                imageTableView.endUpdates()
            }
        }
        
        if taggedContainer.isHidden == true
        {
            UIView.setAnimationsEnabled(false)
            imageTableView.beginUpdates()
            imageTableView.endUpdates()
        }
    }
    
    
    
    func callSearchApi(currentWord:String,numberOfLines:Int)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "Hashtags")
        query.whereKey("name", matchesRegex: "(?i)\(currentWord)")
        query.limit = 100
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                var name : [String] = []
                var ids : [String] = []
                for object in objects! {
                    print(object)
                    name.append(object.object(forKey: "name") as! String)
                    ids.append(object.objectId!)
                }
                
                
                self.searchResultArray = name
                self.hashTagsGlobalIdsArray = ids
                
                
                self.searchActive = true;
                self.taggedContainer.isHidden = false
                if numberOfLines>2
                {
                    self.imageTableView.setContentOffset(CGPoint.init(x: 0, y: (numberOfLines*12)), animated: true)
                }
                
                self.taggedUserTable.reloadData()
                
            }
            else {
                print(error!)
                self.searchActive = false;
                self.taggedContainer.isHidden = true
                self.imageTableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
            }
        })
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        globalRange = range
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        var textWidth = UIEdgeInsetsInsetRect(textView.frame, textView.textContainerInset).width
        textWidth -= 2.0 * textView.textContainer.lineFragmentPadding;
        let boundingRect = sizeOfString(string: newText, constrainedToWidth: Double(textWidth), font: textView.font!)
        let numberOfLines = boundingRect.height / textView.font!.lineHeight;
        // print("text", text)
        ///////////////////////////////////////////////////
        if  textView.tag == 10
        {
            if text == " " || text == "\n" {
                let types: NSTextCheckingResult.CheckingType = .link
                let detector = try? NSDataDetector(types: types.rawValue)
                let matches = detector!.matches(in: textView.text, options: .reportCompletion, range: NSMakeRange(0,textView.text.characters.count))
                if matches.count > 0 {
                    let urlResult = matches.first
                    let url = urlResult?.url
                    print(url!)
                    if self.imagesArray.count==2 && !isUrlExist
                    {
                        self.isUrlExist = true
                        getViewUrl(varibale: url!)
                    }
                }else
                {
                    self.isUrlExist = false
                }
            }
            ////////////////////////////////////////////////////
            
            let isBackSpace = strcmp(text, "\\b")
            if (isBackSpace == -92) {
                globalInt = 0
                removeCount = removeCount-1
            }else
            {
                globalInt = 1
                removeCount = removeCount+1
            }
        }
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if  textView.tag == 10
        {
            taggedContainer.isHidden = true
            imageTableView.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
        }
    }
    
    
    
    
    func sizeOfString (string: String, constrainedToWidth width: Double, font: UIFont) -> CGSize {
        return (string as NSString).boundingRect(with: CGSize(width: width, height: DBL_MAX),
                                                 options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                 attributes: [NSFontAttributeName: font],
                                                 context: nil).size
    }
    
    
    
    
    
    
    
    
    func getViewUrl(varibale:URL) {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        
        
        print("varibale==",varibale)
        showLoader = true
        
        self.isImageExist = false
        self.titleToSend = ""
        self.descriptionToSend = ""
        self.urlToSend = ""
        
        
        let indexPath = NSIndexPath.init(row: 1, section: 0)
        self.imageTableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
        
        
        let urlStr = varibale.absoluteString.lowercased()
        var removeHTTPS : String = urlStr
        if urlStr.contains("https")
        {
            removeHTTPS = urlStr.replacingOccurrences(of: "https", with: "http")
        }
        
        
        let str = "https://api.urlmeta.org/?url="+removeHTTPS
        print("final===",str)
        
        Alamofire.request(str) .responseJSON { response in // 1
            print(response.request!)
            
            if let JSON = response.result.value {
                print("JSON: \(JSON)")
                self.showLoader = false
                let result = (JSON as AnyObject).object(forKey: "result") as? NSDictionary
                if (result! as AnyObject).object(forKey: "status") as? String == "OK"
                {
                    
                    let jsonDictionary = (JSON as AnyObject).object(forKey: "meta") as? NSDictionary
                    print(jsonDictionary!)
                    
                    self.titleToSend = removeHTTPS
                    self.descriptionToSend = removeHTTPS
                    self.urlToSend = removeHTTPS
                    
                    if let title = (jsonDictionary! as AnyObject).object(forKey: "title") as? String
                    {
                        self.titleToSend = title
                    }
                    
                    if let description = (jsonDictionary! as AnyObject).object(forKey: "description") as? String
                    {
                        self.descriptionToSend = description
                    }
                    
                    if let url = (jsonDictionary! as AnyObject).object(forKey: "url") as? String
                    {
                        self.urlToSend = url
                    }
                    if let imageUrl =  (jsonDictionary! as AnyObject).object(forKey: "image") as? String
                    {
                        self.isImageExist = true
                        self.imageToSend = imageUrl
                    }else
                    {
                        self.isImageExist = false
                    }
                    
                }else
                {
                    self.isImageExist = false
                    self.titleToSend = removeHTTPS
                    self.descriptionToSend = removeHTTPS
                    self.urlToSend = removeHTTPS
                }
                let indexPath = NSIndexPath.init(row: 1, section: 0)
                self.imageTableView.reloadRows(at: [indexPath as IndexPath], with: .automatic)
                
            }
        }
    }
    
    
    
    func validateUrl (candidate : String) -> Bool {
        
        let urlRegEx = "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])* ))+?"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        return urlTest.evaluate(with: candidate)
    }
    
    
}

extension NSRange {
    func range(for str: String) -> Range<String.Index>? {
        guard location != NSNotFound else { return nil }
        
        guard let fromUTFIndex = str.utf16.index(str.utf16.startIndex, offsetBy: location, limitedBy: str.utf16.endIndex) else { return nil }
        guard let toUTFIndex = str.utf16.index(fromUTFIndex, offsetBy: length, limitedBy: str.utf16.endIndex) else { return nil }
        guard let fromIndex = String.Index(fromUTFIndex, within: str) else { return nil }
        guard let toIndex = String.Index(toUTFIndex, within: str) else { return nil }
        
        return fromIndex ..< toIndex
    }
}

extension CreatePostViewController
{
    func timeAgoSinceDate(date:Date, numericDates:Bool) -> String {
        
        
        let calendar = Calendar.current
        let now = NSDate()
        let earliest = (now as NSDate).earlierDate(date as Date)
        let latest = (earliest == now as Date) ? date : now as Date
        let components:DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute , NSCalendar.Unit.hour , NSCalendar.Unit.day , NSCalendar.Unit.weekOfYear , NSCalendar.Unit.month , NSCalendar.Unit.year , NSCalendar.Unit.second], from: earliest, to: latest as Date, options: NSCalendar.Options())
        
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) mins ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 min ago"
            } else {
                return "A min ago"
            }
        } else if (components.second! >= 3) {
            return "Just now"
        } else {
            return "Just now"
        }
        
    }
}





