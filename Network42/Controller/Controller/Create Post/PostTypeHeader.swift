//
//  PostTypeHeader.swift
//  Network42
//
//  Created by 42works on 23/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class PostTypeHeader: UITableViewHeaderFooterView {
    @IBOutlet var titleLbl: UILabel!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
