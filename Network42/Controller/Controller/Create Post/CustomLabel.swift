//
//  CustomLabel.swift
//  Network42
//
//  Created by 42works on 24/04/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class CustomLabel: UILabel {

   
        override func drawText(in rect: CGRect) {
            super.drawText(in: UIEdgeInsetsInsetRect(rect, UIEdgeInsets(top: 4, left: 2, bottom: 5, right: 5)))
        }
    

}
