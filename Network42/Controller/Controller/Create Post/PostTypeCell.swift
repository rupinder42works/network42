//
//  PostTypeCell.swift
//  Network42
//
//  Created by 42works on 23/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class PostTypeCell: UITableViewCell {
    @IBOutlet var titleLbl: UILabel!
    @IBOutlet var typeImage: UIImageView!
    @IBOutlet var tickImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
