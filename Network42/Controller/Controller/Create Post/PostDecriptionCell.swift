//
//  PostDecriptionCell.swift
//  Network42
//
//  Created by 42works on 31/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

@objc protocol tagUserDelegate {
    func tagUserClicked()
}

class PostDecriptionCell: UITableViewCell {
    
  
    weak var delegate: tagUserDelegate?
    @IBOutlet var userProfile: UIImageView!
    
    @IBOutlet var tagBtn: UIButton!

    @IBOutlet var postTextView: UITextView!
    @IBOutlet var heightOfTagButton: NSLayoutConstraint!
    @IBOutlet var heightOfTextView: NSLayoutConstraint!
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        print(self.contentView.subviews)
        
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            for subview in self.contentView.subviews as [UIView] {
                if let UITextView = subview as? UITextView {
                    UITextView.becomeFirstResponder()
                }
            }
        }
        
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func tagUserAction(_ sender: Any) {
        self.delegate?.tagUserClicked()
    }
   
    
}
