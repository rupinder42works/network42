//
//  SettingsViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 21/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
class SettingsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    var hedingArray:NSMutableArray = []
    var FbhedingArray:NSMutableArray = []
    var subHeadingArray:NSMutableArray = []
    
    @IBOutlet var indicatorObj: UIActivityIndicatorView!
    @IBAction func back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: "SettingTopTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell")
        tableView.register(UINib(nibName: "SettingsotherTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "cell1")
        
        tableView.estimatedRowHeight = 72
        tableView.rowHeight = UITableViewAutomaticDimension
        
        hedingArray = ["Push Notifications","Change Password","Profile Viewing Options","Privacy Policy","Logout"]
        subHeadingArray = ["Choose which mobile push notifications you receive","Choose a unique password to protect your account","Choose whether you are visible or viewing in private mode","",""]
        let currentUser = PFUser.current()
        if let loginTypeStr = currentUser?.object(forKey: "login_type") as? String{
            print("values",loginTypeStr)
            if loginTypeStr == "facebook"{
                hedingArray.removeObject(at: 1)
                subHeadingArray.removeObject(at: 1)
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        
    }
    
    // MARK:- Table view delgate and data source
    // MARK:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hedingArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        print(indexPath.row)
        
        if indexPath.row > 2{
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
                as! SettingsotherTableViewCell
            cell1.selectionStyle = .none
            cell1.headerTitle.text = hedingArray[indexPath.row] as? String
            return cell1
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
                as! SettingTopTableViewCell
            cell.selectionStyle = .none
            cell.headingTitle.text = hedingArray[indexPath.row] as? String
            cell.subheadingTitle.text = subHeadingArray[indexPath.row] as? String
            if indexPath.row > 2{
                cell.bottom_height.constant = 0
            }
            return cell
        }
        
    }
    
    
    //    func didselectrow
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if hedingArray.object(at: indexPath.row) as? String == "Push Notifications"{
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "PushNotificationViewController") as! PushNotificationViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Change Password" {
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "ChangepasswordViewController") as! ChangepasswordViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Profile Viewing Options" {
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "ViewingProfileViewController") as! ViewingProfileViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Privacy Policy" {
            
            let SettingsStoryboard = UIStoryboard.init(name: "SettingsStoryboard", bundle: nil)
            let vc = SettingsStoryboard.instantiateViewController(withIdentifier: "PrivacyPolicyViewController") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(vc, animated:true)
            
        }
        else if hedingArray.object(at: indexPath.row) as? String == "Logout"{
            
            
            let alert = UIAlertController(title: "Are you sure you want to logout?", message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: nil))
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                nestedprofileArray =  [[],[],"","","",""]
                self.installer()
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func installer()
    {
        self.indicatorObj.startAnimating()
        let installationClass = PFInstallation.current()
        installationClass?["userId"] = PFUser.current()?.objectId!
        installationClass?["isActive"] = "0"
        installationClass?.saveInBackground { (success, error) in
            self.indicatorObj.stopAnimating()
            if(success)
            {
                print("installation saved")
            }
            else{
                print("error",error!)
                //return
            }
            PFUser.logOut()
            UserDefaults.standard.set(false, forKey: "LoggedIn")
            let tabBarSB = UIStoryboard.init(name: "LoginSB", bundle: nil)
            let nav = tabBarSB.instantiateViewController(withIdentifier: "NavViewController") as! NavViewController
            let vc = tabBarSB.instantiateViewController(withIdentifier: "LoginViewController")
            nav.viewControllers = [vc]
            let window = UIApplication.shared.windows[0] as UIWindow
            window.rootViewController=nav
        }
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
//            as! SettingsotherTableViewCell
//        cell.contentView.backgroundColor = UIColor.clear
//        let whiteRoundedView : UIView = UIView(frame: CGRect(x: 0, y: 10, width: self.view.frame.size.width, height: 100))
//        
//        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
//        whiteRoundedView.layer.masksToBounds = false
//        whiteRoundedView.layer.cornerRadius = 2.0
//        // whiteRoundedView.layer.shadowOffset = CGSizeMake(-1, 1)
//        whiteRoundedView.layer.shadowOpacity = 0.2
//        
//        cell.contentView.addSubview(whiteRoundedView)
//        cell.contentView.sendSubview(toBack: whiteRoundedView)
//    }
    
}
