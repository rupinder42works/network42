//
//  ChangepasswordViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 06/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import TKSubmitTransitionSwift3
import Parse
class ChangepasswordViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    // Do any additional setup after loading the view.
        updateBtn.layer.cornerRadius = 2
    }

    @IBOutlet var activityIndcatorBKView: UIView!
    @IBOutlet var reTypeNewPw: MyTextField!
    @IBOutlet var newPasswordTf: MyTextField!
    @IBOutlet var currentPWTf: MyTextField!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet var updateBtn: TKTransitionSubmitButton!
    @IBAction func updateTapped(_ sender: Any) {
        
        if currentPWTf.text! == "" || newPasswordTf.text! == "" || currentPWTf.text! == ""{
            self.showMessage("All fields are required")
            return
        }
        else if (newPasswordTf.text?.characters.count)! < 6 {
            self.showMessage(messages.passwordLength)
            return
        }
        else if newPasswordTf.text! != reTypeNewPw.text!{
            self.showMessage("Password does'nt match")
            return
        }
        else{
            updateBtn.startLoadingAnimation()
            self.activityIndcatorBKView.isHidden = false
            if let currentUser = PFUser.current(){
                self.loginWithUserName(userName:currentUser.username!,name:"")
            }
        }
    }
   
    @IBAction func back(_ sender: Any) {
        
    _ = self.navigationController?.popViewController(animated: true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }

    func changepasswordAPI() {
        //check login Authorizatio first
        if let currentUser = PFUser.current(){
         currentUser.password = newPasswordTf.text!
            currentUser.saveInBackground(block: { (status, error) in
                print(status)
                if status ==  true {
                    self.activityIndcatorBKView.isHidden = true
                    self.updateBtn.returnToOriginalState()
                    self.updateBtn.layer.cornerRadius = self.self.updateBtn.normalCornerRadius!
                    self.showMessage("Password change successfully.")
                    _ = self.navigationController?.popViewController(animated: true)
                    
                } else {
                    let message = (error?.localizedDescription)
                    self.showMessage(message!)
                }
            }
        )
        }
   }
    
    var usernameStr:String = ""
    func loginWithUserName(userName:String,name:String)
    {
        let currentUser = PFUser.current()?.username
        print(currentUser as Any)
        usernameStr = currentUser!
        PFUser.logInWithUsername(inBackground: usernameStr, password:currentPWTf.text!,
                                 block: {(user, error) -> Void in
                                    
                                    if let error = error as? NSError{
                                      //  let errorString = error.userInfo["error"] as? NSString
                                        let errorString = "Invalid password"
                                       // print(errorString!)// In case something went wrong...
                                        print("ended")
                                        self.activityIndcatorBKView.isHidden = true
                                        self.updateBtn.returnToOriginalState()
                                        self.updateBtn.layer.cornerRadius = self.self.updateBtn.normalCornerRadius!
                                        self.showMessage((errorString))
                                    }
                                    else {
                                        
                                        print(user as Any)
                                        if user != nil{
                                            self.changepasswordAPI()
                                        }
                                        
                                    }
        })
    }

}
