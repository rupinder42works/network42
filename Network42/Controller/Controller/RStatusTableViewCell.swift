//
//  RStatusTableViewCell.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 24/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol MarkRStatusDelegate {
    func sectionActionClicked(cell:RStatusTableViewCell)
   
}
class RStatusTableViewCell: UITableViewCell {
    weak var delegate: MarkRStatusDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    @IBOutlet var titleLbl: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var sRStatusBtn: UIButton!
    @IBAction func selectedRStatusTapped(_ sender: Any) {
         self.delegate?.sectionActionClicked(cell: self)
    }
}
