//
//  FollowListViewController.swift
//  Network42
//
//  Created by Anmol Rajdev on 12/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse

class FollowListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    var getfinalArray : NSMutableArray = []
    @IBOutlet var followIndicator: UIActivityIndicatorView!
    var loggedInId = ""
    
    @IBOutlet weak var lbl_subHeading: UILabel!
    @IBOutlet weak var lbl_mainFollow: UILabel!
    
    var type:String = ""
    @IBOutlet var tableView: UITableView!
    @IBOutlet var followHeadingLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if getfinalArray.count > 0{
            lbl_subHeading.isHidden = true
            lbl_mainFollow.isHidden = true
            tableView.isHidden = false
        }else{
            lbl_subHeading.isHidden = false
            lbl_mainFollow.isHidden = false
            tableView.isHidden = true
        }
        tableView.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "SearchUserCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        followIndicator.stopAnimating()
        if type == messages.followersText{
            lbl_subHeading.text = "You'll see all the people who follow you here."
            lbl_mainFollow.text = "Followers!"
            // getFollowList()
        }
        else if type == "FOLLOWING"{
            //  getFollowingList()
            lbl_subHeading.text = "Once you follow people, you'll see them here."
            lbl_mainFollow.text = "People you follow!"
        }
        followHeadingLbl.text = type.lowercased().capitalizingFirstLetter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.view.isUserInteractionEnabled = true
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backTapped(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getfinalArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchUserCell", for: indexPath as IndexPath) as! SearchUserCell
        //
        //        cell.containerImage.layer.cornerRadius = 5.0
        //        cell.containerImage.layer.borderWidth=1.0
        //        cell.containerImage.layer.borderColor=UIColor.clear.cgColor
        cell.tag = indexPath.row
        cell.followBtn.isHidden = true
        cell.followBtn.tag = indexPath.row
        cell.delegate = self
         let currentId = PFUser.current()?.objectId
        print((getfinalArray.object(at: indexPath.row) as AnyObject))
        cell.userName.text = (getfinalArray.object(at: indexPath.row) as AnyObject).value(forKey: "user_name") as? String
        cell.name.text = (getfinalArray.object(at: indexPath.row) as AnyObject).value(forKey: "name") as? String
       
        if let  obj = getfinalArray.object(at: indexPath.row) as? PFObject{
            let otherId = obj.objectId
           
            if otherId?.isEmpty == false && otherId?.isEmpty == false{
                 if type == "FOLLOWING"{
                    if let id = PFUser.current()?.objectId{
                        if loggedInId == id{
                            cell.followBtn.isHidden = false
                            cell.followBtn.setTitle("Following", for: .normal)
                            cell.followBtn.isHidden = false
                            cell.followBtn.layer.borderColor = UIColor.white.cgColor
                            cell.followBtn.layer.borderWidth = 0
                            cell.followBtn.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1)
                            cell.followBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                        }else{
                            checkWhetherUserFollowed(currentUserId:id,otherUserId:otherId!,tag :indexPath.row)
                        }
                    }
               
                 }else{
                   if let id = PFUser.current()?.objectId{
                    checkWhetherUserFollowed(currentUserId:id,otherUserId:otherId!,tag :indexPath.row)
                    }
                }
            }
        }
        if let file = (getfinalArray.object(at: indexPath.row) as AnyObject).value(forKey: "profile_image") as? PFFile
        {
            cell.profileImage.file = file
            cell.profileImage.loadInBackground()
        }
        
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // nestedprofileArray =  [[],[],"","","",""]
        userProfileClicked(tag: indexPath.row)
        
    }
    
    
    
    //    var shownFollowIndexes : [IndexPath] = []
    //    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
    //    {
    //        if (shownFollowIndexes.contains(indexPath) == false) {
    //            shownFollowIndexes.append(indexPath)
    //            let frame = CGRect.init(x: cell.frame.origin.x - 200, y: cell.frame.origin.y , width: cell.frame.width, height: cell.frame.height)
    //            cell.frame = frame
    //            UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
    //
    //                let frame = CGRect.init(x: cell.frame.origin.x + 200, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height)
    //                cell.frame = frame
    //            }) { finished in
    //            }
    //        }
    //    }
    
    
    func userProfileClicked(tag: Int)
    {
        self.view.isUserInteractionEnabled = false
        let storyboardObj = UIStoryboard(name: "TabBarSB",bundle: nil)
        
        let vc = storyboardObj.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        vc.fromSearch = true
        let id = (getfinalArray.object(at: tag) as AnyObject).value(forKey: "objectId") as? String
        print(id!)
        let objId = PFUser.current()?.objectId
        if !(id == objId){
            
            vc.userProfile = false
        }
        vc.friend = "FOLLOWING"
        
        let nameQuery : PFQuery = PFUser.query()!
        nameQuery.whereKey("objectId", equalTo: id!)
        nameQuery.findObjectsInBackground(block: { (objects, error) in
            print(objects!)
            for object in objects! {
                vc.userFromSearch = object as? PFUser
                self.navigationController?.pushViewController(vc, animated: true)
            }
        })
    }
    
    func checkWhetherUserFollowed(currentUserId:String,otherUserId:String,tag:Int){
        let query = PFQuery(className: "User_follow")
        let objId = currentUserId
        query.whereKey("followed_id", equalTo:otherUserId)
        query.whereKey("follower_id", equalTo:objId)
        query.whereKey("status", equalTo: "1")
        query.findObjectsInBackground(block: { (objects, error) in
            
            if error == nil {
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    print(object)
                    PFObjectArr.add(object)
                }
                
                if PFObjectArr.count>0
                {
                    if let cell = self.tableView.cellForRow(at: IndexPath.init(row: tag, section: 0)) as? SearchUserCell{
                        cell.followBtn.setTitle("Following", for: .normal)
                        cell.followBtn.isHidden = false
                        cell.followBtn.layer.borderColor = UIColor.white.cgColor
                        cell.followBtn.layer.borderWidth = 0
                        cell.followBtn.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1)
                        cell.followBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                    }
                }else
                {
                    if let cell = self.tableView.cellForRow(at: IndexPath.init(row: tag, section: 0)) as? SearchUserCell{
                        cell.followBtn.setTitle("Follow", for: .normal)
                        cell.followBtn.isHidden = false
                        cell.followBtn.layer.borderColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1).cgColor
                        cell.followBtn.layer.borderWidth = 1.2
                        cell.followBtn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
                        cell.followBtn.setTitleColor(#colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1), for: .normal)
                    }
                }
            }
            else {
                print(error!)
            }
        })
    }
}


extension FollowListViewController : followDelegate{
    
    func followClickedDelegate(index:Int,cell:SearchUserCell)
    {
        print("==",index)
        
        if cell.followBtn.titleLabel?.text == "Follow"
        {
            cell.indicator.startAnimating()
            cell.followBtn.setTitle("", for: .normal)
            cell.isUserInteractionEnabled = false
            let User_followClass = PFObject(className: "User_follow")
            var objId = ""
            if let currentUserId = PFUser.current()?.objectId{
                objId = currentUserId
                User_followClass["follower_id"] = objId
            }
            if let  obj = getfinalArray.object(at: index) as? PFObject{
                let otherId = obj.objectId ?? ""
                User_followClass["followed_id"] = otherId
                User_followClass["parent_following"] = obj
                
            }
            User_followClass["parent_follower"] = PFUser.current()
            if let profileType = (getfinalArray.object(at: index) as AnyObject).value(forKey: "profile_type") as? String{
                if profileType == "1"
                {
                    User_followClass["status"] = "1"
                }else
                {
                    User_followClass["status"] = "0"
                }
            }
            
            User_followClass.saveInBackground { (success, error) in
                cell.indicator.stopAnimating()
                cell.isUserInteractionEnabled = true
                if(success)
                {
                    print("user follow")
                    // self.getIds()
                    cell.followBtn.setTitle("Following", for: .normal)
                    cell.followBtn.isHidden = false
                    cell.followBtn.layer.borderColor = UIColor.white.cgColor
                    cell.followBtn.layer.borderWidth = 0
                    cell.followBtn.backgroundColor = #colorLiteral(red: 0.4039215686, green: 0, blue: 0.9450980392, alpha: 1)
                    cell.followBtn.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                   // let user = self.PFObjectArray.object(at: index) as? PFUser
                    var user : PFUser?
                    if let  obj = self.getfinalArray.object(at: index) as? PFUser{
                       user = obj
                    }
                    let currentName = PFUser.current()?.value(forKey: "name") as! String
                    
                    if let profileType = (self.getfinalArray.object(at: index) as AnyObject).value(forKey: "profile_type") as? String{
                        if profileType == "1"
                        {
                            let formatter = DateFormatter()
                            formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                            let time = formatter.string(from: Date())
                            
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                            
                           self.saveToNotifications(from_id: objId, to_id: user!.objectId!, notification_type: "follow_public_user", time: time, post_id: "", parent_from: PFUser.current()!, parent_to: user!,commentStr: "")
                            
                           self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.startedfollowing)",type:"follow_public_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                        }else
                        {
                             self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.requestSent)",type:"follow_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                        }
                    }
                }
                else{
                    print("error",error!)
                    cell.indicator.stopAnimating()
                    cell.followBtn.setTitle("Follow", for: .normal)
                    let message = (error?.localizedDescription)
                    self.showMessage(message!)
                    return
                }
            }
        }else
        {
            var alert = UIAlertController()
            if cell.followBtn.titleLabel?.text == "Following"
            {
                var name = ""
                if let t =  (self.getfinalArray.object(at: index) as AnyObject).value(forKey: "user_name") as? String{
                    name = t
                }
                  alert = UIAlertController(title: "Are you sure you want to unfollow \(name)?", message: "", preferredStyle: UIAlertController.Style.alert)
            }else
            {
                alert = UIAlertController(title: "Are you sure you want to cancel this request?", message: "", preferredStyle: UIAlertController.Style.alert)
            }
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                
            }))
            alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
                switch action.style{
                case .default:

                cell.indicator.startAnimating()
                   // cell.isUserInteractionEnabled = false
                 //   cell.followBtn.setTitle("", for: .normal)
                    
                    let loggedIn = PFUser.current()?.objectId ?? ""
                    let query = PFQuery(className:"User_follow")
                    query.whereKey("follower_id", equalTo: loggedIn)
                    if let  obj = self.getfinalArray.object(at: index) as? PFObject{
                        let otherId = obj.objectId ?? ""
                          query.whereKey("followed_id", equalTo: otherId)
                    }
                  
                    query.findObjectsInBackground(block: { (objects, error) in
                        cell.indicator.stopAnimating()
                        if error == nil {
                            for objectt in objects! {
                                
                                objectt.deleteInBackground(block: { (status, error) in
                                    print(status)
                                    cell.isUserInteractionEnabled = true
                                    if status == true{

                                        if self.getfinalArray.count > index{
                                            self.getfinalArray.removeObject(at: index)
                                            self.tableView.reloadData()
                                        }
                                    }else
                                    {
                                    }
                                })
                            }
                        }
                        else {
                            
                            let message = (error?.localizedDescription)
                            self.showMessage(message!)
                            print(error!)
                        }
                    })
                    
                case .cancel:
                     cell.indicator.stopAnimating()
                    print("cancel")
                    
                case .destructive:
                    cell.indicator.stopAnimating()
                    print("destructive")
                }
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
}
