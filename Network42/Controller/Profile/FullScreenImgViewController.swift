//
//  FullScreenImgViewController.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 26/04/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import Firebase

class FullScreenImgViewController: BaseViewController,UIScrollViewDelegate{
     var  bgimageToSend = UIImage()
    
   
    @IBOutlet var scrollV: UIScrollView!
    @IBOutlet var imageView: UIImageView!
    
     let imagePicker = UIImagePickerController()
    var pickerTypeStr:String = ""
     @IBOutlet var profileAcIndicator: UIActivityIndicatorView!
    @IBOutlet var editBtn: UIButton!
    var userProfile : Bool = true
    var profileCallback : (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        
        scrollV.minimumZoomScale=1
        scrollV.maximumZoomScale=3
        scrollV.bounces=false
        scrollV.delegate=self;
        
       
        imageView.image = bgimageToSend
         self.profileAcIndicator.stopAnimating()
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        if userProfile
        {
            self.editBtn.isHidden = false
        }else
        {
            self.editBtn.isHidden = true
        }
    }
    

    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }

    @IBAction func editPicture(_ sender: Any) {
        self.cameraAuthorization()
    }
   }





extension FullScreenImgViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate
{

    func cameraAuthorization(){
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }
    func chooseSheet(){
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openCamera()
        }
        
        let gallaryAction = UIAlertAction(title: "Gallery", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.openGallary()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
        }
        
        
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            alert.view.superview?.isUserInteractionEnabled = true
            
        }
        
    }
    func openGallary() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.photoLibrary)) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openCamera() {
        
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false
            self .present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func alertClose(_ gesture: UITapGestureRecognizer) {
        //        if imageEditBtnFlag == true {
        //            self.dismissViewControllerAnimated(true, completion: nil)
        //        }
    }
    func alertClose1(_ gesture: UITapGestureRecognizer) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        profileAcIndicator.startAnimating()
        if let pickedImage = info[.originalImage] as? UIImage {
            if pickerTypeStr == "user"{
                
                bgimageToSend = pickedImage
                let avatar = PFFile(data: bgimageToSend.jpegData(compressionQuality: 0.5)!)
                PFUser.current()!.setObject(avatar!, forKey: "profile_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    //print(status)
                    if status ==  true{
                        self.addImageAPI()
                        self.profileAcIndicator.stopAnimating()
                        self.profileCallback?()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                })
            }
            if pickerTypeStr == "background"{
                
                bgimageToSend = pickedImage
                
                let avatar = PFFile(data: bgimageToSend.jpegData(compressionQuality: 0.5)!)
                PFUser.current()!.setObject(avatar!, forKey: "cover_image")
                PFUser.current()!.saveInBackground(block: { (status, error) in
                    //print(status)
                    if status ==  true{
                        self.profileAcIndicator.stopAnimating()
                        self.profileCallback?()
                    }
                    else{
                        let message = (error?.localizedDescription)
                        self.showMessage(message!)
                    }
                })
            }
            
            imageView.image = bgimageToSend
            
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func addImageAPI(){
        //        let chatDictionary1:NSDictionary = [:]
        var urlString: String = ""
        
        let imageData:NSData = self.bgimageToSend.mediumQualityJPEGNSData as NSData
        // Create a reference to the file you want to upload
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        let  timeStrToPass = formatter.string(from: Date())
        
        let riversRef = FIRStorage.storage().reference().child("images/\(timeStrToPass).jpg")
        let metadata = FIRStorageMetadata()
        metadata.contentType = "image/jpeg"
        // Upload the file to the path "images/rivers.jpg"
        let uploadTask = riversRef.put(imageData as Data, metadata: metadata) { (metadata, error) in
            
        }
        
        uploadTask.observe(.success) { snapshot in
            print(snapshot) // Upload completed successfully
            let downloadURL = snapshot.metadata?.downloadURL()
            print(downloadURL!)
            urlString = String(describing: downloadURL!)
            
            if urlString != ""{
                let DBref = FIRDatabase.database().reference()
                //Sender data for group
                
                let userId = PFUser.current()!.objectId!
                
                let userDictionary = ["user_img":urlString] as [String : Any]
                let myRef = DBref.child("users/\(userId)")
                myRef.updateChildValues(userDictionary) { (error, DBref) in
                    if error != nil{
                        print("error == ",error!)
                    }
                }
            }
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}





