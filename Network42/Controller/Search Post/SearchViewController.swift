//
//  SearchViewController.swift
//  Network42
//
//  Created by 42works on 06/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse

class SearchViewController: BaseViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet weak var img_background: UIImageView!
    @IBOutlet weak var btnUsers: UIButton!
    @IBOutlet weak var btnHastag: UIButton!
   
    @IBOutlet var indicator: UIActivityIndicatorView!
    
    @IBOutlet var tableViewObj: UITableView!
    
    var searchResultArray : [String] = []
    var hashTagsGlobalIdsArray : [String] = []
    var hashTagsCountArray : [String] = []
    
    var NameArray : NSMutableArray = []
    var idsArray : NSMutableArray = []
    var UserNameArray : NSMutableArray = []
    var profileTypeArray : NSMutableArray = []
    var ProfileImageArray : NSMutableArray = []
    var PFObjectArray : NSMutableArray = []
    
    var PFObjForDeleteArray : NSMutableArray = []
    
    var friendsIdArray : NSMutableArray = []
    @IBOutlet var searchBar: UISearchBar!
    @IBOutlet var postTableview: UITableView!
    
    
    @IBOutlet var scrollView: UIScrollView!
    
    var textFieldInsideSearchBar =  UITextField()
    
    var statusArray : NSMutableArray = []
    var refreshControl: UIRefreshControl!
    
    var selectedSegment : Int = 0
    
    var pointNow = CGPoint.init(x: 0.0, y: 0.0)
//    var shownUserIndexes : [IndexPath] = []
//    var shownPostIndexes : [IndexPath] = []

    
    @IBOutlet var notFound: UILabel!
    
    var isVisible = false
    
    func start() {
        NotificationCenter.default.addObserver(self, selector: #selector(didShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
   @objc func didShow()
    {
        isVisible = true
    }
    
    @objc func didHide()
    {
        isVisible = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        let searchTextField:UITextField = searchBar.subviews[0].subviews.last as! UITextField
        searchTextField.layer.cornerRadius = 15
        searchTextField.textAlignment = NSTextAlignment.left
        let image:UIImage = UIImage.init(named: "search")!
        let imageView:UIImageView = UIImageView.init(image: image)
        searchTextField.leftView = nil
        searchTextField.rightView = imageView
        searchTextField.rightViewMode = .always
        if self.btnHastag.isSelected == true{
             scrollView.setContentOffset(CGPoint(x:self.view.frame.size.width, y:0), animated: false)
        }else{
             scrollView.setContentOffset(CGPoint(x:0, y:0), animated: false)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.start()
        btnUsers.layoutIfNeeded()
        btnHastag.layoutIfNeeded()
        scrollView.isScrollEnabled = false
        btnUsers.isSelected = true
        tableViewObj.register(UINib(nibName: "SearchUserCell", bundle: nil), forCellReuseIdentifier: "SearchUserCell")
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 702
        
        postTableview.register(UINib(nibName: "PostNewCell", bundle: nil), forCellReuseIdentifier: "PostNewCell")
        postTableview.rowHeight = UITableView.automaticDimension
        postTableview.estimatedRowHeight = 702
        
        textFieldInsideSearchBar = (self.searchBar.value(forKey: "searchField") as? UITextField)!
        textFieldInsideSearchBar.textColor = UIColor.white
        textFieldInsideSearchBar.font = UIFont.init(name: "SanFranciscoText-Regular", size: 14)!
        textFieldInsideSearchBar.setValue(UIColor.white, forKeyPath: "_placeholderLabel.textColor")
        
        let glassIconView = textFieldInsideSearchBar.leftView as! UIImageView
        glassIconView.image = glassIconView.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
        glassIconView.tintColor = UIColor.white
        
        let clearButton = textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
    clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        
        clearButton.tintColor = UIColor.white
        
      

        NotificationCenter.default.addObserver(self, selector: #selector(SearchViewController.getIds), name: NSNotification.Name(rawValue: "updateSearchStatus"), object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        tableViewObj.addGestureRecognizer(tapGesture)
        
        tapGesture.delegate = self
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(SearchViewController.hideKeyboard))
        tapGesture2.cancelsTouchesInView = false
        postTableview.addGestureRecognizer(tapGesture2)
        
        tapGesture2.delegate = self
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action:#selector(SearchViewController.callApi), for: UIControl.Event.valueChanged)
        tableViewObj.addSubview(refreshControl)
        
//        let attr = NSDictionary(object: UIFont.init(name: "SanFranciscoText-Semibold", size: 15)!, forKey: NSAttributedString.Key.font as NSCopying)
//        segmentObj.setTitleTextAttributes(attr as? [NSAttributedString.Key : Any], for: .normal)
        
        setBorderForBtns()
        self.callSearchApi(currentWord: "")
        self.callHashTagsApi(currentWord: "#")
    }
    
    func setBorderForBtns(){
        self.view.layoutIfNeeded()
        selectedSegment = 0
        if btnUsers.isSelected == true
        {
            
            self.updateLabel(userTable: true)
            scrollView.setContentOffset(CGPoint(x:0, y:0), animated: false)
            if let all = btnHastag.layer.sublayers {
                for value in all {
                    if value.name == "test"{
                        if let index = all.index(of: value){
                            btnHastag.layer.sublayers?.remove(at: index)
                            break
                        }
                    }
                }
            }
             self.btnUsers.layoutIfNeeded()
              self.btnUsers.layer.addBorder(edge: .bottom, color: ConstantColors.themeColor, thickness: 2.0)
        }else
        {
            
            self.updateLabel(userTable: false)
            let x =  self.view.frame.size.width
            scrollView.setContentOffset(CGPoint(x:x, y:0), animated: false)
            self.notFound.isHidden = true
            if hashTagsCountArray.count>0{
                // self.img_background.isHidden = true
                self.notFound.isHidden = true
            }else{
                //self.img_background.isHidden = false
                self.notFound.text = "No hashtags found"
                self.notFound.isHidden = false
            }
            if let all = btnUsers.layer.sublayers {
                for value in all {
                    if value.name == "test"{
                        if let index = all.index(of: value){
                            btnUsers.layer.sublayers?.remove(at: index)
                            break
                        }
                    }
                }
            }
             self.btnHastag.layoutIfNeeded()
             self.btnHastag.layer.addBorder(edge: .bottom, color: ConstantColors.themeColor, thickness: 2.0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
        self.getIds()
        self.tabBarController?.tabBar.isHidden = true
    }
    
    //MARK:- IBAction
    
    @IBAction func action_topBtns(_ sender: UIButton) {
        if let all = btnUsers.layer.sublayers {
            for value in all {
                if value.name == "test"{
                    if let index = all.index(of: value){
                        btnUsers.layer.sublayers?.remove(at: index)
                        break
                    }
                }
            }
        }
        if let all = self.btnHastag.layer.sublayers {
            for value in all {
                if value.name == "test"{
                    if let index = all.index(of: value){
                        self.btnHastag.layer.sublayers?.remove(at: index)
                        break
                    }
                }
            }
            
        }
        
        if sender == btnUsers{
            if sender.isSelected == true{
                 selectedSegment = 1
                sender.isSelected = false
                btnHastag.isSelected = true
                let x =  self.view.frame.size.width
                scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
                self.updateLabel(userTable: false)
//                if hashTagsCountArray.count > 0{
//                    self.img_background.isHidden = true
//                }else{
//                    self.img_background.isHidden = false
//                }
               
                self.btnHastag.layer.addBorder(edge: .bottom, color: ConstantColors.themeColor, thickness: 2.0)
            }else{
                 selectedSegment = 0
                sender.isSelected = true
                btnHastag.isSelected = false
                self.updateLabel(userTable: true)
               // self.img_background.isHidden = true
                scrollView.setContentOffset(CGPoint(x:0, y:0), animated: true)
                
                sender.layer.addBorder(edge: .bottom, color: ConstantColors.themeColor, thickness: 2.0)
                
                
            }
        }else{
            if sender.isSelected == true{
                 selectedSegment = 0
                sender.isSelected = false
                btnUsers.isSelected = true
                  self.updateLabel(userTable: true)
                scrollView.setContentOffset(CGPoint(x:0, y:0), animated: true)
                // self.img_background.isHidden = true
              
                self.btnUsers.layer.addBorder(edge: .bottom, color: ConstantColors.themeColor, thickness: 2.0)
            }else{
                 selectedSegment = 1
                sender.isSelected = true
                 self.updateLabel(userTable: false)
                btnUsers.isSelected = false
//                if hashTagsCountArray.count > 0{
//                    self.img_background.isHidden = true
//                }else{
//                    self.img_background.isHidden = false
//                }
                let x =  self.view.frame.size.width
                scrollView.setContentOffset(CGPoint(x:x, y:0), animated: true)
               
                sender.layer.addBorder(edge: .bottom, color: ConstantColors.themeColor, thickness: 2.0)
                
            }
        }
    }
    
    @IBAction func backTap(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @objc func getIds()
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
         PFObjForDeleteArray = NSMutableArray()
         friendsIdArray = NSMutableArray()
         statusArray = NSMutableArray()
        let query = PFQuery(className: "User_follow")
        let objId = PFUser.current()?.objectId
        query.limit = 100
        query.whereKey("follower_id", equalTo:objId!)
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                let ids : NSMutableArray = []
                let PFObjectArr : NSMutableArray = []
                for object in objects! {
                    
                    PFObjectArr.add(object)
                    
                    print(object)
                    ids.add((object.object(forKey: "followed_id") as! String))
                    if let status = object.object(forKey: "status") as? String
                    {
                        if status == "0"
                        {
                            self.statusArray.add("Cancel")
                        }else
                        {
                          self.statusArray.add((object.object(forKey: "followed_id") as! String))
                        }
                     
                    }
                }
                self.PFObjForDeleteArray = PFObjectArr
                self.friendsIdArray = ids
                self.tableViewObj.reloadData()
            }
            else {
                self.tableViewObj.reloadData()
                print(error!)
            }
        })
    }
    
    @IBAction func segmentAction(_ sender: UISegmentedControl) {
        scrollView.isScrollEnabled = true
        selectedSegment = sender.selectedSegmentIndex
        switch sender.selectedSegmentIndex
        {
        case 0:
            print(sender.selectedSegmentIndex)
           
           
        case 1:
             print(sender.selectedSegmentIndex)
            
        default:
            break; 
        }
         scrollView.isScrollEnabled = false
    }
    
    
    //MARK:- Gesture Delegate
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: tableViewObj))! || (touch.view?.isDescendant(of: postTableview))!{
            return true
        }
        return true
    }
}

extension SearchViewController:UITableViewDelegate,UITableViewDataSource,followDelegate
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tableViewObj
        {
            return UITableView.automaticDimension
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tableViewObj
        {
            self.updateLabel(userTable: true)
            return NameArray.count
        }else
        {
            self.updateLabel(userTable: false)
            return searchResultArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tableViewObj {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchUserCell", for: indexPath as IndexPath) as! SearchUserCell
            cell.name.text = (NameArray.object(at: indexPath.row) as? String)?.trim()
            cell.userName.text = (UserNameArray.object(at: indexPath.row) as? String)?.trim()
            if let file = ProfileImageArray.object(at: indexPath.row) as? PFFile
            {
                cell.profileImage.file = file
                cell.profileImage.loadInBackground()
            }else
            {
                cell.profileImage.image = UIImage.init(named: "dummyProfile")
            }
            cell.followBtn.layer.borderWidth = 1.0
            
            if friendsIdArray.contains(idsArray.object(at: indexPath.row))
            {
                if statusArray.contains(idsArray.object(at: indexPath.row))
                {
                    cell.followBtn.setTitle("Following", for: .normal)
                }else
                {
                    cell.followBtn.setTitle("Cancel", for: .normal)
                }
                cell.followBtn.backgroundColor = ConstantColors.themeColor
                cell.followBtn.setTitleColor(UIColor.white, for: .normal)
                cell.followBtn.layer.borderColor = ConstantColors.themeColor.cgColor
            }else
            {
                cell.followBtn.setTitle("Follow", for: .normal)
                cell.followBtn.backgroundColor = UIColor.clear
                cell.followBtn.setTitleColor(ConstantColors.themeColor, for: .normal)
                cell.followBtn.layer.borderColor = ConstantColors.themeColor.cgColor
            }
            
            let objId = PFUser.current()?.objectId
            if idsArray.object(at: indexPath.row) as? String == objId {
                cell.followBtn.isHidden = true
            }else
            {
                cell.followBtn.isHidden = false
            }
            
            cell.followBtn.tag = indexPath.row
            cell.delegate = self
            cell.selectionStyle = .none
            return cell
        }else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostNewCell", for: indexPath as IndexPath) as! PostNewCell
        
            cell.titleObj.text = searchResultArray[indexPath.row]
            
            if hashTagsCountArray.count > indexPath.row {
                cell.postCountObj.text = hashTagsCountArray[indexPath.row]
            } else {
                cell.postCountObj.text = ""
            }
            
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isVisible {
            searchBar.resignFirstResponder()
            return            
        }
        
        if tableView == tableViewObj
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            vc.fromSearch = true
            let objId = PFUser.current()?.objectId
            if idsArray.object(at: indexPath.row) as? String == objId {
                vc.userProfile = true
            }else
            {
                vc.userProfile = false
            }
            
            vc.refreshSearchBlocked = true
            vc.callback = { self.callSearchApi(currentWord: self.searchBar.text!) }
            
            if friendsIdArray.contains(idsArray.object(at: indexPath.row))
            {
                if statusArray.contains(idsArray.object(at: indexPath.row))
                {
                    vc.friend = "FOLLOWING"
                }else
                {
                    vc.friend = "Cancel"
                }
            }else
            {
                vc.friend = "FOLLOW"
            }

            let PFUserVar = PFObjectArray.object(at: indexPath.row) as! PFUser
            vc.userFromSearch = PFUserVar
            self.navigationController?.pushViewController(vc, animated: true)
        }else
        {
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "HashTag_VC") as! HashTag_VC
           // vc.secondTime = true
           // vc.isFromHashTag = true
            let leftBrace = "("
            let rightBrace = ")"
            let count = "\(leftBrace)\(hashTagsCountArray[indexPath.row])\(rightBrace)"
           // vc.titleSecond = "\(searchResultArray[indexPath.row])\(count)"
            vc.tagString = searchResultArray[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath)
//    {
//        if selectedSegment == 0
//        {
//            if (shownUserIndexes.contains(indexPath) == false) {
//                shownUserIndexes.append(indexPath)
//                let frame = CGRect.init(x: cell.frame.origin.x - 200, y: cell.frame.origin.y , width: cell.frame.width, height: cell.frame.height)
//                cell.frame = frame
//                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseInOut, animations: {
//                    
//                    let frame = CGRect.init(x: cell.frame.origin.x + 200, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height)
//                    cell.frame = frame
//                }) { finished in
//                }
//            }
//        }else
//        {
//            if (shownPostIndexes.contains(indexPath) == false) {
//                shownPostIndexes.append(indexPath)
//                let frame = CGRect.init(x: cell.frame.origin.x - 200, y: cell.frame.origin.y , width: cell.frame.width, height: cell.frame.height)
//                cell.frame = frame
//                UIView.animate(withDuration: 0.5, delay: 0.0, options: .curveEaseIn, animations: {
//                    
//                    let frame = CGRect.init(x: cell.frame.origin.x + 200, y: cell.frame.origin.y, width: cell.frame.width, height: cell.frame.height)
//                    cell.frame = frame
//                }) { finished in
//                }
//            }
//        }
//    }
    
    
    func followClickedDelegate(index:Int,cell:SearchUserCell) {
        print("==",index)
        
        print(self.profileTypeArray)
        print(self.profileTypeArray.object(at: index))
        
        if cell.followBtn.titleLabel?.text == "Follow"
        {
           checkWhetherUserBlocked(index:index,cell:cell)
        }else
        {
             var alert = UIAlertController()
           if cell.followBtn.titleLabel?.text == "Following"
           {
            alert = UIAlertController(title: "Are you sure you want to unfollow \(NameArray.object(at: index))?", message: "", preferredStyle: UIAlertController.Style.alert)
           }else
           {
            alert = UIAlertController(title: "Are you sure you want to cancel this request?", message: "", preferredStyle: UIAlertController.Style.alert)
            }
            
            
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                let clearButton = self.textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
                clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
                clearButton.tintColor = UIColor.white
            }))
            alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
                switch action.style{
                case .default:
                   
                        cell.indicator.startAnimating()
                        cell.isUserInteractionEnabled = false
                        cell.followBtn.setTitle("", for: .normal)
                        
                        let loggedIn = PFUser.current()?.objectId
                        let query = PFQuery(className:"User_follow")
                        query.whereKey("follower_id", equalTo: loggedIn!)
                        query.whereKey("followed_id", equalTo: self.idsArray.object(at: index) as! String)
                        query.findObjectsInBackground(block: { (objects, error) in
                            if error == nil {
                                for objectt in objects! {
                                    
                                    
                                    objectt.deleteInBackground(block: { (status, error) in
                                        print(status)
                                        cell.isUserInteractionEnabled = true
                                        if status == true{
                                            if cell.followBtn.titleLabel?.text == "Following"
                                            {
                                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                                            }
                                            self.getIds()
                                        }else
                                        {
                                            self.getIds()
                                        }
                                    })
                                }
                            }
                            else {
                                let message = (error?.localizedDescription)
                                self.showMessage(message!)
                                print(error!)
                            }
                        })
                   
                    
                    
                    let clearButton = self.textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
                        clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(.alwaysTemplate) , for: .normal)
                    clearButton.tintColor = UIColor.white
                    
                    
                case .cancel:
                    print("cancel")
                    let clearButton = self.textFieldInsideSearchBar.value(forKey: "clearButton") as! UIButton
                    clearButton.setImage(clearButton.imageView?.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
                    clearButton.tintColor = UIColor.white
                    
                case .destructive:
                    print("destructive")
                }
            }))

            self.present(alert, animated: true, completion: nil)
        }
    }
    //MARK:- unblock and follow
    func unblockUser(index:Int,cell:SearchUserCell){
        var objId = ""
        if let id = idsArray.object(at: index) as? String{
           objId = id
        }
        let blockedUserObj = PFQuery(className: "BlockedUsers")
        blockedUserObj.whereKey("FromUser", equalTo: PFUser.current()?.objectId ?? "")
        blockedUserObj.whereKey("BlockedUser", equalTo:objId )
        blockedUserObj.findObjectsInBackground { (objects, error) in
            if error == nil{
                if objects != nil{
                    if objects!.count > 0{
                        for value in objects!{
                            value.deleteInBackground(block: { (success, error) in
                                if success == true{
                                    self.followUser(index:index,cell:cell)
                                }
                            })
                        }
                    }
                }
            }
        }
    }
    
    func followUser(index:Int,cell:SearchUserCell){
        cell.indicator.startAnimating()
        cell.followBtn.setTitle("", for: .normal)
        cell.isUserInteractionEnabled = false
        let objId = PFUser.current()?.objectId
        let User_followClass = PFObject(className: "User_follow")
        User_followClass["follower_id"] = objId
        User_followClass["followed_id"] = idsArray.object(at: index) as! String
        User_followClass["parent_follower"] = PFUser.current()
        User_followClass["parent_following"] = PFObjectArray.object(at: index)
        if self.profileTypeArray.object(at: index) as! String == "1"
        {
            User_followClass["status"] = "1"
        }else
        {
            User_followClass["status"] = "0"
        }
        
        
        User_followClass.saveInBackground { (success, error) in
            
            cell.isUserInteractionEnabled = true
            if(success)
            {
                print("user follow")
                self.getIds()
                
                let user = self.PFObjectArray.object(at: index) as? PFUser
                let currentName = PFUser.current()?.value(forKey: "name") as! String
                
                if self.profileTypeArray.object(at: index) as! String == "1"
                {
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
                    let time = formatter.string(from: Date())
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTaggedUser"), object: nil)
                    
                    self.saveToNotifications(from_id: objId!, to_id: user!.objectId!, notification_type: "follow_public_user", time: time, post_id: "", parent_from: PFUser.current()!, parent_to: user!,commentStr: "")
                    
                    self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.startedfollowing)",type:"follow_public_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                }else
                {
                    self.sendPushNotification(postId: "", userId: user!.objectId!,settingType: "push_send_follow_request",message:"\(currentName) \(messages.requestSent)",type:"follow_user",userIdArray:[],groupMembersId:"",MsgType:"",reciverId:"",username:"",groupName:"")
                }
            }
            else{
                print("error",error!)
                cell.indicator.stopAnimating()
                cell.followBtn.setTitle("Follow", for: .normal)
                let message = (error?.localizedDescription)
                self.showMessage(message!)
                return
            }
        }
    }
    
    func checkWhetherUserBlocked(index:Int,cell:SearchUserCell){
        let query = PFQuery(className:"BlockedUsers")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        var otherUserId = ""
        if let str = self.idsArray.object(at: index) as? String{
            otherUserId = str
        }
        query.whereKey("BlockedUser", equalTo: otherUserId)
        query.whereKey("FromUser", equalTo: id)
        query.findObjectsInBackground(block: { (objects2, error) in
            //self.isLoading = false
            if error == nil {
                if objects2?.count ?? 0 > 0{
                   let alert = UIAlertController.init(title: "", message:"Do you want to unblock and follow this user?", preferredStyle: .alert)
                    let actionCancel = UIAlertAction.init(title: "Cancel", style: .cancel, handler: { (action) in
                        
                    })
                    let actionYes = UIAlertAction.init(title: "OK", style: .default, handler: { (action) in
                        //for follow action
                       self.unblockUser(index:index,cell:cell)
                    })
                alert.addAction(actionCancel)
                alert.addAction(actionYes)
                self.present(alert, animated: true, completion: nil)
                }else{
                   self.followUser(index: index, cell: cell)
                }
            }
            else {
                self.showMessage((error?.localizedDescription)!)
            }
        })
    }
}


extension SearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {

        self.callSearchApi(currentWord: searchText)
        self.callHashTagsApi(currentWord: "#"+searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
//        guard searchBar.text!.count != 0 else {
//            
//            DispatchQueue.main.async {
//                //searchBar.resignFirstResponder()
//                self.NameArray.removeAllObjects()
//                self.tableViewObj.reloadData()
//                
//                self.searchResultArray.removeAll()
//                self.postTableview.reloadData()
//            }
//            return
//        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        searchBar.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func callApi()
    {
        if !(searchBar.text! == "")
        {
            self.getIds()
            self.callSearchApi(currentWord: searchBar.text!)
        }else
        {
         self.refreshControl.endRefreshing()
        }
    }
    
    func callSearchApi(currentWord:String)
    {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
//userQuery4.whereNotContainedIn("objectId", ActivityHome.getInstance().listBlockedUsers);
        indicator.startAnimating()
        
        var blockedUserArray : [String] = []
        if let blockedUsers = PFUser.current()?.value(forKey: "blockedUsers") as? [String]
        {
            blockedUserArray = blockedUsers
            print(blockedUserArray)
        }
        
        let myIdArray : [String] = [(PFUser.current()?.objectId)!]
        print(myIdArray)
        
        print(currentWord)
        let nameQuery : PFQuery = PFUser.query()!
        if currentWord != "" {
            nameQuery.whereKey("name", hasPrefix: currentWord)
        }
        
        nameQuery.whereKey("objectId", notContainedIn: blockedUserArray)
        nameQuery.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let nameQuerylowercased : PFQuery = PFUser.query()!
        nameQuerylowercased.whereKey("name", hasPrefix: currentWord.smallFirstLetter())
        nameQuerylowercased.whereKey("objectId", notContainedIn: blockedUserArray)
        nameQuerylowercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let nameQueryuppercased : PFQuery = PFUser.query()!
        nameQueryuppercased.whereKey("name", hasPrefix: currentWord.capitalizingFirstLetter())
        nameQueryuppercased.whereKey("objectId", notContainedIn: blockedUserArray)
        nameQueryuppercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        
        let userNameQuery : PFQuery = PFUser.query()!
        userNameQuery.whereKey("user_name", hasPrefix: currentWord)
        userNameQuery.whereKey("objectId", notContainedIn: blockedUserArray)
        userNameQuery.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let userNameQuerylowercased : PFQuery = PFUser.query()!
        userNameQuerylowercased.whereKey("user_name", hasPrefix: currentWord.smallFirstLetter())
        userNameQuerylowercased.whereKey("objectId", notContainedIn: blockedUserArray)
        userNameQuerylowercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let userNameQueryuppercased : PFQuery = PFUser.query()!
        userNameQueryuppercased.whereKey("user_name", hasPrefix: currentWord.capitalizingFirstLetter())
        userNameQueryuppercased.whereKey("objectId", notContainedIn: blockedUserArray)
        userNameQueryuppercased.whereKey("blockedUsers", notContainedIn: myIdArray)
        
        let query = PFQuery.orQuery(withSubqueries: [nameQuery,nameQuerylowercased,nameQueryuppercased,
                                                     userNameQuery,userNameQuerylowercased,userNameQueryuppercased])
        if currentWord != "" {
            query.order(byAscending: "name")
        }
        
        if currentWord == "" {
            query.limit = 10
        } else {
            query.limit = 100
        }
        
        query.findObjectsInBackground(block: { (objects, error) in
            self.indicator.stopAnimating()
             self.refreshControl.endRefreshing()
            if error == nil {
                
                let name : NSMutableArray = []
                let userName : NSMutableArray = []
                let ids : NSMutableArray = []
                let profile : NSMutableArray = []
                let profileType : NSMutableArray = []
                let PFObjectArr : NSMutableArray = []
                
                for object in objects! {
                    //print(object)
                    PFObjectArr.add(object)
                    name.add(object.object(forKey: "name") as? String ?? "")
                    userName.add(object.object(forKey: "user_name") as? String ?? "")
                    
                    if let type = object.object(forKey: "profile_type") as? String
                    {
                         profileType.add(type)
                    }else
                    {
                        profileType.add("1")
                    }
                    
                    ids.add(object.objectId!)
                    if let profileObj = object.object(forKey: "profile_image") as? PFFile
                    {
                        profile.add(profileObj)
                    }
                    else
                    {
                        profile.add("")
                    }
                }
                self.NameArray = name
                self.UserNameArray = userName
                self.idsArray = ids
                self.profileTypeArray = profileType
                self.ProfileImageArray = profile
                self.PFObjectArray = PFObjectArr
                self.tableViewObj.reloadData()
            }
            else {
                print(error!)
            }
        })
    }
    
    
    func callHashTagsApi(currentWord:String)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        //print(currentWord)
        let query = PFQuery(className: "Hashtags")
        
        if currentWord == "" {
            query.limit = 10
        } else {
            query.limit = 100
            query.whereKey("name", hasPrefix: currentWord.lowercased())
        }
        query.findObjectsInBackground(block: { (objects, error) in
            if error == nil {
                
                print(objects!)
                
                var name : [String] = []
                var ids : [String] = []
                for object in objects! {
                    print(object)
                    name.append(object.object(forKey: "name") as! String)
                    ids.append(object.objectId!)
                }
                self.searchResultArray = name
                self.hashTagsGlobalIdsArray = ids
                
                if self.searchResultArray.count == 0
                {
                    self.postTableview.reloadData()
                    return
                }
                
                self.hashTagsCountArray = []
                self.callHashTagPostApi(i: 0)
                
            }
            else {
                print(error!)
            }
        })
    }
    
    
    func callHashTagPostApi(i:Int)
    {
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
            return
        }
        
        let query = PFQuery(className: "Hashtag_posts")
        query.includeKey("parent_post")
        query.whereKey("hashtag_id", equalTo: hashTagsGlobalIdsArray[i])
        query.limit = 100
        query.countObjectsInBackground { (count, error) in
            if error == nil {
                if count>1 {
                    self.hashTagsCountArray.append("\(count) Posts")
                } else {
                    self.hashTagsCountArray.append("\(count) Post")
                }
                
                if self.hashTagsGlobalIdsArray.count > i + 1 {
                    self.callHashTagPostApi(i: i + 1)
                } else {
                    self.postTableview.reloadData()
                }
            }
            else {
                print(error!)
                if self.hashTagsGlobalIdsArray.count > i + 1 {
                    self.callHashTagPostApi(i: i + 1)
                } else {
                    self.postTableview.reloadData()
                }
                
            }
        }
    }
}

extension SearchViewController: UIScrollViewDelegate
{
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
//        let pageNo = round(scrollView.contentOffset.x / scrollView.frame.size.width);
//        print(scrollView.contentOffset.x)
//        if  pageNo < 1 {
//            segmentObj.selectedSegmentIndex = 0
//        }else
//        {
//            segmentObj.selectedSegmentIndex = 1
//        }

     }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        let pageNo = round(scrollView.contentOffset.x / scrollView.frame.size.width);
//        print(scrollView.contentOffset.x)
//        if  pageNo < 1 {
//            segmentObj.selectedSegmentIndex = 0
//        }else
//        {
//            segmentObj.selectedSegmentIndex = 1
//        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let pageNo = round(scrollView.contentOffset.x / scrollView.frame.size.width);
//        print(scrollView.contentOffset.x)
//        if  pageNo < 1 {
//            segmentObj.selectedSegmentIndex = 0
//        }else
//        {
//            segmentObj.selectedSegmentIndex = 1
//        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    }
    
    func updateLabel(userTable:Bool)
    {
        
        if selectedSegment == 0 && NameArray.count == 0 {
            notFound.text = "No users found"
            self.notFound.isHidden = false
        } else if selectedSegment == 1 && searchResultArray.count == 0 {
                notFound.text = "No hashtags found"
                self.notFound.isHidden = false
        } else {
            notFound.text = ""
        }
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        let first = String(characters.prefix(1)).capitalized
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
    
    func smallFirstLetter() -> String {
        let first = String(characters.prefix(1)).lowercased()
        let other = String(characters.dropFirst())
        return first + other
    }
    
    mutating func smalleFirstLetter() {
        self = self.smallFirstLetter()
    }
}

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer();
        border.name = "test"
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x:0, y:self.frame.height - thickness, width:self.frame.width, height:thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x:0, y:0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x:self.frame.width - thickness, y: 0, width: thickness, height:self.frame.height)
            break
        default:
            break
        }
        border.backgroundColor = color.cgColor;
        self.addSublayer(border)
    }
    
}

