//
//  PostTableViewCell.swift
//  Network42
//
//  Created by 42works on 10/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet var postCountObj: UILabel!
    @IBOutlet var titleObj: UILabel!
    @IBOutlet var bottomLayoutObj: NSLayoutConstraint!
    @IBOutlet var topLayout: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
