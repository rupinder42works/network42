//
//  SearchUserCell.swift
//  Network42
//
//  Created by 42works on 06/05/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI

@objc protocol followDelegate {
    @objc optional func followClickedDelegate(index:Int,cell:SearchUserCell)
}


class SearchUserCell: UITableViewCell {
    
    @IBOutlet weak var vw_profilebackground: UIView!
    @IBOutlet var profileImage: PFImageView!
    @IBOutlet var userName: UILabel!
    @IBOutlet var name: UILabel!
    @IBOutlet var followBtn: UIButton!
     weak var delegate: followDelegate?
    @IBOutlet var indicator: UIActivityIndicatorView!
    @IBOutlet var containerImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func followAction(_ sender: Any) {
        self.delegate?.followClickedDelegate!(index: (sender as AnyObject).tag,cell: self)
    }
    
}
