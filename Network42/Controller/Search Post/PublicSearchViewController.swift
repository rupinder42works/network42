//
//  PublicSearchViewController.swift
//  Network42
//
//  Created by Apple3 on 15/10/18.
//  Copyright © 2018 42works. All rights reserved.
//

import UIKit
import Parse
import MBProgressHUD

class PublicSearchViewController: BaseViewController {

    //MARK:- Variables
    var selectedIndex = 0
    var refreshControl = UIRefreshControl()
    var arrayPost = [Post]()
    lazy var postViewModel = PostViewModel()
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonSortBy: UIButton!
    @IBOutlet weak var labelSort: UILabel!
    @IBOutlet weak var collectionViewSearch: UICollectionView! {
        didSet {
            collectionViewSearch.register(UINib(nibName: "HomeProductCell", bundle: nil), forCellWithReuseIdentifier: "homeProductCell")
            collectionViewSearch.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 5, right: 0)
            
            refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
            refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
            collectionViewSearch.addSubview(refreshControl)
        }
    }
    
    @objc func refresh() {
        postViewModel.isRefresh = true
        self.sortPost(selectedIndex)
    }
    
    //MRK:- View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        postInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        self.navigationController.botto
        
        self.tabBarController?.tabBar.isHidden = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK:- Private
    private func postInit() {
        
        postViewModel.endRefreshClausre = {
        }
        postViewModel.updateLoadingStatus = {
            if self.postViewModel.isRefresh {
                self.postViewModel.isRefresh = false
                self.refreshControl.endRefreshing()
            }
            if self.postViewModel.isLoading {
            } else {
            }
        }
        postViewModel.reloadTableViewClosure = {
            DispatchQueue.main.async {
                MBProgressHUD.hide(for: self.view, animated: true)
                self.collectionViewSearch.reloadData()
            }
        }
        self.sortPost(1)
    }
    
    private func sortPost(_ index: Int) {
        postViewModel.sortBy = index
        var title = ""
        switch index {
        case 1:
            title = "Sort by likes"
            break
        case 2:
            title = "Sort by comments"
            break
        default:
            title = "Sort by recent published"
            break
        }
        selectedIndex = index
        labelSort.text = title
        
        postViewModel.getAllPosts()
    }
    
    //MARK:- IBAction
    @IBAction func searchByHashTag() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "Sort by likes", style: .default , handler:{ (UIAlertAction)in
            self.sortPost(1)
        }))
        
        alert.addAction(UIAlertAction(title: "Sort by comments", style: .default , handler:{ (UIAlertAction)in
            self.sortPost(2)
        }))
        
        alert.addAction(UIAlertAction(title: "Sort by recent published", style: .default , handler:{ (UIAlertAction)in
            self.sortPost(3)
        }))
        
        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler:{ (UIAlertAction)in
            
        }))
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func searchByRecentPublish() {
        let searchVC = self.storyboard?.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        self.navigationController?.pushViewController(searchVC, animated: true)
       
    }
}

extension PublicSearchViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if let obj = postViewModel.array?[indexPath.row] {
            let tabbarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
            let vc = tabbarSB.instantiateViewController(withIdentifier: "PostDetailViewController")  as! PostDetailViewController
            vc.postId = obj.id
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension PublicSearchViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postViewModel.array?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeProductCell", for: indexPath)as! HomeProductCell
        cell.imageYoutube.isHidden = true
        cell.imageAlbum.isHidden = true
        
        if let obj = postViewModel.array?[indexPath.row] {
            if obj.postType.lowercased() == "video"{
                 cell.imageYoutube.isHidden = true
                // cell.imageProduct.setImage(fromUrl: obj.thumbnail?.first, defaultImage: #imageLiteral(resourceName: "dummyProfile"))
                cell.imageProduct.setImage(fromUrl: obj.thumbnail?.first, defaultImage: #imageLiteral(resourceName: "dummyProfile"), isCircled: true, borderColor: .white, borderWidth: 1.0, isCache: false, showIndicator: false)
            }
            else{
                if obj.postMedia != nil{
                    //cell.imageProduct.setImage(fromUrl: obj.postMedia?.first, defaultImage: #imageLiteral(resourceName: "dummyProfile"))
                    cell.imageProduct.setImage(fromUrl: obj.postMedia?.first, defaultImage: #imageLiteral(resourceName: "dummyProfile"), isCircled: true, borderColor: .white, borderWidth: 1.0, isCache: false, showIndicator: false)
                }
                if obj.postType.lowercased() == "album"{
                    cell.imageAlbum.isHidden = false
                    cell.bringSubviewToFront(cell.imageAlbum)
                }
            }
            cell.type = obj.type
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3 - 1, height: collectionView.frame.width/3 - 1)
    }
}
