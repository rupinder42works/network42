//
//  TagUserViewController.swift
//  Network42
//
//  Created by 42works on 10/04/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse

class TagUserViewController: UIViewController {
    

    @IBOutlet weak var htConstrnt_outerView: NSLayoutConstraint!
    @IBOutlet weak var lbl_noUser: UILabel!
    @IBOutlet var collectionViewObj: UICollectionView!
    @IBOutlet var tableViewObj: UITableView!
    var tickImageStr : String = "tick.png"
    var defalutImageStr : String = "white.png"
    
    var tagNameArray : NSMutableArray = []
    var idsArray : NSMutableArray = []
    var tagUserNameArray : NSMutableArray = []
    var tagProfileImageArray : NSMutableArray = []
    
    var tagTickArray : NSMutableArray = []
    var collectionArray : NSMutableArray = []
    
    @IBOutlet var flowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet var collectionLbl: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tagNameArray =  globalDic.tagUserDictionary.value(forKey: "name") as! NSMutableArray
        idsArray =  globalDic.tagUserDictionary.value(forKey: "id") as! NSMutableArray
        tagUserNameArray =  globalDic.tagUserDictionary.value(forKey: "userName") as! NSMutableArray
        tagProfileImageArray =  globalDic.tagUserDictionary.value(forKey: "image") as! NSMutableArray
       
        if tagNameArray.count > 0 {
            lbl_noUser.isHidden = true
            collectionViewObj.isHidden = false
            self.htConstrnt_outerView.constant = 60
        }else{
            lbl_noUser.isHidden = false
            collectionViewObj.isHidden = true
            self.htConstrnt_outerView.constant = 0
        }
        for _ in 0..<tagNameArray.count
        {
            tagTickArray.add(defalutImageStr)
        }
        
        if let completeArray =  UserDefaults.standard.object(forKey: "taggedUser") as? NSArray{
            if let taggedArrayUserDefault =  completeArray.value(forKey: "name") as? NSArray{
                print(taggedArrayUserDefault)
                
                if taggedArrayUserDefault.count>0
                {
                    collectionArray =  NSMutableArray(array: taggedArrayUserDefault)
                    collectionLbl.isHidden = true
                    
                    for i in 0..<taggedArrayUserDefault.count
                    {
                        let trimmedString = (taggedArrayUserDefault.object(at: i) as! String).trimmingCharacters(in: .whitespaces)
                        let indexOf = tagNameArray.index(of: trimmedString)
                        tagTickArray.replaceObject(at: indexOf, with: tickImageStr)
                    }
                }
            }
        }
      
        collectionViewObj.register(UINib(nibName: "TagCollectionViewCell", bundle:nil), forCellWithReuseIdentifier: "TagCollectionViewCell")
        tableViewObj.register(UINib(nibName: "LargeTagCell", bundle: nil), forCellReuseIdentifier: "LargeTagCell")
        tableViewObj.rowHeight = UITableView.automaticDimension
        tableViewObj.estimatedRowHeight = 702
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func back(_ sender: Any) {
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func crossButton(_ sender: Any)
    {
        collectionLbl.isHidden = false
        tagTickArray.removeAllObjects()
        for _ in 0..<tagNameArray.count
        {
            tagTickArray.add(defalutImageStr)
        }
        tableViewObj.reloadData()
        collectionArray.removeAllObjects()
        collectionViewObj.reloadData()
        collectionViewObj.collectionViewLayout.invalidateLayout()
    }
    @IBAction func doneButton(_ sender: Any)
    {
        var newArray : NSMutableArray = []
        for i in 0..<collectionArray.count
        {
            let str = collectionArray.object(at: i) as! String
            let trimmedString = str.trimmingCharacters(in: .whitespaces)
            print(trimmedString)
            let index = tagNameArray.index(of: trimmedString)
            
            print(idsArray)
            print(tagNameArray)
            print(collectionArray)
            
            if index > idsArray.count - 1 {
                continue
            }
            
            let dic : NSMutableDictionary = [:]
            dic.setValue(idsArray.object(at: index) as! String, forKey: "id")
            dic.setValue(tagNameArray.object(at: index) as! String, forKey: "name")
            dic.setValue(collectionArray.object(at: i) as! String, forKey: "username")
            
            newArray.add(dic)
        }
        
        
        UserDefaults.standard.set(newArray, forKey: "taggedUser")
        UserDefaults.standard.synchronize()
        
        _ = self.navigationController?.popViewController(animated: true)
    }
    
}


extension TagUserViewController:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tagNameArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "LargeTagCell", for: indexPath as IndexPath) as! LargeTagCell

        
        cell.name.text = tagNameArray.object(at: indexPath.row) as? String
        cell.userName.text = tagUserNameArray.object(at: indexPath.row) as? String
        if let file = tagProfileImageArray.object(at: indexPath.row) as? PFFile
        {
            cell.profileImage.file = file
            cell.profileImage.loadInBackground()
        }else
        {
            cell.profileImage.image = UIImage.init(named: "dummyProfile")
        }
        cell.tick.image = UIImage.init(named: tagTickArray.object(at: indexPath.row) as! String)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tagTickArray.object(at: indexPath.row) as! String == defalutImageStr {
            tagTickArray.replaceObject(at: indexPath.row, with: tickImageStr)
            let spaceName = "\(tagNameArray.object(at: indexPath.row) as! String)"
            collectionArray.add(spaceName)
        }else
        {
            let spaceName = "\(tagNameArray.object(at: indexPath.row) as! String)"
            print(spaceName)
            print(collectionArray)
            collectionArray.remove(spaceName)
            tagTickArray.replaceObject(at: indexPath.row, with: defalutImageStr)
        }
        
        if collectionArray.count>0 {
            collectionLbl.isHidden = true
        }else
        {
            collectionLbl.isHidden = false
        }
        
        
        
        collectionViewObj.reloadData()
        collectionViewObj.collectionViewLayout.invalidateLayout()
        
        let indexPath = IndexPath(item: indexPath.row, section: 0)
        self.tableViewObj.reloadRows(at: [indexPath], with: .automatic)
    }
}

extension TagUserViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView : UICollectionView,layout collectionViewLayout:UICollectionViewLayout, sizeForItemAt indexPath:IndexPath) -> CGSize
    {
        var str = collectionArray.object(at: indexPath.row) as? String
        let width = ((str?.characters.count)! * 8)
        print(width)
        return CGSize(width: 100, height:30)
    }
    

    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(10, 10, 10, 10)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 5
//    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TagCollectionViewCell", for: indexPath) as! TagCollectionViewCell
        cell.name.text = collectionArray.object(at: indexPath.row) as? String
        cell.name.layer.cornerRadius = 3
        cell.name.layer.masksToBounds = true
        cell.name.preferredMaxLayoutWidth = 54
        
        print(indexPath.section)
        
        return cell
    }
    
}



