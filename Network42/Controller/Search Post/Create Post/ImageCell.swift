//
//  ImageCell.swift
//  Network42
//
//  Created by 42works on 27/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol crossButtonDelegate {
    func crossButtonClicked(tag:Int)
    func imageButtonClicked(tag:Int)
}

class ImageCell: UITableViewCell {

     public var videoPath = String()
    @IBOutlet var heightOfCaptionTextView: NSLayoutConstraint!
    @IBOutlet var playImage: UIImageView!
    @IBOutlet var captionTextView: UITextView!
    @IBOutlet var imageObj: UIImageView!
    @IBOutlet var crossButton: UIButton!
    @IBOutlet var imageClikedBtn: UIButton!
    weak var delegate: crossButtonDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWasShown(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
      
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
    internal var aspectConstraint : NSLayoutConstraint? {
        didSet {
            if oldValue != nil {
                imageObj.removeConstraint(oldValue!)
            }
            if aspectConstraint != nil {
                imageObj.addConstraint(aspectConstraint!)
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        aspectConstraint = nil
    }
    
    func setPostedImage(image : UIImage) {
       
        let aspect = image.size.width / image.size.height
        self.aspectConstraint = NSLayoutConstraint(item: self.imageObj, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self.imageObj, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspect, constant: 0.0)
        self.imageObj.image = image
    }
    
    
    
    
    
    @IBAction func crossButtonAction(_ sender: Any)
    {
        self.delegate?.crossButtonClicked(tag: (sender as AnyObject).tag)
    }
    @IBAction func imageButtonAction(_ sender: Any) {
        self.delegate?.imageButtonClicked(tag: (sender as AnyObject).tag)
    }
    
    @IBAction func textFieldAction(_ sender: Any) {
        let textField = sender as! UITextField
        print(textField.text!)
    }
    
}
