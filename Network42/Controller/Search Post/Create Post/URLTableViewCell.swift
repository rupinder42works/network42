//
//  URLTableViewCell.swift
//  Network42
//
//  Created by 42works on 26/04/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
@objc protocol crossUrlDelegate {
    func crossUrlClicked()
}

class URLTableViewCell: UITableViewCell {

    @IBOutlet weak var img_youtube: UIImageView!
    @IBOutlet weak var htconstrnt_youtubeImage: NSLayoutConstraint!
    weak var delegate: crossUrlDelegate?
    @IBOutlet var outterView: UIView!
    
    @IBOutlet weak var imgVw_youtube: UIImageView!
    @IBOutlet var urlImage: UIImageView!
    @IBOutlet var urlTitle: UILabel!
    @IBOutlet var urlDescription: UILabel!
    @IBOutlet var urlAgain: UILabel!
    
    @IBOutlet var widthOfUrlImage: NSLayoutConstraint!
    
    @IBOutlet var indicatorObj: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func crossUrlAction(_ sender: Any) {
        self.delegate?.crossUrlClicked()
    }
    
    
    
}
