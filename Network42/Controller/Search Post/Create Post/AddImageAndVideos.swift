//
//  AddImageAndVideos.swift
//  Network42
//
//  Created by 42works on 28/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit


@objc protocol videoPhotoDelegate {
    func videoPhotoClicked()
}

class AddImageAndVideos: UITableViewCell {

    @IBOutlet var topOFButtom: NSLayoutConstraint!
    @IBOutlet var addButtonObj: UIButton!
    weak var delegate: videoPhotoDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func addVideoPhoto(_ sender: Any)
    {
        self.delegate?.videoPhotoClicked()
    }
    
}
