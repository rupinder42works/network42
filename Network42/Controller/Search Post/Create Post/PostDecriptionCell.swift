//
//  PostDecriptionCell.swift
//  Network42
//
//  Created by 42works on 31/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

@objc protocol tagUserDelegate {
    func tagUserClicked()
}

class PostDecriptionCell: UITableViewCell {
    
    @IBOutlet weak var vw_profileBackground: UIView!
    weak var delegate: tagUserDelegate?
    @IBOutlet var userProfile: UIImageView!
    
    @IBOutlet var tagBtn: UIButton!

    @IBOutlet var postTextView: UITextView!
    @IBOutlet var heightOfTagButton: NSLayoutConstraint!

    override func awakeFromNib() {
        super.awakeFromNib()
        postTextView.textContainer.maximumNumberOfLines = 10
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    @IBAction func tagUserAction(_ sender: Any) {
        self.delegate?.tagUserClicked()
    }
   
    
}
