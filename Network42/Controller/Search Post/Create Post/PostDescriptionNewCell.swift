//
//  PostDecriptionCell.swift
//  Network42
//
//  Created by 42works on 31/03/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Parse
import ParseUI

@objc protocol tagUserNewDelegate {
    func tagUserNewClicked()
}


class PostDescriptionNewCell: UITableViewCell {
    
    
    @IBOutlet weak var vw_profileBackground: UIView!
    @IBOutlet var userProfile: PFImageView!
    @IBOutlet var tagBtn: UIButton!
    @IBOutlet var postTextView: UITextView!
    @IBOutlet var heightOfTagButton: NSLayoutConstraint!
    @IBOutlet var nameLbl: NameLabel!
    @IBOutlet var timeLbl: TimeLabel!
    @IBOutlet var heightOfTextView: NSLayoutConstraint!
    @IBOutlet var topOfTag: NSLayoutConstraint!
     weak var delegate: tagUserNewDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()  
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func tagUserAction(_ sender: Any) {
        self.delegate?.tagUserNewClicked()
    }
    
    
}
