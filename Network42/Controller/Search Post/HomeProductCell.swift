//
//  HomeProductCell.swift
//  Magic
//
//  Created by Harsimranjit kaur on 14/08/18.
//  Copyright © 2018 Apple. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class HomeProductCell: UICollectionViewCell {  
    @IBOutlet weak var imageProduct: PFImageView!
    
    @IBOutlet weak var imageYoutube: UIImageView!
    @IBOutlet weak var imageAlbum: UIImageView!
    
    var type: Type! {
        didSet {
            if let type = type {
                switch type {
                case .image:
                    imageYoutube?.isHidden = true
                    imageAlbum?.isHidden = true
                    break
                case .album:
                    imageYoutube?.isHidden = true
                    imageAlbum?.isHidden = false
                    break
                case .video:
                    imageYoutube?.isHidden = false
                    imageAlbum?.isHidden = true
                    break
                }
            }
        }
    }
}
