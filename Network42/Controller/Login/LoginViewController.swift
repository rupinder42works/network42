//
//  LoginViewController.swift
//  Network42
//
//  Created by 42works on 09/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import ParseFacebookUtilsV4
import Parse
class LoginViewController: BaseViewController, UIViewControllerTransitioningDelegate {
    @IBOutlet var email: MyTextField!
    @IBOutlet var password: MyTextField!
    
    @IBOutlet weak var btn_create: UIButton!
    @IBOutlet weak var vw_or: UIView!
    @IBOutlet var loginStack: UIStackView!
    @IBOutlet var loginButton: TKTransitionSubmitButton!
    @IBOutlet var facebookButton: TKTransitionSubmitButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        email.layer.borderColor = UIColor.lightGray.cgColor
        password.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.logController()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        //        email.text = ""
        //        password.text = ""
    }
    
    
    //Login Action
    @IBAction func loginAction(_ button: TKTransitionSubmitButton)
    {
       // self.loginStack.bringSubviewToFront(loginButton)
        self.view.endEditing(true)
        if self.email.text == ""{
            self.showMessage(messages.emailEmptyText)
        }else if self.password.text == ""{
            self.showMessage(messages.pwdEmptyText)
        }
        else if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
        }
        else{
          
            loginButton.startLoadingAnimation()
            self.activityIndicatorBK.isHidden = false
            self.getUserNameFromEmail()
        }
    }
    
    //for firebase login
    func firebaseSigninAfterLogout(){
        var email = ""
        var password = ""
        if let parseUser = PFUser.current(){
            if let str =  parseUser.email{
                email = str
            }
            if let pwd =  parseUser["passwordStr"] as? String{
                password = pwd
            }
        }
        let credential = FIREmailPasswordAuthProvider.credential(withEmail: email, password: password)
        FIRAuth.auth()?.signIn(with: credential, completion: { (user, error) in
            if error == nil{
                  self.activityIndicatorBK.isHidden = true
            }else{
                self.activityIndicatorBK.isHidden = true
                self.showMessage(error?.localizedDescription ?? "")
            }
        })
    }
    
    func getUserNameFromEmail()
    {
        let query : PFQuery = PFUser.query()!
        query.whereKey("email", equalTo: email.text!)
        query.findObjectsInBackground { (objects, error) in
            //print(objects!)
            if error == nil {
                if let first = objects?.first {
                    print(first["username"])
                    print(first["name"])
                    self.loginWithUserName(userName: first["username"] as! String,name: first["name"] as! String)
                }
                else{
                    self.facebookButton.isHidden = false
                    self.vw_or.isHidden = false
                    self.btn_create.isHidden = false
                    self.activityIndicatorBK.isHidden = true
                    self.loginButton.returnToOriginalState()
                 //   self.loginButton.layer.cornerRadius = self.loginButton.normalCornerRadius
                    self.showMessage(messages.incorrectEmailPwd)
                }
            } else {
                if let error = error as? NSError{
                    let errorString = error.userInfo["error"] as? NSString
                    print(errorString!)// In case something went wrong...
                    print("ended")
                    self.facebookButton.isHidden = false
                    self.vw_or.isHidden = false
                    self.btn_create.isHidden = false
                    self.activityIndicatorBK.isHidden = true
                    self.loginButton.returnToOriginalState()
                   // self.loginButton.layer.cornerRadius = self.loginButton.normalCornerRadius
                    self.showMessage((error.localizedDescription))
                }
            }
        }
    }
    
    func loginWithUserName(userName:String,name:String)
    {
        facebookButton.isHidden = true
        vw_or.isHidden = true
        btn_create.isHidden = true
        PFUser.logInWithUsername(inBackground: userName, password: self.password.text!,
                                 block: {(user, error) -> Void in
                                    
                                    if let error = error as? NSError{
                                        let errorString = error.userInfo["error"] as? NSString
                                        print(errorString!)// In case something went wrong...
                                        print("ended")
                                        self.facebookButton.isHidden = false
                                        self.vw_or.isHidden = false
                                        self.btn_create.isHidden = false
                                        self.activityIndicatorBK.isHidden = true
                                        self.loginButton.returnToOriginalState()
                                 //       self.loginButton.layer.cornerRadius = self.loginButton.normalCornerRadius
                                        self.showMessage((error.localizedDescription))
                                    }
                                    else {
                                        self.LoginUserInfo(user!,name: name)   // Everything went alright here
                                    }
        })
    }
    
    
    
    
    func LoginUserInfo(_ getUser:PFUser,name:String)
    {
        
        print(getUser)
        
        let currentDate = Date()
        print(currentDate)
        
        let beforeupdateUser = PFUser.current()
        print(beforeupdateUser!)
        
        if let currentUser = PFUser.current(){
            currentUser["last_login"] = "\(currentDate)"
            currentUser["login_type"] = "email"
            currentUser["email"] = email.text!
            currentUser["passwordStr"] = self.password.text!
            currentUser.saveInBackground()
        }
        
        
        let currentUser = PFUser.current()
        currentUser?.fetchInBackground { (object, error) -> Void in
            print("Refreshed")
            currentUser?.fetchIfNeededInBackground { (result, error) -> Void in
                if let astatus = currentUser?.object(forKey: "account_status") as? String{
                    print(astatus)
                    self.pushToHome(name: (currentUser?.username)!, button: self.loginButton)
                }
                self.firebaseSigninAfterLogout()
              
            }
        }
    }
    
    
    @IBAction func facebookLogin(_ sender: UIButton) {
        
        if  Reachability.isConnectedToNetwork() == false {
            self.showMessage(messages.internet)
        }else{
            
           // self.loginStack.bringSubviewToFront(facebookButton)
            facebookButton.startLoadingAnimation()
            self.activityIndicatorBK.isHidden = false
            self.fbLoginButtonTapped()
        }
    }
    
    
    
    
    func fbLoginButtonTapped() {
        
        
        let permissions = ["public_profile", "email"]
        PFFacebookUtils.logInInBackground(withReadPermissions: permissions) { (user, error) in
            if let user = user {
                if user.isNew {
                    print("User signed up and logged in through Facebook!")
                    self.saveDataFB(exist: false)
                } else {
                    print("User logged in through Facebook!")
                    
                    self.saveDataFB(exist: true)
                }
            } else {
                
                // self.showMessage((error?.localizedDescription)!)
                self.facebookButton.returnToOriginalState()
                self.activityIndicatorBK.isHidden = true
                self.facebookButton.layer.cornerRadius = self.facebookButton.normalCornerRadius
                
            }
        }
    }
    
    
    
    func saveDataFB(exist:Bool)
    {
        
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"name, email,id,cover,birthday,work,education"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                print("Error: \(error)")
                self.showMessage((error?.localizedDescription)!)
                self.facebookButton.returnToOriginalState()
                self.activityIndicatorBK.isHidden = true
                self.facebookButton.layer.cornerRadius = self.facebookButton.normalCornerRadius
                
            }
            else
            {
                
                guard let accessToken = FBSDKAccessToken.current() else {
                    print()
                    self.showMessage("Failed to get access token")
                    self.facebookButton.returnToOriginalState()
                    self.activityIndicatorBK.isHidden = true
                    self.facebookButton.layer.cornerRadius = self.facebookButton.normalCornerRadius
                    
                    return
                }
                
                var coverPictureData = NSData()
                
                if let responseDict = result as? NSDictionary {
                    print(responseDict)
                    if let cover = (responseDict.object(forKey: "cover") as? NSDictionary)?.value(forKey: "source"){
                        let coverUrl = cover as?  String
                        print(coverUrl!)
                        let cPictureUrl = NSURL(string: coverUrl!)
                        do {
                            try coverPictureData = NSData(contentsOf: cPictureUrl! as URL)
                        } catch {
                            print(error)
                        }
                        
                        self.FacebookUserInfo(name: responseDict.value(forKey: "name") as! String, userName: responseDict.value(forKey: "name") as! String, email: responseDict.value(forKey: "email") as! String, password: "",userId: responseDict.value(forKey: "id") as! String,aToken: accessToken.tokenString,coverPictureData: coverPictureData as Data,dob: "",cover: true,exist: exist)
                        
                    }else
                    {
                        
                        self.FacebookUserInfo(name: responseDict.value(forKey: "name") as! String, userName: responseDict.value(forKey: "name") as! String, email: responseDict.value(forKey: "email") as! String, password: "",userId: responseDict.value(forKey: "id") as! String,aToken: accessToken.tokenString,coverPictureData: coverPictureData as Data,dob: "",cover: false,exist: exist)
                    }
                    
                }
            }
        })
        
        
    }
    
    
    
    func FacebookUserInfo(name:String,userName:String,email:String,password:String,userId:String,aToken:String,coverPictureData:Data,dob:String,cover:Bool,exist:Bool)
    {
        
        let usernameFbStr = userName.replacingOccurrences(of: " ", with: "")
        print(userId)
        print(name)
        print(userName)
        print(dob)
        
        let currentDate = Date()
        PFFacebookUtils.facebookLoginManager().loginBehavior = .systemAccount
        
        if let currentUser = PFUser.current(){
            print(currentUser)
            print(exist)
            if !exist {
                currentUser["user_name"] = usernameFbStr
                currentUser["username"] = usernameFbStr
                currentUser["email"] = email
                currentUser["passwordStr"] = "123456"
                currentUser["name"] = "\(name)"
                currentUser["dob"] = dob
                currentUser["login_type"] = "facebook"
                // Save first name
                currentUser["profile_type"] = "1"
                currentUser["set_password"] = "0"
                currentUser["account_created_at"] = "\(currentDate)"
                currentUser["profile_updated"] = "1"
                currentUser["account_status"] = "1"
                // Get Facebook profile picture
                let userProfile = "https://graph.facebook.com/" + userId + "/picture?type=large"
                let profilePictureUrl = NSURL(string: userProfile)
                let profilePictureData = NSData(contentsOf: profilePictureUrl! as URL)
                
                if(profilePictureData != nil)
                {
                    let profileFileObject = PFFile(name: "myProfile_IOS.png", data: profilePictureData! as Data)
                    currentUser.setObject(profileFileObject!, forKey: "profile_image")
                    
                    let profilePhotoTable = PFObject(className: "Profile_photos")
                    profilePhotoTable["user_photo_url"] = profileFileObject
                    profilePhotoTable["user_id"] = PFUser.current()?.objectId // Little doubt
                    profilePhotoTable.saveInBackground(block: { (status, error) in
                        print("profile image status for Profile photos table == ",status)
                    })
                    
                }
                
                
                if(cover)
                {
                    let coverFileObject = PFFile(name: "myCover_IOS.png", data: coverPictureData)
                    currentUser.setObject(coverFileObject!, forKey: "cover_image")
                    
                    let coverPhotoTable = PFObject(className: "Cover_photos")
                    coverPhotoTable["cover_photo_url"] = coverFileObject
                    coverPhotoTable["user_id"] = PFUser.current()?.objectId // Little doubt
                    coverPhotoTable.saveInBackground(block: { (status, error) in
                        print("cover image status for cover photos table == ",status)
                    })
                }
            }
            currentUser["last_login"] = "\(currentDate)"
            print(currentUser)
            //set other fields the same way....
            
            
            
            currentUser.saveInBackground(
                block: { (success, error) -> Void in
                    if(success)
                    {
                        print("User details are now updated")
                        if !(exist)
                        {
                            self.notificationAPI()
                        }
                        self.registerUserusingFir(nameStr: name,email:email)
                        self.pushToHome(name: "\(name)", button: self.facebookButton)
                        
                    }
                    else{
                        print("error",error!)
                        
                        self.showMessage((error?.localizedDescription)!)
                        self.facebookButton.returnToOriginalState()
                        self.activityIndicatorBK.isHidden = true
                        self.facebookButton.layer.cornerRadius = self.facebookButton.normalCornerRadius
                        
                        return
                        
                    }
                    
            })
            
            
        }
        else{
            print("error here....")
            //            showMessage("")
            self.showMessage("Parse user error")
            self.facebookButton.returnToOriginalState()
            self.activityIndicatorBK.isHidden = true
            self.facebookButton.layer.cornerRadius = self.facebookButton.normalCornerRadius
        }
    }
    
    func registerUserusingFir(nameStr:String,email:String){
        FIRAuth.auth()?.createUser(withEmail: email, password: "123456", completion: { (user, error) in
            if error == nil {
                print("You have successfully signed up")
                let parameters = ["name":nameStr,"email":email,"userName":nameStr] as [String : Any]
                let ref = FIRDatabase.database().reference()
                ref.child("users").child((PFUser.current()?.objectId!)!).setValue(parameters)
                { (error, DBref) in
                    print(DBref)
                    if !(error==nil)
                    {
                        print(error as! String)
                    }
                }
            } else {
                print("Login Error",error?.localizedDescription ?? "")
            }
        })
    }
    
    
    //MARK:-FB login
    func pushToHome(name:String,button:TKTransitionSubmitButton){
        
        let installationClass = PFInstallation.current()
        installationClass?["userId"] = PFUser.current()?.objectId!
        installationClass?["isActive"] = "1"
        installationClass?.saveInBackground { (success, error) in
            if(success)
            {
                print("installation saved")
            }
            else{
                print("error",error!)
                return
            }
        }
        
        
        button.startFinishAnimation(0.5, completion: {
            let currentUser = PFUser.current()
            currentUser?.fetchInBackground { (object, error) -> Void in
                print("Refreshed")
                currentUser?.fetchIfNeededInBackground { (result, error) -> Void in
                    if let astatus = currentUser?.object(forKey: "account_status") as? String{
                        if astatus == "1"{
                            
                            let tabBarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                            let vc = tabBarSB.instantiateViewController(withIdentifier: "TabBarViewController")
                            vc.transitioningDelegate = self
                            // self.loginButton.layer.cornerRadius = self.loginButton.normalCornerRadius
                            
                            self.present(vc, animated: true, completion: nil)
                        }
                        else{
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                            vc.name = name
                            if let currentUser = PFUser.current(){
                                currentUser["account_status"] = "1"
                                currentUser.saveInBackground()
                            }
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    }
                    else{
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeViewController") as! WelcomeViewController
                        vc.name = name
                        if let currentUser = PFUser.current(){
                            currentUser["account_status"] = "1"
                            currentUser.saveInBackground()
                        }
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    
                }
            }
        })
    }
    
    
    
    func notificationAPI(){
        
        let objId = PFUser.current()?.objectId
        print(objId!)
        
        let Settings = PFObject(className: "Settings")
        Settings["push_post_tag"] = "1"
        Settings["push_message_send"] = "1"
        Settings["push_post_share"] = "1"
        Settings["push_post_comment"] = "1"
        Settings["profile_visibility"] = "1"
        Settings["push_post_like"] = "1"
        Settings["push_accept_follow_request"] = "1"
        Settings["push_send_follow_request"] = "1"
        Settings["push_comment_tag"] = "1"
        Settings["user_id"] = objId!
        Settings.saveInBackground(block: { (status, error) in
            print(status)
            if status ==  true{
                print(status)
                
            }
            
        })
    }
    
    
    // MARK: UIViewControllerTransitioningDelegate
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return TKFadeInAnimator(transitionDuration: 0.5, startingAlpha: 0.8)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return nil
    }
    func enterUserDetailsintoFirbase(nameStr:String,userNameStr:String,emailStr:String){
        print(nameStr)
        print(userNameStr)
        print(emailStr)
        FIRAuth.auth()?.signIn(withEmail: emailStr, password: self.password.text!) { (user, error) in
            
            if error == nil {
                
                print("You have successfully logged in")
                
                
            } else {
                let errorString = error?.localizedDescription
                print(errorString!)// In case something went wrong...
                print("ended")
                self.activityIndicatorBK.isHidden = true
                self.loginButton.returnToOriginalState()
               // self.loginButton.layer.cornerRadius = self.loginButton.normalCornerRadius
                self.showMessage((error?.localizedDescription)!)
            }
        }
    }
}
