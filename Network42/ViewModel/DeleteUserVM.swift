//
//  DeleteUserVM.swift
//  Network42
//
//  Created by Apple1 on 05/02/2019.
//  Copyright © 2019 Simran. All rights reserved.
//

import Foundation
import Parse
import Firebase

protocol notifyCompletionDelegate {
    func notify()
}

protocol startIndicatorDelegate {
    func startIndicator()
}

class DeleteUserVM : BaseViewModel{

    var delegate : notifyCompletionDelegate?
    var indicatorDelegate : startIndicatorDelegate?
    
    func deleteUserAccount(){
        self.indicatorDelegate?.startIndicator()
        deleteUserFollows()
    }
    
    func deleteUserFollows(){
        self.isLoading = true
        let query = PFQuery(className:"User_follow")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        query.whereKey("followed_id", equalTo: id)
        query.findObjectsInBackground(block: { (objects2, error) in
            //self.isLoading = false
            if error == nil {
                var count = 0
                if objects2?.count ?? 0 > 0{
                    count = objects2!.count
                    for obj in objects2 ?? [PFObject](){
                        obj.deleteInBackground(block: { (status, error) in
                            if error == nil{
                                count = count - 1
                                if count <= 0{
                                    self.deleteUserFollowings()
                                }
                                print("deleted")
                            }else{
                                print(error?.localizedDescription ?? "")
                            }
                        })
                   }
                }else{
                    self.deleteUserFollowings()
                }
                
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
        
    func deleteUserFollowings(){
       // self.isLoading = true
        let query = PFQuery(className:"User_follow")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        query.whereKey("follower_id", equalTo: id)
        query.findObjectsInBackground(block: { (objects3, error) in
          //  self.isLoading = false
            if error == nil {
                var count = 0
                if objects3?.count ?? 0 > 0{
                    count = objects3!.count
                    for obj in objects3 ?? [PFObject](){
                        obj.deleteInBackground(block: { (status, error) in
                            if error == nil{
                                count = count - 1
                                if count <= 0{
                                    self.deleteUserPosts()
                                }
                                print("follower_id deleted")
                            }else{
                                print(error?.localizedDescription ?? "")
                            }
                        })
                    }
                }else{
                    self.deleteUserPosts()
                }
                
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    func deleteUserPosts(){
       // self.isLoading = true
        let query = PFQuery(className:"Post")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        query.whereKey("user_id", equalTo:id)
        query.findObjectsInBackground(block: { (objects4, error) in
           // self.isLoading = false
            if error == nil {
                var count = 0
                if objects4?.count ?? 0 > 0{
                    count = objects4!.count
                    for obj in objects4 ?? [PFObject](){
                        obj.deleteInBackground(block: { (status, error) in
                            if error == nil{
                                count = count - 1
                                if count <= 0{
                                    self.deleteUserMessages()
                                }
                                
                                print("follower_id deleted")
                            }else{
                                print(error?.localizedDescription ?? "")
                            }
                        })
                    }
                }else{
                    self.deleteUserMessages()
                }
                
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    
    func deleteUserNotifications(){
        self.isLoading = true
        let query = PFQuery(className:"Notifications")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        query.whereKey("from_id", equalTo: id)
        query.findObjectsInBackground(block: { (objects, error) in
            self.isLoading = false
            if error == nil {
                if objects?.count ?? 0 > 0{
                    for obj in objects ?? [PFObject](){
                        obj.deleteInBackground(block: { (status, error) in
                            if error == nil{
                                print("notifications deleted")
                            }else{
                                print(error?.localizedDescription ?? "")
                            }
                        })
                    }
                }
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    func deleteHashtags(){
        self.isLoading = true
        let query = PFQuery(className:"Hashtags")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        query.whereKey("objectId", equalTo: id)
        query.findObjectsInBackground(block: { (objects, error) in
            self.isLoading = false
            if error == nil {
                if objects?.count ?? 0 > 0{
                    for obj in objects ?? [PFObject](){
                        obj.deleteInBackground(block: { (status, error) in
                            if error == nil{
                                print("Hashtags deleted")
                            }else{
                                print(error?.localizedDescription ?? "")
                            }
                        })
                    }
                }
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    func deleteBlockedUsers(){
        self.isLoading = true
        let query = PFQuery(className:"BlockedUsers")
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        query.whereKey("BlockedUser", equalTo: id)
        query.findObjectsInBackground(block: { (objects, error) in
            self.isLoading = false
            if error == nil {
                if objects?.count ?? 0 > 0{
                    for obj in objects ?? [PFObject](){
                        obj.deleteInBackground(block: { (status, error) in
                            if error == nil{
                                print("Hashtags deleted")
                            }else{
                                print(error?.localizedDescription ?? "")
                            }
                        })
                    }
                }
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    func deleteUser(){
       // self.isLoading = true
        var userId = ""
        if let id = PFUser.current()!.objectId{
            userId = id
        }
        PFUser.current()?.deleteInBackground(block: { (sucess, error) in
            if sucess == true{
                
                let myRef = FIRDatabase.database().reference().child("users/\(userId)")
                myRef.removeValue(completionBlock:
                    { (error, DBref) in
                       self.delegate?.notify()
                })
                
                let bookmarkRef = FIRDatabase.database().reference().child("bookmarkMessage").child(userId)
                bookmarkRef.removeValue(completionBlock:
                    { (error, DBref) in
                        
                })
                
                let messageref = FIRDatabase.database().reference().child("messages").child(userId)
                messageref.removeValue(completionBlock:
                    { (error, DBref) in
                        
                })
                
            }else{
                self.delegate?.notify()
            }
        })
    }
    
    
    func deleteUserMessages(){
        if  Reachability.isConnectedToNetwork() == false {
            
            return
        }
        let DBref = FIRDatabase.database().reference()
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        let bookmarkRef =  DBref.child("bookmarkMessage").child(id)
        bookmarkRef.removeValue()
        deleteUserBookmark()
    }

    func deleteUserBookmark(){
        if  Reachability.isConnectedToNetwork() == false {
           
            return
        }
        let DBref = FIRDatabase.database().reference()
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
        let bookmarkRef =  DBref.child("messages").child(id)
        bookmarkRef.removeValue()
        deleteUserFromFirebase()
    }
    
    func checkGroupReference(){
        if  Reachability.isConnectedToNetwork() == false {
            
            return
        }
        let DBref = FIRDatabase.database().reference()
        var id = ""
        if let objId = PFUser.current()?.objectId{
            id = objId
        }
    }
    
    func deleteUserFromFirebase(){
        
        if  Reachability.isConnectedToNetwork() == false {
            return
        }
        let user = FIRAuth.auth()?.currentUser
        var email = ""
        var password = ""
        if let parseUser = PFUser.current(){
            if let str =  parseUser.email{
                email = str
            }
            if let pwd =  parseUser["passwordStr"] as? String{
                password = pwd
            }
        }
        let credential = FIREmailPasswordAuthProvider.credential(withEmail: email, password: password)
        
        // Prompt the user to re-provide their sign-in credentials
        
        user?.reauthenticate(with: credential) { error in
            if let error = error {
                // An error happened.
            } else {
                // User re-authenticated.
                user?.delete(completion: { (error) in
                    if error != nil {
                        print("Error unable to delete user")
                        
                    } else {
                        self.deleteUser()
                        print("user Deleted")
                    }
                })
            }
        }
    }
}
