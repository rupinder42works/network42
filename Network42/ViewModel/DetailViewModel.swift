//
//  DetailViewModel.swift
//  Network42
//
//  Created by Apple3 on 23/01/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import Parse
import Foundation


class DetailViewModel: BaseViewModel {
    var array: [Any]!
    var postId: String
    
    init(postId: String) {
        array = [Any]()
        self.postId = postId
    }
    
    func getPost() {
        self.isLoading = true
        let query = PFQuery(className: "Post")
        query.whereKey("objectId", equalTo:postId)
        query.includeKey("parent_id")
        query.findObjectsInBackground(block: { (objects, error) in
            
            if error == nil {
                if let objects = objects {
                    print(objects)
                    for obj in objects {
                        let post = Post().getPost(obj)
                        self.array.append(post)
                    }
                }
                
                self.getComments()
                
            } else {
                self.isLoading = false
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    func getComments() {
        
        let query = PFQuery(className:"Post_comments")
        query.includeKey("parent_id")
        query.whereKey("post_id", equalTo: postId)
        query.order(byAscending: "createdAt")
        query.findObjectsInBackground(block: { (objects, error) in
            
            self.isLoading = false
            if error == nil {
                if let objects = objects {
                    for object in objects {
                        self.array.append(Comment().getComment(object))
                    }
                }
                
                self.reloadTableViewClosure?()
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    func sendComment(_ comment: String, completed:@escaping ((Bool)->())) {
        
        let commentObj = PFObject(className: "Post_comments")
        commentObj["post_id"] = postId
        commentObj["comment"] = comment
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy_HH:mm:ss"
        commentObj["comment_time"] = formatter.string(from: Date())

      //  commentObj["comment_time"] = Date().getString("dd-MM-yyyy_HH:mm:ss a")
        commentObj["parent_id"] = PFUser.current()
        //commentClass["users_tagged_in_comments"] = savedArrayUser
        
        commentObj.saveInBackground { (success, error) in
            if(success) {
                if let post = self.array.first as? Post {
                    var tempPost = post
                    tempPost.commentCount = (post.commentCount ?? 0) + 1
                    self.array[0] = tempPost
                }
                
                self.array.append(Comment().tempComment(comment, commentObj.objectId ?? ""))
                self.reloadTableViewClosure?()
                completed(true)
                
                self.updateCommentCount(self.array.count - 1)
                
            } else {
                self.alertMessage = error?.localizedDescription ?? ""
                completed(false)
            }
        }
    }
    
    func deleteComment(_ index: Int, completed:@escaping ((Bool)->())) {
        self.isLoading = true
        let commentObj = PFObject(className: "Post_comments")
        commentObj.objectId = (self.array[index] as? Comment)?.id ?? ""
        
        commentObj.deleteInBackground(block: { (status, error) in
            print(status)
            self.isLoading = false
            if status == true{
                self.array.remove(at: index)
                self.reloadTableViewClosure?()
                completed(true)
                self.updateCommentCount(self.array.count - 1)
            } else {
                self.alertMessage = error?.localizedDescription ?? ""
                completed(false)
            }
        })
    }
    
    func likeUpdate(_ isLike: Bool, count: Int) {
        guard let post = self.array.first as? Post else {
            return
        }
        var tempPost = post
        tempPost.likeCount = count
        self.array[0] = tempPost

        if isLike {
            if let likes = post.likeArr{
                if likes.count > 0{
                    let likeObj = PFObject(className: "Post_likes")
                    likeObj["post_id"] =  postId
                    likeObj["user_id"] =  PFUser.current()?.objectId
                    likeObj["parent_id"] =  PFUser.current()
                    if let id = PFUser.current()?.objectId{
                        if likes.contains(id) == false{
                            likeObj.saveInBackground { (status, error) in
                                if status {
                                    let query = PFQuery(className: "Post")
                                    query.includeKey("parent_id")
                                    query.whereKey("objectId", equalTo: self.postId)
                                    
                                    query.findObjectsInBackground { (objects, error) in
                                        if error == nil{
                                            if let objs = objects{
                                                if objects!.count > 0{
                                                    var values = objects![0]
                                                    var arrLikes = post.likeArr!
                                                    arrLikes.append(PFUser.current()?.objectId ?? "")
                                                    values["user_like_post"] = arrLikes
                                                    values.saveInBackground(block: { (sucess, error) in
                                                        if sucess{
                                                            self.updateLikeCount(count, isLike)
                                                        }
                                                    })
                                                    
                                                }
                                            }
                                            
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                    let likeObj = PFObject(className: "Post_likes")
                    likeObj["post_id"] =  postId
                    likeObj["user_id"] =  PFUser.current()?.objectId
                    likeObj["parent_id"] =  PFUser.current()
                    likeObj.saveInBackground { (status, error) in
                        if status {
                            let query = PFQuery(className: "Post")
                            query.includeKey("parent_id")
                            query.whereKey("objectId", equalTo: self.postId)
                            
                            query.findObjectsInBackground { (objects, error) in
                                if error == nil{
                                    if let objs = objects{
                                        if objects!.count > 0{
                                            var values = objects![0]
                                            var arrLikes = [String]()
                                            arrLikes.append(PFUser.current()?.objectId ?? "")
                                            values["user_like_post"] = arrLikes
                                            values.saveInBackground(block: { (sucess, error) in
                                                if sucess{
                                                    self.updateLikeCount(count, isLike)
                                                }
                                            })
                                            
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                }
            }else{
                let likeObj = PFObject(className: "Post_likes")
                likeObj["post_id"] =  postId
                likeObj["user_id"] =  PFUser.current()?.objectId
                likeObj["parent_id"] =  PFUser.current()
                likeObj.saveInBackground { (status, error) in
                    if status {
                        let query = PFQuery(className: "Post")
                        query.includeKey("parent_id")
                        query.whereKey("objectId", equalTo: self.postId)
                        
                        query.findObjectsInBackground { (objects, error) in
                            if error == nil{
                                if let objs = objects{
                                    if objects!.count > 0{
                                        var values = objects![0]
                                        var arrLikes = [String]()
                                        arrLikes.append(PFUser.current()?.objectId ?? "")
                                        values["user_like_post"] = arrLikes
                                        values.saveInBackground(block: { (sucess, error) in
                                            if sucess{
                                                self.updateLikeCount(count, isLike)
                                            }
                                        })
                                        
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        } else {
            let likeObj = PFQuery(className: "Post_likes")
            likeObj.whereKey("post_id", equalTo: postId)
            likeObj.whereKey("user_id", equalTo: PFUser.current()?.objectId)
            likeObj.whereKey("parent_id", equalTo: PFUser.current())
            likeObj.findObjectsInBackground { (objects, error) in
                if error == nil{
                    if let objs = objects{
                        if objects!.count > 0{
                            for values in objects!{
                                values.deleteInBackground(block: { (sucess, error) in
                                    if sucess == true{
                                        let query = PFQuery(className: "Post")
                                        query.includeKey("parent_id")
                                        query.whereKey("objectId", equalTo: self.postId)
                                        
                                        query.findObjectsInBackground { (objects, error) in
                                            if error == nil{
                                                if let objs = objects{
                                                    if objects!.count > 0{
                                                        var values = objects![0]
                                                        var likes = [String]()
                                                        if let  arrLikes = post.likeArr as? [String]{
                                                            likes = arrLikes
                                                            let index = likes.index { $0 ==  PFUser.current()?.objectId}
                                                            if index != nil{
                                                                likes.remove(at: index!)
                                                            }
                                                        }
                                                       
                                                        values["user_like_post"] = likes
                                                        values.saveInBackground(block: { (sucess, error) in
                                                            if sucess{
                                                                self.updateLikeCount(count, isLike)
                                                            }
                                                        })
                                                        
                                                    }
                                                }
                                                
                                            }
                                        }
                                    }
                                })
                            }
                        }
                    }
                    
                }
            }
//            likeObj.deleteInBackground { (status, error) in
//                print(error?.localizedDescription ?? "")
//                if status {
//                    self.updateLikeCount(count, isLike)
//                }
//            }
        }
    }
    
    private func updateCommentCount(_ count: Int) {
        let postObj = PFObject(className: "Post")
        postObj.objectId = self.postId
        postObj.setValue("\(count)", forKey: "comment_count")
        postObj.saveInBackground()
    }
    
    private func updateLikeCount(_ count: Int, _ isLike: Bool) {
        guard let post = self.array.first as? Post else {
            return
        }
        
        let postObj = PFObject(className: "Post")
        postObj.objectId = self.postId
        var likeArr = [String]()
        if let temp = post.likeArr {
            likeArr = temp
        }
        
        if isLike {
            likeArr.append(PFUser.current()?.objectId ?? "")
        } else {
            let arr = likeArr.filter { obj -> Bool in
                return !(obj == PFUser.current()?.objectId)
            }
            likeArr = arr
        }
        
        postObj.setObject(likeArr, forKey: "user_like_post")
        postObj.setValue("\(count)", forKey: "like_count")
        postObj.saveInBackground { (status, error) in
            print(error?.localizedDescription ?? "")
        }
    }
}
