//
//  CoverA.swift
//  MMPlayerView
//
//  Created by Millman YANG on 2017/8/22.
//  Copyright © 2017年 CocoaPods. All rights reserved.
//

import UIKit
import MMPlayerView
import AVFoundation

class CoverA: UIView, MMPlayerCoverViewProtocol, MMPlayerBasePlayerProtocol {
//    func currentPlayer(status: MMPlayerLayer.PlayStatus) {
//        
//    }
    
    weak var playLayer: MMPlayerLayer?
    fileprivate var isUpdateTime = false
    
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var playSlider: UISlider!
    @IBOutlet weak var labTotal: UILabel!
    @IBOutlet weak var labCurrent: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var muteBtn: UIButton!
    @IBOutlet weak var replayBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       // btnPlay.imageView?.tintColor = UIColor.white
    }
//    @IBAction func btnAction() {
//        self.playLayer?.delayHideCover()
//        if playLayer?.player?.rate == 0{
//            self.playLayer?.player?.play()
//        } else {
//            self.playLayer?.player?.pause()
//        }
//    }
    
        @IBAction func replayBtnAction() {
           // self.playLayer?.delayHideCover()
            self.playLayer?.player?.play()
        }
    
    func currentPlayer(status: MMPlayerLayer.PlayStatus) {
        switch status {
        case .playing:
            replayBtn.isHidden = true
            break
            //self.btnPlay.setImage(#imageLiteral(resourceName: "ic_pause_circle_filled"), for: .normal)
        case .end:
            replayBtn.isHidden = false
            self.timeLabel.text = ""
            delaySeekTime()
            break
            //playSlider.setValue(0.0, animated: false)
            //delaySeekTime()
            //self.btnPlay.setImage(#imageLiteral(resourceName: "ic_play_circle_filled"), for: .normal)
        default:
            replayBtn.isHidden = true
            break
            //self.btnPlay.setImage(#imageLiteral(resourceName: "ic_play_circle_filled"), for: .normal)
        }
    }
    
    func timerObserver(time: CMTime) {
        if let duration = self.playLayer?.player?.currentItem?.asset.duration ,
            !duration.isIndefinite{
//            if self.playSlider.maximumValue != Float(duration.seconds) {
//                self.playSlider.maximumValue = Float(duration.seconds)
//            }
              self.timeLabel.text = self.convert(second: time.seconds)
//            self.labTotal.text = self.convert(second: duration.seconds-time.seconds)
//            self.playSlider.value = Float(time.seconds)
        }
    }
    
    fileprivate func convert(second: Double) -> String {
        let component =  Date.dateComponentFrom(second: second)
        if let hour = component.hour ,
            let min = component.minute ,
            let sec = component.second {
            
            let fix =  hour > 0 ? NSString(format: "%02d:", hour) : ""
            return NSString(format: "%@%02d:%02d", fix,min,sec) as String
        } else {
            return "-:-"
        }
    }
    
    @IBAction func sliderValueChange(slider: UISlider) {
        self.isUpdateTime = true
        self.playLayer?.delayHideCover()
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(delaySeekTime), object: nil)
        self.perform(#selector(delaySeekTime), with: nil, afterDelay: 0.1)
    }

    @objc func delaySeekTime() {
    
        let time = CMTime(value: Int64(0), timescale: 1)
        self.playLayer?.player?.seek(to: time, completionHandler: { [unowned self] (finish) in
            self.isUpdateTime = false
        })
    }
    
    func player(isMuted: Bool) {
        if isMuted == true{
            muteBtn.isSelected = true
        }
        else{
            muteBtn.isSelected = false
        }
    }
    
    func removeObserver() {
        
    }
    
    func addObserver() {
        
    }
}

extension Date {
    static func dateComponentFrom(second: Double) -> DateComponents {
        let interval = TimeInterval(second)
        let date1 = Date()
        let date2 = Date(timeInterval: interval, since: date1)
        let c = NSCalendar.current
        
        var components = c.dateComponents([.year,.month,.day,.hour,.minute,.second,.weekday], from: date1, to: date2)
        components.calendar = c
        return components
    }
}

