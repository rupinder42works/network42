//
//  PostViewModel.swift
//  Network42
//
//  Created by Apple3 on 15/10/18.
//  Copyright © 2018 42works. All rights reserved.
//

import Parse
import Foundation

class PostViewModel: BaseViewModel {
    var isRefresh = false
    var array : [Post]?
    var sortBy: Int = 1 // 1- Like, 2 - comment, 3 - createdBy
    
    func getAllPosts() {
        self.isLoading = !isRefresh
        let query = PFQuery(className: "Post")
        if sortBy == 3 {
            query.order(byDescending: "createdAt")
        }        
        query.whereKey("post_type", containedIn: ["IMAGE", "VIDEO", "ALBUM"])
        query.includeKey("parent_id")
        query.findObjectsInBackground(block: { (objects, error) in
            
            self.isLoading = false
            if error == nil {
                if let objects = objects {
                    print(objects)
                    self.array = [Post]()
                    for obj in objects {
                        let post = Post().getPost(obj)
                        if let postType = post.postedBy?.profileType, postType == "1" {
                            self.array?.append(post)
                        }
                    }
                    
                    if self.sortBy == 1 {
                        self.array?.sort(by: { (post1, post2) -> Bool in
                            return post1.likeCount > post2.likeCount
                        })
                    } else if self.sortBy == 2 {
                        self.array?.sort(by: { (post1, post2) -> Bool in
                            return post1.commentCount > post2.commentCount
                        })
                    }
                }
                
                self.reloadTableViewClosure?()
            } else {
                print(error!)
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
}
