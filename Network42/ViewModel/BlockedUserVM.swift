//
//  BlockedUserVM.swift
//  Network42
//
//  Created by Apple1 on 04/02/2019.
//  Copyright © 2019 Simran. All rights reserved.
//

import Foundation
import Parse

class BlockedUserVM: BaseViewModel {
   
    //MARK:- Variables
    var blockedUserArr : [User]?
    
    //MARK:- Clausers
    var responseRecieved:(()->())?
    
    func getBlockedUsers() {
        self.isLoading = true
        let query = PFQuery(className:"BlockedUsers")
        query.includeKey("Parent_Blocked_User")
        query.whereKey("FromUser", equalTo: PFUser.current()?.objectId ?? "")
        query.order(byAscending: "createdAt")
        query.findObjectsInBackground(block: { (objects, error) in
            
            self.isLoading = false
            if error == nil {
                self.blockedUserArr = [User]()
                if let objects = objects {
                    for object in objects {
                        self.blockedUserArr?.append(User().getUser(object.value(forKey: "Parent_Blocked_User") as? PFUser))
                    }
                    self.responseRecieved?()
                }
            }
            else {
                self.alertMessage = error?.localizedDescription ?? ""
            }
        })
    }
    
    func removeUserInBlockedClassInDB(_ userId : String, _ currentUserId : String){
        let query = PFQuery(className: "BlockedUsers")
        query.whereKey("FromUser", equalTo: currentUserId)
        query.whereKey("BlockedUser", equalTo: userId)
        query.getFirstObjectInBackground { (object, error) in
            self.isLoading = false
            if error == nil {
                object?.deleteInBackground(block: { (status, error) in
                    if error == nil{
                        print("remove successfully")
                    }else{
                        print(error.debugDescription)
                    }
                })
            }
        }
        
    }

}
