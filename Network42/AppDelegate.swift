//
//  AppDelegate.swift
//  Network42
//
//  Created by Apple3 on 21/01/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import FBSDKCoreKit
import Parse
import Bolts
import ParseFacebookUtilsV4
import UserNotifications
import FirebaseMessaging 
import UserNotifications
import ChimpKit
import GoogleMobileAds
import AVFoundation

let appDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, FIRMessagingDelegate {
    
    var showBanner = true
    var isIPad: Bool = false
    var window: UIWindow?
    let tokenAs:String = ""
    var activeGroupId = ""
    var activeUser = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
       
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
        
        /**********************************************/
        //Google Analytics
        guard let gai = GAI.sharedInstance() else {
            assert(false, "Google Analytics not configured correctly")
            return true
        }
        gai.tracker(withTrackingId: "UA-105230866-1")
        gai.trackUncaughtExceptions = true
        
        //        gai.logger.logLevel = .verbose
        /**********************************************/
        
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        /**********************************************/
        //Firebase configure
        FIRApp.configure()
        /**********************************************/
        
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        FIRDatabase.database().persistenceEnabled = true
        
        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView{
            statusBar.backgroundColor = #colorLiteral(red: 0.2117647059, green: 0, blue: 0.7058823529, alpha: 1)
            
        }
       // UIApplication.shared.statusBarStyle = .lightContent
        if (UserDefaults.standard.object(forKey: "LoggedIn") != nil)
        {
            if UserDefaults.standard.object(forKey: "LoggedIn") as! Bool == true
            {
                print("logged in")
                
                let tabBarSB = UIStoryboard.init(name: "TabBarSB", bundle: nil)
                let vc = tabBarSB.instantiateViewController(withIdentifier: "TabBarViewController")
                
                let navigationController = UINavigationController()
                navigationController.setViewControllers([vc], animated: true)
                navigationController.isNavigationBarHidden = true
                self.window?.rootViewController = navigationController
                
            }else
            {
                print("not")
            }
        }else
        {
            print("Bool is not set yet.")
        }
        
        ChimpKit.shared().apiKey = "61dbd3f0b98aab6a53b31462ecf91ac9-us16"
        
//        $0.applicationId = "FnGEtKzxzYapvip7ZtO9X8iWyrCWI5ckPX6tNMLP"
//        $0.clientKey = "k9lvcslIcVZKgv2zpdPesVNOoCQtr1knAOaTW2J5"
//        $0.server = "https://parseapi.back4app.com"
        
        // Initialize Parse.
        let configuration = ParseClientConfiguration {
            
            $0.applicationId = "FnGEtKzxzYapvip7ZtO9X8iWyrCWI5ckPX6tNMLP"
            $0.clientKey = "k9lvcslIcVZKgv2zpdPesVNOoCQtr1knAOaTW2J5"
            
//            $0.applicationId = "8wKBxshPXhfHCAy2rXCGb2BnBUVzRDYV0lelM3GX"
//            $0.clientKey = "sPs6BUIHsjBdHuPEWSUB08gnx97FtKWuY5RyUIOe"
            $0.server = "https://parseapi.back4app.com/"
        }
        
        Parse.initialize(with: configuration)
        PFFacebookUtils.initializeFacebook(applicationLaunchOptions: launchOptions)
        
        //        PFFacebookUtils.initializeFacebookWithApplicationLaunchOptions(launchOptions)
        PFUser.enableRevocableSessionInBackground()
        PFUser.enableAutomaticUser()
        
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge,.sound, .alert], completionHandler: {(granted, error) in
            if (granted) {
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
            else{
                //Do stuff if unsuccessful...
                print("unsuccessful")
            }
        })
        
        application.registerForRemoteNotifications()
        
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        
        return handled
    }
    
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        
    }
    
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            
            let data = refreshedToken.data(using: .utf8)
            print(data!)
            
        }
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard FIRInstanceID.instanceID().token() != nil else {
            return
        }
        // Disconnect previous FCM connection if it exists.
        FIRMessaging.messaging().disconnect()
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        FIRMessaging.messaging().disconnect()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        connectToFcm()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        print("acessToken",token)
        FIRInstanceID.instanceID().setAPNSToken(deviceToken as Data, type: FIRInstanceIDAPNSTokenType.sandbox)
        
        let currentDate = Date()
        print(currentDate)
        
        if (isKeyPresentInUserDefaults(key: "installer"))
        {
            if UserDefaults.standard.object(forKey: "installer") as! Bool == false
            {
                let installation = PFInstallation.current()
                installation?.setDeviceTokenFrom(deviceToken  as Data)
                installation?.saveInBackground()
                installation?["GCMSenderId"] = "91562383435"
                installation?["androidId"] = "dummy \(currentDate)"
                installation?["pushType"] = "dummy"
                installation?["userId"] = "dummy"
                installation?["isActive"] = "dummy"
                installation?.saveInBackground(block: { (status, error) in
                    print(status)
                    UserDefaults.standard.set(status, forKey: "installer")
                    UserDefaults.standard.synchronize()
                })
                PFPush.subscribeToChannel(inBackground: "globalChannel")
            }
        }else
        {
            let installation = PFInstallation.current()
            installation?.setDeviceTokenFrom(deviceToken  as Data)
            installation?.saveInBackground()
            installation?["GCMSenderId"] = "91562383435"
            installation?["androidId"] = "dummy \(currentDate)"
            installation?["pushType"] = "dummy"
            installation?["userId"] = "dummy"
            installation?["isActive"] = "dummy"
            installation?.saveInBackground(block: { (status, error) in
                print(status)
                UserDefaults.standard.set(status, forKey: "installer")
                UserDefaults.standard.synchronize()
            })
            PFPush.subscribeToChannel(inBackground: "globalChannel")
        }
    }

    func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        Constants.notificationFire = 1
        var apsDict:NSDictionary = [:]
        if let aps = userInfo["aps"] as? NSDictionary {
            apsDict = aps
        }
        
        let state = UIApplication.shared.applicationState
        let dictionary : NSMutableDictionary = [:]
        dictionary.setValue(apsDict, forKey: "aps")
        
        
        if state == .active {
            // print(dictionary)
            dictionary.setValue("foreground", forKey: "state")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToNavigation"), object: dictionary)
            
        }else
        {
            //print(dictionary)
            dictionary.setValue("background", forKey: "state")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToNavigation"), object: dictionary)
        }
    }
    
    func alertPushNotification(alertObj:String,type:String)
    {
        var presentedVC = self.window?.rootViewController
        while (presentedVC!.presentedViewController != nil)  {
            presentedVC = presentedVC!.presentedViewController
        }
        
        var alert = UIAlertController()
        alert = UIAlertController(title: alertObj, message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Constants.okTitle, style: .default, handler: { action in
            switch action.style{
            case .default:
                
                if type == "follow_user"
                {
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToNavigation"), object: nil)
                }
            case .cancel:
                print("cancel")
                
                
            case .destructive:
                print("destructive")
            }
        }))
        
        presentedVC!.present(alert, animated: true, completion: nil)
    }

}


extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        print(userInfo)
        //        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.received), object: nil)
      
            if showBanner == true{
               completionHandler([.alert, .sound, .badge])
            }else{
                completionHandler([])
            }
        
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        //        NotificationCenter.default.post(name: NSNotification.Name(rawValue: AppNotification.received), object: nil)
        completionHandler()

        Constants.notificationFire = 1
        var apsDict:NSDictionary = [:]
        if let aps = userInfo["aps"] as? NSDictionary {
            apsDict = aps
        }

        let state = UIApplication.shared.applicationState
        let dictionary : NSMutableDictionary = [:]
        dictionary.setValue(apsDict, forKey: "aps")


        if state == .active {
            // print(dictionary)
            dictionary.setValue("foreground", forKey: "state")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToNavigation"), object: dictionary)

        } else
        {
            //print(dictionary)
            dictionary.setValue("background", forKey: "state")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToNavigation"), object: dictionary)
        }
        return;

        if let pushType = ((userInfo["aps"] as? NSDictionary)?.value(forKey: "alert") as? NSDictionary)?.value(forKey: "pushType") as? String {
            switch pushType {
            case "follow_public_user","follow_user","follow_request_accept":
                //Profile open
                break
            default:
                //Detail open
                break
            }
        }

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RedirectToNavigation"), object: nil)
        
    }
}
