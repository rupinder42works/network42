//
//  ViewController+Extension.swift
//  Network42
//
//  Created by 42Works-Worksys2 on 24/08/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit
import Foundation

extension UIViewController {
    
    func logController()
    {
        guard let tracker = GAI.sharedInstance().defaultTracker else { return }
        tracker.set(kGAIScreenName, value: NSStringFromClass(self.classForCoder).components(separatedBy: ".").last ?? "")
    }
}
