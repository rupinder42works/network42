//
//  PFFile+Extension.swift
//  Network42
//
//  Created by Apple3 on 23/01/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import ParseUI
import Foundation

extension PFImageView {
    func setImage(fromUrl url:PFFile?, defaultImage image:UIImage?, isCircled:Bool = false, borderColor:UIColor? = .clear, borderWidth:CGFloat? = 1.0, isCache:Bool = true, showIndicator:Bool = true) {
        
        guard let url = url else {
            return
        }
        let indicator = UIActivityIndicatorView(style: .white)
        indicator.color = UIColor.black
        indicator.center = CGPoint.init(x: self.frame.size.width/2, y: self.frame.size.height/2)
        indicator.startAnimating()
        
//        print(indicator.frame)
//        print(self.frame)
        
        self.file = url
        if showIndicator {
            self.addSubview(indicator)
        }
        self.load { (image, error) in
            if image != nil {
                indicator.removeFromSuperview()
            }
        }
    }
}
