//
//  ProfileBackground.swift
//  Network42
//
//  Created by Apple on 08/02/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit

class ProfileBackground: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.borderColor = UIColor.init(red: 205/255, green: 205/255, blue: 205/255, alpha: 0.8).cgColor
    }

}
