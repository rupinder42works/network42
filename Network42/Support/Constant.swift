//
//  Constant.swift
//  Network42
//
//  Created by 42works on 13/02/17.
//  Copyright © 2017 42works. All rights reserved.
//

import Foundation
import UIKit


struct messages {
    static var validEmail = "Please enter valid email address."
    
    static var allRequired = "All fields are required."
    static var incorrectPassword = "Password is incorrect or empty."
    static var notSame = "Password and confirm password are not same."
    static var resetPassword = "Password reset link has been sent on the registered email address. Follow the instructions in the email to reset your password."
    static var passwordLength = "Password should be atleast 6 characters long."
    static var internet = "Please connect to internet."
    static var noPost = "No more posts"
    static var blockedUser = "User blocked successfully"
    static var notFound = "User not found"
    static var alreadyBlocked = "This user is already blocked"
    
    // new addition text by Rupinder
    
    // sign up and login and forget pwd
    static var noSuchEmailExsits = "This email is not registered with us. Please check your email ID."
    static var emptyEmail = "Please enter email address."
    static var termsText = "Please accept terms and conditions."
    static var incorrectEmailPwd = "Incorrect email and password."
    
    static var emailEmptyText = "Please enter email address."
    static var pwdEmptyText = "Please enter password."
    static var accountAlreadyExists = "The email address is already registered with us. Please try with another email."
    static var usernameAlreadyExists = "The username already exist. Please use another username."
    static var noFriendText = "No user found!\nFind friends from the search tab to follow and message."
    
    //chat listing
    static var noChat = "No chat history!\nSend private messages or share your favorite network42 post directly with your friends."
    
    //Profile Alerts
    static var profileAlertMsg = "Profile updated successfully."
    static var requestSent = "sent you a follow request."
    static var startedfollowing = "started to follow you."
    static var requestAccept =  "accepted your request."
    static var likePost =  "liked your post."
    static var commentPost =  "commented on your post."
    static var userTagged =  "tagged you in a post."
    static var userMentionedInComment =  "tagged you in a comment."
    static var singleChatMessage = "messaged you"
    static var newAdminMessage = "exit group"
    static var noComment = "Cannot post empty comment"
    
    
    // profile text
    static let followerText = "FOLLOWER"
    static let followersText = "FOLLOWERS"
}

struct globalgroupName {
    static var gName : String = ""
}

struct globalDic {
    static var tagUserDictionary : NSMutableDictionary = [:]
    //static var hashTagDictionary : NSMutableDictionary = [:]
    static var followingUserCountDictionary : NSMutableDictionary = [:]
}

struct globalArray {
}

struct Constants {
    static let okTitle = "OK"
    static let placeHolder = "Type message..."
    
    static var notificationFire = 0
    
    static var visitedSecondHome = false
    
    static let advertisement = "advertisement"
    
     static let addAfter = 20
}
struct globalBool {
    static var showProgess : Bool = false
}


struct  ConstantColors  {
    static let themeColor = UIColor.init(red: 103/255.0, green: 0/255.0, blue: 241/255.0, alpha: 1.0)
    static let darkAquaColor = UIColor.init(red: 30/255.0, green: 167/255.0, blue: 182/255.0, alpha: 1.0)
    static let lightGrayColor = UIColor.init(red: 170/255.0, green: 170/255.0, blue: 170/255.0, alpha: 0.5)
     static let GrayBgColor = UIColor.init(red: 230/255.0, green: 233/255.0, blue: 236/255.0, alpha: 0.5)
    static let bacgroundColor = UIColor(red: 245, green: 245, blue: 245, alpha: 1)
}
