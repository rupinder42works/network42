//
//  MessageLbl.swift
//  Network42
//
//  Created by Apple on 07/02/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit

class MessageLbl: UILabel {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "SanFranciscoText-Regular", size: 16)!
    }

}
