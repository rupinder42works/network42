//
//  NameLabel.swift
//  Network42
//
//  Created by 42works on 21/06/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class NameLabel: UILabel {

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.font = UIFont.init(name: "SanFranciscoText-Medium", size: 17)!
    }

}
