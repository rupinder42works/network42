//
//  ChatBubbleView.swift
//  Network42
//
//  Created by Apple on 07/02/19.
//  Copyright © 2019 Simran. All rights reserved.
//

import UIKit

class ChatBubbleView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.roundCorners([.bottomLeft,.topLeft,.topRight], radius: 5.0)
    }

}
