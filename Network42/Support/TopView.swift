//
//  TopView.swift
//  Network42
//
//  Created by 42works on 19/04/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class TopView: UIView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = ConstantColors.themeColor
    }
}
