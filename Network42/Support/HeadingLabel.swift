//
//  HeadingLabel.swift
//  Network42
//
//  Created by 42works on 20/06/17.
//  Copyright © 2017 42works. All rights reserved.
//

import UIKit

class HeadingLabel: UILabel {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        //self.backgroundColor = ConstantColors.lightGrayColor
        self.font = UIFont.init(name: "SanFranciscoText-Regular", size: 22)!
    }


   
}
